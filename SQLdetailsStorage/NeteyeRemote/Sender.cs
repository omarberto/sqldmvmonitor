﻿using System;
using System.Collections.Generic;
using NATS.Client;
using System.Linq;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal class Sender : IDisposable
    {
        private string subjectIN;
        private string subjectOUT;

        internal Sender(string dataSource)
        {
            subjectIN =  DMVmonitor.Outputs.Configuration.Configuration.config.output.neteyeremotestorage.inChannelSubject + "." + dataSource;
            subjectOUT = DMVmonitor.Outputs.Configuration.Configuration.config.output.neteyeremotestorage.outChannelSubject + "." + dataSource;

            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"].SubscribeAsync(subjectIN, messageHandler);
        }
        
        private async void messageHandler(object sender, MsgHandlerEventArgs args)
        {
            ServerMessageType type;
            byte[] Message = NeteyeRemote.Message.GetMessage(args.Message.Data, out type);

            if (upload_result == UploadResult.Sending)
            {
                upload_lastMessage = DateTime.UtcNow;

                if (type == ServerMessageType.Upload_RequestFile)
                {
                    var fileInfo = new System.IO.FileInfo(upload_filePath);
                    //lettura file in memoria
                    upload_memoryStream = new System.IO.MemoryStream();
                    {
                        var fileStream = fileInfo.OpenRead();
                        int bufferPosition = 0;
                        while (true)
                        {
                            byte[] buffer = new byte[8192];
                            int nread = await fileStream.ReadAsync(buffer, bufferPosition, 8192);
                            if (nread > 0)
                            {
                                await upload_memoryStream.WriteAsync(buffer, bufferPosition, nread);
                                //bufferPosition += nread;
                            }
                            if (fileStream.Position == fileStream.Length)
                                break;
                        }
                    }

                    upload_memoryStream.Seek(0, System.IO.SeekOrigin.Begin);

                    upload_sendnread = await upload_memoryStream.ReadAsync(upload_sendbuffer, 0, 8192);
                    if (upload_sendnread > 0)
                    {
                        //invio pacchetto
                        var fileBlock = new FileBlock(upload_fileInfoExt.Hash, (int)upload_memoryStream.Position - upload_sendnread, upload_sendnread, upload_sendbuffer);

                        DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                            .Publish(subjectOUT, new Message(ClientMessageType.FilePacket, fileBlock.Raw).Raw);
                    }
                    else
                        closeSending();
                }
                else if (type == ServerMessageType.Upload_FilePacketOk)
                {
                    //invio successivo
                    upload_packetSendTemptative = 0;
                    if (upload_memoryStream.Length == upload_memoryStream.Position)
                    {
                        //TODO: close ALL
                        closeSending();
                    }
                    else
                    {
                        upload_sendnread = await upload_memoryStream.ReadAsync(upload_sendbuffer, 0, 8192);
                        if (upload_sendnread > 0)
                        {
                            //var fileInfo = new System.IO.FileInfo(upload_filePath);
                            //invio pacchetto
                            var fileBlock = new FileBlock(upload_fileInfoExt.Hash, (int)upload_memoryStream.Position - upload_sendnread, upload_sendnread, upload_sendbuffer);

                            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                                .Publish(subjectOUT, new Message(ClientMessageType.FilePacket, fileBlock.Raw).Raw);
                        }
                        else
                        {
                            closeSending();
                        }
                    }
                }
                else if (type == ServerMessageType.Upload_FilePacketError)
                {
                    upload_packetSendTemptative++;
                    if (upload_packetSendTemptative == 3)
                    {
                        //TODO: close ALL
                        closeSending();
                        upload_result = UploadResult.Error;
                    }
                    else
                        //reinvio (mettiamo un massimo)
                        upload_memoryStream.Seek(-upload_sendnread, System.IO.SeekOrigin.Current);
                }
                else if (type == ServerMessageType.Upload_AlreadyPresentFile)
                {
                    //fatto touch
                    //TODO: close ALL
                    closeSending();
                    upload_result = UploadResult.AlreadyPresent;
                }
                else if (type == ServerMessageType.Upload_FileComplete)
                {
                    closeSending();
                    upload_result = UploadResult.OK;
                }
                else if (type == ServerMessageType.Upload_Error)
                {
                    closeSending();
                    upload_result = UploadResult.Error;
                }
            }

            if (type == ServerMessageType.FileMissing)
            {
                var remoteFileMissing = new RemoteFileMissing(Message);
                fileMissing?.Invoke(remoteFileMissing.fileType, remoteFileMissing.Hash);
            }

            if (type == ServerMessageType.Pong)
            {
                pongReceived?.Invoke();
            }
            else
            {
                pongReceived?.Invoke(false);
            }
        }

        internal delegate void PongReceived(bool original = true);
        internal event PongReceived pongReceived;
        
        private DateTime upload_lastMessage = DateTime.UtcNow;
        private string upload_filePath = null;
        private int upload_sendnread = 0;
        private FileInfoExt upload_fileInfoExt = null;
        private int upload_packetSendTemptative = 0;
        private System.IO.MemoryStream upload_memoryStream = null;
        private byte[] upload_sendbuffer = new byte[8192];
        private UploadResult upload_result = UploadResult.None;
        private enum UploadResult
        {
            None,
            Sending,
            AlreadyPresent,
            Error,
            OK,
        }

        private void sendFileHeader(FileType fileType, ulong Hash, string filePath)
        {
            upload_filePath = filePath;
            upload_lastMessage = DateTime.UtcNow;
            upload_packetSendTemptative = 0;
            upload_sendnread = 0;
            upload_sendbuffer = new byte[8192];
            var fileInfo = new System.IO.FileInfo(filePath);
            upload_fileInfoExt = new FileInfoExt(fileType, Hash, fileInfo.LastWriteTimeUtc, fileInfo.Length);

            upload_result = UploadResult.Sending;

            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                .Publish(subjectOUT, new Message(ClientMessageType.FileHeader, upload_fileInfoExt.Raw).Raw);
        }
        private void closeSending()
        {
            upload_fileInfoExt = null;
            if (upload_memoryStream != null)
            {
                upload_memoryStream.Dispose();
                upload_memoryStream = null;
            }
            upload_filePath = null;
        }
        internal bool sendFile(FileType fileType, ulong Hash, string filePath)
        {
            Console.WriteLine("sendFile {0} {1} {2}", fileType, Hash, filePath);
            sendFileHeader(fileType, Hash, filePath);
            
            while (upload_result == UploadResult.Sending)
            {
                //Console.WriteLine("sendFile {0} {1}", Hash, isSending);
                System.Threading.Thread.Sleep(100);
                if ((DateTime.UtcNow - upload_lastMessage).TotalSeconds > 30)
                {
                    closeSending();
                    upload_filePath = null;//upload unactive!!!!
                    upload_result = UploadResult.None;
                    return false;
                }
            }

            bool res = upload_result == UploadResult.OK || upload_result == UploadResult.AlreadyPresent;

            Console.WriteLine("sentFile {0} {1} {2} : {3}", fileType, Hash, filePath, upload_result);

            upload_result = UploadResult.None;

            return res;
        }
        
        internal void cleanFiles(FileType fileType, DateTime lastValidTime)
        {
            var listPacket = new FilesTimestampLimitPacket(fileType, lastValidTime.ToUniversalTime());

            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                .Publish(subjectOUT, new Message(ClientMessageType.ClearFiles, listPacket.Raw).Raw);
        }

        internal void touchFile(FileType fileType, ulong Hash, DateTime lastExecutionTime)
        {
            var fileHash = new FileHash(fileType, Hash, lastExecutionTime.ToUniversalTime());

            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                .Publish(subjectOUT, new Message(ClientMessageType.TouchFile, fileHash.Raw).Raw);
        }


        internal delegate void FileMissing(FileType fileType, ulong Hash);
        internal event FileMissing fileMissing;
        internal void checkFiles(FileType fileType, IEnumerable<NeteyeRemote.FileInfo> toBeUploaded)
        {
            for (int i = 0; i < toBeUploaded.Count(); i += 1000)
            {
                var listPacket = new NeteyeRemote.FileInfoList(fileType, toBeUploaded.Skip(i).Take(1000));

                DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"]
                    .Publish(subjectOUT, new Message(ClientMessageType.CheckFiles, listPacket.Raw).Raw);
            }
        }
        
        internal void ping()
        {
            DMVmonitor.Outputs.Nats.Client.Instance["NETEYEREMOTESTORAGE"].Publish(subjectOUT, new Message(ClientMessageType.Ping, System.Text.Encoding.Unicode.GetBytes(subjectIN)).Raw);
        }

        public void Dispose()
        {
        }
    }
}
