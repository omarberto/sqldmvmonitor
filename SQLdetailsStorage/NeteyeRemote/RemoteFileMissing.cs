﻿using System;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal class RemoteFileMissing
    {
        internal FileType fileType { get; private set; }
        internal ulong Hash { get; private set; }

        public RemoteFileMissing(byte[] message)
        {
            fileType = (FileType)message[0];
            Hash = BitConverter.ToUInt64(message, 1);

            Console.WriteLine("RemoteFileMissing {0} {1}", fileType, Hash);
        }
    }
}