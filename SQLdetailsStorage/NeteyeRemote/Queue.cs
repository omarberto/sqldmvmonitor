﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage.NeteyeRemote
{
    public class RemoteSaveAction
    {
        public RemoteSaveAction(Action action, params object[] parameters)
        {
            this.action = action;
            this.parameters = parameters;
        }
        
        public Action action { get; private set; }
        public object[] parameters { get; private set; }
    }

    internal sealed class QueueAppendable : IDisposable
    {
        private static readonly Lazy<QueueAppendable> lazy = new Lazy<QueueAppendable>(() => new QueueAppendable());

        internal static QueueAppendable Instance { get { return lazy.Value; } }

        internal int CurrentlyQueuedTasks { get { return _Queue.Count; } }

        //Queue<FileSaveAction> _Queue = new Queue<FileSaveAction>();
        BlockingCollection<RemoteSaveAction> _Queue = new BlockingCollection<RemoteSaveAction>();
        private DateTime _lastCall = DateTime.MinValue;
        private QueueAppendable()
        {
            //Task.Run(() =>
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Logging.Log.Instance.Info("SQLdetailsStorage.NeteyeRemote.QueueAppendable _Queue.TryTake() start");

                    try
                    {
                        RemoteSaveAction action;
                        bool hasAction = _Queue.TryTake(out action, 300000);

                        if (hasAction && _online)
                        {
                            Logging.Log.Instance.Info("SQLdetailsStorage.NeteyeRemote.QueueAppendable _Queue.TryTake() strarting");
                            action.action();
                            Logging.Log.Instance.Info("SQLdetailsStorage.NeteyeRemote.QueueAppendable _Queue.TryTake() ended");
                        }
                    }
                    catch (InvalidOperationException e)
                    {
                        Logging.Log.Instance.Error(e, "SQLdetailsStorage.NeteyeRemote.QueueAppendable InvalidOperationException");
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("SQLdetailsStorage.NeteyeRemote.QueueAppendable Exception\n {0}", e.Message);
                        Logging.Log.Instance.Error(e, "SQLdetailsStorage.NeteyeRemote.QueueAppendable Exception");
                    }

                    //if ((DateTime.UtcNow - _lastCall).TotalSeconds > Config.Configuration.config.storage.fileSystem.updateIntervalSeconds)
                    //{
                    //    SQLdetailsStorage.Storage.Instance.CleanOld();
                    //    _lastCall = DateTime.UtcNow;
                    //}
                }
            });
        }


        public bool isOnline { get { return _online; } }
        private bool _online = false;
        public void SetOnline(bool isTest)
        {
            if (!_online)
            {
                _online = true;
                if (!isTest)
                    SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => SQLdetailsStorage.Storage.Instance.TestDictionaries()));
            }
        }
        public void SetOffline()
        {
            if(_online)
                _online = false; 
        }

        public void Append(RemoteSaveAction action)
        {
            if (_online)
            {
                _Queue.Add(action);
            }
            else Console.WriteLine("Remote storage is offline, operation not queued");
        }

        public void Dispose()
        {
            _Queue.CompleteAdding();
        }

        internal void Ping(Action action)
        {
            _Queue.Add(new RemoteSaveAction(action));
        }
    }
}
