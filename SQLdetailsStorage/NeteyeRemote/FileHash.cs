﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal class FileHash
    {
        internal FileType fileType { get; private set; }
        internal ulong Hash { get; private set; }
        internal DateTime LastWriteTimeUtc { get; private set; }

        public FileHash(FileType fileType, ulong Hash, DateTime LastWriteTimeUtc)
        {
            this.fileType = fileType;
            this.Hash = Hash;
            this.LastWriteTimeUtc = LastWriteTimeUtc;
        }

        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[17];
                buffer[0] = (byte)this.fileType;
                Buffer.BlockCopy(BitConverter.GetBytes(this.Hash), 0, buffer, 1, 8);
                Buffer.BlockCopy(BitConverter.GetBytes(LastWriteTimeUtc.ToBinary()), 0, buffer, 9, 8);
                return buffer;
            }
        }
    }
}
