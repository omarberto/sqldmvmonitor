﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal class FilesToCheckPacket
    {
        internal byte[] Raw { get; private set; }

        public FilesToCheckPacket(IEnumerable<ulong> toBeUploaded)
        {
            this.Raw = new byte[8 + toBeUploaded.Count()];
            for (int i = 0, j = 8; i < toBeUploaded.Count(); i++, j += 8)
                Buffer.BlockCopy(BitConverter.GetBytes(toBeUploaded.ElementAt(i)), 0, this.Raw, j, 8);
        }
    }
}