﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal class FilesTimestampLimitPacket
    {
        internal byte[] Raw { get; private set; }

        public FilesTimestampLimitPacket(FileType fileType, DateTime dateTime)
        {
            this.Raw = new byte[9];
            this.Raw[0] = (byte)fileType;
            Buffer.BlockCopy(BitConverter.GetBytes(dateTime.ToBinary()), 0, this.Raw, 1, 8);
        }
    }
}
