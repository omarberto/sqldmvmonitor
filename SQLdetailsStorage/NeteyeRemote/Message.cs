﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal enum ClientMessageType : uint
    {
        Ping = 0x00,

        FileHeader = 0x01,
        FilePacket = 0x02,

        ClearFiles = 0x11,
        TouchFile = 0x12,
        CheckFiles = 0x13,
    }
    internal enum ServerMessageType : uint
    {
        Pong = 0x00,

        Upload_RequestFile = 0x01,
        Upload_AlreadyPresentFile = 0x02,
        Upload_FilePacketOk = 0x03,
        Upload_FileComplete = 0x04,
        Upload_FilePacketError = 0x05,
        Upload_Error = 0x06,

        FileMissing = 0x11,
    }

    internal class Message
    {
        internal byte[] Raw { get; private set; }
        internal Message(ClientMessageType type, byte[] Payload)
        {
            Raw = new byte[4 + Payload.Length];
            Buffer.BlockCopy(BitConverter.GetBytes((uint)type), 0, Raw, 0, 4);
            Buffer.BlockCopy(Payload, 0, Raw, 4, Payload.Length);
        }

        internal static byte[] GetMessage(byte[] Raw, out ServerMessageType type)
        {
            Console.Write("GetMessage Raw ");
            foreach (var b in Raw)
            {
                Console.Write("{0:X}", b);
            }

            type = (ServerMessageType)BitConverter.ToUInt32(Raw, 0);
            Console.WriteLine(" of type {0}", type);
            return Raw.Skip(4).ToArray();
        }
    }
}
