﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage.NeteyeRemote
{
    internal struct FileInfo
    {
        public FileInfo(ulong Hash, DateTime LastWriteTimeUtc)
        {
            this.Hash = Hash;
            this.LastWriteTimeUtc = LastWriteTimeUtc;
        }

        internal ulong Hash { get; private set; }
        public DateTime LastWriteTimeUtc { get; private set; }
    }

    internal struct FileInfoList
    {
        internal FileType fileType { get; private set; }
        public IEnumerable<FileInfo> List { get; private set; }
        public FileInfoList(FileType fileType, IEnumerable<FileInfo> List)
        {
            this.fileType = fileType;
            this.List = List;
        }


        public byte[] Raw
        {
            get
            {
                byte[] buffer = new byte[5 + (16 * this.List.Count())];
                buffer[0] = (byte)this.fileType;
                Buffer.BlockCopy(BitConverter.GetBytes(this.List.Count()), 0, buffer, 1, 4);
                for (int i = 0, j = 5; i < this.List.Count(); i++)
                {
                    Buffer.BlockCopy(BitConverter.GetBytes(this.List.ElementAt(i).Hash), 0, buffer, j, 8); j += 8;
                    Buffer.BlockCopy(BitConverter.GetBytes(this.List.ElementAt(i).LastWriteTimeUtc.ToBinary()), 0, buffer, j, 8); j += 8;
                }
                return buffer;

            }
        }
    }
}
