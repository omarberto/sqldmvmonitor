﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DMVmonitor.Outputs.Configuration;
using System.IO;

namespace SQLdetailsStorage
{
    //internal interface ITimeStats
    //{
    //}
    internal abstract class TimeStats// : ITimeStats
    {
        private DateTime _lastExecutionTime;
        internal DateTime lastExecutionTime { get { return _lastExecutionTime; } }
        private DateTime _lastValidExecutionTime;
        internal bool retentionIntervalOverdue
        {
            get
            {
                return (DateTime.Now - _lastValidExecutionTime).TotalDays > Config.Configuration.config.storage.fileSystem.retentionInterval;
            }
        }

        internal bool remoteExists { get; set; }

        private long _bytesCount;
        internal long bytesCount { get { return _bytesCount; } }

        internal TimeStats(DateTime lastValidExecutionTime, long bytesCount)
        {
            this._lastExecutionTime = this._lastValidExecutionTime = lastValidExecutionTime;
            this._bytesCount = bytesCount;
        }
        private TimeStats()
        {

        }

        internal bool UpdateAndVerifyTime(DateTime lastExecutionTime)
        {
            this._lastExecutionTime = lastExecutionTime;

            bool toUpdate = ((lastExecutionTime - this._lastValidExecutionTime).TotalSeconds > Config.Configuration.config.storage.fileSystem.updateIntervalSeconds);
            if (toUpdate) this._lastValidExecutionTime = lastExecutionTime;
            return toUpdate;
        }

        internal static T FromXml<T>(System.Xml.XmlElement xml) where T : TimeStats,new()
        {
            T res = new T();
            res._lastExecutionTime = res._lastValidExecutionTime = System.Xml.XmlConvert.ToDateTime(xml.GetAttribute("let"), System.Xml.XmlDateTimeSerializationMode.RoundtripKind);
            res._bytesCount = System.Xml.XmlConvert.ToInt64(xml.GetAttribute("bc"));
            return res;
        }
        internal System.Xml.XmlElement ToXml(System.Xml.XmlDocument doc)
        {
            System.Xml.XmlElement res = doc.CreateElement("file");
            res.SetAttribute("let", System.Xml.XmlConvert.ToString(lastExecutionTime, System.Xml.XmlDateTimeSerializationMode.RoundtripKind));
            res.SetAttribute("bc", System.Xml.XmlConvert.ToString(bytesCount));
            return res;
        }
    }
    internal class QueryTimeStats : TimeStats
    {
        internal QueryTimeStats(DateTime lastValidExecutionTime, long bytesCount) : base(lastValidExecutionTime, bytesCount)
        {
        }
    }
    internal class PlanTimeStats : TimeStats
    {
        internal PlanTimeStats(DateTime lastValidExecutionTime, long bytesCount) : base(lastValidExecutionTime, bytesCount)
        {
        }
    }
    internal class DeadlockTimeStats : TimeStats
    {

        internal DeadlockTimeStats(DateTime lastValidExecutionTime, long bytesCount) : base(lastValidExecutionTime, bytesCount)
        {
        }
    }

    //TODO limit T only to specidfic classes
    internal class StatsCollection<T> where T : TimeStats
    {
        internal StatsCollection(ConcurrentDictionary<ulong, T> initValue)
        {
            buffer = initValue;
        }

        ConcurrentDictionary<ulong, T> buffer = new ConcurrentDictionary<ulong, T>();

        internal bool ContainsKey(ulong query_hash)
        {
            return buffer.ContainsKey(query_hash);
        }
        internal bool TryRemove(ulong query_hash, out T tmp)
        {
            return buffer.TryRemove(query_hash, out tmp);
        }
        internal void TryAdd(ulong query_hash, T queryTimeStats)
        {
            buffer.TryAdd(query_hash, queryTimeStats);
        }
        internal void SetRemote(ulong query_hash, bool remoteExists)
        {
            if (buffer.ContainsKey(query_hash))
                buffer[query_hash].remoteExists = remoteExists;
            else
                Console.WriteLine("buffer[query_hash] key {0} not present", query_hash);
        }

        internal T this[ulong hash]
        {
            get
            {
                //Console.WriteLine("porca merda {0}", _senders == null);
                return buffer[hash];
            }
        }
        internal IDictionary<ulong, T> Cast()
        {
            return buffer;
        }

        internal void Update(ConcurrentDictionary<ulong, T> queries)
        {
            buffer = new ConcurrentDictionary<ulong, T>(buffer.Where(x => queries.ContainsKey(x.Key)).Concat(queries.Where(x => !buffer.ContainsKey(x.Key))));
        }

        internal IEnumerable<NeteyeRemote.FileInfo> toBeUploaded
        {
            get
            {
                return buffer.Where(el => !el.Value.remoteExists).Select(el => new NeteyeRemote.FileInfo(el.Key, el.Value.lastExecutionTime));
            }
        }
    }

    public delegate void NewFileSaved();
    public delegate void NewQuerySaved(ulong query_hash, string file_path, DateTime lastExecutionTime, long bytes);
    public sealed class Storage
    {
        private static readonly Lazy<Storage> lazy = new Lazy<Storage>(() => new Storage());
        public static Storage Instance { get { return lazy.Value; } }


        private FileSystem_Storage _localStorage;
        private FileSystem_Storage localStorage { get { return _localStorage; } }


        private NeteyeRemoteStorage neteyeRemoteStorage = null;
        private bool hasRemote { get { return neteyeRemoteStorage != null; } }

        public bool Start()
        {
            if (Config.Configuration.config.storage.filesystemActive)
            {
                Console.WriteLine("Storage Start _localStorage SIMPLE");
                this._localStorage = new FileSystem_Storage();
                ReloadAndClean();
                Console.WriteLine("Storage Start _localStorage SIMPLE");
                return true;
            }

            return false;
        }
        
        public bool Start(string dataSource, bool isTest = false)
        {
            if (Config.Configuration.config.storage.filesystemActive)
            {
                Console.WriteLine("Storage Start _localStorage EXTENDED {0}", dataSource);
                this._localStorage = new FileSystem_Storage();
                ReloadAndClean();
                this.neteyeRemoteStorage = new NeteyeRemoteStorage(dataSource, isTest);
                this.neteyeRemoteStorage.Start();

                int checkCnt = 0;
                while (isTest && checkCnt < 600)
                {
                    System.Threading.Thread.Sleep(100);
                    if (NeteyeRemote.QueueAppendable.Instance.isOnline)
                    {
                        Console.WriteLine("Storage Start _localStorage EXTENDED TEST");
                        return true;
                    }
                    checkCnt++;
                }
                Console.WriteLine("Storage Start _localStorage EXTENDED");
                return !isTest;
            }

            return false;
        }



        public event NewFileSaved newLocalFileSaved;
        public event NewFileSaved newRemoteFileSent;
        public event NewFileSaved newRemoteFileFailed;
        public long total_bytes_saved_on_filesystem = 0;
        internal void remoteFileSent()
        {
            newRemoteFileSent?.Invoke();
        }
        internal void remoteFileFailed(NeteyeRemote.FileType type, ulong hash, string filepath)
        {
            newRemoteFileFailed?.Invoke();
            Logging.Log.Instance.Error("Error in file upload to Neteye hash:{0} filepath:{1}", hash, filepath);
        }

        internal void localQueryFileSaved(ulong query_hash, string filepath, DateTime lastExecutionTime, long bytes)
        {
            this.queries.TryAdd(query_hash, new QueryTimeStats(lastExecutionTime, bytes));
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertQuery(query_hash, filepath), query_hash, filepath));

            total_bytes_saved_on_filesystem += bytes;
            newLocalFileSaved?.Invoke();
        }
        internal void localQueryFileTest(ulong query_hash, string filepath)
        {
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertQuery(query_hash, filepath), query_hash, filepath));
        }

        internal void localDeadlockFileSaved(ulong deadlock_key, string filepath, DateTime lastExecutionTime, long bytes)
        {
            this.deadlocks.TryAdd(deadlock_key, new DeadlockTimeStats(lastExecutionTime, bytes));
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertDeadlock(deadlock_key, filepath), deadlock_key, filepath));

            total_bytes_saved_on_filesystem += bytes;
            newLocalFileSaved?.Invoke();
        }
        internal void localDeadlockFileTest(ulong deadlock_key, string filepath)
        {
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertDeadlock(deadlock_key, filepath), deadlock_key, filepath));
        }

        internal void localPlanFileSaved(ulong query_plan_hash, string filepath, DateTime lastExecutionTime, long bytes)
        {
            this.plans.TryAdd(query_plan_hash, new PlanTimeStats(lastExecutionTime, bytes));
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertPlan(query_plan_hash, filepath), query_plan_hash, filepath));

            total_bytes_saved_on_filesystem += bytes;
            newLocalFileSaved?.Invoke();
        }
        internal void localPlanFileTest(ulong query_plan_hash, string filepath)
        {
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.InsertPlan(query_plan_hash, filepath), query_plan_hash, filepath));
        }




        private Storage()
        {
        }
        
        
        //only for old fashioned sqlstatswriter
        public bool HasQuery(ulong query_hash)
        {
            Logging.Log.Instance.Debug("HasQuery {0}", query_hash);
            return this.queries.ContainsKey(query_hash);
        }
        public bool HasPlan(ulong query_plan_hash)
        {
            return plans.ContainsKey(query_plan_hash);
        }

        internal StatsCollection<QueryTimeStats> queries { get; private set; }
        internal void refreshQueries(ConcurrentDictionary<ulong, QueryTimeStats> queries)
        {
            if (this.queries == null)
                this.queries = new StatsCollection<QueryTimeStats>(queries);
            else
                this.queries.Update(queries);
        }
        public bool HasQuery(ulong query_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("HasQuery {0} - {1}", query_hash, lastExecutionTime);

            bool exists = this.queries.ContainsKey(query_hash);

            Logging.Log.Instance.Info("HasQuery.this.Query {0} - {1}", query_hash, lastExecutionTime);
            
            if (exists && queries[query_hash].UpdateAndVerifyTime(lastExecutionTime))
            {
                SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => localStorage.QueryTouch(query_hash, lastExecutionTime), query_hash, lastExecutionTime));

                if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.QueryTouch(query_hash, lastExecutionTime), query_hash, lastExecutionTime));
            }

            return exists;
        }
        public void InsertQuery(ulong query_hash, string sql_text, DateTime lastExecutionTime)
        {
            if (string.IsNullOrEmpty(sql_text)) return;
            localStorage.InsertQuery(query_hash, sql_text, lastExecutionTime);
        }
        public void InsertQueryTest(string fileName)
        {
            localStorage.InsertQueryTest(fileName);
        }

        internal StatsCollection<PlanTimeStats> plans { get; private set; }
        internal void refreshPlans(ConcurrentDictionary<ulong, PlanTimeStats> plans)
        {
            if (this.plans == null)
                this.plans = new StatsCollection<PlanTimeStats>(plans);
            else
                this.plans.Update(plans);
        }
        public bool HasPlan(ulong query_plan_hash, DateTime lastExecutionTime)
        {
            string extension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";

            bool exists = this.plans.ContainsKey(query_plan_hash);
            if (exists && plans[query_plan_hash].UpdateAndVerifyTime(lastExecutionTime))
            {
                //mettiamo qui interazione con storage remoto, visto che potenzialmente in futuro 
                SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => localStorage.PlanTouch(query_plan_hash, lastExecutionTime, extension), query_plan_hash, lastExecutionTime, extension));

                if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.PlanTouch(query_plan_hash, lastExecutionTime), query_plan_hash, lastExecutionTime));
            }
            return exists;
        }
        public void InsertPlan(ulong query_plan_hash, string query_plan, DateTime lastExecutionTime)
        {
            if (string.IsNullOrEmpty(query_plan)) return;
            localStorage.InsertPlan(query_plan_hash, query_plan, lastExecutionTime);
        }
        public void InsertPlanTest(string fileName)
        {
            localStorage.InsertPlanTest(fileName);
        }

        internal StatsCollection<DeadlockTimeStats> deadlocks { get; private set; }
        internal void refreshDeadlocks(ConcurrentDictionary<ulong, DeadlockTimeStats> deadlocks)
        {
            if (this.deadlocks == null)
                this.deadlocks = new StatsCollection<DeadlockTimeStats>(deadlocks);
            else
                this.deadlocks.Update(deadlocks);
        }
        public void InsertDeadlock(ulong deadlock_key, string xml, DateTime creationTime)
        {
            if (string.IsNullOrEmpty(xml)) return;
            localStorage.InsertDeadlock(deadlock_key, xml, creationTime);
        }
        public void InsertDeadlockTest(string fileName)
        {
            localStorage.InsertDeadlockTest(fileName);
        }

        public void CleanOld()
        {
            Console.WriteLine("FileSystem_Storage.CleanOld BEGIN");
            CleanOldQueries();
            CleanOldPlans();
            CleanOldDeadlocks();
            Console.WriteLine("FileSystem_Storage.CleanOld END");
        }
        private void CleanOldQueries()
        {
            var filesToRemove = this.queries.Cast().Where(el => el.Value.retentionIntervalOverdue).Select(el => el.Key);
            DateTime lastValidTime = DateTime.MinValue;

            foreach (var query_hash in filesToRemove)
            {
                try
                {
                    QueryTimeStats tmp;
                    FileInfo finfo = new FileInfo(Path.Combine(Config.Configuration.config.storage.fileSystem.queriespath, string.Format("{0}.stmt.txt", query_hash)));
                    long len = finfo.Length;
                    finfo.Delete();
                    bool res = this.queries.TryRemove(query_hash, out tmp);
                    if (res)
                    {
                        this.total_bytes_saved_on_filesystem -= len;
                        if (tmp.lastExecutionTime > lastValidTime)
                            lastValidTime = tmp.lastExecutionTime;
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "Exception deleting query {0}.stmt.txt", query_hash);
                }
            }

            //TODO: pulire lo schifo append
            if (hasRemote && lastValidTime > DateTime.MinValue)
            {
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CleanOldQueries(lastValidTime), lastValidTime));
            }
        }
        private void CleanOldPlans()
        {
            var filesToRemove = this.plans.Cast().Where(el => el.Value.retentionIntervalOverdue).Select(el => el.Key);
            DateTime lastValidTime = DateTime.MinValue;

            string extension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";

            foreach (var queryplan_hash in filesToRemove)
            {
                try
                {
                    PlanTimeStats tmp;
                    FileInfo finfo = new FileInfo(Path.Combine(Config.Configuration.config.storage.fileSystem.planspath, string.Format("{0}{1}", queryplan_hash, extension)));
                    long len = finfo.Length;
                    finfo.Delete();
                    bool res = this.plans.TryRemove(queryplan_hash, out tmp);
                    if (res)
                    {
                        this.total_bytes_saved_on_filesystem -= len;
                        if (tmp.lastExecutionTime > lastValidTime)
                            lastValidTime = tmp.lastExecutionTime;
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "Exception deleting plan {0}{1}", queryplan_hash, extension);
                }
            }

            //TODO: pulire lo schifo append
            if (hasRemote && lastValidTime > DateTime.MinValue)
            {
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CleanOldPlans(lastValidTime), lastValidTime));
            }
        }
        private void CleanOldDeadlocks()
        {
            var filesToRemove = deadlocks.Cast().Where(el => el.Value.retentionIntervalOverdue).Select(el => el.Key);
            DateTime lastValidTime = DateTime.MinValue;

            foreach (var deadlock_key in filesToRemove)
            {
                try
                {
                    //DateTime lastValidExecutionTime = Storage.Instance.deadlocks[deadlock_key].lastValidExecutionTime;
                    //string fileName = string.Format("deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}.xdl", deadlock_key, lastValidExecutionTime);
                    string fileNamePath = string.Format("deadlock_{0:X16}_*.xdl", deadlock_key);

                    //Logging.Log.Instance.Debug("Deleting file for {0}", fileName);
                    DeadlockTimeStats tmp;
                    DirectoryInfo dinfo = new DirectoryInfo(Config.Configuration.config.storage.fileSystem.deadlockspath);
                    foreach (FileInfo finfo in dinfo.GetFiles(fileNamePath, SearchOption.TopDirectoryOnly))
                    {
                        long len = finfo.Length;
                        finfo.Delete();
                        bool res = deadlocks.TryRemove(deadlock_key, out tmp);
                        if (res)
                        {
                            total_bytes_saved_on_filesystem -= len;
                            if (tmp.lastExecutionTime > lastValidTime)
                                lastValidTime = tmp.lastExecutionTime;
                        }
                    }
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "Exception deleting deadlock {0}", deadlock_key);
                }
            }

            if (hasRemote && lastValidTime > DateTime.MinValue)
            {
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CleanOldDeadlocks(lastValidTime), lastValidTime));
            }
        }

        private void RefreshDictionaries()
        {
            /*********************************************************************************************************/
            long total_bytes_saved_on_filesystem = 0;
            try
            {
                var queries = new ConcurrentDictionary<ulong, QueryTimeStats>(Directory.GetFiles(Config.Configuration.config.storage.fileSystem.queriespath, "*.stmt.txt").Select(path => new FileInfo(path)).Select(finfo => new { hash = ulong.Parse(finfo.Name.Replace(".stmt.txt", "")), time = finfo.LastWriteTime, len = finfo.Length }).ToDictionary(obj => obj.hash, obj => new QueryTimeStats(obj.time, obj.len)));
                long total_bytes_queries = queries.Sum(el => el.Value.bytesCount);
                refreshQueries(queries);
                Logging.Log.Instance.Info("started collecting queries info ... done, got {0} files for {1} bytes", queries.Count, total_bytes_queries);

                total_bytes_saved_on_filesystem += total_bytes_queries;
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error("Storage.RefreshDictionaries queries failed with {0}", e.Message);
            }

            if (hasRemote)
            {
                var toBeUploaded = this.queries.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckQueries(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/

            try
            {
                string planextension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";
                var plans = new ConcurrentDictionary<ulong, PlanTimeStats>(Directory.GetFiles(Config.Configuration.config.storage.fileSystem.planspath, "*" + planextension).Select(path => new FileInfo(path)).Select(finfo => new { hash = ulong.Parse(finfo.Name.Replace(planextension, "")), time = finfo.LastWriteTime, len = finfo.Length }).ToDictionary(obj => obj.hash, obj => new PlanTimeStats(obj.time, obj.len)));
                long total_bytes_plans = plans.Sum(el => el.Value.bytesCount);
                refreshPlans(plans);
                Logging.Log.Instance.Info("started collecting plans info ... done, got {0} files for {1} bytes", plans.Count, total_bytes_plans);

                total_bytes_saved_on_filesystem += total_bytes_plans;
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error("Storage.RefreshDictionaries plans failed with {0}", e.Message);
            }

            if (hasRemote)
            {
                var toBeUploaded = this.plans.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckPlans(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/

            try
            {
                Console.WriteLine("started collecting deadlocks info ... {0}", Config.Configuration.config.storage.fileSystem.deadlockspath);
                foreach (var dlock in Directory.GetFiles(Config.Configuration.config.storage.fileSystem.deadlockspath, "*.xdl").Select(path => new FileInfo(path)))
                {
                    Console.WriteLine("dlock {0}", dlock.Name);
                    Console.WriteLine("dlock {0}", dlock.LastWriteTime);
                    Console.WriteLine("dlock {0}", ulong.Parse(dlock.Name.Substring(9, 16), System.Globalization.NumberStyles.HexNumber));
                }


                var deadlocks = new ConcurrentDictionary<ulong, DeadlockTimeStats>(Directory.GetFiles(Config.Configuration.config.storage.fileSystem.deadlockspath, "*.xdl").Select(path => new FileInfo(path)).Select(finfo => new { hash = ulong.Parse(finfo.Name.Substring(9, 16), System.Globalization.NumberStyles.HexNumber), time = finfo.LastWriteTime, len = finfo.Length }).ToDictionary(obj => obj.hash, obj => new DeadlockTimeStats(obj.time, obj.len)));
                long total_bytes_deadlocks = deadlocks.Sum(el => el.Value.bytesCount);
                this.refreshDeadlocks(deadlocks);
                Logging.Log.Instance.Info("started collecting deadlocks info ... done, got {0} files for {1} bytes", deadlocks.Count, total_bytes_deadlocks);

                total_bytes_saved_on_filesystem += total_bytes_deadlocks;
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error("Storage.RefreshDictionaries deadlocks failed with {0}", e.Message);
            }

            if (hasRemote)
            {
                var toBeUploaded = this.deadlocks.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckDeadlocks(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/

            this.total_bytes_saved_on_filesystem = total_bytes_saved_on_filesystem;
        }
        public void TestDictionaries()
        {
            /*********************************************************************************************************/
            
            if (hasRemote)
            {
                var toBeUploaded = this.queries.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckQueries(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/
            
            if (hasRemote)
            {
                var toBeUploaded = this.plans.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckPlans(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/
            
            if (hasRemote)
            {
                var toBeUploaded = this.deadlocks.toBeUploaded;
                NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.CheckDeadlocks(toBeUploaded), toBeUploaded));
            }

            /*********************************************************************************************************/


        }

        public void ReloadAndClean()
        {
            RefreshDictionaries();
            CleanOld();
        }

        //inserite per test
        public void TouchQuery(ulong query_hash, DateTime lastExecutionTime)
        {
            localStorage.QueryTouch(query_hash, lastExecutionTime);
            if (hasRemote) NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.QueryTouch(query_hash, lastExecutionTime), query_hash, lastExecutionTime));
        }
        public void TouchPlan(ulong query_plan_hash, DateTime lastExecutionTime)
        {
            localStorage.PlanTouch(query_plan_hash, lastExecutionTime, ".sqlplan.zip");
            if (hasRemote)  NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(() => neteyeRemoteStorage.PlanTouch(query_plan_hash, lastExecutionTime), query_plan_hash, lastExecutionTime));
        }
    }
}
