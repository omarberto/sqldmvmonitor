﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage
{
    internal class NeteyeRemoteStorage
    {
        private string queriesPath;
        private string plansPath;
        private string deadlocksPath;
        private bool _isTest;

        NeteyeRemote.Sender sender;
        public NeteyeRemoteStorage(string dataSource, bool isTest)
        {
            sender = new NeteyeRemote.Sender(dataSource);
            sender.fileMissing += Sender_fileMissing;
            sender.pongReceived += Sender_pongReceived;

            this._isTest = isTest;
            this.queriesPath = Config.Configuration.config.storage.fileSystem.queriespath;
            this.plansPath = Config.Configuration.config.storage.fileSystem.planspath;
            this.deadlocksPath = Config.Configuration.config.storage.fileSystem.deadlockspath;
        }

        private DateTime periodicConnectionTestLastMessage = DateTime.MinValue;
        private int pingCount = 0;
        System.Threading.Timer periodicConnectionTestTimer;
        internal void periodicConnectionTest(Object stateInfo)
        {
            if ((DateTime.UtcNow - periodicConnectionTestLastMessage).TotalMilliseconds > DMVmonitor.Outputs.Configuration.Configuration.config.output.neteyeremotestorage.connectioncheckIntervalSeconds * 4000)
            {
                Console.WriteLine("periodicConnectionTest pinging...");
                sender.ping();
                pingCount++;
                if (pingCount > 3)
                    NeteyeRemote.QueueAppendable.Instance.SetOffline();
            }
        }
        private void Sender_pongReceived(bool original)
        {
            if (original)
            {
                if (_isTest)
                    Console.WriteLine("received response to ping from remote server");
                NeteyeRemote.QueueAppendable.Instance.SetOnline(_isTest);
            }
            //ogni messsaggio ricevuto aggiorna il timestamp
            periodicConnectionTestLastMessage = DateTime.UtcNow;
            pingCount = 0;
        }

        public void Start()
        {
            this.periodicConnectionTestTimer = new System.Threading.Timer(periodicConnectionTest, null, 0, DMVmonitor.Outputs.Configuration.Configuration.config.output.neteyeremotestorage.connectioncheckIntervalSeconds * 1000);
        }
        
        private void Sender_fileMissing(NeteyeRemote.FileType fileType, ulong Hash)
        {
            switch (fileType)
            {
                case NeteyeRemote.FileType.Query:
                    {
                        var filepath = Path.Combine(queriesPath, string.Format("{0}.stmt.txt", Hash));
                        if (File.Exists(filepath))
                        {
                            NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(
                                () => InsertQuery(Hash, filepath),
                                Hash, filepath));
                        }
                    }
                    break;
                case NeteyeRemote.FileType.Plan:
                    {
                        var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan", Hash));
                        if (File.Exists(filepath))
                        {
                            NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(
                                () => InsertPlan(Hash, filepath),
                                Hash, filepath));
                        }
                    }
                    break;
                case NeteyeRemote.FileType.PlanZipped:
                    {
                        var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan.zip", Hash));

                        if (File.Exists(filepath))
                        {
                            NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(
                                () => InsertPlan(Hash, filepath),
                                Hash, filepath));
                        }
                    }
                    break;
                case NeteyeRemote.FileType.Deadlock:
                    {
                        Console.WriteLine("MISSING NeteyeRemote.FileType.Deadlock {0}", Hash);

                        var lastExecutionTime = new DMVmonitor.Outputs.EpochNanoseconds((long)Hash, DateTimeKind.Utc).dateTime.ToLocalTime();
                        string fileName = string.Format("deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}.xdl", Hash, lastExecutionTime);
                        var filepath = Path.Combine(deadlocksPath, fileName);

                        Console.WriteLine("MISSING NeteyeRemote.FileType.Deadlock {0}", filepath);

                        if (File.Exists(filepath))
                        {
                            NeteyeRemote.QueueAppendable.Instance.Append(new NeteyeRemote.RemoteSaveAction(
                                () => InsertDeadlock(Hash, filepath),
                                Hash, filepath));

                            return;
                        }
                    }
                    break;
            }
            
        }

        /*
         da qui in poi costruisco mano a mano come estensione dello storage locale
             */

        internal void CleanOldQueries(DateTime lastValidTime)
        {
            sender.cleanFiles(NeteyeRemote.FileType.Query, lastValidTime);
        }
        internal void CleanOldPlans(DateTime lastValidTime)
        {
            sender.cleanFiles(NeteyeRemote.FileType.Plan, lastValidTime);
        }
        internal void CleanOldDeadlocks(DateTime lastValidTime)
        {
            sender.cleanFiles(NeteyeRemote.FileType.Deadlock, lastValidTime);
        }

        public void QueryTouch(ulong query_hash, DateTime lastExecutionTime)
        {
            sender.touchFile(NeteyeRemote.FileType.Query, query_hash, lastExecutionTime);
        }
        public void PlanTouch(ulong query_plan_hash, DateTime lastExecutionTime)
        {
            sender.touchFile(NeteyeRemote.FileType.PlanZipped, query_plan_hash, lastExecutionTime);
        }
        public void DeadlockTouch(ulong deadlock_key, DateTime lastExecutionTime)
        {
            sender.touchFile(NeteyeRemote.FileType.Deadlock, deadlock_key, lastExecutionTime);
        }

        public void InsertQuery(ulong query_hash, string filePath)
        {
            bool res = sender.sendFile(NeteyeRemote.FileType.Query, query_hash, filePath);

            Storage.Instance.remoteFileSent();

            if (res)
            {
                //Storage.Instance.remoteFileSaved();
                Storage.Instance.queries.SetRemote(query_hash, true);
                //Storage.Instance.newLocalFileSavedCall(length);
            }
            else Storage.Instance.remoteFileFailed(NeteyeRemote.FileType.Query, query_hash, filePath);
        }
        public void InsertPlan(ulong query_plan_hash, string filePath)
        {
            bool res = sender.sendFile(Config.Configuration.config.storage.fileSystem.zipped ? NeteyeRemote.FileType.PlanZipped : NeteyeRemote.FileType.Plan, query_plan_hash, filePath);

            Storage.Instance.remoteFileSent();

            if (res)
            {
                Storage.Instance.plans.SetRemote(query_plan_hash, true);
                //Storage.Instance.newLocalFileSavedCall(length);
            }
            else Storage.Instance.remoteFileFailed(Config.Configuration.config.storage.fileSystem.zipped ? NeteyeRemote.FileType.PlanZipped : NeteyeRemote.FileType.Plan, query_plan_hash, filePath);
        }
        public void InsertDeadlock(ulong deadlock_key, string filePath)
        {
            bool res = sender.sendFile(NeteyeRemote.FileType.Deadlock, deadlock_key, filePath);

            Storage.Instance.remoteFileSent();

            if (res)
            {
                Storage.Instance.deadlocks.SetRemote(deadlock_key, true);
                //Storage.Instance.newLocalFileSavedCall(length);
            }
            else Storage.Instance.remoteFileFailed(NeteyeRemote.FileType.Deadlock, deadlock_key, filePath);
        }

        internal void CheckQueries(IEnumerable<NeteyeRemote.FileInfo> toBeUploaded)
        {
            sender.checkFiles(NeteyeRemote.FileType.Query, toBeUploaded);
        }

        internal void CheckPlans(IEnumerable<NeteyeRemote.FileInfo> toBeUploaded)
        {
            sender.checkFiles(NeteyeRemote.FileType.PlanZipped, toBeUploaded);
        }

        internal void CheckDeadlocks(IEnumerable<NeteyeRemote.FileInfo> toBeUploaded)
        {
            sender.checkFiles(NeteyeRemote.FileType.Deadlock, toBeUploaded);
        }
    }
}
