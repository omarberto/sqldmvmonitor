﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SQLdetailsStorage.Queued
{
    //public enum FileSaveActionType
    //{
    //    Query,
    //    QueryTouch,
    //    Deadlock,
    //    Plan,
    //    PlanTouch,
    //    CleanOld,
    //    ReloadClean,
    //}
    public class FileSaveAction
    {
        public FileSaveAction(Action action, params object[] parameters)
        {
            //this.type = type;
            this.action = action;
            this.parameters = parameters;
        }

        //public FileSaveActionType type { get; private set; }
        public Action action { get; private set; }
        public object[] parameters { get; private set; }
    }

    public sealed class QueueAppendable : IDisposable
    {
        private static readonly Lazy<QueueAppendable> lazy = new Lazy<QueueAppendable>(() => new QueueAppendable());

        public static QueueAppendable Instance { get { return lazy.Value; } }

        public int CurrentlyQueuedTasks { get { return _Queue.Count; } }

        //PERIODIC CLEANUP: 24h

        System.Threading.Timer periodicCleanup = new System.Threading.Timer(CleanupCallback, null, Config.Configuration.config.storage.fileSystem.checkIntervalMilliseconds, Config.Configuration.config.storage.fileSystem.checkIntervalMilliseconds);
        public static void CleanupCallback(Object stateInfo)
        {
            SQLdetailsStorage.Storage.Instance.ReloadAndClean();
        }

        //Queue<FileSaveAction> _Queue = new Queue<FileSaveAction>();
        BlockingCollection<FileSaveAction> _Queue = new BlockingCollection<FileSaveAction>();
        private DateTime _lastCall = DateTime.UtcNow;
        private QueueAppendable()
        {
            //Task.Run(() =>
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Logging.Log.Instance.Info("QueueAppendable _Queue.TryTake() start");
                    
                    try
                    {
                        FileSaveAction action;
                        bool hasAction = _Queue.TryTake(out action, 300000);

                        if (hasAction)
                            action.action();

                        Logging.Log.Instance.Info("QueueAppendable _Queue.TryTake() ended with result {0}", hasAction);
                    }
                    catch (InvalidOperationException e)
                    {
                        Logging.Log.Instance.Error(e, "QuerySave.QueueAppendable InvalidOperationException");
                        break;
                    }
                    catch (Exception e)
                    {
                        Logging.Log.Instance.Error(e, "QuerySave.QueueAppendable Exception");
                    }

                    if ((DateTime.UtcNow - _lastCall).TotalSeconds > Config.Configuration.config.storage.fileSystem.updateIntervalSeconds)
                    {
                        SQLdetailsStorage.Storage.Instance.CleanOld();
                        _lastCall = DateTime.UtcNow;
                    }
                }
            });
        }
        public void Append(FileSaveAction action)
        {
            _Queue.Add(action);
        }

        public void Dispose()
        {
            _Queue.CompleteAdding();
        }
    }
}
