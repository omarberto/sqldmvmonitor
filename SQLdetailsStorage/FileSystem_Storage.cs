﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLdetailsStorage
{
    internal class FileSystem_Storage
    {
        private string queriesPath;
        private string plansPath;
        private string deadlocksPath;

        //NeteyeRemoteStorage neteyeRemoteStorage = null;
        //private bool hasRemote { get { return neteyeRemoteStorage != null; } }
        public FileSystem_Storage()
        {
            //string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            this.queriesPath = Config.Configuration.config.storage.fileSystem.queriespath;
            this.plansPath = Config.Configuration.config.storage.fileSystem.planspath;
            this.deadlocksPath = Config.Configuration.config.storage.fileSystem.deadlockspath;
            if (!Directory.Exists(queriesPath)) Directory.CreateDirectory(queriesPath);//parametrizzare, generalizzare etc....
            if (!Directory.Exists(plansPath)) Directory.CreateDirectory(plansPath);//parametrizzare, generalizzare etc....
            if (!Directory.Exists(deadlocksPath)) Directory.CreateDirectory(deadlocksPath);//parametrizzare, generalizzare etc....
        }
        //public FileSystem_Storage(string dataSource, bool isTest = false)
        //{
        //    //string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        //    this.queriesPath = Config.Configuration.config.storage.fileSystem.queriespath;
        //    this.plansPath = Config.Configuration.config.storage.fileSystem.planspath;
        //    this.deadlocksPath = Config.Configuration.config.storage.fileSystem.deadlockspath;
        //    if (!Directory.Exists(queriesPath)) Directory.CreateDirectory(queriesPath);//parametrizzare, generalizzare etc....
        //    if (!Directory.Exists(plansPath)) Directory.CreateDirectory(plansPath);//parametrizzare, generalizzare etc....
        //    if (!Directory.Exists(deadlocksPath)) Directory.CreateDirectory(deadlocksPath);//parametrizzare, generalizzare etc....

        //    this.neteyeRemoteStorage = new NeteyeRemoteStorage(dataSource, isTest);
        //    this.neteyeRemoteStorage.Start();
        //}
        

        public FileInfo GetQueryInfo(ulong query_hash)
        {
            var filepath = Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash));
            return new FileInfo(filepath);
        }
        public void QueryTouch(ulong query_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("HasQuery {2} updating {0} - {1}", query_hash, lastExecutionTime, Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)));
            try
            {
                System.IO.File.SetLastWriteTime(Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)), lastExecutionTime);
                Logging.Log.Instance.Info("HasQuery {2} updated {0} - {1}", query_hash, lastExecutionTime, Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash)));
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException touching query {0}.stmt.txt", query_hash);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error(e, "other Exception touching query {0}.stmt.txt", query_hash);
            }
        }
        public void InsertQuery(ulong query_hash, string sql_text, DateTime lastExecutionTime)
        {
            string filepath = Path.Combine(queriesPath, string.Format("{0}.stmt.txt", query_hash));
            if (File.Exists(filepath)) return;
            
            try
            {
                long Length = 0;
                using (var filestream = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true))
                {
                    using (var sourceStream = new StreamWriter(filestream, Encoding.Unicode))
                    {
                        sourceStream.WriteAsync(sql_text).Wait();
                    }
                }
                System.IO.File.SetLastWriteTime(filepath, lastExecutionTime);
                Length = new FileInfo(filepath).Length;
                
                Storage.Instance.localQueryFileSaved(query_hash, filepath, lastExecutionTime, Length);

                return;
            }
            catch//(System.IO.IOException e)
            {

            }

            return;
        }
        public void InsertQueryTest(string fileName)
        {
            if (!fileName.EndsWith(".stmt.txt")) fileName = fileName + ".stmt.txt";
            if (!fileName.StartsWith(queriesPath)) fileName = Path.Combine(queriesPath, fileName);

            string filepath = fileName;
            ulong query_hash = 0;
            if (File.Exists(filepath) && ulong.TryParse(Path.GetFileName(filepath).Replace(".stmt.txt", ""), out query_hash))
            {
                Storage.Instance.localQueryFileTest(query_hash, filepath);
            }
        }

        public void InsertDeadlock(ulong deadlock_key, string xml, DateTime lastExecutionTime)
        {
            string fileName = string.Format("deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}.xdl", deadlock_key, lastExecutionTime);

            string filepath = Path.Combine(deadlocksPath, fileName);
            if (File.Exists(filepath)) return;

            try
            {
                long Length = 0;
                using (var filestream = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true))
                {
                    using (var sourceStream = new StreamWriter(filestream, Encoding.Unicode))
                    {
                        sourceStream.WriteAsync(xml).Wait();
                    }
                }
                System.IO.File.SetLastWriteTime(filepath, lastExecutionTime);
                Length = new FileInfo(filepath).Length;

                Storage.Instance.localDeadlockFileSaved(deadlock_key, filepath, lastExecutionTime, Length);
            }
            catch//(System.IO.IOException e)
            {

            }
        }
        public void InsertDeadlockTest(string fileName)
        {
            if (!fileName.EndsWith(".xdl")) fileName = fileName + ".xdl";
            if (!fileName.StartsWith(deadlocksPath)) fileName = Path.Combine(deadlocksPath, fileName);

            string filepath = fileName;
            string tmp = Path.GetFileName(filepath).Replace(".xdl", "");

            ulong deadlock_key;
            if (tmp.StartsWith("deadlock_") && tmp.Length == 41 && File.Exists(filepath) && ulong.TryParse(tmp.Substring(9, 16), System.Globalization.NumberStyles.HexNumber, null, out deadlock_key))
            {
                Storage.Instance.localDeadlockFileTest(deadlock_key, filepath);
            }
        }
        
        public FileInfo GetPlanInfo(ulong query_plan_hash)
        {
            string extension = Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan";
            var filepath = Path.Combine(queriesPath, string.Format("{0}{1}", query_plan_hash, extension));
            return new FileInfo(filepath);
        }
        public void PlanTouch(ulong query_plan_hash, DateTime creationTime, string extension)
        {
            Logging.Log.Instance.Info("HasPlan {2} updating {0} - {1}", query_plan_hash, creationTime, Path.Combine(queriesPath, string.Format("{0}{1}", query_plan_hash, extension)));
            try
            {
                System.IO.File.SetLastWriteTime(Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)), creationTime);
                Logging.Log.Instance.Info("HasPlan {2} updated {0} - {1}", query_plan_hash, creationTime, Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
                var reso = System.IO.File.GetLastWriteTime(Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
                Logging.Log.Instance.Info("HasPlan {2} updated resulkt {0} - {1}", query_plan_hash, reso, Path.Combine(plansPath, string.Format("{0}{1}", query_plan_hash, extension)));
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException touching plan {0}.sqlplan", query_plan_hash);
            }
            catch (Exception e)
            {
                //Console.WriteLine("{2} {1} {0}", e.Message, e.InnerException, e);
                Logging.Log.Instance.Error(e, "other Exception touching plan {0}.sqlplan", query_plan_hash);
            }
        }
        public void InsertPlan(ulong query_plan_hash, string query_plan, DateTime creationTime)
        {
            int res = -1;
            long len = 0;
            if (Config.Configuration.config.storage.fileSystem.zipped)
            {
                res = InsertPlanZipped(query_plan_hash, query_plan, out len);
            }
            else
            {
                res = InsertPlanText(query_plan_hash, query_plan, out len);
                len = query_plan.Length;
            }

            if (res == 0)
            {

                var filepath = Config.Configuration.config.storage.fileSystem.zipped ?
                    Path.Combine(plansPath, string.Format("{0}.sqlplan.zip", query_plan_hash))
                    :
                    Path.Combine(plansPath, string.Format("{0}.sqlplan", query_plan_hash));

                Storage.Instance.localPlanFileSaved(query_plan_hash, filepath, creationTime, len);
            }
        }
        private int InsertPlanText(ulong query_plan_hash, string query_plan, out long Length)
        {
            Length = 0;
            Logging.Log.Instance.Trace("adding plan {0}.sqlplan", query_plan_hash);
            var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan", query_plan_hash));
            if (File.Exists(filepath)) return 1;
            

            try
            {
                using (var filestream = new FileStream(filepath, FileMode.Create, FileAccess.Write, FileShare.Read, bufferSize: 4096, useAsync: true))
                {
                    using (var sourceStream = new StreamWriter(filestream, Encoding.Unicode))
                    {
                        sourceStream.WriteAsync(query_plan).Wait();
                    }
                }
                Length = new FileInfo(filepath).Length;
                Logging.Log.Instance.Trace("write plan end");

                return 0;
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Debug(e, "IOException {0}", filepath);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Debug(e, "Exception {0}", filepath);
            }

            return -1;
        }
        private int InsertPlanZipped(ulong query_plan_hash, string query_plan, out long Length)
        {
            Length = 0;
            Logging.Log.Instance.Trace("adding plan {0}.sqlplan.zip length {1}", query_plan_hash, query_plan.Length);
            var filepath = Path.Combine(plansPath, string.Format("{0}.sqlplan.zip", query_plan_hash));
            if (File.Exists(filepath)) return 1;

            try
            {
                using (FileStream zipToOpen = new FileStream(filepath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read, bufferSize: 4096, useAsync: true))
                {
                    using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                    {
                        var planEntry = archive.CreateEntry(string.Format("{0}.sqlplan", query_plan_hash));
                        using (StreamWriter writer = new StreamWriter(planEntry.Open(), Encoding.Unicode))
                        {
                            writer.WriteAsync(query_plan).Wait();
                        }
                    }
                }
                Length = new FileInfo(filepath).Length;

                Logging.Log.Instance.Trace("write plan end");

                return 0;
            }
            catch (System.IO.IOException e)
            {
                Logging.Log.Instance.Error(e, "IOException {0}", filepath);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Error(e, "Exception {0}", filepath);
            }

            return -1;
        }
        public void InsertPlanTest(string fileName)
        {
            if (Config.Configuration.config.storage.fileSystem.zipped)
            {
                if (!fileName.EndsWith(".sqlplan.zip")) fileName = fileName + ".sqlplan.zip";
            }
            else
            {
                if (!fileName.EndsWith(".sqlplan")) fileName = fileName + ".sqlplan";
            }
            if (!fileName.StartsWith(plansPath)) fileName = Path.Combine(plansPath, fileName);

            string filepath = fileName;
            string tmp = Path.GetFileName(filepath).Replace(Config.Configuration.config.storage.fileSystem.zipped ? ".sqlplan.zip" : ".sqlplan", "");

            ulong query_plan_hash;
            if (File.Exists(filepath) && ulong.TryParse(tmp, out query_plan_hash))
            {
                Storage.Instance.localPlanFileTest(query_plan_hash, filepath);
            }
        }
    }
}
