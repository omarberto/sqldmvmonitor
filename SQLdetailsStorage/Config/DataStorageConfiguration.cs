﻿namespace SQLdetailsStorage.Config
{
    public class DataStorageConfiguration
    {
        public SqlLiteStorageConfiguration sqlLite { get; set; }
        public bool sqliteActive
        {
            get
            {
                return sqlLite != null;
            }
        }
        public FileSystemStorageConfiguration fileSystem { get; set; }
        public bool filesystemActive
        {
            get
            {
                return fileSystem != null;
            }
        }
    }
}