﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;

namespace SQLdetailsStorage.Config
{
    public class FileSystemStorageConfiguration
    {
        public bool zipped { set; get; } = true;

        public string queriespath { set; get; }

        public string planspath { set; get; }

        public string deadlockspath { set; get; }

        public int retentionInterval { set; get; } = 15;
        public int updateInterval { get; internal set; } = 30;
        [Nett.TomlIgnore]
        public int updateIntervalSeconds { get { return 60 * updateInterval; } }
        public int checkIntervalHours { get; internal set; } = 24;
        [Nett.TomlIgnore]
        public int checkIntervalMilliseconds { get { return 3600000 * checkIntervalHours; } }

        //path autoimatiche eliminate

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(queriespath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.queriespath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(queriespath))
                {
                    var acl = Directory.GetAccessControl(queriespath);
                    AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                    var NtAccountName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    bool found = false;
                    //Go through the rules returned from the DirectorySecurity
                    foreach (AuthorizationRule rule in rules)
                    {
                        //If we find one that matches the identity we are looking for
                        if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var filesystemAccessRule = (FileSystemAccessRule)rule;

                            //Cast to a FileSystemAccessRule to check for access rights
                            if ((filesystemAccessRule.FileSystemRights & FileSystemRights.WriteData) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                        errors.Add(string.Format("{0} does not have write access to {1}", NtAccountName, queriespath));
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", queriespath));
                }
            }

            if (string.IsNullOrEmpty(planspath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.planspath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(planspath))
                {
                    var acl = Directory.GetAccessControl(planspath);
                    AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                    var NtAccountName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    bool found = false;
                    //Go through the rules returned from the DirectorySecurity
                    foreach (AuthorizationRule rule in rules)
                    {
                        //If we find one that matches the identity we are looking for
                        if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var filesystemAccessRule = (FileSystemAccessRule)rule;

                            //Cast to a FileSystemAccessRule to check for access rights
                            if ((filesystemAccessRule.FileSystemRights & FileSystemRights.WriteData) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                        errors.Add(string.Format("{0} does not have write access to {1}", NtAccountName, planspath));
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", planspath));
                }
            }

            //TODO decidere se questo è opzionale...
            //TODO dopotutto potrebbe essere necessario omettere tutto e fare da remoto
            if (string.IsNullOrEmpty(deadlockspath))
            {
                //non accettabile
                if (string.IsNullOrEmpty(queriespath))
                    errors.Add("main.storage.fileSystem.deadlockspath & main.storage.fileSystem.queriespath configuration are missing");
                else
                    deadlockspath = queriespath;
            }
            else
            {
                //accettabile
                if (Directory.Exists(deadlockspath))
                {
                    var acl = Directory.GetAccessControl(deadlockspath);
                    AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                    var NtAccountName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    bool found = false;
                    //Go through the rules returned from the DirectorySecurity
                    foreach (AuthorizationRule rule in rules)
                    {
                        //If we find one that matches the identity we are looking for
                        if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var filesystemAccessRule = (FileSystemAccessRule)rule;

                            //Cast to a FileSystemAccessRule to check for access rights
                            if ((filesystemAccessRule.FileSystemRights & FileSystemRights.WriteData) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                        errors.Add(string.Format("{0} does not have write access to {1}", NtAccountName, deadlockspath));
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", deadlockspath));
                }
            }

            if (retentionInterval < 1) retentionInterval = 1;
            if (updateInterval < 1) updateInterval = 1;
            if (checkIntervalHours < 1) checkIntervalHours = 1;
        }

        internal void Init(string accountName)
        {
            if (!Directory.Exists(queriespath))
            {
                Directory.CreateDirectory(queriespath);//parametrizzare, generalizzare etc....
            }
            { 
                var acl = Directory.GetAccessControl(queriespath);

                AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));

                bool found = false;
                //Go through the rules returned from the DirectorySecurity
                foreach (AuthorizationRule rule in rules)
                {
                    //If we find one that matches the identity we are looking for
                    if (rule.IdentityReference.Value.Equals(accountName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var filesystemAccessRule = (FileSystemAccessRule)rule;

                        //Cast to a FileSystemAccessRule to check for access rights
                        if ((filesystemAccessRule.FileSystemRights & FileSystemRights.FullControl) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                        {
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    System.Security.Principal.NTAccount group = new System.Security.Principal.NTAccount(accountName);
                    FileSystemAccessRule myrule = new FileSystemAccessRule(group, FileSystemRights.FullControl, AccessControlType.Allow);
                    acl.SetAccessRule(myrule);
                    Directory.SetAccessControl(queriespath, acl);
                }
            }

            if (!Directory.Exists(planspath))
            {
                Directory.CreateDirectory(planspath);//parametrizzare, generalizzare etc....
            }
            {
                var acl = Directory.GetAccessControl(planspath);

                AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));

                bool found = false;
                //Go through the rules returned from the DirectorySecurity
                foreach (AuthorizationRule rule in rules)
                {
                    //If we find one that matches the identity we are looking for
                    if (rule.IdentityReference.Value.Equals(accountName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var filesystemAccessRule = (FileSystemAccessRule)rule;

                        //Cast to a FileSystemAccessRule to check for access rights
                        if ((filesystemAccessRule.FileSystemRights & FileSystemRights.FullControl) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                        {
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    System.Security.Principal.NTAccount group = new System.Security.Principal.NTAccount(accountName);
                    FileSystemAccessRule myrule = new FileSystemAccessRule(group, FileSystemRights.FullControl, AccessControlType.Allow);
                    acl.SetAccessRule(myrule);
                    Directory.SetAccessControl(planspath, acl);
                }
            }

            if (!Directory.Exists(deadlockspath))
            {
                Directory.CreateDirectory(deadlockspath);//parametrizzare, generalizzare etc....
            }
            {
                var acl = Directory.GetAccessControl(deadlockspath);

                AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));

                bool found = false;
                //Go through the rules returned from the DirectorySecurity
                foreach (AuthorizationRule rule in rules)
                {
                    //If we find one that matches the identity we are looking for
                    if (rule.IdentityReference.Value.Equals(accountName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var filesystemAccessRule = (FileSystemAccessRule)rule;

                        //Cast to a FileSystemAccessRule to check for access rights
                        if ((filesystemAccessRule.FileSystemRights & FileSystemRights.FullControl) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                        {
                            found = true;
                        }
                    }
                }
                if (!found)
                {
                    System.Security.Principal.NTAccount group = new System.Security.Principal.NTAccount(accountName);
                    FileSystemAccessRule myrule = new FileSystemAccessRule(group, FileSystemRights.FullControl, AccessControlType.Allow);
                    acl.SetAccessRule(myrule);
                    Directory.SetAccessControl(deadlockspath, acl);
                }
            }

            if (retentionInterval < 1) retentionInterval = 1;
            if (updateInterval < 1) updateInterval = 1;
            if (checkIntervalHours < 1) checkIntervalHours = 1;
        }
    }
}