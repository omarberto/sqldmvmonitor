using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.IO;
using System.Windows.Forms;
using System.DirectoryServices.AccountManagement;

using System.Reflection;

using SQLdetailsStorage;
//using DMVmonitor.Configuration;
using DMVmonitor.Outputs.Configuration;
using System.Security.AccessControl;
using System.ComponentModel;

namespace SQLDMVTracingCA
{
    public class CustomActions
    {
        static string GetComputerDomainName()
        {
            string domain = String.Empty;
            try
            {
                domain = System.DirectoryServices.ActiveDirectory.Domain.GetComputerDomain().Name;
            }
            catch
            {
                // Handle exception here if desired.
            }
            return domain;
        }

        public static bool DedectConfigFile( string configdir,string configfilename,out Record errormsg,out string fullFilePath )
        {
            bool aResult = false;

            String Computerdomain = GetComputerDomainName();
            String Computername = System.Environment.MachineName;
            string msg;
            Record r = new Record();

            if (configdir != null)
            {
                configdir = configdir.Trim();
                if (!configdir.EndsWith("\\"))
                {
                    configdir = configdir + "\\";
                }
                string computerdir = configdir + Computername;
                string computerdomaindir = configdir + Computername + "." + Computerdomain;

                string _path = null;
                if (File.Exists(computerdomaindir + "\\" + configfilename))
                {
                    _path = computerdomaindir + "\\" + configfilename;
                }
                else if (File.Exists(computerdir + "\\" + configfilename))
                {
                    _path = computerdir + "\\" + configfilename;
                }
                else if (File.Exists(configdir + "\\" + configfilename))
                {
                    _path = configdir + configfilename;
                }

                if (_path == null)
                {
                    msg = "Configuration File " + configfilename + " does not exist in directory: \n\t" + computerdomaindir + " or \n\t" + computerdir + " or \n\t" + configdir + "\nVERIFY that directory exist and User installing and running service has read permissions !!";
                    r[0] = msg;
                }
                else
                {
                    aResult = true;
                }
                fullFilePath = _path;
            }
            else
            {
                msg = "Config Path not set!!";
                r[0] = msg;
                fullFilePath = null;
            }

            errormsg = null;
            if(!aResult)
            {
                errormsg = r;
            }

            return aResult;
        }


        private static bool loadAssembliesforImpersonation(out List<Record> errors)
        {
            bool result = false;
            string[] assemblyNames = new string[] {
                        "Logging",
                        "Nett",
                        "SQLdetailsStorage",
                        "NATS.Client",
                        "Microsoft.SqlServer.XE.Core",
                        "Microsoft.SqlServer.XEvent.Linq",
                        "DMVmonitor.Configuration",
                        "DMVmonitor.Outputs",
                        //"OpenSSL.X509Certificate2.Provider",
                        "OpenSSL.PrivateKeyDecoder"
                    };

            string currentAssemblyName = "";

            errors = new List<Record>();

            try
            {
                foreach (string assemstr in assemblyNames)
                {
                    currentAssemblyName = assemstr;
                    Assembly assem = Assembly.Load(currentAssemblyName);
                }
                result = true;
            }
            catch (Exception e)
            {
                Record _rerror = new Record(0);
                _rerror[0] = "Error loading assemblies" + currentAssemblyName + " which are needed to validate config file!!!";
                errors.Add(_rerror);
                _rerror = new Record(0);
                _rerror[0] = "Details: " + e.Message;
                errors.Add(_rerror);
                result = false;
            }
            return result;
        }
        public static bool ValidateConfigFile(string sqldmvconfigfile, string serviceAccountname, string password, out List<Record> errormsg,out List<Record> warningmsg)
        {
            List<string> warnings;
            List<string> errors = null;
            bool result = false;
            bool hasPermissionToImpersonate = true;

            errormsg = new List<Record>();
            warningmsg = new List<Record>();
            //init and create directory for  sql query and sql plan and setting permissions for serviceaccount
            //SQLdetailsStorage.Config.Configuration.Init(sqldmvconfigfile, serviceAccountname);
            String user;
            String domain;


            if ((ParseUserName(serviceAccountname, out user, out domain)))
            {
                result = false; //important as if gmsa o msa then validation is not made
                if (!isComputerPrincipal(domain, user)) //exclude gmsa and msa and computer objects
                {
                    List<Record> errorAssembliesLoad;

                    //load assemblies required for validation during impersonation, so that dll are in memory and we have no permission issues with impersonated users with lower rights on dll files extracted during CA
                    result = loadAssembliesforImpersonation(out errorAssembliesLoad);
                    errormsg.AddRange(errorAssembliesLoad);

                    if (result)
                    {

                        try
                        {
                            using (new Impersonator(user, domain, password))
                            {
                                result = true;
                            }
                        }
                        catch (Win32Exception e)
                        {
                            if (e.NativeErrorCode == 1326 || e.NativeErrorCode == 1331 || e.NativeErrorCode == 1909) // only error if password is wrong otherwise,blocked temporarely or disabled
                            {
                                Record _rerror = new Record(0);
                                _rerror[0] = "Windows Account " + serviceAccountname + " does not exist or Password is not valid!!!\nDetails: " + e.Message + " (" + e.NativeErrorCode + ")" ;
                                errormsg.Add(_rerror);
                                result = false;
                            }
                            else
                            {
                                if (e.NativeErrorCode != 1385) //login request can be granted
                                {
                                    Record _rwarning = new Record(0);
                                    _rwarning[0] = "Windows Account " + serviceAccountname + " has some restriction. Please check message.\n " + e.Message + " (" + e.NativeErrorCode + ")";
                                    warningmsg.Add(_rwarning);
                                }
                                else  //login request can not be granted
                                {
                                    hasPermissionToImpersonate = false;
                                    Record _rwarning = new Record(0);
                                    _rwarning[0] = "Setup cannot Impersonate to user " + serviceAccountname + ". Validation is skipped.\n  " + e.Message + " (" + e.NativeErrorCode + ")";
                                    warningmsg.Add(_rwarning);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Record _rerror = new Record(0);
                            _rerror[0] = "Windows Account " + serviceAccountname + " does not exist or Password is not valid !!!";
                            errormsg.Add(_rerror);
                            _rerror = new Record(0);
                            _rerror[0] = "Details: " + e.Message;
                            errormsg.Add(_rerror);
                            result = false;
                        }

                        if (result && hasPermissionToImpersonate)
                        {
                            try
                            {
                                using (new Impersonator(user, domain, password))
                                {
                                    // cannot impersonate as custom dlls are loaded on different %TEMP% Path breacking the install
                                    // for this we try to load dlls before impersontation running configuration.test with install account. see few line above
                                    result = Configuration.Test(sqldmvconfigfile, out warnings, out errors, false);
                                }
                            }
                            catch (Exception e)
                            {
                                Record _rerror = new Record(0);
                                _rerror[0] = "Error Validating Configuration File. Please check configurations in " + sqldmvconfigfile + " are correct and service account " + serviceAccountname + " can read the file !!!";
                                errormsg.Add(_rerror);
                                _rerror = new Record(0);
                                _rerror[0] = "Details: " + e.Message;
                                errormsg.Add(_rerror);
                                result = false;
                            }

                        }
                    }
                }
                else //important as if gmsa o msa then validation is not made against sql as we are not impersonating this service accounts
                {
                    result = Configuration.Test(sqldmvconfigfile, out warnings, out errors, true);
                }

                if (!result)
                {
                    if (errors != null)
                    {
                        foreach (var error in errors)
                        {
                            Record _r = new Record(0);
                            _r[0] = error;
                            errormsg.Add(_r);

                        }

                    }
                }
            }

            return result;
        }
        public static void logErrors (bool isUI, List<Record> rErrors, Session session=null)
        {
            if (rErrors!=null)
            { 
                foreach (Record error in rErrors)
                {
                    logError(isUI, error, session);
                }
            }
        }

        public static void logError(bool isUI, Record rError, Session session = null)
        {
            if(rError!=null)
            { 
                if (isUI)
                    MessageBox.Show((string)rError[0], "Error validating SQL DMV Monitor Config File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Record tmp = new Record(1);
                tmp[0] = "Error: [1] ";
                tmp[1] = rError[0];
                if (session != null) session.Message(InstallMessage.Error, tmp);
            }
        }

        private static void logWarnings(bool isUI, List<Record> rWarnings, Session session=null)
        {
            foreach (Record warning in rWarnings)
            {
                if (isUI)
                    MessageBox.Show((string)warning[0], "Warnings validating SQL DMV Monitor Config File", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                Record tmp = new Record(1);
                tmp[0] = "WARNING: [1] ";
                tmp[1] = warning[0];
                if (session != null) session.Message(InstallMessage.Warning, tmp);
            }
        }

        private static ActionResult DedectConfigFile(Session session,bool isUI)
        {
            ActionResult aresult;
            Record msg;
            List<Record> rErrors;
            List<Record> rWarnings;
            bool isActionResultSuccess = true;

            aresult = ActionResult.Success;

            string configdir = session["SQLDMVTRCCONFDIR"];//session.GetTargetPath("AXETWCONFIGFOLDER"); //session["AXETWTRACINGCONFDIR"];
            string configfilename = session["SQLDMVTRCCONFIGFILENAME"];
            session["SQLDMVTRC_FULLCONFIGPATH"] = null;
            string fullFilePath;
            if (!DedectConfigFile(configdir, configfilename, out msg, out fullFilePath))
            {
                isActionResultSuccess = false;
                session["SQLDMVTRC_FULLCONFIGPATH"] = null;
                if (isUI) MessageBox.Show((string)msg[0], "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                session.Message(InstallMessage.Error, msg);
            }
            else
            {
                session["SQLDMVTRC_FULLCONFIGPATH"] = fullFilePath;

                string serviceaccountname = session["SQLDMVTRCSERVICEACCOUNT"];
                string serviceaccountpwd = session["SQLDMVTRCSERVICEACCOUNTPWD"];

                bool isValid = ValidateConfigFile(fullFilePath, serviceaccountname, serviceaccountpwd, out rErrors, out rWarnings);
                if (!isValid)
                {
                    logErrors(isUI, rErrors, session);
                    session["SQLDMVTRC_FULLCONFIGPATH"] = null;
                    isActionResultSuccess = false;
                }
                logWarnings(isUI, rWarnings, session);
            }
            if (!isActionResultSuccess)
            {
                aresult = ActionResult.Failure;
                if (isUI)
                {
                    aresult = ActionResult.NotExecuted;
                }

            }
            return aresult;
        }

        private static bool SQLDMVValidateLicenceaccept(Session session, bool isUI)
        {
            bool result = true;
            string validLic = session["LICENSEACCEPTED"];

            if (validLic != "1")
            {
                result = false;
                string msg;

                msg = "License Terms has not benn accepted!!";

                if (isUI)
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Record _r = new Record(0);
                _r[0] = msg;
                session.Message(InstallMessage.Error, _r);
            }

            return result;
        }


        [CustomAction]
        public static ActionResult SQLDMVInitEnv(Session session)
        {

            if (session["LICENSEACCEPTED"] == "1")
            {
                session["LicenseAccepted"] = session["LICENSEACCEPTED"];
            }
            else
            {
                session["LicenseAccepted"] = null;
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult SQLDMVSETLICENSEACCEPT(Session session)
        {
            ActionResult aresult;
            session["LICENSEACCEPTED"] = "1";
            aresult = ActionResult.Success;
            return aresult;
        }

        [CustomAction]
        public static ActionResult DedectConfigFileUI(Session session)
        {
            
            ActionResult aresult= DedectConfigFile(session, true);

            return aresult;
        }

        [CustomAction]
        public static ActionResult DedectConfigFile(Session session)
        {

            ActionResult aresult = DedectConfigFile(session, false);


            if (!SQLDMVValidateLicenceaccept(session, false))
            {
                aresult = ActionResult.Failure;
            }
            return aresult;
        }

        public static bool ParseUserName(string logonName, out string user, out string domain)
        {
            char cSeparator;
            string strTmplogonname;
            bool bresult = false;
            String Computername = System.Environment.MachineName;
            String Computerdomain = GetComputerDomainName();

            user = null;
            domain = null;
            cSeparator = '.';

            if (!string.IsNullOrEmpty(logonName))
            {
                strTmplogonname = logonName.Trim();

                if (logonName.Contains("\\"))
                {
                    cSeparator = '\\';
                }
                else if (logonName.Contains("@"))
                {
                    cSeparator = '@';
                }

                if (cSeparator != '.')
                {
                    string[] logonParts;
                    logonParts = logonName.Split(cSeparator);

                    if (cSeparator == '\\')
                    {
                        domain = logonParts[0].Trim();
                        user = logonParts[1].Trim();
                    }
                    else
                    {
                        user = logonParts[0].Trim();
                        domain = logonParts[1].Trim();
                    }

                    if ( domain == ".")
                    {
                        domain = Computername;
                    }
                }
                else
                {
                    user = strTmplogonname;
                }

                if (string.IsNullOrEmpty(domain))
                {
                    domain = Computername;
                }
                if (!string.IsNullOrEmpty(user))
                {
                    bresult = true;
                }
            }

            return bresult;
        }

        private static ContextType getPrincipalContext(string domain)
        {
            ContextType ctx;

            ctx = ContextType.Machine;
            String Computername = System.Environment.MachineName;

            if (!string.IsNullOrEmpty(domain))
            {
                if (!(domain.ToUpper().Equals(Computername.ToUpper())))
                {
                    ctx = ContextType.Domain;
                }

            }
            return ctx;
        }

        public static bool IsValidPrincipal(string domain, string principal, bool onlyAuthPrincipals = false)
        {
            bool isValid = false;
            ContextType ctx;

            ctx = getPrincipalContext(domain);

            try
            {
                using (PrincipalContext pc = new PrincipalContext(ctx, domain))
                {
                    Principal up = null;
                    if (onlyAuthPrincipals)
                    {
                        up = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                        if (up == null)
                            up = ComputerPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    }
                    else
                        up = Principal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    isValid = (up != null);
                }
            }
            catch (Exception e)
            {
                isValid = false;
            }
            return isValid;
        }

        public static bool isComputerPrincipal(string domain, string principal)
        {
            bool isValid = false;
            ContextType ctx = getPrincipalContext(domain);
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ctx, domain))
                {
                    Principal up = null;
                    up = ComputerPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, principal);
                    isValid = (up != null);
                }
            }
            catch (Exception e)
            {
                isValid = false;
            }
            return isValid;

        }


        private static bool DedectAdminAccount(Session session, out Record errormsg)
        {
            bool aResult = true;
            string msg;
            Record r = new Record(0);

            string assignadmaccount = session["SQLDMVTRCASSIGNADMIN"];
            string admaccount = session["SQLDMVTRCADMINACCOUNT"];
            string admname = null;
            string admdomain = null;

            if (admaccount == null)
                admaccount = "";

            if (assignadmaccount != null && assignadmaccount.Equals("1"))
            {
                if (!(ParseUserName(admaccount, out admname, out admdomain) && IsValidPrincipal(admdomain, admname)))
                {
                    msg = "Windows Account/Group " + admaccount + " for administrate Service is not valid !!";
                    admname = null;
                    admdomain = null;
                    r[0] = msg;
                    aResult = false;
                }
                session["SQLDMVTRC_ADMINACCOUNTNAME"] = admname;
                session["SQLDMVTRC_ADMINACCOUNTDOMAIN"] = admdomain;
            }
            else
            {
                session["SQLDMVTRCADMINACCOUNT"] = null;
                session["SQLDMVTRC_ADMINACCOUNTNAME"] = admname;
                session["SQLDMVTRC_ADMINACCOUNTDOMAIN"] = admdomain;
            }
            errormsg = r;
            return aResult;
        }

        private static bool DedectServiceAccount(Session session, out Record errormsg)
        {
            bool aResult = true;
            string msg;
            Record r = new Record(0);


            {
                string svcaccount = session["SQLDMVTRCSERVICEACCOUNT"];
                string svcaname;
                string svcadomain;

                if (svcaccount == null)
                    svcaccount = "";
                if (!(ParseUserName(svcaccount, out svcaname, out svcadomain)  && IsValidPrincipal(svcadomain, svcaname,true)))
                {
                    msg = "Service Account " + svcaccount + " is not valid !!";
                    r[0] = msg;
                    svcaname=null;
                    svcadomain=null;

                    aResult = false;
                }
                session["SQLDMVTRC_SERVICEACCOUNTNAME"] = svcaname;
                session["SQLDMVTRC_SERVICEACCOUNTDOMAIN"] = svcadomain;
            }

            errormsg = r;
            return aResult;
        }

        [CustomAction]
        public static ActionResult ValidateAccountsUI(Session session)
        {
            ActionResult aresult;
            Record msg;

            aresult = ActionResult.Success;
            if (!DedectAdminAccount(session, out msg))
            {
                logError(true, msg, session);
                aresult = ActionResult.NotExecuted;
            }

            if (!DedectServiceAccount(session, out msg))
            {
                logError(true, msg, session);
                aresult = ActionResult.NotExecuted;
            }
            return aresult;
        }

        [CustomAction]
        public static ActionResult ValidateAccounts(Session session)
        {
            ActionResult aresult;
            Record msg;

            aresult = ActionResult.Success;

            if (!DedectAdminAccount(session, out msg))
            {
                logError(false, msg, session);
                aresult = ActionResult.Failure;
            }

            if (!DedectServiceAccount(session, out msg))
            {
                logError(false, msg, session);
                aresult = ActionResult.Failure;
            }
            return aresult;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<string> warnings;
            List<string> errors;
            bool result = false;

            List<Record> rErrors;
       
            string domain;
            string user;
            bool ok = SQLDMVTracingCA.CustomActions.ParseUserName("wp", out user, out domain);

            ok = SQLDMVTracingCA.CustomActions.ParseUserName("wp\\pb00013", out user, out domain);
            ok= SQLDMVTracingCA.CustomActions.IsValidPrincipal(domain, user, true);
            ok = SQLDMVTracingCA.CustomActions.IsValidPrincipal(domain, user);
            Record msg;

            string fullFilePath;
            bool res= SQLDMVTracingCA.CustomActions.DedectConfigFile("c:\\tmp","sqltrace.conf", out msg, out fullFilePath);

            SQLDMVTracingCA.CustomActions.logError(true, msg);

            bool i = SQLDMVTracingCA.CustomActions.IsValidPrincipal("wp", "pb00013");
            i = SQLDMVTracingCA.CustomActions.IsValidPrincipal("wp.lan", "gMSA-test-LEPE$",true);
            i = SQLDMVTracingCA.CustomActions.IsValidPrincipal("wp", "lg-systemintegration-rw",true);
            i = SQLDMVTracingCA.CustomActions.IsValidPrincipal("pbzdev01", "Administrator");
            i = SQLDMVTracingCA.CustomActions.IsValidPrincipal("pbzdev01.test", "Administrator");

            List<string> _warnings;
            List<Record> _errors=null ;
            bool _result = false;
            //_result=SQLDMVTracingCA.CustomActions.ValidateConfigFile("c:\\tmp\\sqltrace.conf", "wp\\test_leitner", "yudSOuXM0Xjq", out _errors);
            //_result = SQLDMVTracingCA.CustomActions.ValidateConfigFile("c:\\tmp\\sqltrace.conf", "wp\\gMSA-test-LEPE$", "", out _errors);
            SQLDMVTracingCA.CustomActions.logErrors(true, _errors);
        }
    }

}
