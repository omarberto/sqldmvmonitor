﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nett;
using System.Reflection;

namespace DMVmonitor.Configuration
{
    public class Configuration
    {
        private static Configuration _config;
        public static Configuration config
        {
            get
            {
                return _config;
            }
        }
        public static bool Init(string configPath, ref List<string> warnings, ref List<string> errors)
        {
            try
            {
                _config = Toml.ReadFile<Configuration>(configPath);
                //if(_config.output.hasOutput && _config.output.useTornado)
                //{
                //    if (string.IsNullOrEmpty(_config.output.tornado.address))
                //        _config.output.tornado.address = _config.output.nats.address;
                //    else
                //        _config.output.tornado.openconnection = true;
                    
                //    if (string.IsNullOrEmpty(_config.output.tornado.tls_ca))
                //        _config.output.tornado.tls_ca = _config.output.nats.tls_ca;
                //    else
                //        _config.output.tornado.openconnection = true;
                    
                //    if (!_config.output.tornado.openconnection)
                //        if (!_config.output.tornado.secure)
                //        {
                //            _config.output.tornado.secure = _config.output.nats.secure;
                //            if (string.IsNullOrEmpty(_config.output.tornado.tls_cert) && string.IsNullOrEmpty(_config.output.tornado.tls_key))
                //            {
                //                _config.output.tornado.tls_cert = _config.output.nats.tls_cert;
                //                _config.output.tornado.tls_key = _config.output.nats.tls_key;
                //            }
                //            else if (string.IsNullOrEmpty(_config.output.tornado.tls_name))
                //            {
                //                _config.output.tornado.tls_name = _config.output.nats.tls_name;
                //            }
                //            else if (string.IsNullOrEmpty(_config.output.tornado.tls_pfx))
                //            {
                //                _config.output.tornado.tls_pfx = _config.output.nats.tls_pfx;
                //            }
                //        }
                    
                //}

                var assembly = Assembly.GetExecutingAssembly();
                var assemblyName = assembly.GetName().Name;

                using (var stream = assembly.GetManifestResourceStream(assemblyName + ".DX.DMV_HB.conf"))
                {
                    _config.DMV_HB = Toml.ReadStream(stream).Values.ElementAt(0).Get<DX.DMV_HB>();
                }


                if (_config.hasMain && _config.hasDmv)
                {
                    _config.dmv.Set_DMV_ST();
                }

                if (_config.hasMain && _config.hasDmv_LT)
                {
                    _config.dmv.DXValuesInit(_config.main.dataSource, _config.main.initialCatalog);
                }

                //if (_config.output.hasOutput)
                //{
                //    _config.output.nats.LoadRetentionPolicies(assembly);
                //}
            }
            catch
            (Exception e)
            {
                errors.Add(string.Format("Configuration --> Init {0} \n Exception Message {1}", configPath, e.Message));

                return false;
            }

            return true;
        }

        public GeneralConfiguration general { get; set; } = new GeneralConfiguration();

        /*
        TODO: trasferire tutto in nuova library!!!!
        public Outputs output { get; set; } = new Outputs();
        */

        private MainConfiguration _main;
        public MainConfiguration main
        {
            set
            {
                _main = value;
                if (!hasDmv)
                {
                    _dmv = new DmvConfiguration();
                }
                if (!_dmv.hasConnectionString)
                {
                    _dmv.connectionString = value.connectionString;
                }
            }
            get
            {
                return _main;
            }
        }
        [Nett.TomlIgnore]
        public bool hasMain
        {
            get
            {
                //per ora ne ho uno e basta... (di base è privilegiato nats)
                return _main != null;
            }
        }

        private DmvConfiguration _dmv;
        public DmvConfiguration dmv
        {
            set
            {
                _dmv = value;
                if (hasMain && !_dmv.hasConnectionString)
                {
                    _dmv.connectionString = main.connectionString;
                }
            }
            get
            {
                return _dmv;
            }
        }
        [Nett.TomlIgnore]
        public bool hasDmv
        {
            get
            {
                //
                return _dmv != null;
            }
        }
        
        public DX.DMV_HB DMV_HB { get; set; } = new DX.DMV_HB();

        [Nett.TomlIgnore]
        public bool hasDmv_LT
        {
            get
            {
                //
                return !DMV_HB.disabled || (hasDmv && dmv.DXValues.Count > 0);
            }
        }

        //public XeventsConfiguration xevents { get; set; }
        //[Nett.TomlIgnore]
        //public bool hasXevents
        //{
        //    get
        //    {
        //        //per ora ne ho uno e basta... (di base è privilegiato nats)
        //        return xevents != null;
        //    }
        //}

        public static bool Test(string configfile, out List<string> warnings, out List<string> errors, bool minimal = true)
        {
            warnings = new List<string>();
            errors = new List<string>();

            if (Init(configfile, ref warnings, ref errors))
            {
                if (config.hasMain
                    //&& DMVmonitor.Outputs.Configuration.Configuration.Init(configfile, ref warnings, ref errors)
                    )
                {
                    var warningsS = new List<string>();
                    var errorsS = new List<string>();
                    DMVmonitor.Outputs.Configuration.Configuration.Test(configfile, out warningsS, out errorsS);
                    warnings.AddRange(warningsS);
                    errors.AddRange(errorsS);
                }

                if (minimal)
                {

                }
                else
                {
                    if (config.hasMain)
                    {
                        //accettabile
                        config.main.Test(ref warnings, ref errors);

                        if (config.main.isReader)
                        {
                            var warningsS = new List<string>();
                            var errorsS = new List<string>();
                            SQLdetailsStorage.Config.Configuration.Test(configfile, out warningsS, out errorsS);
                            warnings.AddRange(warningsS);
                            errors.AddRange(errorsS);
                        }

                    }
                    else
                    {
                        //non accettabile?????
                        errors.Add("main configuration is missing");
                    }

                    if (config.hasDmv_LT)
                    {
                        //accettabile
                        config.DMV_HB.Test(ref warnings, ref errors);
                        //accettabile
                        config.dmv.TestDX(ref warnings, ref errors);
                    }
                    else
                    {
                    }
                }
            }

            return errors.Count == 0;
        }
    }
}
