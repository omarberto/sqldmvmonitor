﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.AccessControl;

namespace DMVmonitor.Configuration
{
    public class FileSystemStorageConfiguration
    {
        public bool zipped { set; get; }

        public string queriespath { set; get; }

        public string planspath { set; get; }

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(queriespath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.queriespath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(queriespath))
                {
                    var acl = Directory.GetAccessControl(queriespath);
                    AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                    var NtAccountName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    bool found = false;
                    //Go through the rules returned from the DirectorySecurity
                    foreach (AuthorizationRule rule in rules)
                    {
                        //If we find one that matches the identity we are looking for
                        if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var filesystemAccessRule = (FileSystemAccessRule)rule;

                            //Cast to a FileSystemAccessRule to check for access rights
                            if ((filesystemAccessRule.FileSystemRights & FileSystemRights.WriteData) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                            {
                                found = true;
                            }
                        }
                    }

                    if (!found)
                        errors.Add(string.Format("{0} does not have write access to {1}", NtAccountName, queriespath));
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", queriespath));
                }
            }

            if (string.IsNullOrEmpty(planspath))
            {
                //non accettabile
                errors.Add("main.storage.fileSystem.planspath configuration is missing");
            }
            else
            {
                //accettabile
                if (Directory.Exists(planspath))
                {
                    var acl = Directory.GetAccessControl(planspath);
                    AuthorizationRuleCollection rules = acl.GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));
                    var NtAccountName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                    bool found = false;
                    //Go through the rules returned from the DirectorySecurity
                    foreach (AuthorizationRule rule in rules)
                    {
                        //If we find one that matches the identity we are looking for
                        if (rule.IdentityReference.Value.Equals(NtAccountName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var filesystemAccessRule = (FileSystemAccessRule)rule;

                            //Cast to a FileSystemAccessRule to check for access rights
                            if ((filesystemAccessRule.FileSystemRights & FileSystemRights.WriteData) > 0 && filesystemAccessRule.AccessControlType != AccessControlType.Deny)
                            {
                                found = true;
                            }
                        }
                    }

                    if(!found)
                        errors.Add(string.Format("{0} does not have write access to {1}", NtAccountName, queriespath));
                }
                else
                {
                    //non accettabile
                    errors.Add(string.Format("{0} isn't a valid path", queriespath));
                }
            }
        }
    }
}