﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace DMVmonitor.Configuration
{
    //public abstract class InfluxDataSend : IDisposable
    //{
    //    protected IConnection nats_connection = null;
    //    private Options opts;

    //    private bool verifyServerCert(object sender, X509Certificate certificate, X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
    //    {
    //        if (sslPolicyErrors == System.Net.Security.SslPolicyErrors.None)
    //            return true;

    //        foreach (var chainElement in chain.ChainElements)
    //        {
    //            //Console.WriteLine(chainElement.Information);
    //        }

    //        return true;
    //    }
    //    public InfluxDataSend(string address, X509Certificate2 certificate = null)
    //    {
    //        this.opts = NATS.Client.ConnectionFactory.GetDefaultOptions();
    //        this.opts.Url = address;
    //        this.opts.AllowReconnect = true;
    //        this.opts.MaxReconnect = int.MaxValue;
    //        if (certificate != null)
    //        {
    //            this.opts.Secure = true;
    //            this.opts.TLSRemoteCertificationValidationCallback = verifyServerCert;
    //            this.opts.AddCertificate(certificate);
    //        }



    //        this.opts.AsyncErrorEventHandler += (sender, args) =>
    //        {
    //            ErrorMessage(true, "NATS.io AsyncErrorEventHandler Server: {0}\n Message: {1}\n Subject: {2}", args.Conn.ConnectedUrl, args.Error, args.Subscription.Subject);
    //        };

    //        this.opts.ReconnectedEventHandler += (sender, args) =>
    //        {
    //            ErrorMessage(false, "NATS.io ReconnectedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
    //        };

    //        this.opts.ClosedEventHandler += (sender, args) =>
    //        {
    //            ErrorMessage(false, "NATS.io ClosedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
    //        };

    //        this.opts.DisconnectedEventHandler += (sender, args) =>
    //        {
    //            ErrorMessage(false, "NATS.io DisconnectedEventHandler: {0}\n State: {1}", args.Conn.ConnectedUrl, args.Conn.State);
    //        };
    //    }

    //    public void Connect()
    //    {
    //        //si potrebbe mettere in classe
    //        var cf = new NATS.Client.ConnectionFactory();
            
    //        try
    //        {
    //            nats_connection = cf.CreateConnection(this.opts);
    //        }
    //        catch (Exception e)
    //        {
    //            ErrorMessage(true, "nats_connection CreateConnection() for {1} exception: {0}", e.Message, this.opts.Url);
    //        }
    //    }


    //    public void Dispose()
    //    {
    //        try
    //        {
    //            if (!nats_connection.IsClosed())
    //                nats_connection.Close();
    //        }
    //        catch (Exception e)
    //        {
    //            ErrorMessage(true, "nats_connection.Close() exception: {0}", e.Message);
    //        }

    //        try
    //        {
    //            nats_connection.Dispose();
    //        }
    //        catch (Exception e)
    //        {
    //            ErrorMessage(true, "nats_connection.Dispose() exception: {0}", e.Message);
    //        }
    //    }

    //    public abstract void ErrorMessage(bool error, string format, params object[] args);
    //}

    //internal class InfluxTestDataSend : InfluxDataSend
    //{

    //    public List<string> warnings = new List<string>();
    //    public List<string> errors = new List<string>();

    //    public InfluxTestDataSend(string address, X509Certificate2 certificate = null) : base(address, certificate)
    //    {

    //    }

    //    public override void ErrorMessage(bool error, string format, params object[] args)
    //    {
    //        if (error)
    //            errors.Add(string.Format(format, args));
    //        else
    //            warnings.Add(string.Format(format, args));
    //    }
    //}
}