﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration
{
    public class GeneralConfiguration
    {
        public string name { set; get; } = "DMVmonitor"; //logging purpouse
        public string version { set; get; } = "DMVmonitor 0.1"; //
    }
}
