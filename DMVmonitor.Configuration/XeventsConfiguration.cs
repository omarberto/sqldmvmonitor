﻿using System;
using System.Collections.Generic;

namespace DMVmonitor.Configuration
{
    public class XeventsConfiguration
    {
        public XeventsProbeConfiguration queriesProbe { get; set; }
    }
    public class XeventsProbeConfiguration
    {
        public XeventsReaderConfiguration reader { get; set; }
    }
    public class XeventsReaderConfiguration
    {
        public string dataSource { get; set; }
        public string sourceName { get; set; }

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            
        }
    }
}