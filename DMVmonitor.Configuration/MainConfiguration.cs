﻿using Microsoft.SqlServer.XEvent.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DMVmonitor.Configuration
{
    public class MainConfiguration
    {
        public string type { get; set; } = "READER";
        [Nett.TomlIgnore]
        public bool isReader { get { return type == "READER"; } }
        public string dataSource { get; set; }
        [Nett.TomlIgnore]
        public string dataSourceTcpPort
        {
            get
            {
                int dpIndex = dataSource.IndexOf(':');
                if (dpIndex < 0)
                    return "1433";
                else
                {
                    return dataSource.Substring(1 + dpIndex);
                }
            }
        }
        [Nett.TomlIgnore]
        public bool isStandardTcpPort
        {
            get
            {
                return dataSourceTcpPort == "1433";
            }
        }
        public string initialCatalog { get; set; } = "master";
        [Nett.TomlIgnore]
        public string connectionString
        {
            get
            {
                var sb = new System.Data.SqlClient.SqlConnectionStringBuilder();
                sb.DataSource = dataSource;
                if(!string.IsNullOrEmpty(initialCatalog))
                    sb.InitialCatalog = initialCatalog;
                sb.ConnectTimeout = 60;
                sb.IntegratedSecurity = true;
                sb.ApplicationIntent = System.Data.SqlClient.ApplicationIntent.ReadOnly;
                return sb.ConnectionString;
            }
        }
        
        //public string sql_instance_name { get { return dataSource; } }

        public string queriesProbe { get; set; }
        [Nett.TomlIgnore]
        public bool hasQueriesProbe { get { return !string.IsNullOrEmpty(queriesProbe); } }
        public string fetchesProbe { get; set; }
        [Nett.TomlIgnore]
        public bool hasFetchesProbe { get { return !string.IsNullOrEmpty(fetchesProbe); } }
        public string blockingsProbe { get; set; }
        [Nett.TomlIgnore]
        public bool hasBlockingsProbe { get { return !string.IsNullOrEmpty(blockingsProbe); } }

        public ulong fetchesFilter { get; set; } = 100000;

        public void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (isReader)
            {
                if (string.IsNullOrEmpty(dataSource))
                {
                    errors.Add("main dataSource is empty or missing");
                }
                else
                {
                    bool connected = false;
                    //test connection string, da vedere se le sottoimpostazioni sono capaci di leggere questa...
                    using (var sqlConnection = new System.Data.SqlClient.SqlConnection(connectionString))
                    {
                        try
                        {
                            sqlConnection.Open();
                            connected = true;
                            GetInstance(sqlConnection);
                            TestVersion(ref errors, sqlConnection);
                        }
                        catch (Exception e)
                        {
                            errors.Add(string.Format("dmv connection test failed with exception {0}: {1}", e.GetType().Name, e.Message));
                        }

                    }
                    if (connected && hasQueriesProbe)
                        TestSession(ref errors, queriesProbe);
                    if (connected && hasFetchesProbe)
                        TestSession(ref errors, fetchesProbe);
                    if (connected && hasBlockingsProbe)
                        TestSession(ref errors, blockingsProbe);

                }

                
            }
            else
            {

            }
            
        }

        private void GetInstance(SqlConnection sqlConnection)
        {
            
            using (SqlCommand cmd = sqlConnection.CreateCommand())
            {
                cmd.CommandText = "select @@servername";
                cmd.CommandType = System.Data.CommandType.Text;

                try
                {
                    dataSource = cmd.ExecuteScalar() as string;
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void TestVersion(ref List<string> errors, SqlConnection sqlConnection)
        {
            var t = sqlConnection.ServerVersion.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            int mv = 0;
            if (t.Length == 0 || !int.TryParse(t[0], out mv) || mv < 11)
            {
                errors.Add(string.Format("sql server version {0} is not supported", sqlConnection.ServerVersion));
            }
        }

        private void TestSession(ref List<string> errors, string sessionName)
        {
            try
            {
                using (QueryableXEventData stream = new QueryableXEventData(connectionString, sessionName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                {

                }
            }
            catch (Exception e)
            {
                errors.Add(string.Format("test on session {0} failed with exception {1}: {2}", sessionName, e.GetType().Name, e.Message));
            }
        }
    }

}