﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{
    public class DMV_HB
    {
        public bool disabled { get; set; } = false;

        public int polling_interval { get; set; } = 5000;

        public string measurementType1 { get; set; } //axuser.... --> mettere i valori definitivi
        [Nett.TomlIgnore]
        public bool toNATS1 { get { return !string.IsNullOrEmpty(measurementType1); } }

        public string measurementType2 { get; set; } //sessionId
        [Nett.TomlIgnore]
        public bool toNATS2 { get { return !string.IsNullOrEmpty(measurementType2); } }

        public int min_wait_duration { get; set; } = 5;

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            using (var sqlConnection = new System.Data.SqlClient.SqlConnection(Configuration.config.dmv.connectionString))
            {
                try
                {
                    using (System.Data.SqlClient.SqlCommand cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandText =
                            "DECLARE @pollingTime datetime; set @pollingTime=GETDATE(); " +
                                        "select " +
                                        "d_es.session_id," +
                                        "d_es.login_name, d_es.host_name, d_es.program_name," +
                                        "d_er.wait_resource, d_er.wait_time, d_er.wait_type," +
                                        "d_er.transaction_id, d_es.database_id," +
                                        "d_es.context_info," +
                                        "d_er.blocking_session_id," +
                                        "@pollingTime" +
                                        " from sys.dm_exec_sessions as d_es" +
                                        " left outer join sys.dm_exec_requests as d_er on d_es.session_id = d_er.session_id" +
                                " option (loop join)";

                        cmd.CommandType = System.Data.CommandType.Text;


                        sqlConnection.Open();


                        using (System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader())
                        {
                            try
                            {
                                while (reader.Read()) ;
                            }
                            catch (System.Data.SqlClient.SqlException ex)
                            {
                                errors.Add(string.Format("SqlException during headblockers query test {0}", ex.Message));
                            }
                            catch (Exception ex)
                            {
                                errors.Add(string.Format("Exception {1} during headblockers query test {0}", ex.Message, ex.GetType().Name));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
