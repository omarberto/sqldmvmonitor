﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{
    public class DMV_TLEnh
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return !string.IsNullOrEmpty(subject); } }

        public int min_tran_duration { get; set; }
    }
}
