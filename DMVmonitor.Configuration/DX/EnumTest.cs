﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{

    public class DynamicDMVParamsBase
    {
        public string dataSource { set; get; }
        public string databaseName { set; get; }

        public string connectionString(string mainDataSource, string mainDatabaseName)
        {
            var sb = new System.Data.SqlClient.SqlConnectionStringBuilder();
            sb.DataSource = (string.IsNullOrEmpty(this.dataSource)) ? mainDataSource : this.dataSource;
            sb.InitialCatalog = (string.IsNullOrEmpty(this.databaseName)) ? mainDatabaseName : this.databaseName;
            sb.ConnectTimeout = 60;
            sb.IntegratedSecurity = true;
            sb.ApplicationIntent = System.Data.SqlClient.ApplicationIntent.ReadOnly;
            return sb.ConnectionString;
        }
    }
    public class DynamicDMVParams : DynamicDMVParamsBase
    {
        public string[] databaseNames { set; get; }
        
        internal DynamicDMVParamsBase[] Init()
        {
            string tmp_dataSource = (string.IsNullOrEmpty(this.dataSource)) ? "" : this.dataSource;
            string tmp_databaseName = (string.IsNullOrEmpty(this.databaseName)) ? "" : this.databaseName;
            string[] tmp_databaseNames = (this.databaseNames == null) ? new string[0] : this.databaseNames;
            
            if (tmp_databaseNames.Length == 0 && !string.IsNullOrEmpty(tmp_databaseName))
                tmp_databaseNames = new string[] { tmp_databaseName };

            if (tmp_databaseNames.Length == 0)
            {
                return new DynamicDMVParamsBase[] { new DynamicDMVParamsBase()
                {
                    dataSource = tmp_dataSource,
                    databaseName = tmp_databaseName
                }};
            }
            else
            {
                return tmp_databaseNames.Select(name => new DynamicDMVParamsBase()
                {
                    dataSource = tmp_dataSource,
                    databaseName = name
                }).ToArray();
            }
        }
    }
    public class DynamicDMV
    {
        public Dictionary<string,EnumTest> DX { set; get; }

        [Nett.TomlIgnore]
        public DynamicDMVParamsBase connectionParams { get; set; }
        [Nett.TomlIgnore]
        public string connectionString { get; set; }

        public DynamicDMV Clone()
        {
            return (DynamicDMV)this.MemberwiseClone();
        }
    }

    public class EnumTest
    {
        public string name { set; get; }

        public int polling_interval { get; set; }
        public string sql { set; get; }


        public string formatString { get; set; }
        public InfluxFormat[] influx { get; set; }

        public Nett.TomlTable Enums { get; set; }

        [Nett.TomlIgnore]
        public EnumD EnumsD
        {
            get
            {
                Console.WriteLine("Enums == null {0}", Enums == null);
                return new EnumD(Enums);
            }
        }


        public Nett.TomlTable Maps { get; set; }

        [Nett.TomlIgnore]
        public MapD MapsD
        {
            get
            {
                Console.WriteLine("Maps == null {0}", Maps == null);
                return new MapD(Maps);
            }
        }

        public class InfluxFormat
        {
            public int index { set; get; }
            public string type { set; get; }
            public string escape { set; get; }
            [Nett.TomlIgnore]
            public bool fieldEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "field";
                }
            }
            [Nett.TomlIgnore]
            public bool tagEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "tag";
                }
            }
            public string enumname { set; get; }

            public string formatString { set; get; }
            [Nett.TomlIgnore]
            public bool itemFormat
            {
                get
                {
                    return !string.IsNullOrEmpty(formatString);
                }
            }
            //funziona davvero solo se è un tag, altrimenti incasini tutto
            public bool uniquifier { set; get; } = false;
        }


        public class InfluxFieldFormat
        {
            public int index { set; get; }
            public string type { set; get; }
            public string escape { set; get; }
            [Nett.TomlIgnore]
            public bool fieldEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "field";
                }
            }
            [Nett.TomlIgnore]
            public bool tagEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "tag";
                }
            }
            public string enumname { set; get; }
            public string aggr { set; get; }
            [Nett.TomlIgnore]
            public List<FieldAggregationFunction> aggrList
            {
                get
                {
                    var subs = aggr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    List<FieldAggregationFunction> resValues = new List<FieldAggregationFunction>();
                    foreach (var sub in subs)
                        resValues.Add(new FieldAggregationFunction(sub));
                    return resValues;
                }
            }

            public string formatString { set; get; }
            [Nett.TomlIgnore]
            public bool itemFormat
            {
                get
                {
                    return !string.IsNullOrEmpty(formatString);
                }
            }
        }
        public enum FieldAggregationFunctionTypes
        {
            Count,
            Concat,
            Last,
            First,
            LastAsc,
            FirstAsc,
            LastDesc,
            FirstDesc,
            Min,
            Max,
        }
        public class FieldAggregationFunction
        {
            public FieldAggregationFunction(string sub)
            {
                var tmp = sub.ToLower().Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
                string name = tmp[0];
                string extref = tmp.Length > 1 ? tmp[1] : "";

                switch (tmp[0])
                {
                    case "count":
                        this.type = FieldAggregationFunctionTypes.Count;
                        break;
                    case "concat":
                        this.type = FieldAggregationFunctionTypes.Concat;
                        break;
                    case "last":
                        if (tmp.Length > 1)
                        {
                            var specs = tmp[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            this.extref = int.Parse(specs[0]);
                            switch (specs[1])
                            {
                                case "asc":
                                    this.type = FieldAggregationFunctionTypes.LastAsc;
                                    break;
                                case "desc":
                                    this.type = FieldAggregationFunctionTypes.LastDesc;
                                    break;
                            }
                        }
                        else
                            this.type = FieldAggregationFunctionTypes.Last;
                        break;
                    case "first":
                        if (tmp.Length > 1)
                        {
                            var specs = tmp[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            this.extref = int.Parse(specs[0]);
                            switch (specs[1])
                            {
                                case "asc":
                                    this.type = FieldAggregationFunctionTypes.FirstAsc;
                                    break;
                                case "desc":
                                    this.type = FieldAggregationFunctionTypes.FirstDesc;
                                    break;
                            }
                        }
                        else
                            this.type = FieldAggregationFunctionTypes.First;
                        break;
                    case "min":
                        this.type = FieldAggregationFunctionTypes.Min;
                        break;
                    case "max":
                        this.type = FieldAggregationFunctionTypes.Max;
                        break;
                }
            }

            public FieldAggregationFunctionTypes type { get; set; }
            public int extref { get; set; }
        }

        public string tags_FormatString { get; set; }

        public InfluxFormat[] tags { get; set; }

        public string fields_FormatString { get; set; }

        public InfluxFieldFormat[] fields { get; set; }

        public string[] orderby { get; set; }
        public class InfluxOrderFunction
        {
            public InfluxOrderFunction(string sub)
            {
                var tmp = sub.ToLower().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                this.index = int.Parse(tmp[0]);
                this.isdesc = tmp.Length > 1 ? (tmp[1] == "desc") : false;
            }
            public int index { get; set; }
            public bool isdesc { get; set; }
        }
        public InfluxOrderFunction[] orderbyFunc
        {
            get
            {
                if (orderby == null) return new InfluxOrderFunction[0];
                return orderby.Select(el => new InfluxOrderFunction(el)).ToArray();
            }
        }

        public int timeindex { get; set; }
    }
}
