﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{
    public class EnumD
    {
        private Dictionary<string, Dictionary<int, string>> values = new Dictionary<string, Dictionary<int, string>>();
        public string Get(string name, int id)
        {
            string value;
            if (!values[name].TryGetValue(id, out value))
            {
                Console.WriteLine("missing value for {0} id {1}", name, id);
            }
            return value;
            return values[name][id];
        }
        public EnumD(Nett.TomlTable t)
        {
            if (t != null)
            {
                this.values = t.Rows.ToDictionary(
                    t2 => t2.Key,
                    t2 => t2.Value.Get<Nett.TomlTableArray>().Items.Select(item => item.Get<IdTextPair>()).ToDictionary(p => p.id, p => p.text)
                    );
            }
        }


        public class IdTextPair
        {
            public int id { set; get; }
            public string text { set; get; }
        }
    }
}
