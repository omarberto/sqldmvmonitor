﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{
    public class MapD
    {
        private Dictionary<string, Dictionary<string, string>> values = new Dictionary<string, Dictionary<string, string>>();
        public string Get(string name, string id)
        {
            string value;
            if (!values[name].TryGetValue(id, out value))
            {
                //Console.WriteLine("missing value for {0} id {1}", name, id);
                value = "NONE";
            }
            return value;
            return values[name][id];
        }
        public MapD(Nett.TomlTable t)
        {
            if (t != null)
            {
                this.values = t.Rows.ToDictionary(
                    t2 => t2.Key,
                    t2 => t2.Value.Get<Nett.TomlTableArray>().Items.Select(item => item.Get<IdTextPair>()).ToDictionary(p => p.id, p => p.text)
                    );
            }
        }


        public class IdTextPair
        {
            public string id { set; get; }
            public string text { set; get; }
        }
    }
}
