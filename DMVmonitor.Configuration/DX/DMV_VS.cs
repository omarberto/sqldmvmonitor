﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Configuration.DX
{
    public class DMV_VS
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string db_name { get; set; }
    }
}
