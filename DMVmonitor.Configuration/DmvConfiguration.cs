﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DMVmonitor.Configuration.DX;

namespace DMVmonitor.Configuration
{
    public class DmvConfiguration
    {
        public string type { get; set; }
        [Nett.TomlIgnore]
        public bool isReader { get { return type.ToUpper() == "READER"; } }
        public bool toNATS { get; set; } = true;
        public bool toFILE { get; set; } = false;
        public string connectionString { get; set; }
        [Nett.TomlIgnore]
        public bool hasConnectionString { get { return !string.IsNullOrEmpty(connectionString); } }

        public string whereClause { get; set; }
        [Nett.TomlIgnore]
        public bool hasWhereClause { get { return !string.IsNullOrEmpty(whereClause); } }

        public int thresholdMODE { get; set; } = 0;
        public long durationThreshold { get; set; }
        public long executionThreshold { get; set; }

        public string textThreshold { get; set; }

        internal void DXValuesInit(string dataSource, string initialCatalog)
        {
            foreach (var el in DXValues)
            {
                el.Value.connectionString = el.Value.connectionParams.connectionString(dataSource, initialCatalog);
            }
        }

        public int polling_interval { get; set; }

        internal void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(connectionString.Trim()))
            {
                //eccezione
                errors.Add("dmv connectionString is empty or missing");
            }
            else
            {
                //test connessione
                using (var sqlConnection = new System.Data.SqlClient.SqlConnection(connectionString))
                {
                    try
                    {
                        sqlConnection.Open();
                    }
                    catch(Exception e)
                    {
                        errors.Add(string.Format("dmv connection test failed with exception {0}: {1}", e.GetType().Name, e.Message));
                    }
                }
            }
        }

        //qui mettiamo DX
        private Dictionary<string, DX.DynamicDMV> _DXValues = new Dictionary<string, DX.DynamicDMV>();
        public Dictionary<string, DX.DynamicDMV> DXValues
        {
            get { return _DXValues; }
        }



        internal void Set_DMV_ST()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName().Name;
            
            DX.DynamicDMV objectR;

            Console.WriteLine("DX_set: " + assemblyName + ".DX.DMV_ST.conf");

            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".DX.DMV_ST.conf"))
            {
                objectR = Nett.Toml.ReadStream<DX.DynamicDMV>(stream);
            }

            var copy = objectR.Clone();
            copy.connectionParams = new DynamicDMVParamsBase() { databaseName = "", dataSource = "" };
            _DXValues.Add("DMV_ST", copy);
        }

        private Dictionary<string, DX.DynamicDMVParams> _DX;
        public Dictionary<string, DX.DynamicDMVParams> DX
        {
            set
            {

                var assembly = Assembly.GetExecutingAssembly();
                var assemblyName = assembly.GetName().Name;



                foreach (var p in value)
                {
                    //Console.WriteLine(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("DX\\{0}.conf", p.Key)));
                    var tmp = p.Value.Init();
                    //if(!Connections.ContainsKey(tmp))
                    //   Connections.Add(tmp, tmp.connectionString);

                    DX.DynamicDMV objectR;

                    Console.WriteLine("DX_set: " + assemblyName + string.Format(".DX.{0}.conf", p.Key));

                    using (var stream = assembly.GetManifestResourceStream(assemblyName + string.Format(".DX.{0}.conf", p.Key)))
                    {
                        objectR = Nett.Toml.ReadStream<DX.DynamicDMV>(stream);
                    }
                    //var objectR = Nett.Toml.ReadFile<DX.DynamicDMV>(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("DX\\{0}.conf", p.Key)));
                    
                    foreach (var el in tmp)
                    {
                        var copy = objectR.Clone();
                        copy.connectionParams = el;
                        _DXValues.Add(p.Key + (string.IsNullOrEmpty(el.databaseName) ? "" : ("_" + el.databaseName)), copy);
                    }
                }
                _DX = value;
            }
            get { return _DX; }
        }
        

        internal void TestDX(ref List<string> warnings, ref List<string> errors)
        {
            foreach (var DXValue in DXValues)
            {
                try
                {
                    using (var sqlConnection = new System.Data.SqlClient.SqlConnection(DXValue.Value.connectionString))
                    {
                        sqlConnection.Open();
                        foreach (var DXcommand in DXValue.Value.DX)
                        {
                            try
                            {
                                using (System.Data.SqlClient.SqlCommand cmd = sqlConnection.CreateCommand())
                                {
                                    cmd.CommandText = DXcommand.Value.sql;

                                    cmd.CommandType = System.Data.CommandType.Text;



                                    using (System.Data.SqlClient.SqlDataReader reader = cmd.ExecuteReader())
                                    {
                                        try
                                        {
                                            while (reader.Read()) ;
                                            //{
                                            //    Console.WriteLine("TestDX {0}.{1} on {2}", DXValue.Key, DXcommand.Key, reader.FieldCount);
                                            //}
                                        }
                                        catch (System.Data.SqlClient.SqlException ex)
                                        {
                                            errors.Add(string.Format("SqlException during {0}.{1} query test {2}", DXValue.Key, DXcommand.Key, ex.Message));
                                        }
                                        catch (Exception ex)
                                        {
                                            errors.Add(string.Format("Exception {3} during {0}.{1} query test {2}", DXValue.Key, DXcommand.Key, ex.Message, ex.GetType().Name));
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                errors.Add(string.Format("Exception {3} during {0}.{1} query test {2}", DXValue.Key, DXcommand.Key, ex.Message, ex.GetType().Name));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    errors.Add(string.Format("Exception {2} during {0} queries test {1}", DXValue.Key, ex.Message, ex.GetType().Name));
                }
            }
        }
    }
    
}