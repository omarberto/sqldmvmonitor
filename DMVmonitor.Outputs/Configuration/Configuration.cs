﻿using Nett;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Configuration
{
    public class Configuration
    {
        private static Configuration _config;
        public static Configuration config
        {
            get
            {
                return _config;
            }
        }
        public static bool Init(string configPath, ref List<string> warnings, ref List<string> errors)
        {

            try
            {
                _config = Toml.ReadFile<Configuration>(configPath);
                
                if (_config.output.hasOutput && _config.output.useTornado)
                {
                    if (string.IsNullOrEmpty(_config.output.tornado.address))
                        _config.output.tornado.address = _config.output.nats.address;
                    else
                        _config.output.tornado.openconnection = true;

                    //questa parte è sostanzialmente inutile....
                    if (string.IsNullOrEmpty(_config.output.tornado.tls_ca))
                        _config.output.tornado.tls_ca = _config.output.nats.tls_ca;
                    else
                        _config.output.tornado.openconnection = true;

                    //solo se address non è cambiato
                    if (!_config.output.tornado.openconnection)
                        //solo se secure non è impostato (possibili casini se nel tornado scrivo secure false e in nats secure true
                        //
                        if (!_config.output.tornado.secure)
                        {
                            _config.output.tornado.secure = _config.output.nats.secure;
                            //copio i settings da nats
                            if (string.IsNullOrEmpty(_config.output.tornado.tls_cert) && string.IsNullOrEmpty(_config.output.tornado.tls_key))
                            {
                                _config.output.tornado.tls_cert = _config.output.nats.tls_cert;
                                _config.output.tornado.tls_key = _config.output.nats.tls_key;
                            }
                            else if (string.IsNullOrEmpty(_config.output.tornado.tls_name))
                            {
                                _config.output.tornado.tls_name = _config.output.nats.tls_name;
                            }
                            else if (string.IsNullOrEmpty(_config.output.tornado.tls_pfx))
                            {
                                _config.output.tornado.tls_pfx = _config.output.nats.tls_pfx;
                            }
                        }
                }

                if (_config.output.hasOutput && _config.output.useNeteyeremotestorage)
                {
                    if (string.IsNullOrEmpty(_config.output.neteyeremotestorage.address))
                        _config.output.neteyeremotestorage.address = _config.output.nats.address;
                    else
                        _config.output.neteyeremotestorage.openconnection = true;

                    //questa parte è sostanzialmente inutile....
                    if (string.IsNullOrEmpty(_config.output.neteyeremotestorage.tls_ca))
                        _config.output.neteyeremotestorage.tls_ca = _config.output.nats.tls_ca;
                    else
                        _config.output.neteyeremotestorage.openconnection = true;

                    //solo se address non è cambiato
                    if (!_config.output.neteyeremotestorage.openconnection)
                        //solo se secure non è impostato (possibili casini se nel neteyeremotestorage scrivo secure false e in nats secure true
                        //
                        if (!_config.output.neteyeremotestorage.secure)
                        {
                            _config.output.neteyeremotestorage.secure = _config.output.nats.secure;
                            //copio i settings da nats
                            if (string.IsNullOrEmpty(_config.output.neteyeremotestorage.tls_cert) && string.IsNullOrEmpty(_config.output.neteyeremotestorage.tls_key))
                            {
                                _config.output.neteyeremotestorage.tls_cert = _config.output.nats.tls_cert;
                                _config.output.neteyeremotestorage.tls_key = _config.output.nats.tls_key;
                            }
                            else if (string.IsNullOrEmpty(_config.output.neteyeremotestorage.tls_name))
                            {
                                _config.output.neteyeremotestorage.tls_name = _config.output.nats.tls_name;
                            }
                            else if (string.IsNullOrEmpty(_config.output.neteyeremotestorage.tls_pfx))
                            {
                                _config.output.neteyeremotestorage.tls_pfx = _config.output.nats.tls_pfx;
                            }
                        }
                }

                //embededds
                //TODO: mini-test per load config da dll chiamante
                //cioè se io qui definisco una config la posso leggere
                //ma se chimo questa classe da altra dll funziona subito o mi serve qualche trucco???
                var assembly = Assembly.GetExecutingAssembly();
                var assemblyName = assembly.GetName().Name;
                
                if (_config.output.hasOutput)
                {
                    //facciamo load di valori preselvati, in ogni caso
                    _config.output.nats.LoadRetentionPolicies(assembly);
                }
            }
            catch (Exception e)
            {
                errors.Add(string.Format("Configuration --> Init {0} \n Exception Message {1}", configPath, e.Message));

                return false;
            }

            return true;
        }

        public Outputs output { get; set; } = new Outputs();
        
        public static bool Test(string configfile, out List<string> warnings, out List<string> errors, bool minimal = true)
        {

            warnings = new List<string>();
            errors = new List<string>();

            if (Init(configfile, ref warnings, ref errors))
            {
                if (config.output.hasOutput)
                {
                    //accettabile
                    config.output.nats.Test(ref warnings, ref errors);
                }
                else
                {
                    //non accettabile, andrebbe verificato tipo
                    errors.Add("output.nats configuration is missing");
                }
                if (config.output.useTornado)
                {
                    //accettabile
                    config.output.tornado.Test(ref warnings, ref errors);
                }
                else
                {
                    //non accettabile, andrebbe verificato tipo
                    warnings.Add("output.tornado configuration is missing");
                }


                if (config.output.useNeteyeremotestorage)
                {
                    //accettabile
                    config.output.neteyeremotestorage.Test(ref warnings, ref errors);
                }
                else
                {
                    //non accettabile, andrebbe verificato tipo
                    warnings.Add("output.neteyeremotestorage configuration is missing");
                }
                
            }

            return errors.Count == 0;
        }
    }
}
