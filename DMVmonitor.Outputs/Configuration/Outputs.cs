﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Configuration
{
    public class Outputs
    {
        [Nett.TomlIgnore]
        public bool hasOutput
        {
            get
            {
                //per ora ne ho uno e basta... (di base è privilegiato nats)
                return nats != null;
            }
        }
        public Nats nats { set; get; }

        [Nett.TomlIgnore]
        public bool useTornado
        {
            get
            {
                return tornado != null && tornado.active;
            }
        }
        public Tornado tornado { set; get; }

        [Nett.TomlIgnore]
        public bool useNeteyeremotestorage
        {
            get
            {
                return neteyeremotestorage != null && neteyeremotestorage.active;
            }
        }
        public NeteyeRemoteStorage neteyeremotestorage { set; get; }
    }
}
