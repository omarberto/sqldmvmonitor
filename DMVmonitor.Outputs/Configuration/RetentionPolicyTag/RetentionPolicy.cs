﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Configuration.RetentionPolicyTag
{
    public class RetentionPolicy
    {
        public string[] measurements { set; get; }

        [Nett.TomlIgnore]
        public bool hasMeasurements { get { return measurements != null && measurements.Length > 0; } }
    }
}
