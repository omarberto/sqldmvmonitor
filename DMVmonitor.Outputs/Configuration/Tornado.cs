﻿using DMVmonitor.Outputs.Nats;
using System.Collections.Generic;

namespace DMVmonitor.Outputs.Configuration
{
    public class Tornado : Nats
    {
        public bool openconnection { get; set; } = false;

        [Nett.TomlIgnore]
        public bool active { get { return !string.IsNullOrEmpty(topic); } }
        public string topic { get; set; }// = "tornado_events_itoa";
        public string hostnameformat { get; set; } = "fullqualified";//hostonly
        public int keepaliveInterval { get; set; } = 5;
        public int monitordataInterval { get; set; } = 30;
        public int seconds_delay_dmv_queries_avg_window { get; set; } = 16;
        public int seconds_delay_dmv_queries_wrn { get; set; } = 10;
        public int seconds_delay_dmv_queries_err { get; set; } = 30;

        //added later

        public override void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(topic.Trim()))
            {
                //eccezione
                errors.Add("main.output.tornado topic is empty or missing");
            }

            if (string.IsNullOrEmpty(address.Trim()))
            {
                errors.Add("main.output.tornado address is empty or missing");
            }
            else
            {
                //test connessione

                if (secure)
                {
                    bool test = true;
                    if (!System.IO.File.Exists(tls_cert))
                    {
                        //eccezione
                        errors.Add("main.output.tornado tls_cert file does not exist");
                        test = false;
                    }
                    if (!System.IO.File.Exists(tls_key))
                    {
                        //eccezione
                        errors.Add("main.output.tornado tls_key file does not exist");
                        test = false;
                    }
                    if (test)
                    {
                        using (var sender = new DataSendTest(address, certificate))
                        {
                            sender.Connect();
                            warnings.AddRange(sender.warnings);
                            errors.AddRange(sender.errors);
                        }
                    }
                }
                else
                {
                    using (var sender = new DataSendTest(address))
                    {
                        sender.Connect();
                        warnings.AddRange(sender.warnings);
                        errors.AddRange(sender.errors);
                    }
                }
            }
        }
    }
}