﻿using DMVmonitor.Outputs.Nats;
using System.Collections.Generic;

namespace DMVmonitor.Outputs.Configuration
{
    public class NeteyeRemoteStorage : Nats
    {
        public bool openconnection { get; set; } = false;
        
        [Nett.TomlIgnore]
        public bool active
        {
            get
            {
                return !string.IsNullOrEmpty(inChannel) && !string.IsNullOrEmpty(outChannel);
                //return !string.IsNullOrEmpty(group);
            }
        }
        //public string group { get; set; }
        public string outChannel { get; set; }
        public string inChannel { get; set; }

        [Nett.TomlIgnore]
        public string outChannelSubject
        {
            get
            {
                //if (outChannel.EndsWith(group))
                    return outChannel;
                //else
                //    return outChannel + "." + group;
            }
        }
        [Nett.TomlIgnore]
        public string inChannelSubject
        {
            get
            {
                //if (inChannel.EndsWith(group))
                    return inChannel;
                //else
                //    return inChannel + "." + group;
            }
        }
        
        public uint connectioncheckIntervalSeconds { get; set; } = 15;

        [Nett.TomlIgnore]
        public new string subject { get; set; } = "";//TODO test if this is a valid mask

        public override void Test(ref List<string> warnings, ref List<string> errors)
        {
            bool doTest = true;
            if (string.IsNullOrEmpty(address.Trim()))
            {
                errors.Add("main.output.neteyeremotestorage address is empty or missing");
                doTest = false;
            }
            if (string.IsNullOrEmpty(outChannel.Trim()))
            {
                errors.Add("main.output.neteyeremotestorage outChannel is empty or missing");
                doTest = false;
            }
            if (string.IsNullOrEmpty(inChannel.Trim()))
            {
                errors.Add("main.output.neteyeremotestorage inChannel is empty or missing");
                doTest = false;
            }
            else if (inChannel == outChannel)
            {
                errors.Add("main.output.neteyeremotestorage inChannel and outChannel are equal");
                doTest = false;
            }

            if (doTest)
            {
                //test connessione && invio

                if (secure)
                {
                    if (!System.IO.File.Exists(tls_cert))
                    {
                        //eccezione
                        errors.Add("main.output.neteyeremotestorage tls_cert file does not exist");
                        doTest = false;
                    }
                    if (!System.IO.File.Exists(tls_key))
                    {
                        //eccezione
                        errors.Add("main.output.neteyeremotestorage tls_key file does not exist");
                        doTest = false;
                    }
                    if (doTest)
                    {
                        using (var sender = new DataSendTest(address, certificate))
                        {
                            sender.Connect();
                            warnings.AddRange(sender.warnings);
                            errors.AddRange(sender.errors);
                        }
                    }
                }
                else
                {
                    using (var sender = new DataSendTest(address))
                    {
                        sender.Connect();
                        warnings.AddRange(sender.warnings);
                        errors.AddRange(sender.errors);
                    }
                }
            }
        }
    }
}