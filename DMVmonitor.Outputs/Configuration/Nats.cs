﻿using DMVmonitor.Outputs.Nats;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace DMVmonitor.Outputs.Configuration
{
    public class Nats
    {
        public string address { get; set; }
        public string subject { get; set; }


        public bool secure { get; set; } = false;
        public string tls_ca { get; set; } = "";
        private string _tls_cert = "";
        public string tls_cert { get { return _tls_cert; } set { _tls_cert = Path.IsPathRooted(value) ? value : Path.GetFullPath(value); } }
        private string _tls_key = "";
        public string tls_key { get { return _tls_key; } set { _tls_key = Path.IsPathRooted(value) ? value : Path.GetFullPath(value); } }
        public string tls_pfx { get; set; } = "";
        public string tls_name { get; set; } = "";

        public enum TLSmechType
        {
            none,
            type_files,//due files, linux-like
            type_pfx,//sempre file ma linux like
            type_store//
        }
        [Nett.TomlIgnore]
        public TLSmechType mechType
        {
            get
            {
                if (!secure) return TLSmechType.none;
                if (!string.IsNullOrEmpty(tls_name)) return TLSmechType.type_store;
                if (!string.IsNullOrEmpty(tls_pfx)) return TLSmechType.type_pfx;
                if (!string.IsNullOrEmpty(tls_key) && !string.IsNullOrEmpty(tls_cert)) return TLSmechType.type_files;
                return TLSmechType.none;
            }
        }


        [Nett.TomlIgnore]
        public X509Certificate2 certificate
        {
            get
            {
                switch (mechType)
                {
                    case TLSmechType.type_files:
                        {
                            string certificateText = File.ReadAllText(tls_cert);
                            string privateKeyText = File.ReadAllText(tls_key);
                            //var certificateOLD = new OpenSSL.X509Certificate2Provider.CertificateFromFileProvider(certificateText, privateKeyText, false).Certificate;
                            //Console.WriteLine("certificateOLD {0}", certificateOLD);
                            var certificate = new MyOpenSSL.CertificateFromFileProvider(certificateText, privateKeyText, false).Certificate;
                            return certificate;
                        }
                    case TLSmechType.type_pfx:
                        return new X509Certificate2(tls_pfx);
                    case TLSmechType.type_store:
                        {
                            X509Store store = new X509Store();
                            store.Open(OpenFlags.OpenExistingOnly);

                            foreach (X509Certificate2 cert in store.Certificates)
                            {
                                if (cert.GetNameInfo(X509NameType.SimpleName, false) == tls_name) return cert;
                            }
                            return new X509Certificate2();
                        }
                    default:
                        return new X509Certificate2();
                }
            }
        }

        public Nett.TomlTable retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public string default_retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public bool active_retention_policy_tag { set; get; }
        [Nett.TomlIgnore]
        public string retention_policy_tagname { set; get; }
        [Nett.TomlIgnore]
        public Dictionary<string, string> retention_policy_tags { set; get; } = new Dictionary<string, string>();
        public string getRetentionTag(string measurement)
        {
            if (active_retention_policy_tag)
            {
                if (retention_policy_tags.ContainsKey(measurement))
                {
                    //Console.WriteLine("{2} default_retention_policy_tag {0} {1}", measurement, default_retention_policy_tag, string.IsNullOrEmpty(default_retention_policy_tag));

                    return "," + retention_policy_tagname + "=" + retention_policy_tags[measurement];
                }
                //Console.WriteLine("{2} default_retention_policy_tag {0} {1}", measurement, default_retention_policy_tag, string.IsNullOrEmpty(default_retention_policy_tag));
                //Console.ReadKey(true);
                return "," + retention_policy_tagname + "=" + default_retention_policy_tag;
            }
            return string.Empty;
        }
        internal void LoadRetentionPolicies(System.Reflection.Assembly assembly)
        {
            using (var stream = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Configuration.RetentionPolicyTag.retention_policies_tag.conf"))
            {
                var internalS = Nett.Toml.ReadStream(stream);
                var retention_policy_tag_settings = (Nett.TomlTable)internalS.Values.First();
                foreach (var retention_policy_tagEl in retention_policy_tag_settings)
                {
                    if (retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Table)
                    {
                        var retentionPolicy = retention_policy_tagEl.Value.Get<RetentionPolicyTag.RetentionPolicy>();
                        if (retentionPolicy.hasMeasurements)
                        {
                            foreach (var measurement in retentionPolicy.measurements)
                            {
                                if (retention_policy_tags.ContainsKey(measurement))
                                    retention_policy_tags[measurement] = retention_policy_tagEl.Key;
                                else
                                    retention_policy_tags.Add(measurement, retention_policy_tagEl.Key);
                            }
                        }
                    }
                    else if (retention_policy_tagEl.Key == "active" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Bool)
                        active_retention_policy_tag = retention_policy_tagEl.Value.Get<bool>();
                    else if (retention_policy_tagEl.Key == "default" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        default_retention_policy_tag = retention_policy_tagEl.Value.Get<string>();
                    else if (retention_policy_tagEl.Key == "tag_name" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        retention_policy_tagname = retention_policy_tagEl.Value.Get<string>();
                }
            }
            //leggo da sqltrace per vedere se devo sovrascrivere
            if (retention_policy_tag != null)
            {
                foreach (var retention_policy_tagEl in retention_policy_tag)
                {
                    if (retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Table)
                    {
                        //Console.WriteLine("B RETENTION retention_policy_tagname {0}: {1}", retention_policy_tagEl.Key, retention_policy_tagEl.Value.TomlType);
                        var retentionPolicy = retention_policy_tagEl.Value.Get<RetentionPolicyTag.RetentionPolicy>();
                        if (retentionPolicy.hasMeasurements)
                        {
                            foreach (var measurement in retentionPolicy.measurements)
                            {
                                if (retention_policy_tags.ContainsKey(measurement))
                                    retention_policy_tags[measurement] = retention_policy_tagEl.Key;
                                else
                                    retention_policy_tags.Add(measurement, retention_policy_tagEl.Key);
                            }
                        }
                    }
                    else if (retention_policy_tagEl.Key == "active" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.Bool)
                        active_retention_policy_tag = retention_policy_tagEl.Value.Get<bool>();
                    else if (retention_policy_tagEl.Key == "default" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        default_retention_policy_tag = retention_policy_tagEl.Value.Get<string>();
                    else if (retention_policy_tagEl.Key == "tag_name" && retention_policy_tagEl.Value.TomlType == Nett.TomlObjectType.String)
                        retention_policy_tagname = retention_policy_tagEl.Value.Get<string>();
                }
            }
            if (string.IsNullOrEmpty(retention_policy_tagname)) active_retention_policy_tag = false;
            if (string.IsNullOrEmpty(default_retention_policy_tag)) active_retention_policy_tag = false;
        }

        public virtual void Test(ref List<string> warnings, ref List<string> errors)
        {
            if (string.IsNullOrEmpty(subject.Trim()))
            {
                //eccezione
                errors.Add("main.output.nats subject is empty or missing");
            }

            if (string.IsNullOrEmpty(address.Trim()))
            {
                errors.Add("main.output.nats address is empty or missing");
            }
            else
            {
                //test connessione

                if (secure)
                {
                    bool test = true;
                    if (!System.IO.File.Exists(tls_cert))
                    {
                        //eccezione
                        errors.Add("main.output.nats tls_cert file does not exist");
                        test = false;
                    }
                    if (!System.IO.File.Exists(tls_key))
                    {
                        //eccezione
                        errors.Add("main.output.nats tls_key file does not exist");
                        test = false;
                    }
                    if (test)
                    {
                        using (var sender = new DataSendTest(address, certificate))
                        {
                            sender.Connect();
                            warnings.AddRange(sender.warnings);
                            errors.AddRange(sender.errors);
                        }
                    }
                }
                else
                {
                    using (var sender = new DataSendTest(address))
                    {
                        sender.Connect();
                        warnings.AddRange(sender.warnings);
                        errors.AddRange(sender.errors);
                    }
                }
            }
        }
    }

}