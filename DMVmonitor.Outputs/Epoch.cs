﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs
{
    public class EpochNanoseconds
    {
        private DateTimeKind Kind;

        public EpochNanoseconds(DateTime dateTime)
        {
            TimeSpan t = dateTime - new DateTime(1970, 1, 1, 0, 0, 0, dateTime.Kind);
            Value = (long)(t.TotalMilliseconds * 1000000);
            this.Kind = dateTime.Kind;
        }
        public EpochNanoseconds(long epoch, DateTimeKind Kind)
        {
            Value = epoch;
            this.Kind = Kind;
        }

        public long Value
        {
            get;
            private set;
        }
        public DateTime dateTime
        {
            get
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, this.Kind).AddMilliseconds(Value/1000000D);
            }
        }

        public static EpochNanoseconds Now
        {
            get
            {
                return new EpochNanoseconds(DateTime.Now);
            }
        }
        public static EpochNanoseconds UtcNow
        {
            get
            {
                return new EpochNanoseconds(DateTime.UtcNow);
            }
        }
    }
}
