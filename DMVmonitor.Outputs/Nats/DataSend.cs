﻿using NATS.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Nats
{
    public class DataSend : DataSendBase
    {
        public string subject;
        public string server
        {
            get
            {
                return nats_connection.Servers[0];
            }
        }


        public DataSend(string address, string subject, X509Certificate2 certificate = null) : base(address, certificate)
        {
            this.subject = subject;

            Connect();
        }

        public override void ErrorMessage(bool error, string format, params object[] args)
        {
            //Console.WriteLine("ErrorMessage override: " + string.Format(format, args));

            if (error)
                Logging.Log.Instance.Error("NATS.io AsyncErrorEventHandler: {0}\n Message: {1}\n Subject: {2}", args);
            else
                Logging.Log.Instance.Warn("NATS.io AsyncErrorEventHandler: {0}\n Message: {1}\n Subject: {2}", args);
        }
        /****************  PARTE MESSAGGISTICA  **********/

        public void Publish(byte[] message)
        {
            if (nats_connection.State == ConnState.CLOSED)
                Connect();

            //Console.WriteLine("Publish on {0}: {1}", subject, nats_connection.Servers[0]);
            if (nats_connection.State != ConnState.CLOSED)
                nats_connection.Publish(this.subject, message);
        }

        public void Publish(string subject, byte[] message)
        {
            if (nats_connection.State == ConnState.CLOSED)
            {
                Connect();
            }
            if (nats_connection.State != ConnState.CLOSED)
                nats_connection.Publish(subject, message);
        }

        public void PublishMessage(string message, bool duplicateMessage = false)
        {
            //Console.WriteLine("PublishMessage ({1}) : {0}", message, duplicateMessage);
            Publish(Encoding.UTF8.GetBytes(message));
            //TODO Tornado
            //if (!duplicateMessage)
            //    DMVmonitor.Tornado.TornadoEnpointData.values.number_of_measurments_sent_to_nats++;
        }
        public void PublishMessage(string subject, string message)
        {
            Publish(subject, Encoding.UTF8.GetBytes(message));
        }

        public static string EscapeTag(string value)
        {
            string retvalue = "";
            foreach (char c in value)
            {
                switch (c)
                {
                    case ',':
                    case '=':
                    case ' ':
                        retvalue += '\\';
                        break;
                }
                retvalue += c;
            }
            return retvalue;
        }
    }
}
