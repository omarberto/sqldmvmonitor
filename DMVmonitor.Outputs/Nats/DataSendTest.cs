﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Nats
{
    internal class DataSendTest : DataSendBase
    {

        public List<string> warnings = new List<string>();
        public List<string> errors = new List<string>();

        public DataSendTest(string address, X509Certificate2 certificate = null) : base(address, certificate)
        {

        }

        public override void ErrorMessage(bool error, string format, params object[] args)
        {
            if (error)
                errors.Add(string.Format(format, args));
            else
                warnings.Add(string.Format(format, args));
        }
    }
}
