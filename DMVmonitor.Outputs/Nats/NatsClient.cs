﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Outputs.Nats
{
    public sealed class Client : IDisposable
    {
        private static readonly Lazy<Client> lazy = new Lazy<Client>(() => new Client());

        public static Client Instance
        {
            get
            {
                return lazy.Value;
            }
        }

        Dictionary<string, DataSend> _senders = new Dictionary<string, DataSend>();
        private Client()
        {
            //TODO aggiungere un publisher per DATI
            if (Configuration.Configuration.config.output.hasOutput)
            {
                DataSend sender = Configuration.Configuration.config.output.nats.secure ?
                    new DataSend(Configuration.Configuration.config.output.nats.address, Configuration.Configuration.config.output.nats.subject, Configuration.Configuration.config.output.nats.certificate)
                    :
                    new DataSend(Configuration.Configuration.config.output.nats.address, Configuration.Configuration.config.output.nats.subject);

                _senders.Add("DATA",
                    sender
                );
                if (Configuration.Configuration.config.output.useTornado && !Configuration.Configuration.config.output.tornado.openconnection)
                {
                    _senders.Add("TORNADO",
                        sender
                    );
                }
                if (Configuration.Configuration.config.output.useNeteyeremotestorage && !Configuration.Configuration.config.output.neteyeremotestorage.openconnection)
                {
                    _senders.Add("NETEYEREMOTESTORAGE",
                        sender
                    );
                }
            }

            if (Configuration.Configuration.config.output.useTornado && Configuration.Configuration.config.output.tornado.openconnection)
            {
                DataSend sender = Configuration.Configuration.config.output.tornado.secure ?
                    new DataSend(Configuration.Configuration.config.output.tornado.address, Configuration.Configuration.config.output.tornado.topic, Configuration.Configuration.config.output.tornado.certificate)
                    :
                    new DataSend(Configuration.Configuration.config.output.tornado.address, Configuration.Configuration.config.output.tornado.topic);
                _senders.Add("TORNADO",
                    sender
                );
            }
            if (Configuration.Configuration.config.output.useNeteyeremotestorage && Configuration.Configuration.config.output.neteyeremotestorage.openconnection)
            {
                DataSend sender = Configuration.Configuration.config.output.neteyeremotestorage.secure ?
                    new DataSend(Configuration.Configuration.config.output.neteyeremotestorage.address, 
                    Configuration.Configuration.config.output.neteyeremotestorage.outChannelSubject, 
                    Configuration.Configuration.config.output.neteyeremotestorage.certificate)
                    :
                    new DataSend(Configuration.Configuration.config.output.neteyeremotestorage.address, Configuration.Configuration.config.output.neteyeremotestorage.outChannelSubject);
                _senders.Add("NETEYEREMOTESTORAGE",
                    sender
                );
            }
        }

        public DataSend this[string name]
        {
            get
            {
                //Console.WriteLine("porca merda {0}", _senders == null);
                return _senders[name];
            }
        }

        public void Dispose()
        {
            foreach (var sender in _senders.Keys)
                _senders[sender].Dispose();
        }

        public bool hasSender(string name)
        {
            return _senders.ContainsKey(name);
        }
    }
}
