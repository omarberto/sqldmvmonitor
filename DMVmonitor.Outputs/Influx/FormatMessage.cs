﻿using DMVmonitor.Outputs.Configuration;
using System;

namespace Influx
{
    public class FormatMessage
    {
        private string strMessage = string.Empty;

        public FormatMessage(string measurement)
        {
            strMessage = measurement + Configuration.config.output.nats.getRetentionTag(measurement);
            //Console.WriteLine("strMessage {0}", strMessage);
        }

        public void AddTag(string name, string value)
        {
            //TODO: add null value check
            //TODO: add default formatting methods
            strMessage += "," + name + "=" + value;
        }

        private string strFields = "";
        public void AddStringField(string name, string value)
        {
            if (value != null)
                strFields += "," + name + "=\"" + value + "\"";
        }
        public void AddStringField(string name, object value, string format)
        {
            if (value != null)
                strFields += "," + name + "=\"" + string.Format(format, value) + "\"";
        }
        public void AddBooleanField(string name, bool value)
        {
            strFields += "," + name + "=" + (value ? "t" : "f");
        }
        public void AddIntegerField(string name, ulong? value)
        {
            if (value.HasValue)
                strFields += "," + name + "=" + value.Value.ToString() + "i";
        }
        public void AddIntegerField(string name, int? value)
        {
            if (value.HasValue)
                strFields += "," + name + "=" + value.Value.ToString() + "i";
        }

        private string strEpoch;
        public void AddEpochNs(long nsTimeStampEpoch)
        {
            strEpoch = " " + nsTimeStampEpoch.ToString();
        }
        public void AddTimestamp(DateTime timeStampUtc)
        {
            AddEpochNs(new DMVmonitor.Outputs.EpochNanoseconds(timeStampUtc).Value);
        }

        public string value
        {
            get
            {
                return strMessage + " " + strFields.TrimStart(',') + strEpoch;
            }
        }

    }
}