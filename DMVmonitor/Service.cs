﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    internal class Service
    {
        private string ConfigPath;
        private bool _use_tornado;
        private string _ServiceName;

        private Outputs.Configuration.Nats nats;

        internal Service()
        {
            this._use_tornado = false;
        }

        internal Service(string ServiceName)
        {
            this._use_tornado = true;
            this._ServiceName = ServiceName;
        }
        
        private static bool TestConfig(string ConfigPath)
        {
            bool result;

            List<string> warnings;
            List<string> errors;
            try
            {
                result = Configuration.Configuration.Test(ConfigPath, out warnings, out errors, false);
            }
            catch
            (Exception e)
            {
                warnings = new List<string>();
                errors = new List<string>();
                errors.Add(string.Format("TestConfig {0} \n Exception Message {0}", e.Message));

                result = false;
            }
            foreach (var w in warnings)
                Logging.Log.Instance.Warn(w);
            foreach (var e in errors)
                Logging.Log.Instance.Error(e);
            return result;
        }

        private System.Threading.Thread thTornado = null;
        private System.Threading.Thread thDMV_lt = null;
        private System.Threading.Thread thDuration = null;
        private System.Threading.Thread thCursors = null;
        private System.Threading.Thread thDeadlocks = null;

        internal void Start(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }
            
            this.ConfigPath = ConfigPath;
            
            this.nats = Outputs.Configuration.Configuration.config.output.nats;
            //fa partire il thread DOWork
            //this.hostControl = hostControl;
            
            if (SQLdetailsStorage.Config.Configuration.config.hasStorage)
            {
                if(Outputs.Configuration.Configuration.config.output.useNeteyeremotestorage)
                    SQLdetailsStorage.Storage.Instance.Start(Configuration.Configuration.config.main.dataSource);
                else
                    SQLdetailsStorage.Storage.Instance.Start();
                //remote part is missing here.. pay attention in merging
            }

            //rimuovi il false poi
            if (Configuration.Configuration.config.hasMain && Configuration.Configuration.config.main.isReader)
            {
                if (this._use_tornado && Outputs.Configuration.Configuration.config.output.useTornado)
                {
                    Tornado.TornadoEndpoint endpoint = new Tornado.TornadoEndpoint(this._ServiceName);

                    var thSt = new System.Threading.ThreadStart(endpoint.DoWork);
                    thTornado = new System.Threading.Thread(thSt);
                    thTornado.Start();
                }

                if (Configuration.Configuration.config.main.hasQueriesProbe)
                {
                    Logging.Log.Instance.Trace("queriesProbe->OnStart");

                    var session = new SQLsessionQueries(Configuration.Configuration.config.main.connectionString, Configuration.Configuration.config.main.queriesProbe);

                    var thSt = new System.Threading.ThreadStart(session.DoWork);
                    this.thDuration = new System.Threading.Thread(thSt);
                    this.thDuration.Start();
                }
                else
                    Logging.Log.Instance.Trace("queriesProbe not started");

                if (Configuration.Configuration.config.main.hasFetchesProbe)
                {
                    Logging.Log.Instance.Trace("fetchesProbe->OnStart");

                    var session = new SQLsessionFetches(Configuration.Configuration.config.main.connectionString, Configuration.Configuration.config.main.fetchesProbe, Configuration.Configuration.config.main.fetchesFilter);

                    var thSt = new System.Threading.ThreadStart(session.DoWork);
                    this.thCursors = new System.Threading.Thread(thSt);
                    this.thCursors.Start();
                }
                else
                    Logging.Log.Instance.Trace("fetchesProbe not started");

                if (Configuration.Configuration.config.main.hasBlockingsProbe)
                {
                    Logging.Log.Instance.Trace("blockingsProbe->OnStart");

                    var session = new SQLsessionBlockings(Configuration.Configuration.config.main.connectionString, Configuration.Configuration.config.main.blockingsProbe);

                    var thSt = new System.Threading.ThreadStart(session.DoWork);
                    this.thDeadlocks = new System.Threading.Thread(thSt);
                    this.thDeadlocks.Start();
                }
                else
                    Logging.Log.Instance.Trace("blockingsProbe not started");
                
                if (Configuration.Configuration.config.hasDmv_LT)
                {
                    var dmv_lt = new DMVmonitorEXE_TL();

                    Logging.Log.Instance.Trace("thDMV_lt starting");
                    var thSt = new System.Threading.ThreadStart(dmv_lt.DoWork);
                    this.thDMV_lt = new System.Threading.Thread(thSt);
                    this.thDMV_lt.Start();
                }
                else
                    Logging.Log.Instance.Trace("thDMV_lt not started");
            }
        }
        internal bool StartTornado(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }

            this.ConfigPath = ConfigPath;

            this.nats = Outputs.Configuration.Configuration.config.output.nats;
            //fa partire il thread DOWork
            //this.hostControl = hostControl;


            if (Configuration.Configuration.config.hasMain && Configuration.Configuration.config.main.isReader)
            {
                if (this._use_tornado && Outputs.Configuration.Configuration.config.output.useTornado)
                {
                    Tornado.TornadoEndpoint endpoint = new Tornado.TornadoEndpoint(this._ServiceName);
                    endpoint.Test();
                    return true;
                }
                else
                {
                    Console.WriteLine("Configure tornado before testing it!");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("Set the correct configuration before testing!");
                return false;
            }
        }
        internal bool StartRemoteStorage(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }

            this.ConfigPath = ConfigPath;

            this.nats = Outputs.Configuration.Configuration.config.output.nats;
            //fa partire il thread DOWork
            //this.hostControl = hostControl;

            if (SQLdetailsStorage.Config.Configuration.config.hasStorage && Outputs.Configuration.Configuration.config.output.useNeteyeremotestorage)
            {
                return SQLdetailsStorage.Storage.Instance.Start(Configuration.Configuration.config.main.dataSource, true);
            }
            return false;
        }

        internal void StartUploader(string ConfigPath)
        {
            if (!TestConfig(ConfigPath))
            {
                throw new Exception("Some errors have occurred!");
            }

            this.ConfigPath = ConfigPath;

            this.nats = Outputs.Configuration.Configuration.config.output.nats;
            
            if (Configuration.Configuration.config.hasMain)
            {

                if (Configuration.Configuration.config.main.hasQueriesProbe)
                {
                    Logging.Log.Instance.Trace("queriesProbe->OnStart");

                    var session = new SQLsessionQueries(Configuration.Configuration.config.main.queriesProbe);

                    var thSt = new System.Threading.ThreadStart(session.DoWork);
                    this.thDuration = new System.Threading.Thread(thSt);
                    this.thDuration.Start();
                }
                else
                    Logging.Log.Instance.Trace("queriesProbe not started");
            }
        }

        internal void Stop()
        {
            if (thTornado != null)
            {
                Tornado.TornadoEndpoint.active = false;
                thTornado.Join();
                Logging.Log.Instance.Debug("thTornado END");
            }
            if (thDMV_lt != null)
            {
                DMVmonitorEXE_TL.active = false;
                thDMV_lt.Join();
                Logging.Log.Instance.Debug("thDMV_lt END");
            }
            if (thDuration != null)
            {
                thDuration.Abort();
                try
                {
                    thDuration.Join();
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Debug(e, "thDuration Join Exception");
                }
                Logging.Log.Instance.Debug("thDuration END");
            }
            if (thCursors != null)
            {
                thCursors.Abort();
                try
                {
                    thCursors.Join();
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Debug(e, "thCursors Join Exception");
                }
                Logging.Log.Instance.Debug("thCursors END");
            }
            if (thDeadlocks != null)
            {
                thDeadlocks.Abort();
                try
                {
                    thDeadlocks.Join();
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Debug(e, "thDeadlocks Join Exception");
                }
                Logging.Log.Instance.Debug("thDeadlocks END");
            }
        }
        internal void Join()
        {
            if (this._use_tornado && thTornado != null)
            {
                thTornado.Join();
                Logging.Log.Instance.Debug("thTornado END");
            }
            if (thDMV_lt != null)
            {
                thDMV_lt.Join();
                Logging.Log.Instance.Trace("thDMV_lt END");
            }
            if (thDuration != null)
            {
                thDuration.Join();
                Logging.Log.Instance.Trace("thDuration END");
            }
            if (thCursors != null)
            {
                thCursors.Join();
                Logging.Log.Instance.Trace("thCursors END");
            }
            if (thDeadlocks != null)
            {
                thDeadlocks.Join();
                Logging.Log.Instance.Trace("thDeadlocks END");
            }
        }
    }
}
