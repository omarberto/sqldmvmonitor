﻿//using DMVmonitor;
using Microsoft.SqlServer.XEvent.Linq;
using System;
//using System.Linq;
using System.Text;

namespace DMVmonitor.Transactions.Events
{
    /*
    commit_tran_starting
    Occurs when a COMMIT TRANSACTION request that was sent from a client application through the transaction management interface has started.
    */
    class CommitTranStarting
    {
        public CommitTranStarting(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;
            
            this.new_transaction_started = Convert.ToBoolean(evt.Fields["new_transaction_started"].Value);
            this.statement = Convert.ToString(evt.Fields["statement"].Value);

            this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
            this.transaction_id = Convert.ToInt64(evt.Actions["transaction_id"].Value);
            this.transaction_sequence = Convert.ToInt64(evt.Actions["transaction_sequence"].Value);
            this.last_error = Convert.ToInt32(evt.Actions["last_error"].Value);
            this.is_system = Convert.ToBoolean(evt.Actions["is_system"].Value);
        }
        public DateTimeOffset time_stamp { get; private set; }

        //fields
        public bool new_transaction_started { get; private set; }
        public string statement { get; private set; }

        //actions
        public int session_id { get; private set; }
        public long transaction_id { get; private set; }
        public long transaction_sequence { get; private set; }
        public int last_error { get; private set; }
        public bool is_system { get; private set; }
    }
}
