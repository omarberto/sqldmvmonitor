﻿//using DMVmonitor;
using Microsoft.SqlServer.XEvent.Linq;
using System;
//using System.Linq;
using System.Text;

namespace DMVmonitor.Transactions.Events
{
    /*
    promote_tran_starting

Occurs when a PROMOTE TRANSACTION request that was sent from a client application through the transaction management interface has started.
    */
    class PromoteTranStarting
    {
        public PromoteTranStarting(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            this.server_instance_name = Convert.ToString(evt.Actions["server_instance_name"].Value);
            this.database_id = Convert.ToInt32(evt.Actions["database_id"].Value);
            this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
            this.transaction_id = Convert.ToInt64(evt.Actions["transaction_id"].Value);
            this.transaction_sequence = Convert.ToInt64(evt.Actions["transaction_sequence"].Value);
            this.last_error = Convert.ToInt32(evt.Actions["last_error"].Value);
            this.is_system = Convert.ToBoolean(evt.Actions["is_system"].Value);
            this.client_hostname = Convert.ToString(evt.Actions["client_hostname"].Value);
            var context_info = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(evt.Actions["context_info"].Value as byte[]));//OK
            this.AxSession = context_info.AxSessionId;
            this.AxUser = context_info.AxUserName;
            this.plan_handle = evt.Actions["plan_handle"].Value;
            this.query_hash = Convert.ToUInt64(evt.Actions["query_hash"].Value);
            this.query_plan_hash = Convert.ToUInt64(evt.Actions["query_plan_hash"].Value);
            this.sql_text = Convert.ToString(evt.Actions["sql_text"].Value);
            this.tsql_frame = Convert.ToString(evt.Actions["tsql_frame"].Value);
            this.tsql_stack = Convert.ToString(evt.Actions["tsql_stack"].Value);
        }
        public DateTimeOffset time_stamp { get; private set; }
        public long nsTimeStampEpoch
        {
            get
            {
                return new EpochNanoseconds(time_stamp.UtcDateTime).Value;
            }
        }

        //actions
        public string server_instance_name { get; private set; }
        public int database_id { get; private set; }
        public int session_id { get; private set; }
        public long transaction_id { get; private set; }
        public long transaction_sequence { get; private set; }
        public int last_error { get; private set; }
        public bool is_system { get; private set; }
        public string client_hostname { get; private set; }
        public int AxSession { get; private set; }
        public string AxUser { get; private set; }
        public object plan_handle { get; private set; }
        public ulong query_hash { get; private set; }
        public ulong query_plan_hash { get; private set; }
        public string sql_text { get; private set; }
        public string tsql_frame { get; private set; }
        public string tsql_stack { get; private set; }
    }
}
