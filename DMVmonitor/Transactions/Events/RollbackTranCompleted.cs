﻿//using DMVmonitor;
using Microsoft.SqlServer.XEvent.Linq;
using System;
//using System.Linq;
using System.Text;

namespace DMVmonitor.Transactions.Events
{
    /*
    rollback_tran_completed

Occurs when a ROLLBACK TRANSACTION request that was sent from a client application through the transaction management interface has completed.
    */
    class RollbackTranCompleted
    {
        public RollbackTranCompleted(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            this.error_number = Convert.ToInt32(evt.Fields["error_number"].Value);
            this.success = Convert.ToBoolean(evt.Fields["success"].Value);
            this.new_transaction_started = Convert.ToBoolean(evt.Fields["new_transaction_started"].Value);
            
            this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
            this.last_error = Convert.ToInt32(evt.Actions["last_error"].Value);
            this.is_system = Convert.ToBoolean(evt.Actions["is_system"].Value);
        }
        public DateTimeOffset time_stamp { get; private set; }

        //fields
        public int error_number { get; private set; }
        public bool success { get; private set; }
        public bool new_transaction_started { get; private set; }

        //actions
        public int session_id { get; private set; }
        public int last_error { get; private set; }
        public bool is_system { get; private set; }
    }
}
