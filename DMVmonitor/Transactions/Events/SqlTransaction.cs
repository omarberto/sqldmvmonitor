﻿//using DMVmonitor;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System;
//using System.Linq;
using System.Text;

namespace DMVmonitor.Transactions.Events
{
    /*
    promote_tran_completed

Occurs when a PROMOTE TRANSACTION request that was sent from a client application through the transaction management interface has completed.
    */
    class SqlTransaction
    {
        public SqlTransaction(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            this.duration = Convert.ToUInt64(evt.Fields["duration"].Value);
            this.object_name = Convert.ToString(evt.Fields["object_name"].Value);
            this.savepoint_name = Convert.ToString(evt.Fields["savepoint_name"].Value);
            this.transaction_id = Convert.ToInt64(evt.Fields["transaction_id"].Value);
            this.transaction_state = ((MapValue)evt.Fields["transaction_state"].Value).Value;
            this.transaction_type = ((MapValue)evt.Fields["transaction_type"].Value).Value;

            this.server_instance_name = Convert.ToString(evt.Actions["server_instance_name"].Value);
            this.database_id = Convert.ToInt32(evt.Actions["database_id"].Value);
            this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);;
            this.transaction_sequence = Convert.ToInt64(evt.Actions["transaction_sequence"].Value);
            this.last_error = Convert.ToInt32(evt.Actions["last_error"].Value);
            this.is_system = Convert.ToBoolean(evt.Actions["is_system"].Value);
            this.client_hostname = Convert.ToString(evt.Actions["client_hostname"].Value);
            var context_info = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(evt.Actions["context_info"].Value as byte[]));//OK
            this.AxSession = context_info.AxSessionId;
            this.AxUser = context_info.AxUserName;
        }
        public DateTimeOffset time_stamp { get; private set; }

        //fields
        public ulong duration { get; private set; }
        public string object_name { get; private set; }
        public string savepoint_name { get; private set; }
        public long transaction_id { get; private set; }
        public string transaction_state { get; private set; }
        public string transaction_type { get; private set; }

        //actions
        public string server_instance_name { get; private set; }
        public int database_id { get; private set; }
        public int session_id { get; private set; }
        public long transaction_sequence { get; private set; }
        public int last_error { get; private set; }
        public bool is_system { get; private set; }
        public string client_hostname { get; private set; }
        public int AxSession { get; private set; }
        public string AxUser { get; private set; }
    }
}
