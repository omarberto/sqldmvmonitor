﻿//using DMVmonitor;
using Microsoft.SqlServer.XEvent.Linq;
using System;
//using System.Linq;
using System.Text;

namespace DMVmonitor.Transactions.Events
{
    /*
    begin_tran_starting
    Occurs when a BEGIN TRANSACTION request that was sent from a client application through the transaction management interface has started.
    */
    class BeginTranStarting
    {
        public BeginTranStarting(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;
            
            this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
            this.last_error = Convert.ToInt32(evt.Actions["last_error"].Value);
            this.is_system = Convert.ToBoolean(evt.Actions["is_system"].Value);
        }
        public DateTimeOffset time_stamp { get; private set; }

        //actions
        public int session_id { get; private set; }
        public int last_error { get; private set; }
        public bool is_system { get; private set; }
    }
}
