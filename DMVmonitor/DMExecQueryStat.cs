﻿using System;
using System.Data.SQLite;

namespace DMVmonitor
{
    internal class DMExecQueryStat
    {
        public DMExecQueryStat(SQLiteDataReader datareader)
        {
            this.last_execution_time = new EpochNanoseconds(datareader.GetInt64(0), DateTimeKind.Local);//questo va messo a posto
            this.polling_time = new EpochNanoseconds(datareader.GetInt64(1), DateTimeKind.Local);//questo va messo a posto
            this.polling_interval = datareader.GetInt64(2);
            this.execution_count = datareader.GetInt64(3);
            this.total_cpu = datareader.GetInt64(4);
            this.total_duration = datareader.GetInt64(5);
            this.total_physical_reads = datareader.GetInt64(6);
            this.total_logical_reads = datareader.GetInt64(7);
            this.total_writes = datareader.GetInt64(8);
            this.total_clr_time = datareader.GetInt64(9);
        }

        public DMExecQueryStat()
        {
        }

        public static DMExecQueryStat operator -(DMExecQueryStat step, DMExecQueryStat prev)
        {
            if (prev != null)
            {
                return new DMExecQueryStat()
                {
                    last_execution_time = step.last_execution_time,
                    polling_time = step.polling_time,
                    polling_interval = step.polling_interval,

                    execution_count = step.execution_count - prev.execution_count,
                    total_cpu = step.total_cpu - prev.total_cpu,
                    total_duration = step.total_duration - prev.total_duration,
                    total_physical_reads = step.total_physical_reads - prev.total_physical_reads,
                    total_logical_reads = step.total_logical_reads - prev.total_logical_reads,
                    total_writes = step.total_writes - prev.total_writes,
                    total_clr_time = step.total_clr_time - prev.total_clr_time
                };
            }
            return new DMExecQueryStat()
            {
                last_execution_time = step.last_execution_time,
                polling_time = step.polling_time,
                polling_interval = step.polling_interval,

                execution_count = step.execution_count,
                total_cpu = step.total_cpu,
                total_duration = step.total_duration,
                total_physical_reads = step.total_physical_reads,
                total_logical_reads = step.total_logical_reads,
                total_writes = step.total_writes,
                total_clr_time = step.total_clr_time
            };
        }

        public long execution_count { get; private set; }
        public EpochNanoseconds last_execution_time { get; private set; }
        public long polling_interval { get; private set; }
        public bool isValidStep { get { return polling_interval > 0; } }
        public EpochNanoseconds polling_time { get; private set; }
        public long total_clr_time { get; private set; }
        public long total_cpu { get; private set; }
        public long total_duration { get; private set; }
        public long total_logical_reads { get; private set; }
        public long total_physical_reads { get; private set; }
        public long total_writes { get; private set; }
    }
}