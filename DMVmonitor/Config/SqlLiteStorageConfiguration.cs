﻿using System;
using System.IO;

namespace DMVmonitor.Config
{
    public class SqlLiteStorageConfiguration
    {
        public string filepathFormat { set; get; }
        //public string dbFilePath
        //{
        //    get
        //    {
        //        long maxEpoch = long.MinValue;
        //        foreach (var finfo in new DirectoryInfo(Path.GetDirectoryName(filepathFormat)).GetFiles(Path.GetFileName(filepathFormat).Replace("{0}", "*")))
        //        {
        //            var FileToken = Path.GetFileName(filepathFormat).Split(new string[]{ "{0}"}, StringSplitOptions.RemoveEmptyEntries);
        //            var epoch = long.Parse(finfo.Name.Split(FileToken, StringSplitOptions.RemoveEmptyEntries)[0]);
        //            if (epoch > maxEpoch) maxEpoch = epoch;
        //        }
        //        if (maxEpoch > 0)
        //        {
        //            return string.Format(filepathFormat, (ulong)maxEpoch);
        //        }
        //        return string.Format(filepathFormat, (ulong)EpochNanoseconds.UtcNow.Value);
        //    }
        //}
        public string newFilePath(long epoch)
        {
            return string.Format(filepathFormat, (ulong)epoch);
        }

        public long maxLength { set; get; }

        public int maxInterval { set; get; }
        public int maxNFiles { set; get; }

        public int maxRows { set; get; }
    }
}