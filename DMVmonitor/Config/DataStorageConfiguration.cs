﻿namespace DMVmonitor.Config
{
    public class DataStorageConfiguration
    {
        public SqlLiteStorageConfiguration sqlLite { get; set; }
        public bool sqliteActive
        {
            get
            {
                return sqlLite != null;
            }
        }
        public bool skipSave { get; set; }
    }
}