﻿namespace SQLsessions.Config
{
    public class CursorsProbe
    {
        public string dataSource { get; set; }
        public string sourceName { get; set; }

        public ulong? minduration { get; set; }
        [Nett.TomlIgnore]
        public bool saveCursors { get { return minduration.HasValue; } }

        public ulong? fetchesFilter { get; set; }
        [Nett.TomlIgnore]
        public bool getfetches { get { return fetchesFilter.HasValue; } }
    }
}