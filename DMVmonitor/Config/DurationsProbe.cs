﻿namespace SQLsessions.Config
{
    public class DurationsProbe
    {
        public string type { get; set; }
        public string sql_instance_name { get; set; }

        [Nett.TomlIgnore]
        public bool forceInstance { get { return !string.IsNullOrWhiteSpace(sql_instance_name); } }

        public Reader reader { get; set; }
        public Writer writer { get; set; }
    }
}