﻿namespace DMVmonitor.Config
{
    public class NatsConfiguration
    {
        public string address { get; set; }
        public string subject { get; set; }
        public int msSendCycle { get; set; }
    }
}