﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace DMVmonitor.Config
{
    public class General
    {
        public string type { get; set; }
        public bool toNATS { get; set; }
        public bool toFILE { get; set; }

        public int polling_interval { get; set; }

        //andrebbe risolto in modo elegante per ora metto un mode 0 per non filtrare, 1 per filtrare modo duration, 2 per filtrare modo execution
        public int durationThreshold { get; set; }
        public int executionThreshold { get; set; }
        public int thresholdMODE { get; set; }
        public string textThreshold { get; set; }

        public string connectionString { get; set; }
        public string whereClause { get; set; }
    }

    public class General_LT
    {
        public string type { get; set; }
        public bool toNATS { get; set; }
        public string connectionString { get; set; }

        [Nett.TomlIgnore]
        public bool ltran_snapshot_active { get { return (ltran_snapshot != null && !ltran_snapshot.disabled); } }
        public DMV_TL ltran_snapshot { get; set; }
        [Nett.TomlIgnore]
        public bool ltran_database_active { get { return (ltran_database != null && !ltran_database.disabled); } }
        public DMV_TLEnh ltran_database { get; set; }
        [Nett.TomlIgnore]
        public bool headblockers_active { get { return (headblockers != null && !headblockers.disabled); } }
        public DMV_HB headblockers { get; set; }
        [Nett.TomlIgnore]
        public bool indexes_usage_active { get { return (indexes_usage != null && !indexes_usage.disabled); } }
        public DMV_IU indexes_usage { get; set; }
        [Nett.TomlIgnore]
        public bool version_store_active { get { return (version_store != null && !version_store.disabled); } }
        public DMV_VS version_store { get; set; }


        public Dictionary<string, EnumTest> DirectSQL1 { get; set; }
    }

    public class EnumTest
    {
        public string name { set; get; }
 
        public int polling_interval { get; set; }
        public string sql { set; get; }
        public string subject { set; get; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return !string.IsNullOrEmpty(subject); } }
        public bool traceOutput { set; get; }
        

        public string formatString { get; set; }
        public InfluxFormat[] influx { get; set; }

        public Nett.TomlTable Enums { get; set; }

        [Nett.TomlIgnore]
        public EnumD EnumsD
        {
            get
            {
                Logging.Log.Instance.Debug("Enums == null {0}", Enums == null);
                return new EnumD(Enums);
            }
        }


        public Nett.TomlTable Maps { get; set; }

        [Nett.TomlIgnore]
        public MapD MapsD
        {
            get
            {
                Logging.Log.Instance.Trace("Maps == null {0}", Maps == null);
                return new MapD(Maps);
            }
        }

        public class InfluxFormat
        {
            public int index { set; get; }
            public string type { set; get; }
            public string escape { set; get; }
            [Nett.TomlIgnore]
            public bool fieldEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "field";
                }
            }
            [Nett.TomlIgnore]
            public bool tagEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "tag";
                }
            }
            public string enumname { set; get; }

            public string formatString { set; get; }
            [Nett.TomlIgnore]
            public bool itemFormat
            {
                get
                {
                    return !string.IsNullOrEmpty(formatString);
                }
            }
        }


        public class InfluxFieldFormat
        {
            public int index { set; get; }
            public string type { set; get; }
            public string escape { set; get; }
            [Nett.TomlIgnore]
            public bool fieldEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "field";
                }
            }
            [Nett.TomlIgnore]
            public bool tagEscape
            {
                get
                {
                    return !string.IsNullOrEmpty(escape) && escape == "tag";
                }
            }
            public string enumname { set; get; }
            public string aggr { set; get; }
            [Nett.TomlIgnore]
            public List<FieldAggregationFunction> aggrList
            {
                get
                {
                    var subs = aggr.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    List<FieldAggregationFunction> resValues = new List<FieldAggregationFunction>();
                    foreach (var sub in subs)
                        resValues.Add(new FieldAggregationFunction(sub));
                    return resValues;
                }
            }

            public string formatString { set; get; }
            [Nett.TomlIgnore]
            public bool itemFormat
            {
                get
                {
                    return !string.IsNullOrEmpty(formatString);
                }
            }
        }
        public enum FieldAggregationFunctionTypes
        {
            Count,
            Concat,
            Last,
            First,
            LastAsc,
            FirstAsc,
            LastDesc,
            FirstDesc,
            Min,
            Max,
        }
        public class FieldAggregationFunction
        {
            public FieldAggregationFunction(string sub)
            {
                var tmp = sub.ToLower().Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
                string name = tmp[0];
                string extref = tmp.Length > 1 ? tmp[1] : "";

                switch (tmp[0])
                {
                    case "count":
                        this.type = FieldAggregationFunctionTypes.Count;
                        break;
                    case "concat":
                        this.type = FieldAggregationFunctionTypes.Concat;
                        break;
                    case "last":
                        if (tmp.Length > 1)
                        {
                            var specs = tmp[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            this.extref = int.Parse(specs[0]);
                            switch (specs[1])
                            {
                                case "asc":
                                    this.type = FieldAggregationFunctionTypes.LastAsc;
                                    break;
                                case "desc":
                                    this.type = FieldAggregationFunctionTypes.LastDesc;
                                    break;
                            }
                        }
                        else
                            this.type = FieldAggregationFunctionTypes.Last;
                        break;
                    case "first":
                        if (tmp.Length > 1)
                        {
                            var specs = tmp[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            this.extref = int.Parse(specs[0]);
                            switch (specs[1])
                            {
                                case "asc":
                                    this.type = FieldAggregationFunctionTypes.FirstAsc;
                                    break;
                                case "desc":
                                    this.type = FieldAggregationFunctionTypes.FirstDesc;
                                    break;
                            }
                        }
                        else
                            this.type = FieldAggregationFunctionTypes.First;
                        break;
                    case "min":
                        this.type = FieldAggregationFunctionTypes.Min;
                        break;
                    case "max":
                        this.type = FieldAggregationFunctionTypes.Max;
                        break;
                }
            }

            public FieldAggregationFunctionTypes type { get; set; }
            public int extref { get; set; }
        }

        public string tags_FormatString { get; set; }

        public InfluxFormat[] tags { get; set; }

        public string fields_FormatString { get; set; }

        public InfluxFieldFormat[] fields { get; set; }

        public int timeindex { get; set; }
    }

    public class EnumD
    {
        private Dictionary<string, Dictionary<int, string>> values = new Dictionary<string, Dictionary<int, string>>();
        public string Get(string name, int id)
        {
            string value;
            if (!values[name].TryGetValue(id, out value))
            {
                Logging.Log.Instance.Trace("missing value for {0} id {1}", name, id);
            }
            return value;
            return values[name][id];
        }
        public EnumD(Nett.TomlTable t)
        {
            if (t != null)
            {
                this.values = t.Rows.ToDictionary(
                    t2 => t2.Key,
                    t2 => t2.Value.Get<Nett.TomlTableArray>().Items.Select(item => item.Get<IdTextPair>()).ToDictionary(p => p.id, p => p.text)
                    );
            }
        }


        public class IdTextPair
        {
            public int id { set; get; }
            public string text { set; get; }
        }
    }
    public class MapD
    {
        private Dictionary<string, Dictionary<string, string>> values = new Dictionary<string, Dictionary<string, string>>();
        public string Get(string name, string id)
        {
            string value;
            if (!values[name].TryGetValue(id, out value))
            {
                value = "NONE";
            }
            return value;
            return values[name][id];
        }
        public MapD(Nett.TomlTable t)
        {
            if (t != null)
            {
                this.values = t.Rows.ToDictionary(
                    t2 => t2.Key,
                    t2 => t2.Value.Get<Nett.TomlTableArray>().Items.Select(item => item.Get<IdTextPair>()).ToDictionary(p => p.id, p => p.text)
                    );
            }
        }


        public class IdTextPair
        {
            public string id { set; get; }
            public string text { set; get; }
        }
    }

    public class DMV_TL
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return !string.IsNullOrEmpty(subject); } }

        public int min_tran_duration { get; set; }//seconds
    }
    
    public class DMV_TLEnh
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return !string.IsNullOrEmpty(subject); } }

        public int min_tran_duration { get; set; }
    }

    public class DMV_HB
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }

        public string measurementType1 { get; set; } //axuser....
        [Nett.TomlIgnore]
        public bool toNATS1 { get { return !string.IsNullOrEmpty(measurementType1); } }

        public string measurementType2 { get; set; } //sessionId
        [Nett.TomlIgnore]
        public bool toNATS2 { get { return !string.IsNullOrEmpty(measurementType2); } }

        public int min_wait_duration { get; set; }
    }

    public class DMV_IU
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return string.IsNullOrEmpty(subject); } }

        public string db_name { get; set; }
    }
    
    public class DMV_VS
    {
        public bool disabled { get; set; }

        public int polling_interval { get; set; }

        public string subject { get; set; }
        [Nett.TomlIgnore]
        public bool toNATS { get { return string.IsNullOrEmpty(subject); } }
    }
}