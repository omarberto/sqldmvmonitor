﻿namespace SQLsessions.Config
{
    public class DeadlocksProbe
    {
        public string dataSource { get; set; }
        public string sourceName { get; set; }
        public string Instance { get; set; }
    }
}