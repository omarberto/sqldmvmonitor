﻿using Nett;
using System;

namespace DMVmonitor.Config
{
    public class TomlConfiguration
    {
        private static Configuration _config;
        public static Configuration config
        {
            get
            {
                return _config;
            }
        }
        public static void Init(string configPath)
        {
            _config = Toml.ReadFile<Configuration>(configPath);
        }
    }
}