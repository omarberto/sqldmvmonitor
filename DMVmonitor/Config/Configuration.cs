﻿
namespace DMVmonitor.Config
{
    public class Configuration
    {
        public General dmvmonitorProbe { get; set; }
        public General_LT dmvmonitorProbe_LT { get; set; }
        public SQLsessions.Config.DurationsProbe durationsProbe { get; set; }
        public SQLsessions.Config.CursorsProbe cursorsProbe { get; set; }
        public SQLsessions.Config.DeadlocksProbe deadlocksProbe { get; set; }
        public SQLsessions.Config.TransactionsProbe transactionsProbe { get; set; }

        public DataStorageConfiguration dmvStorage { get; set; }
        public NatsConfiguration nats { get; set; }
    }
}
