﻿using Microsoft.SqlServer.XEvent.Linq;
using SQLsessions.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace DMVmonitor
{
    internal class SQLsessionBlockings
    {
        private bool isReader;
        private string connectionString;
        private string sessionName;
        private string sql_host;
        private string sql_instance;

        public SQLsessionBlockings(string connectionString, string sessionName)
        {
            this.isReader = true;
            this.connectionString = connectionString;
            this.sessionName = sessionName;
        }

        internal void DoWork()
        {
            if (isReader)
            {
                OnlineSession();
            }
            else
            {
            }
        }

        private void OnlineSession()
        {
            int number_of_errors = 0;
            while (true)
            {
                try
                {
                    if (string.IsNullOrEmpty(this.sql_host) || string.IsNullOrEmpty(this.sql_instance))
                    {
                        using (var sqlConnection = new SqlConnection(connectionString))
                        {
                            sqlConnection.Open();
                            var globalTags = new GlobalTags(sqlConnection);
                            this.sql_host = globalTags.sql_host;
                            this.sql_instance = globalTags.sql_instance;
                        }
                    }
                    using (QueryableXEventData stream = new QueryableXEventData(this.connectionString, this.sessionName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                    {
                        Logging.Log.Instance.Trace("Connection!!!!");
                        foreach (PublishedEvent evt in stream)
                        {
                            Tornado.TornadoEnpointData.values.number_of_events_read++;
                            switch (evt.Name)
                            {
                                //case "blocked_process_report":
                                //case "lock_escalation":
                                //    break;
                                case "xml_deadlock_report":
                                    {
                                        Logging.Log.Instance.Debug(evt.Name);
                                        var evtParse = new XmlDeadlockReport(evt);
                                        if (evtParse.isValid && evtParse.hasDeadlockEvt)
                                        {
                                            {
                                                //message without Statement
                                                var message = new Influx.FormatMessage("SQLDeadlockStats");
                                                message.AddTag("host", this.sql_host);
                                                message.AddTag("SQLInstance", this.sql_instance);
                                                message.AddTag("ClientHostName", evtParse.deadlockEvt.ClientHostName);
                                                message.AddTag("LoginName", evtParse.deadlockEvt.LoginName);
                                                message.AddTag("WaitObjectname", evtParse.deadlockEvt.WaitObjectname);
                                                message.AddTag("WaitIndexname", evtParse.deadlockEvt.WaitIndexname);

                                                message.AddIntegerField("NrOfVictims", evtParse.deadlockEvt.NrOfVictims);
                                                message.AddStringField("SQLSessionId", evtParse.deadlockEvt.SQLSessionId, "{0}");
                                                message.AddIntegerField("Waittime", evtParse.deadlockEvt.Waittime);
                                                message.AddStringField("WaitResource", evtParse.deadlockEvt.WaitResource);
                                                message.AddStringField("WaitMode", evtParse.deadlockEvt.WaitMode);
                                                message.AddStringField("WaitObject", evtParse.deadlockEvt.WaitObjectname);
                                                message.AddStringField("WaitIndex", evtParse.deadlockEvt.WaitIndexname);

                                                message.AddEpochNs(evtParse.nsTimeStampEpoch);

                                                DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(message.value, true);
                                            }

                                            {
                                                //message completed with Statement
                                                var message = new Influx.FormatMessage("SQLDeadlockStats");
                                                message.AddTag("host", this.sql_host);
                                                message.AddTag("SQLInstance", this.sql_instance);
                                                message.AddTag("ClientHostName", evtParse.deadlockEvt.ClientHostName);
                                                message.AddTag("LoginName", evtParse.deadlockEvt.LoginName);
                                                message.AddTag("WaitObjectname", evtParse.deadlockEvt.WaitObjectname);
                                                message.AddTag("WaitIndexname", evtParse.deadlockEvt.WaitIndexname);
                                                
                                                message.AddStringField("WaitStatement", evtParse.deadlockEvt.WaitStatement);

                                                string fielname = new DeadlockSave(evtParse.xml).Save(evtParse.deadlockEvt.Elementname, evtParse.deadlockEvt.WaitObjectname, evtParse.deadlockEvt.WaitIndexname, evt.Timestamp.DateTime);
                                                message.AddStringField("SqlDeadlockHash", fielname);

                                                message.AddEpochNs(evtParse.nsTimeStampEpoch);

                                                DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(message.value);
                                            }

                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    Logging.Log.Instance.Error(e, "deadlocksOnlineSession OnlineSession -> SqlException");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (EventEnumerationException e)
                {
                    Logging.Log.Instance.Error(e, "deadlocksOnlineSession OnlineSession -> SESSIONE SOSPESA");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (System.IO.IOException e)
                {
                    Logging.Log.Instance.Error(e, "deadlocksOnlineSession OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
                }
                catch (System.Threading.ThreadAbortException e)
                {
                    Logging.Log.Instance.Error(e, "deadlocksOnlineSession OnlineSession -> System.Threading.ThreadAbortException {1} {0}", e.Message, e.Source);
                    break;
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "deadlocksOnlineSession OnlineSession -> Generica Exception {1} {0}", e.Message, e.Source);
                }
                number_of_errors++;


                System.Threading.Thread.Sleep(1000);
            }
            Logging.Log.Instance.Trace("OnlineSession -> ExitThread");
        }
    }

    //internal class SQLsessionDeadlocks
    //{
    //    private bool isReader;
    //    private DataSend sender = null;
    //    private bool hasSender { get { return sender != null; } }
    //    private string connectionString;
    //    private string sessionName;

    //    public SQLsessionDeadlocks(string connectionString, string sessionName, DataSend sender)
    //    {
    //        this.isReader = true;
    //        this.connectionString = connectionString;
    //        this.sessionName = sessionName;
    //        this.sender = sender;//si da per scontato ora che ci si colleghi in automatico
    //    }

    //    internal void DoWork()
    //    {
    //        if (isReader)
    //        {
    //            OnlineSession();
    //        }
    //        else
    //        {
    //        }
    //    }
        

    //    private Dictionary<ulong, LockTimeout> locktimeoutDictionary = new Dictionary<ulong, LockTimeout>();
    //    private Dictionary<uint, LockDeadlockExt> deadlockDictionary = new Dictionary<uint, LockDeadlockExt>();
    //    private void OnlineSession()
    //    {

    //        int totalEventCount = 0;
    //        int totalExceptionCount = 0;
    //        while (true)
    //        {
    //            try
    //            {
    //                using (QueryableXEventData stream = new QueryableXEventData(this.connectionString, this.sessionName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
    //                {
    //                    Logging.Log.Instance.Trace("Connection!!!!");
    //                    foreach (PublishedEvent evt in stream)
    //                    {
    //                        switch (evt.Name)
    //                        {

    //                            case "lock_timeout":
    //                                {
    //                                    var evtParse = new LockTimeout(evt);
    //                                    if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //                                        locktimeoutDictionary[evtParse.transaction_id] = evtParse;
    //                                    else
    //                                        locktimeoutDictionary.Add(evtParse.transaction_id, evtParse);

    //                                    //mi serve evento transactionClose... // oppure polla ogni tanto...
    //                                    //checkTransactionIsOpen();

    //                                }
    //                                break;
    //                            case "lock_timeout_greater_than_0":
    //                                {
    //                                    //mi interesserà sapere se esiste....
    //                                    Logging.Log.Instance.Trace(evt.Name);
    //                                    var evtParse = new LockTimeout(evt);
    //                                    if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //                                        locktimeoutDictionary[evtParse.transaction_id] = evtParse;
    //                                    else
    //                                        locktimeoutDictionary.Add(evtParse.transaction_id, evtParse);

    //                                    //mi serve evento transactionClose... // oppure polla ogni tanto...
    //                                }
    //                                break;
    //                            case "lock_deadlock_chain":
    //                                {
    //                                    Logging.Log.Instance.Trace(evt.Name);
    //                                    var evtParse = new LockDeadlockChain(evt);
    //                                    if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //                                    {
    //                                        evtParse.program_name = locktimeoutDictionary[evtParse.transaction_id].program_name;
    //                                        evtParse.host_name = locktimeoutDictionary[evtParse.transaction_id].host_name;
    //                                        evtParse.ax_session = locktimeoutDictionary[evtParse.transaction_id].ax_session;
    //                                        evtParse.ax_user = locktimeoutDictionary[evtParse.transaction_id].ax_user;
    //                                        evtParse.query_hash = locktimeoutDictionary[evtParse.transaction_id].query_hash;
    //                                        evtParse.query_plan_hash = locktimeoutDictionary[evtParse.transaction_id].query_plan_hash;
    //                                    }

    //                                    if (!deadlockDictionary.ContainsKey(evtParse.deadlock_id))
    //                                        deadlockDictionary.Add(evtParse.deadlock_id, new LockDeadlockExt());
    //                                    deadlockDictionary[evtParse.deadlock_id].AddChain(evtParse);

    //                                    //provare a fare queri su sessions e vediamo se ci riesco sempre!!!!
    //                                }
    //                                break;
    //                            case "lock_deadlock":
    //                                {
    //                                    Logging.Log.Instance.Trace(evt.Name);
    //                                    var evtParse = new LockDeadlock(evt);
    //                                    if (deadlockDictionary.ContainsKey(evtParse.deadlock_id))
    //                                    {
    //                                        deadlockDictionary[evtParse.deadlock_id].AddDeadlock(evtParse);
    //                                    }
    //                                }
    //                                break;
    //                            case "xml_deadlock_report":
    //                                {
    //                                    Logging.Log.Instance.Trace(evt.Name);
    //                                    var evtParse = new XmlDeadlockReport(evt);
    //                                    if (evtParse.isValid)
    //                                    {
    //                                        uint deadlock_id = 0;
    //                                        foreach (var deadlock in deadlockDictionary)
    //                                        {
    //                                            foreach (var chain in deadlock.Value.chain)
    //                                            {
    //                                                var xmlChain = evtParse.getProcess(chain.transaction_id);
    //                                                //write-tonats
    //                                                //tags: axuser -- objectname indexname
    //                                                //fields 
    //                                                var snats = string.Format("sqlXeDeadlocks,ax_user={0},object_name={1},index_name={2} transaction_id=\"{3}\",query_hash=\"0x{4:X16}\",query_plan_hash=\"0x{5:X16}\",ax_session=\"{6}\",is_victim={7}",
    //                                                    chain.ax_user, xmlChain.objectname, xmlChain.indexname,
    //                                                    chain.transaction_id, chain.query_hash, chain.query_plan_hash, chain.ax_session, xmlChain.isVictim ? "t" : "f");
                                                    
    //                                                sender.PublishMessage(snats);
    //                                            }
    //                                            deadlock_id = deadlock.Key;
    //                                        }
    //                                        if (deadlock_id > 0)
    //                                            deadlockDictionary.Remove(deadlock_id);
    //                                    }
    //                                }
    //                                break;
    //                        }
    //                        totalEventCount++;
    //                        if (totalEventCount % 1000 == 0)//parametrizzare!!
    //                            Logging.Log.Instance.Trace("Events {0} -- Exceptions {1}", totalEventCount, totalExceptionCount);
    //                    }
    //                }
    //            }
    //            catch (System.Data.SqlClient.SqlException e)
    //            {
    //                Logging.Log.Instance.Trace(e, "OnlineSession -> SqlException");
    //            }
    //            catch (EventEnumerationException e)
    //            {
    //                Logging.Log.Instance.Trace(e, "OnlineSession -> SESSIONE SOSPESA");
    //            }
    //            catch (System.IO.IOException e)
    //            {
    //                Logging.Log.Instance.Trace(e, "OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
    //            }
    //            catch (Exception e)
    //            {
    //                Logging.Log.Instance.Trace(e, "SQLsessionBlockings OnlineSession -> Generica Exception {1} {0}", e.Message, e.Source);
    //            }
    //            totalExceptionCount++;


    //            System.Threading.Thread.Sleep(1000);
    //        }
    //    }


    //    //private void ParseEventsDeadlocks(PublishedEvent evt)
    //    //{
    //    //    switch (evt.Name)
    //    //    {


    //    //        case "lock_timeout":
    //    //            {
    //    //                var evtParse = new LockTimeout(evt);
    //    //                if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //    //                    locktimeoutDictionary[evtParse.transaction_id] = evtParse;
    //    //                else
    //    //                    locktimeoutDictionary.Add(evtParse.transaction_id, evtParse);

    //    //                //mi serve evento transactionClose... // oppure polla ogni tanto...
    //    //                //checkTransactionIsOpen();

    //    //            }
    //    //            break;
    //    //        case "lock_timeout_greater_than_0":
    //    //            {
    //    //                //mi interesserà sapere se esiste....
    //    //                var evtParse = new LockTimeout(evt);
    //    //                if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //    //                    locktimeoutDictionary[evtParse.transaction_id] = evtParse;
    //    //                else
    //    //                    locktimeoutDictionary.Add(evtParse.transaction_id, evtParse);

    //    //                //mi serve evento transactionClose... // oppure polla ogni tanto...
    //    //            }
    //    //            break;
    //    //        case "lock_deadlock_chain":
    //    //            {
    //    //                var evtParse = new LockDeadlockChain(evt);
    //    //                if (locktimeoutDictionary.ContainsKey(evtParse.transaction_id))
    //    //                {
    //    //                    evtParse.program_name = locktimeoutDictionary[evtParse.transaction_id].program_name;
    //    //                    evtParse.host_name = locktimeoutDictionary[evtParse.transaction_id].host_name;
    //    //                    evtParse.ax_session = locktimeoutDictionary[evtParse.transaction_id].ax_session;
    //    //                    evtParse.ax_user = locktimeoutDictionary[evtParse.transaction_id].ax_user;
    //    //                    evtParse.query_hash = locktimeoutDictionary[evtParse.transaction_id].query_hash;
    //    //                    evtParse.query_plan_hash = locktimeoutDictionary[evtParse.transaction_id].query_plan_hash;
    //    //                }

    //    //                if (!deadlockDictionary.ContainsKey(evtParse.deadlock_id))
    //    //                    deadlockDictionary.Add(evtParse.deadlock_id, new LockDeadlockExt());
    //    //                deadlockDictionary[evtParse.deadlock_id].AddChain(evtParse);

    //    //                //provare a fare queri su sessions e vediamo se ci riesco sempre!!!!
    //    //            }
    //    //            break;
    //    //        case "lock_deadlock":
    //    //            {
    //    //                var evtParse = new LockDeadlock(evt);
    //    //                if (deadlockDictionary.ContainsKey(evtParse.deadlock_id))
    //    //                {
    //    //                    deadlockDictionary[evtParse.deadlock_id].AddDeadlock(evtParse);
    //    //                }
    //    //            }
    //    //            break;
    //    //        case "xml_deadlock_report":
    //    //            {
    //    //                var evtParse = new XmlDeadlockReport(evt);
    //    //                if (evtParse.isValid && evtParse.hasDeadlockEvt)
    //    //                {
    //    //                    var snats = string.Format("SQLDeadlockStats,host={0},SQLInstance={1},ClientHostName={2},LoginName={3},WaitObjectname={4},WaitIndexname={5} NrOfVictims={6}i,SQLSessionId=\"{7}\",Waittime={8}i,WaitResource=\"{9}\",WaitMode=\"{10}\",WaitStatement=\"{11}\" {12}",
    //    //                        "host",
    //    //                        "SQLInstance",
    //    //                        evtParse.deadlockEvt.ClientHostName,
    //    //                        evtParse.deadlockEvt.LoginName,
    //    //                        evtParse.deadlockEvt.WaitObjectname,
    //    //                        evtParse.deadlockEvt.WaitIndexname,

    //    //                        evtParse.deadlockEvt.NrOfVictims,
    //    //                        evtParse.deadlockEvt.SQLSessionId,
    //    //                        evtParse.deadlockEvt.Waittime,
    //    //                        evtParse.deadlockEvt.WaitResource,
    //    //                        evtParse.deadlockEvt.WaitMode,
    //    //                        evtParse.deadlockEvt.WaitStatement,

    //    //                        evtParse.nsTimeStampEpoch);
    //    //                }
    //    //            }
    //    //            break;

    //    //        default:
    //    //            {
    //    //                //var evtParse = new CompleteUnknown(evt);
    //    //            }
    //    //            break;
    //    //    }
    //    //}

    //    #region HEADBLOCKS
    //    //private class HeadlockChain
    //    //{
    //    //    public long monitorLoop;
    //    //    public Dictionary<ushort, BlockedProcess> blockedProcesses = new Dictionary<ushort, BlockedProcess>();
    //    //    public HeadlockChain(BlockedProcess blockedProcess)
    //    //    {
    //    //        blockedProcesses.Add(blockedProcess.Spid, blockedProcess);
    //    //    }
    //    //    public void Add(BlockedProcess blockedProcess)
    //    //    {
    //    //        if (this.monitorLoop == blockedProcess.monitorLoop)
    //    //        {
    //    //            //potrei mettere qui la "stampa" quando ho attesa + lunga (che e'headlocker... no quello è il primo)


    //    //        }
    //    //        else
    //    //        {
    //    //            //stampa...//mettere un timeout = 2 sec * 2 se si vuole perfezione. Altrimenti accontentarsi che si perdono ultimi 2 secondi.... 
    //    //            var levels0 = blockedProcesses.Values.Select(el => el.blockingSpid).Except(blockedProcesses.Keys);
    //    //            string outS = "";
    //    //            Recurse(levels0, new ushort[] { }, 1, ref outS);
        

    //    //            //elimino tutti i vecchi 
    //    //            blockedProcesses = new Dictionary<ushort, BlockedProcess>();

    //    //            this.monitorLoop = blockedProcess.monitorLoop;

    //    //        }

    //    //        blockedProcesses.Add(blockedProcess.Spid, blockedProcess);

    //    //        //uso keys per heads. Poi dentro creo classi con dictionary a loro volta 
    //    //    }

    //    //    private void Recurse(IEnumerable<ushort> levels1, IEnumerable<ushort> chain, int level, ref string outS)
    //    //    {
    //    //        foreach (var level1 in levels1)
    //    //        {
    //    //            var levels2 = blockedProcesses.Where(el => el.Value.blockingSpid == level1).Select(el => el.Key);

    //    //            if (levels2.Count() == 0)
    //    //            {
    //    //                string s = string.Format("{0}{1} ", PrintChain(chain), level1, level);
    //    //                outS += "\r\n" + s;
    //    //            }
    //    //            else
    //    //            {
    //    //                Recurse(levels2, chain.Concat(new ushort[] { level1 }), level + 1, ref outS);
    //    //            }

    //    //            if (level == 1)
    //    //            {
    //    //                outS = "";
    //    //            }
    //    //        }
    //    //    }

    //    //    public string PrintChain(IEnumerable<ushort> list)
    //    //    {
    //    //        string s = "";
    //    //        foreach (var el in list) s += string.Format("{0} -> ", el);
    //    //        return s;
    //    //    }
    //    //    public string PrintString(IEnumerable<ushort> list)
    //    //    {
    //    //        string s = "";
    //    //        foreach (var el in list) s += string.Format("{0},", el);
    //    //        return s.TrimEnd(',');
    //    //    }
    //    //}
    //    //private Dictionary<Guid, HeadlockChain> headlocks = new Dictionary<Guid, HeadlockChain>();
    //    //private void ParseEventsDevelopment(PublishedEvent evt)
    //    //{
    //    //    switch (evt.Name)
    //    //    {
    //    //        case "blocked_process_report":
    //    //            {
    //    //                var evtParse = new BlockedProcessReport(evt);
        
    //    //                //attach_activity_id --> se trovo anche xfer, altrimenti mi sono illuso e sono tutti uguali....

    //    //                //è per non avere doppioni, ma a sto punto è chiaro che non me ne faccio nulla
    //    //                if (evtParse.attach_activity_id.HasValue)
    //    //                {
    //    //                    var monitorLoop = evtParse.blocked_process_report.monitorLoop;
    //    //                    var blockingSpid = evtParse.blocked_process_report.blockingprocess.process.spid;
    //    //                    var blockedSpid = evtParse.blocked_process_report.blockedprocess.process.spid;

    //    //                    //lascia perdere.... però interessante per i deadlocks...
    //    //                    var blockingTrancount = evtParse.blocked_process_report.blockingprocess.process.trancount;
    //    //                    var blockedTrancount = evtParse.blocked_process_report.blockedprocess.process.trancount;
    //    //                    //unica cosa e' che sono a gruppi, secondo ordine catena (quindi la posso riprodurre banalmente)
        
    //    //                    var blockedProcess = new BlockedProcess()
    //    //                    {
    //    //                        Spid = blockedSpid,
    //    //                        blockingSpid = blockingSpid,
    //    //                        monitorLoop = monitorLoop,
    //    //                        nsTimeStampEpoch = evtParse.nsTimeStampEpoch
    //    //                    };

    //    //                    if (headlocks.ContainsKey(evtParse.attach_activity_id.Value))
    //    //                    {
    //    //                        headlocks[evtParse.attach_activity_id.Value].Add(blockedProcess);
    //    //                    }
    //    //                    else
    //    //                    {
    //    //                        headlocks.Add(evtParse.attach_activity_id.Value, new HeadlockChain(blockedProcess));
    //    //                    }
        
    //    //                }
    //    //            }
    //    //            break;

    //    //        case "xml_deadlock_report2":
    //    //            {
    //    //                var evtParse = new XmlDeadlockReport(evt);

    //    //                var victimSpid = 0;
    //    //                foreach (var process in evtParse.xml_deadlock_report.processlist)
    //    //                {

    //    //                    //if (processes.ContainsKey(process.inputbuf))
    //    //                    //{
    //    //                    //    processes[process.inputbuf]++;
    //    //                    //}
    //    //                    //else
    //    //                    //{
    //    //                    //    processes.Add(process.inputbuf, 1);
    //    //                    //}

    //    //                    //if (process.id != evtParse.xml_deadlock_report.victimlist.victimProcess.id)
    //    //                    //{
    //    //                    //    if (lastKill.ContainsKey(process.spid))
    //    //                    //    {
    //    //                    //        lastKill[process.spid].last = new EpochNanoseconds(evtParse.nsTimeStampEpoch, DateTimeKind.Utc).dateTime;
    //    //                    //        lastKill[process.spid].count++;
    //    //                    //        lastKill[process.spid].logused = process.logused;
    //    //                    //    }
    //    //                    //    else
    //    //                    //    {
    //    //                    //        lastKill.Add(process.spid, new ProcessDeadlockStats() {
    //    //                    //            first = new EpochNanoseconds(evtParse.nsTimeStampEpoch, DateTimeKind.Utc).dateTime,
    //    //                    //            last = new EpochNanoseconds(evtParse.nsTimeStampEpoch, DateTimeKind.Utc).dateTime,
    //    //                    //            count = 1,
    //    //                    //            logused = process.logused,
    //    //                    //            ownerId = process.ownerId
    //    //                    //        });
    //    //                    //    }


        
    //    //                    //}
    //    //                    //else
    //    //                    //{
    //    //                    //    victimSpid = process.spid;
    //    //                    //}
    //    //                    ////ownerId --> transaction id

    //    //                }

    //    //                //if (lastKill.ContainsKey(victimSpid))
    //    //                //{
    //    //                //    lastKill.Remove(victimSpid);
    //    //                //    //Console.ReadKey(true);
    //    //                //}
    //    //            }
    //    //            break;
    //    //        default:
    //    //            {
    //    //                //var evtParse = new CompleteUnknown(evt);
    //    //            }
    //    //            break;
    //    //    }
    //    //}

    //    #endregion


    //    //adesso riesco ad avere statistiche sui kill!!!
    //    //incrocio con queries e magari se cambia transazione azzero
    //    //se riesco a capire quella che consuma piu log meglio...
    //    //in tooria ho rpc Abort//Error ad aiutarmi
    //    //o transaction 
    //    //start
    //    //direi che dovrei avere una soglia di deadlocks 
    //    //class ProcessDeadlockStats
    //    //{
    //    //    public DateTime first;
    //    //    public DateTime last;
    //    //    public int count;

    //    //    public int logused;
    //    //    public ulong ownerId;
    //    //}
    //    //Dictionary<int, ProcessDeadlockStats> lastKill = new Dictionary<int, ProcessDeadlockStats>();



    //    //private class BlockedProcess
    //    //{
    //    //    public ushort Spid;
    //    //    public ushort blockingSpid;
    //    //    public long nsTimeStampEpoch;
    //    //    public long monitorLoop;
    //    //}
    //    //private class BlockedProsessesChain
    //    //{
    //    //    public long nsTimeStampEpoch;
    //    //    public BlockedProsessesChain(ushort headSpid, ushort tailSpid)
    //    //    {
    //    //        list.Add(headSpid);
    //    //        list.Add(tailSpid);
    //    //    }

    //    //    public ushort headSpid
    //    //    {
    //    //        get { return list.First(); }
    //    //        set { list.Insert(0, value); }
    //    //    }
    //    //    public ushort tailSpid
    //    //    {
    //    //        get { return list.Last(); }
    //    //        set { list.Add(value); }
    //    //    }

    //    //    public void Add(BlockedProsessesChain tail)
    //    //    {
    //    //        this.list.AddRange(tail.list.Skip(1));
    //    //    }

    //    //    public List<ushort> list = new List<ushort>();

    //    //    public override string ToString()
    //    //    {
    //    //        string s = string.Format("{0} :", new EpochNanoseconds(nsTimeStampEpoch, DateTimeKind.Utc).dateTime);
    //    //        foreach (var el in list) s += el + ",";
    //    //        return s.TrimEnd(',');
    //    //    }
    //    //}
    //    //private Dictionary<ushort, BlockedProcess> blockedProcesses = new Dictionary<ushort, BlockedProcess>();


    //    //private string PrintString(IEnumerable<BlockedProcess> chains)
    //    //{
    //    //    string s = "";// string.Format("{0} :", new EpochNanoseconds(nsTimeStampEpoch, DateTimeKind.Utc).dateTime);
    //    //    foreach (var el in chains) s += string.Format("({0:yyyy/MM/dd HH:mm:ss.ffffff}||{1}-{2})", new EpochNanoseconds(el.nsTimeStampEpoch, DateTimeKind.Utc).dateTime.ToLocalTime(), el.blockingSpid, el.Spid);

    //    //    return s.TrimEnd(',');
    //    //}
    //}
}