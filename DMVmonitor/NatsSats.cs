﻿namespace DMVmonitor
{
    internal class NatsSats
    {
        private long execution_count;
        private ulong query_hash;
        private ulong query_plan_hash;
        private long total_clr_time;
        private long total_cpu;
        private long total_duration;
        private long total_logical_reads;
        private long total_physical_reads;
        private long total_rows;
        private long total_writes;
        private long value;

        public NatsSats(ulong query_hash, ulong query_plan_hash, long execution_count, long total_cpu, long total_duration, long total_physical_reads, long total_logical_reads, long total_writes, long total_clr_time, long total_rows, long value)
        {
            this.query_hash = query_hash;
            this.query_plan_hash = query_plan_hash;

            this.execution_count = execution_count;
            this.total_cpu = total_cpu;
            this.total_duration = total_duration;
            this.total_physical_reads = total_physical_reads;
            this.total_logical_reads = total_logical_reads;
            this.total_writes = total_writes;
            this.total_clr_time = total_clr_time;
            this.total_rows = total_rows;
            this.value = value;
            
        }
    }
}