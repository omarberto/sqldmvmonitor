﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NATS.Client;
using System.Security.Cryptography.X509Certificates;

namespace DMVmonitor
{

    enum UniquifierClass
    {
        sqlXeEvents,
        sqlXeFetches,
        SQLHeadBlockers,
    }
    class Uniquifier
    {
        private uint uniquifierId = 0;
        private long lastmessagetimestamp = long.MinValue;
        internal uint uniquifier(long nsTimeStampEpoch)
        {
            if (this.lastmessagetimestamp == nsTimeStampEpoch)
            {
                this.uniquifierId++;
            }
            else
            {
                this.uniquifierId = 0;
                this.lastmessagetimestamp = nsTimeStampEpoch;
            }
            return this.uniquifierId;
        }

        private static Dictionary<UniquifierClass, Uniquifier> uniquifierIds = new UniquifierClass[] {
            UniquifierClass.sqlXeEvents,
            UniquifierClass.sqlXeFetches,
            UniquifierClass.SQLHeadBlockers
        }.ToDictionary(el=>el,el=> new Uniquifier());
        internal static uint uniquifier(UniquifierClass @class, long nsTimeStampEpoch)
        {
            return uniquifierIds[@class].uniquifier(nsTimeStampEpoch);
        }
    }
}
