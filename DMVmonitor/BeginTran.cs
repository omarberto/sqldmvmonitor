﻿using System;
using DMVmonitor.Transactions.Events;

namespace DMVmonitor
{
    internal class BeginTran
    {
        internal string server_instance_name;
        private int AxSession;
        private string AxUser;
        private string client_hostname;
        private int database_id;
        private string object_name;
        private long transaction_id;
        private string transaction_type;
        private ulong duration;
        internal string Message { get { return string.Format("client_hostname=\"{0}\",database_id=\"{1}\",AxUser=\"{2}\",AxSession=\"{3}\",object_name=\"{4}\",transaction_id={5}i,transaction_type=\"{6}\",duration={7}i", client_hostname, database_id, AxUser, AxSession, object_name, transaction_id, transaction_type, duration); } }

        //sql_transaction(state: begin)
        internal DateTimeOffset stb_time_stamp;
        private bool stb_is_system;
        private int stb_last_error;
        private long stb_transaction_sequence;
        private string stb_savepoint_name;
        private int stb_session_id;
        //esiste per forza!!!!
        internal string stb_Message { get { return string.Format(",stb_is_system={0},stb_last_error={1}i,stb_transaction_sequence={2}i,stb_savepoint_name=\"{3}\",currentDuration={4}i,stb_session_id={5}i", stb_is_system ? "t" : "f", stb_last_error, stb_transaction_sequence, stb_savepoint_name, currentDuration, stb_session_id); } }
        internal BeginTran(SqlTransaction parsedEvent)
        {
            UpdateBegin(parsedEvent);
        }
        internal void UpdateBegin(SqlTransaction parsedEvent)
        {
            this.stb_time_stamp = parsedEvent.time_stamp;//vitale
            this.stb_transaction_sequence = parsedEvent.transaction_sequence;//
            this.stb_is_system = parsedEvent.is_system;//--> eliminabile?
            this.stb_last_error = parsedEvent.last_error;//--> eliminabile??
            this.stb_savepoint_name = parsedEvent.savepoint_name;//
            this.stb_session_id = parsedEvent.session_id;//vitale

            this.database_id = parsedEvent.database_id;
            this.server_instance_name = parsedEvent.server_instance_name;
            this.transaction_id = parsedEvent.transaction_id;//
            this.AxSession = parsedEvent.AxSession;//
            this.AxUser = parsedEvent.AxUser;//
            this.client_hostname = parsedEvent.client_hostname;//
            this.object_name = parsedEvent.object_name;//
            this.transaction_type = parsedEvent.transaction_type;//
        }

        //sql_transaction(state: commit)
        private DateTimeOffset? stc_time_stamp;
        private long stc_transaction_sequence;
        private bool stc_is_system;
        private int stc_last_error;
        private string stc_savepoint_name;
        private int stc_session_id;
        internal bool stc_Exists { get { return stc_time_stamp.HasValue; } }
        internal string stc_Message { get { return string.Format(",stc_is_system={0},stc_last_error={1}i,stc_transaction_sequence={2}i,stc_savepoint_name=\"{3}\"", stc_is_system ? "t" : "f", stc_last_error, stc_transaction_sequence, stc_savepoint_name); } }
        internal void UpdateCommit(SqlTransaction parsedEvent)
        {
            this.stc_time_stamp = parsedEvent.time_stamp;//vitale
            this.duration = parsedEvent.duration;//
            this.stc_transaction_sequence = parsedEvent.transaction_sequence;//
            this.stc_is_system = parsedEvent.is_system;//--> eliminabile?
            this.stc_last_error = parsedEvent.last_error;//--> eliminabile??
            this.stc_savepoint_name = parsedEvent.savepoint_name;//
            this.stc_session_id = parsedEvent.session_id;//vitale
        }

        //sql_transaction(state: rollback)
        private DateTimeOffset? str_time_stamp;
        private long str_transaction_sequence;
        private bool str_is_system;
        private int str_last_error;
        private string str_savepoint_name;
        private int str_session_id;
        internal bool str_Exists { get { return stc_time_stamp.HasValue; } }
        internal string str_Message { get { return string.Format(",stcr_is_system={0},str_last_error={1}i,str_transaction_sequence={2}i,str_savepoint_name=\"{3}\"", str_is_system ? "t" : "f", str_last_error, str_transaction_sequence, str_savepoint_name); } }
        internal void UpdateRollback(SqlTransaction parsedEvent)
        {
            this.str_time_stamp = parsedEvent.time_stamp;//vitale
            this.duration = parsedEvent.duration;//
            this.str_transaction_sequence = parsedEvent.transaction_sequence;//
            this.str_is_system = parsedEvent.is_system;//--> eliminabile?
            this.str_last_error = parsedEvent.last_error;//--> eliminabile??
            this.str_savepoint_name = parsedEvent.savepoint_name;//
            this.str_session_id = parsedEvent.session_id;//vitale
        }

        internal DateTimeOffset? time_stamp
        {
            get
            {
                return stc_time_stamp.HasValue ? stc_time_stamp : str_time_stamp;
            }
        }
        internal bool endTS_Exists { get { return time_stamp.HasValue; } }
        internal long nsTimeStampEpoch
        {
            get
            {
                if (endTS_Exists)
                    return new EpochNanoseconds(time_stamp.Value.UtcDateTime).Value;
                return -1L;
            }
        }
        internal long currentDuration
        {
            get
            {
                return (long)((DateTimeOffset.Now - stb_time_stamp).TotalMilliseconds * 1000000);
            }
        }

        internal BeginTran Clone()
        {
            return this.MemberwiseClone() as BeginTran;
        }

    }

    internal class BeginTranDetailed
    {
        internal string server_instance_name;
        private int AxSession;
        private string AxUser;
        private string client_hostname;
        private int database_id;
        private string object_name;
        private long transaction_id;
        private string transaction_type;
        private ulong duration;
        internal string Message { get { return string.Format("client_hostname=\"{0}\",database_id=\"{1}\",AxUser=\"{2}\",AxSession=\"{3}\",object_name=\"{4}\",transaction_id={5}i,transaction_type=\"{6}\",duration={7}i", client_hostname, database_id, AxUser, AxSession, object_name, transaction_id, transaction_type, duration); } }


        //begin_tran_starting 
        private bool bts_is_system;
        private int bts_last_error;
        private int bts_session_id;
        private DateTimeOffset? bts_time_stamp;
        private long bts_timediff { get { return Convert.ToInt64((bts_time_stamp.Value - stb_time_stamp).TotalMilliseconds * 1000000); } }
        internal bool bts_Exists { get { return bts_time_stamp.HasValue; } }
        internal string bts_Message { get { return string.Format(",bts_is_system={0},bts_last_error={1}i,bts_timediff={2}i,bts_session_id={3}i", bts_is_system ? "t":"f", bts_last_error, bts_timediff, bts_session_id); } }
        public BeginTranDetailed(BeginTranStarting parsedEvent)
        {
            this.bts_is_system = parsedEvent.is_system;
            this.bts_last_error = parsedEvent.last_error;
            this.bts_time_stamp = parsedEvent.time_stamp;
            this.bts_session_id = parsedEvent.session_id;
        }


        //sql_transaction(state: begin)
        private DateTimeOffset stb_time_stamp;
        private bool stb_is_system;
        private int stb_last_error;
        private long stb_transaction_sequence;
        private string stb_savepoint_name;
        private int stb_session_id;
        //esiste per forza!!!!
        internal string stb_Message { get { return string.Format(",stb_is_system={0},stb_last_error={1}i,stb_transaction_sequence={2}i,stb_savepoint_name=\"{3}\",currentDuration={4}i, stb_session_id={5}i", stb_is_system ? "t" : "f", stb_last_error, stb_transaction_sequence, stb_savepoint_name, currentDuration, stb_session_id); } }
        public BeginTranDetailed(SqlTransaction parsedEvent)
        {
            UpdateBegin(parsedEvent);
        }
        public void UpdateBegin(SqlTransaction parsedEvent)
        {
            this.stb_time_stamp = parsedEvent.time_stamp;//vitale
            this.stb_transaction_sequence = parsedEvent.transaction_sequence;//
            this.stb_is_system = parsedEvent.is_system;//--> eliminabile?
            this.stb_last_error = parsedEvent.last_error;//--> eliminabile??
            this.stb_savepoint_name = parsedEvent.savepoint_name;//
            this.stb_session_id = parsedEvent.session_id;//vitale

            this.database_id = parsedEvent.database_id;
            this.server_instance_name = parsedEvent.server_instance_name;
            this.transaction_id = parsedEvent.transaction_id;//
            this.AxSession = parsedEvent.AxSession;//
            this.AxUser = parsedEvent.AxUser;//
            this.client_hostname = parsedEvent.client_hostname;//
            this.object_name = parsedEvent.object_name;//
            this.transaction_type = parsedEvent.transaction_type;//
        }

        //begin_tran_completed
        private DateTimeOffset? btc_time_stamp;
        private bool btc_is_system;
        private int btc_last_error;
        private int btc_error_number;
        private bool btc_success;
        private int btc_session_id;
        private long btc_timediff { get { return Convert.ToInt64((btc_time_stamp.Value - stb_time_stamp).TotalMilliseconds * 1000000); } }
        internal bool btc_Exists { get { return btc_time_stamp.HasValue; } }
        internal string btc_Message { get { return string.Format(",btc_is_system={0},btc_last_error={1}i,btc_timediff={2}i,btc_error_number={3}i,btc_success={4}", btc_is_system ? "t" : "f", btc_last_error, btc_timediff, btc_error_number, btc_success ? "t" : "f"); } }
        public void Update(BeginTranCompleted parsedEvent)
        {
            //spostare questa cosa su classe Transaction

            this.btc_is_system = parsedEvent.is_system;
            this.btc_last_error = parsedEvent.last_error;
            this.btc_time_stamp = parsedEvent.time_stamp;
            this.btc_session_id = parsedEvent.session_id;//vitale

            this.btc_error_number = parsedEvent.error_number;//
            this.btc_success = parsedEvent.success;//
        }

        //commit_tran_starting
        private bool cts_is_system;
        private int cts_last_error;
        private bool cts_new_transaction_started;
        private string cts_statement;
        private int cts_session_id;
        private DateTimeOffset? cts_time_stamp;
        private long cts_timediff { get { return Convert.ToInt64((cts_time_stamp.Value - stc_time_stamp.Value).TotalMilliseconds * 1000000); } }
        internal bool cts_Exists { get { return stc_Exists && cts_time_stamp.HasValue; } }
        internal string cts_Message { get { return string.Format(",cts_is_system={0},cts_last_error={1}i,cts_timediff={2}i,cts_new_transaction_started={3},cts_statement={4}", cts_is_system ? "t" : "f", cts_last_error, cts_timediff,cts_new_transaction_started, cts_statement); } }
        internal void Update(CommitTranStarting parsedEvent)
        {
            this.cts_is_system = parsedEvent.is_system;
            this.cts_last_error = parsedEvent.last_error;
            this.cts_time_stamp = parsedEvent.time_stamp;
            this.cts_session_id = parsedEvent.session_id;//vitale

            this.cts_new_transaction_started = parsedEvent.new_transaction_started;//
            this.cts_statement = parsedEvent.statement;//
        }

        //sql_transaction(state: commit)
        private DateTimeOffset? stc_time_stamp;
        private long stc_transaction_sequence;
        private bool stc_is_system;
        private int stc_last_error;
        private string stc_savepoint_name;
        private int stc_session_id;
        internal bool stc_Exists { get { return stc_time_stamp.HasValue; } }
        internal string stc_Message { get { return string.Format(",stc_is_system={0},stc_last_error={1}i,stc_transaction_sequence={2}i,stc_savepoint_name=\"{3}\"", stc_is_system ? "t" : "f", stc_last_error, stc_transaction_sequence, stc_savepoint_name); } }
        public void UpdateCommit(SqlTransaction parsedEvent)
        {
            this.stc_time_stamp = parsedEvent.time_stamp;//vitale
            this.duration = parsedEvent.duration;//
            this.stc_transaction_sequence = parsedEvent.transaction_sequence;//
            this.stc_is_system = parsedEvent.is_system;//--> eliminabile?
            this.stc_last_error = parsedEvent.last_error;//--> eliminabile??
            this.stc_savepoint_name = parsedEvent.savepoint_name;//
            this.stc_session_id = parsedEvent.session_id;//vitale
        }

        //commit_tran_completed
        private int ctc_error_number;
        private bool ctc_is_system;
        private int ctc_last_error;
        private bool ctc_success;
        private bool ctc_new_transaction_started;
        private string ctc_statement;
        private int ctc_session_id;
        private DateTimeOffset? ctc_time_stamp;
        private long ctc_timediff { get { return Convert.ToInt64((ctc_time_stamp.Value - stc_time_stamp.Value).TotalMilliseconds * 1000000); } }
        internal bool ctc_Exists { get { return stc_Exists && ctc_time_stamp.HasValue; } }
        internal string ctc_Message { get { return string.Format(",ctc_is_system={0},ctc_last_error={1}i,ctc_timediff={2}i,ctc_error_number={3}i,ctc_new_transaction_started={4},ctc_statement={5},ctc_success={6}", ctc_is_system ? "t" : "f", ctc_last_error, ctc_timediff, ctc_error_number, ctc_new_transaction_started, ctc_statement, ctc_success ? "t" : "f"); } }
        internal void Update(CommitTranCompleted parsedEvent)
        {
            this.ctc_is_system = parsedEvent.is_system;
            this.ctc_last_error = parsedEvent.last_error;
            this.ctc_time_stamp = parsedEvent.time_stamp;

            this.ctc_error_number = parsedEvent.error_number;//
            this.ctc_success = parsedEvent.success;//
            this.ctc_new_transaction_started = parsedEvent.new_transaction_started;//
            this.ctc_statement = parsedEvent.statement;//
            this.ctc_session_id = parsedEvent.session_id;//vitale
        }

        //rollback_tran_starting
        private bool rts_is_system;
        private int rts_last_error;
        private bool rts_new_transaction_started;
        private int rts_session_id;
        private DateTimeOffset? rts_time_stamp;
        private long rts_timediff { get { return Convert.ToInt64((rts_time_stamp.Value - str_time_stamp.Value).TotalMilliseconds * 1000000); } }
        internal bool rts_Exists { get { return str_Exists && rts_time_stamp.HasValue; } }
        internal string rts_Message { get { return string.Format(",rts_is_system={0},rts_last_error={1}i,crts_timediff={2}i,rts_new_transaction_started={3}", rts_is_system ? "t" : "f", rts_last_error, rts_timediff, rts_new_transaction_started); } }
        internal void Update(RollbackTranStarting parsedEvent)
        {
            this.rts_is_system = parsedEvent.is_system;
            this.rts_last_error = parsedEvent.last_error;
            this.rts_time_stamp = parsedEvent.time_stamp;
            this.rts_session_id = parsedEvent.session_id;//vitale

            this.rts_new_transaction_started = parsedEvent.new_transaction_started;//
        }

        //sql_transaction(state: rollback)
        private DateTimeOffset? str_time_stamp;
        private long str_transaction_sequence;
        private bool str_is_system;
        private int str_last_error;
        private string str_savepoint_name;
        private int str_session_id;
        internal bool str_Exists { get { return stc_time_stamp.HasValue; } }
        internal string str_Message { get { return string.Format(",stcr_is_system={0},str_last_error={1}i,str_transaction_sequence={2}i,str_savepoint_name=\"{3}\"", str_is_system ? "t" : "f", str_last_error, str_transaction_sequence, str_savepoint_name); } }
        public void UpdateRollback(SqlTransaction parsedEvent)
        {
            this.str_time_stamp = parsedEvent.time_stamp;//vitale
            this.duration = parsedEvent.duration;//
            this.str_transaction_sequence = parsedEvent.transaction_sequence;//
            this.str_is_system = parsedEvent.is_system;//--> eliminabile?
            this.str_last_error = parsedEvent.last_error;//--> eliminabile??
            this.str_savepoint_name = parsedEvent.savepoint_name;//
            this.str_session_id = parsedEvent.session_id;//vitale
        }


        //rollback_tran_completed
        private int rtc_error_number;
        private bool rtc_is_system;
        private int rtc_last_error;
        private bool rtc_success;
        private bool rtc_new_transaction_started;
        private int rtc_session_id;
        private DateTimeOffset? rtc_time_stamp;
        private long rtc_timediff { get { return Convert.ToInt64((rtc_time_stamp.Value - rtc_time_stamp.Value).TotalMilliseconds * 1000000); } }
        internal bool rtc_Exists { get { return str_Exists && rtc_time_stamp.HasValue; } }
        internal string rtc_Message { get { return string.Format(",rtc_is_system={0},rtc_last_error={1}i,rtc_timediff={2}i,rtc_error_number={3}i,rtc_new_transaction_started={4},rtc_success={5}", rtc_is_system ? "t" : "f", rtc_last_error, rtc_timediff, rtc_error_number, rtc_new_transaction_started, rtc_success ? "t" : "f"); } }
        internal void Update(RollbackTranCompleted parsedEvent)
        {
            this.rtc_is_system = parsedEvent.is_system;
            this.rtc_last_error = parsedEvent.last_error;
            this.rtc_time_stamp = parsedEvent.time_stamp;
            this.rtc_session_id = parsedEvent.session_id;//vitale

            this.rtc_error_number = parsedEvent.error_number;//
            this.rtc_success = parsedEvent.success;//
            this.rtc_new_transaction_started = parsedEvent.new_transaction_started;//
        }

        internal DateTimeOffset? time_stamp
        {
            get
            {
                return stc_time_stamp.HasValue ? stc_time_stamp : str_time_stamp;
            }
        }
        internal bool endTS_Exists { get { return time_stamp.HasValue; } }
        public long nsTimeStampEpoch
        {
            get
            {
                if(endTS_Exists)
                    return new EpochNanoseconds(time_stamp.Value.UtcDateTime).Value;
                return -1L;
            }
        }
        public long currentDuration
        {
            get
            {
                return (long)((DateTimeOffset.Now - stb_time_stamp).TotalMilliseconds * 1000000);
            }
        }


        internal bool stcsp_is_system;
        internal int stcsp_last_error;
        internal bool stcsp_is_delayed_durable;
        private int stcsp_session_id;
        internal DateTimeOffset? stcsp_time_stamp;
        private long stcsp_timediff { get { return Convert.ToInt64((stcsp_time_stamp.Value - stc_time_stamp.Value).TotalMilliseconds * 1000000); } }
        internal bool stcsp_Exists { get { return stc_Exists && stcsp_time_stamp.HasValue; } }
        internal string stcsp_Message { get { return string.Format(",stcsp_is_system={0},stcsp_last_error={1}i,stcsp_timediff={2}i,stcsp_is_delayed_durable={3}", stcsp_is_system ? "t" : "f", stcsp_last_error, stcsp_timediff, stcsp_is_delayed_durable ? "t" : "f"); } }
        internal void Update(SqlTransactionCommitSinglePhase parsedEvent)
        {
            this.stcsp_is_system = parsedEvent.is_system;
            this.stcsp_last_error = parsedEvent.last_error;
            this.stcsp_time_stamp = parsedEvent.time_stamp;
            this.stcsp_session_id = parsedEvent.session_id;//vitale

            this.stcsp_is_delayed_durable = parsedEvent.is_delayed_durable;//
        }


        public BeginTranDetailed Clone()
        {
            return this.MemberwiseClone() as BeginTranDetailed;
        }
        
    }
}