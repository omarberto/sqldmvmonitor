﻿using Microsoft.SqlServer.XEvent.Linq;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    internal class SQLsessionTransactions
    {
        private DataSend sender;

        internal static void transactionsDoWork()
        {
            Logging.Log.Instance.Trace("SQLsessionEXE transactionsDoWork starting READER");

            new SQLsessionTransactions().transactionsOnlineSession(Config.TomlConfiguration.config.transactionsProbe);
        }

        private Dictionary<long, BeginTran> opentranDictionary = new Dictionary<long, BeginTran>();
        private long oldestTranId = -1;
        private DateTimeOffset oldestTranTime = DateTimeOffset.MaxValue;
        private void transactionsOnlineSession(SQLsessions.Config.TransactionsProbe config)
        {
            //mettere if per config oppure gestirlo....
            this.sender = new DataSend();

            int totalEventCount = 0;
            int totalExceptionCount = 0;
            while (true)
            {
                string evtName = "";
                try
                {
                    var sb = new System.Data.SqlClient.SqlConnectionStringBuilder();
                    sb.DataSource = config.dataSource;
                    sb.InitialCatalog = "master";
                    sb.ConnectTimeout = 60;
                    sb.IntegratedSecurity = true;
                    sb.ApplicationIntent = ApplicationIntent.ReadOnly;
                    using (QueryableXEventData stream = new QueryableXEventData(sb.ConnectionString, config.sourceName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                    {
                        Logging.Log.Instance.Trace("Connection!!!!");
                        foreach (PublishedEvent evt in stream)
                        {
                            evtName = evt.Name;
                            switch (evt.Name)
                            {
                                case "sql_transaction":
                                    {
                                        var parsedEvent = new Transactions.Events.SqlTransaction(evt);
                                        switch (parsedEvent.transaction_state)
                                        {
                                            case "Begin":
                                                {

                                                    var tmp = new BeginTran(parsedEvent);
                                                    if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                    {
                                                        //poi andrá chiusa ...
                                                        opentranDictionary[parsedEvent.transaction_id] = tmp;
                                                    }
                                                    else
                                                    {
                                                        opentranDictionary.Add(parsedEvent.transaction_id, tmp);
                                                    }

                                                    if (oldestTranId < 0)
                                                    {
                                                        oldestTranTime = parsedEvent.time_stamp;
                                                        oldestTranId = parsedEvent.transaction_id;
                                                    }
                                                }
                                                break;

                                            case "Commit":
                                                {

                                                    if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                    {
                                                        opentranDictionary[parsedEvent.transaction_id].UpdateCommit(parsedEvent);


                                                        PublishTransaction(parsedEvent.transaction_id, "commit");

                                                        if (oldestTranId == parsedEvent.transaction_id)
                                                        {
                                                            oldestTranId = -1;
                                                            oldestTranTime = DateTimeOffset.MaxValue;
                                                            foreach (var opentran in opentranDictionary)
                                                            {
                                                                if (oldestTranTime > opentran.Value.stb_time_stamp)
                                                                {
                                                                    oldestTranTime = opentran.Value.stb_time_stamp;
                                                                    oldestTranId = opentran.Key;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //segnala mancanza!!!
                                                        Logging.Log.Instance.Trace("Commit transaction_id {0} on {1} not found ", parsedEvent.transaction_id, parsedEvent.session_id);
                                                    }

                                                }
                                                break;

                                            case "Rollback":
                                                {
                                                    if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                    {
                                                        opentranDictionary[parsedEvent.transaction_id].UpdateRollback(parsedEvent);

                                                        PublishTransaction(parsedEvent.transaction_id, "rollback");

                                                        if (oldestTranId == parsedEvent.transaction_id)
                                                        {
                                                            oldestTranId = -1;
                                                            oldestTranTime = DateTimeOffset.MaxValue;
                                                            foreach (var opentran in opentranDictionary)
                                                            {
                                                                if (oldestTranTime > opentran.Value.stb_time_stamp)
                                                                {
                                                                    oldestTranTime = opentran.Value.stb_time_stamp;
                                                                    oldestTranId = opentran.Key;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //segnala mancanza!!!
                                                        Logging.Log.Instance.Trace("Rollback transaction_id {0} on {1} not found ", parsedEvent.transaction_id, parsedEvent.session_id);
                                                    }

                                                }
                                                break;
                                        }
                                    }
                                    break;

                            }
                            

                            var longestTransaction = (DateTimeOffset.Now - oldestTranTime).TotalMilliseconds;
                            if (longestTransaction > 0)
                            {
                                string message = string.Format("sqlXeTransactions,SqlInstance={0}  maxDuration={1}i,transactionId={2}i",
                                    "1871DB0011",
                                    (long)(longestTransaction * 1000000),
                                    oldestTranId);
                                sender.PublishMessage(message);
                            }

                            totalEventCount++;
                            if (totalEventCount % 1000 == 0)//parametrizzare!!
                                Logging.Log.Instance.Trace("Events {0} -- Exceptions {1}", totalEventCount, totalExceptionCount);
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    Logging.Log.Instance.Trace(e, "OnlineSession -> SqlException");
                }
                catch (EventEnumerationException e)
                {
                    Logging.Log.Instance.Trace(e, "OnlineSession -> SESSIONE SOSPESA");
                }
                catch (System.IO.IOException e)
                {
                    Logging.Log.Instance.Trace(e, "OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Trace(e, "OnlineSession -> Generica Exception {1} {0} {2}", e.Message, e.Source, evtName);
                }
                totalExceptionCount++;


                System.Threading.Thread.Sleep(1000);
            }
        }

        private void PublishTransaction(long transaction_id, string type)
        {
            var tmp = opentranDictionary[transaction_id];
            string message = string.Format("sqlXeTransactions2,SqlInstance={0}  "
                + tmp.Message
                + tmp.stb_Message //spazio
                + (tmp.stc_Exists ? tmp.stc_Message : "")
                + (tmp.str_Exists ? tmp.str_Message : "")
                + ",close=\"{2}\" {1}",
                                tmp.server_instance_name,
                                tmp.nsTimeStampEpoch,
                                type);

            
            sender.PublishMessage(message);

            opentranDictionary.Remove(transaction_id);
        }
        private class transactionsOnlineSession_Detailed
        {
            private DataSend sender;

            private Dictionary<int, BeginTranDetailed> begintranDictionary = new Dictionary<int, BeginTranDetailed>();
            private Dictionary<int, long> endtranDictionary = new Dictionary<int, long>();
            private Dictionary<long, BeginTranDetailed> opentranDictionary = new Dictionary<long, BeginTranDetailed>();
            private DateTimeOffset oldestTran = DateTimeOffset.MinValue;
            internal void transactionsOnlineSession(SQLsessions.Config.TransactionsProbe config)
            {
                //mettere if per config oppure gestirlo....
                this.sender = new DataSend();
                
                int totalEventCount = 0;
                int totalExceptionCount = 0;
                while (true)
                {
                    string evtName = "";
                    try
                    {
                        var sb = new System.Data.SqlClient.SqlConnectionStringBuilder();
                        sb.DataSource = config.dataSource;
                        sb.InitialCatalog = "master";
                        sb.ConnectTimeout = 60;
                        sb.IntegratedSecurity = true;
                        sb.ApplicationIntent = ApplicationIntent.ReadOnly;
                        using (QueryableXEventData stream = new QueryableXEventData(sb.ConnectionString, config.sourceName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                        {
                            Logging.Log.Instance.Trace("Connection!!!!");
                            foreach (PublishedEvent evt in stream)
                            {
                                evtName = evt.Name;
                                switch (evt.Name)
                                {
                                    case "sql_transaction":
                                        {
                                            var parsedEvent = new Transactions.Events.SqlTransaction(evt);
                                            switch (parsedEvent.transaction_state)
                                            {
                                                case "Begin":
                                                    {
                                                        if (begintranDictionary.ContainsKey(parsedEvent.session_id))
                                                        {
                                                            //NB: se starting lo elimino qui va ripensato!
                                                            begintranDictionary[parsedEvent.session_id].UpdateBegin(parsedEvent);

                                                            var tmp = begintranDictionary[parsedEvent.session_id].Clone();
                                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                            {
                                                                //poi va chiusa ...
                                                                opentranDictionary[parsedEvent.transaction_id] = tmp;
                                                            }
                                                            else
                                                            {
                                                                opentranDictionary.Add(parsedEvent.transaction_id, tmp);
                                                            }

                                                            begintranDictionary.Remove(parsedEvent.session_id);
                                                        }
                                                        else
                                                        {
                                                            var tmp = new BeginTranDetailed(parsedEvent);//qui ho il grosso dell'informazione. Forse anche la conferma che se non funziona non ho questo evento
                                                                                                         //mi interessa tsql_frame per capire se riesco a cavare qualcosa, anche se finché non fondo con cursors, non ha per nulla senso!!!

                                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                            {
                                                                //poi andrá chiusa ...
                                                                opentranDictionary[parsedEvent.transaction_id] = tmp;
                                                            }
                                                            else
                                                            {
                                                                opentranDictionary.Add(parsedEvent.transaction_id, tmp);
                                                            }
                                                        }
                                                        if (oldestTran > parsedEvent.time_stamp)
                                                        {
                                                            oldestTran = parsedEvent.time_stamp;
                                                        }
                                                    }
                                                    break;

                                                case "Commit":
                                                    {

                                                        if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                        {
                                                            opentranDictionary[parsedEvent.transaction_id].UpdateCommit(parsedEvent);
                                                        }
                                                        else
                                                        {
                                                            //segnala mancanza!!!
                                                        }

                                                        if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                                        {
                                                            //succede solo se ho starting
                                                        }
                                                        else
                                                        {
                                                            PublishTransaction(parsedEvent.transaction_id, "commit_v1");
                                                        }
                                                    }
                                                    break;

                                                case "Rollback":
                                                    {
                                                        if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                                        {
                                                            opentranDictionary[parsedEvent.transaction_id].UpdateRollback(parsedEvent);
                                                        }
                                                        else
                                                        {
                                                            //segnala mancanza!!!
                                                        }


                                                        if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                                        {
                                                            //succede solo se ho starting
                                                        }
                                                        else
                                                        {
                                                            PublishTransaction(parsedEvent.transaction_id, "rollback_v1");
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;

                                    case "sql_transaction_commit_single_phase":
                                        {
                                            var parsedEvent = new Transactions.Events.SqlTransactionCommitSinglePhase(evt);
                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                            {
                                                opentranDictionary[parsedEvent.transaction_id].Update(parsedEvent);
                                            }
                                            else
                                            {
                                                //segnala mancanza!!!
                                            }
                                        }
                                        break;

                                    /********** IN STUDIO per abbinamento ********/

                                    case "begin_tran_starting":
                                        {
                                            var parsedEvent = new Transactions.Events.BeginTranStarting(evt);

                                            var tmp = new BeginTranDetailed(parsedEvent);//qui misuro solo il gap con sql_transaction // poi a breve elimino
                                            if (begintranDictionary.ContainsKey(parsedEvent.session_id))
                                            {
                                                //succede solo se sessione giá aperta.... da segnalare come errore
                                                begintranDictionary[parsedEvent.session_id] = tmp;
                                            }
                                            else
                                            {
                                                begintranDictionary.Add(parsedEvent.session_id, tmp);
                                            }
                                        }
                                        break;
                                    case "begin_tran_completed":
                                        {
                                            var parsedEvent = new Transactions.Events.BeginTranCompleted(evt);

                                            //qui misuro il gap con sql_transaction, e ho in aggiunta il risultato success_error
                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                            {
                                                opentranDictionary[parsedEvent.transaction_id].Update(parsedEvent);
                                            }
                                        }
                                        break;

                                    case "commit_tran_starting":
                                        {
                                            var parsedEvent = new Transactions.Events.CommitTranStarting(evt);

                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                            {
                                                opentranDictionary[parsedEvent.transaction_id].Update(parsedEvent);
                                            }
                                            
                                            if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                            {
                                                //succede solo se sessione giá aperta.... da segnalare come errore
                                                endtranDictionary[parsedEvent.session_id] = parsedEvent.transaction_id;
                                            }
                                            else
                                            {
                                                endtranDictionary.Add(parsedEvent.session_id, parsedEvent.transaction_id);
                                            }
                                        }
                                        break;
                                    case "commit_tran_completed":
                                        {
                                            var parsedEvent = new Transactions.Events.CommitTranCompleted(evt);
                                            try
                                            {
                                                if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                                {
                                                    var transaction_id = endtranDictionary[parsedEvent.session_id];

                                                    try
                                                    {
                                                        if (opentranDictionary.ContainsKey(transaction_id))
                                                        {
                                                            opentranDictionary[transaction_id].Update(parsedEvent);
                                                        }
                                                        
                                                        PublishTransaction(transaction_id, "commit_v2");
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        Logging.Log.Instance.Trace(e, "commit_tran_completed -> opentranDictionary {1} {0} : {2}//{3}", e.Message, e.Source, transaction_id, parsedEvent.session_id);
                                                    }

                                                    endtranDictionary.Remove(parsedEvent.session_id);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Logging.Log.Instance.Trace(e, "commit_tran_completed -> endtranDictionary {1} {0} : {2}", e.Message, e.Source, parsedEvent.session_id);
                                            }
                                        }
                                        break;

                                    case "rollback_tran_starting":
                                        {
                                            var parsedEvent = new Transactions.Events.RollbackTranStarting(evt);

                                            if (opentranDictionary.ContainsKey(parsedEvent.transaction_id))
                                            {
                                                opentranDictionary[parsedEvent.transaction_id].Update(parsedEvent);
                                            }
                                            
                                            if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                            {
                                                //succede solo se sessione giá aperta.... da segnalare come errore
                                                endtranDictionary[parsedEvent.session_id] = parsedEvent.transaction_id;
                                            }
                                            else
                                            {
                                                endtranDictionary.Add(parsedEvent.session_id, parsedEvent.transaction_id);
                                            }
                                        }
                                        break;
                                    case "rollback_tran_completed":
                                        {
                                            var parsedEvent = new Transactions.Events.RollbackTranCompleted(evt);
                                            try
                                            {
                                                if (endtranDictionary.ContainsKey(parsedEvent.session_id))
                                                {
                                                    var transaction_id = endtranDictionary[parsedEvent.session_id];

                                                    try
                                                    {

                                                        if (opentranDictionary.ContainsKey(transaction_id))
                                                        {
                                                            opentranDictionary[transaction_id].Update(parsedEvent);
                                                        }
                                                        
                                                        PublishTransaction(transaction_id, "rollback_v2");
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        Logging.Log.Instance.Trace(e, "rollback_tran_completed -> opentranDictionary {1} {0} : {2}//{3}", e.Message, e.Source, transaction_id, parsedEvent.session_id);
                                                    }

                                                    endtranDictionary.Remove(parsedEvent.session_id);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                Logging.Log.Instance.Trace(e, "rollback_tran_completed -> endtranDictionary {1} {0} : {2}", e.Message, e.Source, parsedEvent.session_id);
                                            }
                                        }
                                        break;

                                    /********** IN STUDIO 
                                        mando ogni evento in registrazione, prevedere un limite....
                                    ********/

                                    case "dtc_transaction":
                                        {
                                            var parsedEvent = new Transactions.Events.DtcTransaction(evt);
                                            string message = string.Format("sqlXeTransactions,name=dtc_transaction,SqlInstance={0} client_hostname=\"{1}\",database_id=\"{2}\",AxUser=\"{3}\",AxSession=\"{4}\",isolation_level=\"{5}\",is_system={6},session_id={7}i,last_error=\"{8}\",transaction_id={10}i,transaction_sequence={11}i,transaction_state=\"{12}\",tsql_frame=\"{13}\",tsql_stack=\"{14}\",unit_of_work_id=\"{15}\" {9}",
                                                                                                parsedEvent.server_instance_name, parsedEvent.client_hostname, parsedEvent.database_id,
                                                                                                parsedEvent.AxUser, parsedEvent.AxSession,
                                                                                                parsedEvent.isolation_level,
                                                                                                parsedEvent.is_system ? "t" : "f",
                                                                                                parsedEvent.session_id,
                                                                                                parsedEvent.last_error,
                                                                                                parsedEvent.nsTimeStampEpoch,
                                                                                                parsedEvent.transaction_id,
                                                                                                parsedEvent.transaction_sequence,
                                                                                                parsedEvent.transaction_state,
                                                                                                parsedEvent.tsql_frame,
                                                                                                parsedEvent.tsql_stack,
                                                                                                parsedEvent.unit_of_work_id);
                                            sender.PublishMessage(message);
                                        }
                                        break;

                                    case "promote_tran_starting":
                                        {
                                            var parsedEvent = new Transactions.Events.PromoteTranStarting(evt);
                                            string message = string.Format("sqlXeTransactions,name=promote_tran_starting,SqlInstance={0} client_hostname=\"{1}\",database_id=\"{2}\",AxUser=\"{3}\",AxSession=\"{4}\",plan_handle=\"{5}\",is_system={6},session_id={7}i,last_error=\"{8}\",transaction_id={10}i,transaction_sequence={11}i,query_hash=\"0x{12:X}\",tsql_frame=\"{13}\",tsql_stack=\"{14}\",query_plan_hash=\"0x{15:X}\" {9}",
                                                                                                parsedEvent.server_instance_name, parsedEvent.client_hostname, parsedEvent.database_id,
                                                                                                parsedEvent.AxUser, parsedEvent.AxSession,
                                                                                                parsedEvent.plan_handle,
                                                                                                parsedEvent.is_system ? "t" : "f",
                                                                                                parsedEvent.session_id,
                                                                                                parsedEvent.last_error,
                                                                                                parsedEvent.nsTimeStampEpoch,
                                                                                                parsedEvent.transaction_id,
                                                                                                parsedEvent.transaction_sequence,
                                                                                                parsedEvent.query_hash,
                                                                                                parsedEvent.tsql_frame,
                                                                                                parsedEvent.tsql_stack,
                                                                                                parsedEvent.query_plan_hash);
                                            sender.PublishMessage(message);
                                        }
                                        break;
                                    case "promote_tran_completed":
                                        {
                                            var parsedEvent = new Transactions.Events.PromoteTranCompleted(evt);
                                            string message = string.Format("sqlXeTransactions,name=promote_tran_completed,SqlInstance={0} client_hostname=\"{1}\",database_id=\"{2}\",AxUser=\"{3}\",AxSession=\"{4}\",plan_handle=\"{5}\",is_system={6},session_id={7}i,last_error=\"{8}\",transaction_id={10}i,transaction_sequence={11}i,query_hash=\"0x{12:X}\",tsql_frame=\"{13}\",tsql_stack=\"{14}\",query_plan_hash=\"0x{15:X}\",error_number=\"{17}\",success={18} {9}",
                                                                                                parsedEvent.server_instance_name, parsedEvent.client_hostname, parsedEvent.database_id,
                                                                                                parsedEvent.AxUser, parsedEvent.AxSession,
                                                                                                parsedEvent.plan_handle,
                                                                                                parsedEvent.is_system ? "t" : "f",
                                                                                                parsedEvent.session_id,
                                                                                                parsedEvent.last_error,
                                                                                                parsedEvent.nsTimeStampEpoch,
                                                                                                parsedEvent.transaction_id,
                                                                                                parsedEvent.transaction_sequence,
                                                                                                parsedEvent.query_hash,
                                                                                                parsedEvent.tsql_frame,
                                                                                                parsedEvent.tsql_stack,
                                                                                                parsedEvent.query_plan_hash,
                                                                                                parsedEvent.dtc_trasaction_token,
                                                                                                parsedEvent.error_number,
                                                                                                parsedEvent.success ? "t" : "f");
                                            sender.PublishMessage(message);
                                        }
                                        break;

                                    case "save_tran_starting":
                                        {
                                            var parsedEvent = new Transactions.Events.SaveTranStarting(evt);
                                            string message = string.Format("sqlXeTransactions,name=save_tran_starting,SqlInstance={0} client_hostname=\"{1}\",database_id=\"{2}\",AxUser=\"{3}\",AxSession=\"{4}\",plan_handle=\"{5}\",is_system={6},session_id={7}i,last_error=\"{8}\",transaction_id={10}i,transaction_sequence={11}i,query_hash=\"0x{12:X}\",tsql_frame=\"{13}\",tsql_stack=\"{14}\",query_plan_hash=\"0x{15:X}\",statement=\"{16}\" {9}",
                                                                                                parsedEvent.server_instance_name, parsedEvent.client_hostname, parsedEvent.database_id,
                                                                                                parsedEvent.AxUser, parsedEvent.AxSession,
                                                                                                parsedEvent.plan_handle,
                                                                                                parsedEvent.is_system ? "t" : "f",
                                                                                                parsedEvent.session_id,
                                                                                                parsedEvent.last_error,
                                                                                                parsedEvent.nsTimeStampEpoch,
                                                                                                parsedEvent.transaction_id,
                                                                                                parsedEvent.transaction_sequence,
                                                                                                parsedEvent.query_hash,
                                                                                                parsedEvent.tsql_frame,
                                                                                                parsedEvent.tsql_stack,
                                                                                                parsedEvent.query_plan_hash,
                                                                                                parsedEvent.statement);
                                            sender.PublishMessage(message);
                                        }
                                        break;
                                    case "save_tran_completed":
                                        {
                                            var parsedEvent = new Transactions.Events.SaveTranCompleted(evt);
                                            string message = string.Format("sqlXeTransactions,name=save_tran_completed,SqlInstance={0} client_hostname=\"{1}\",database_id=\"{2}\",AxUser=\"{3}\",AxSession=\"{4}\",plan_handle=\"{5}\",is_system={6},session_id={7}i,last_error=\"{8}\",transaction_id={10}i,transaction_sequence={11}i,query_hash=\"0x{12:X}\",tsql_frame=\"{13}\",tsql_stack=\"{14}\",query_plan_hash=\"0x{15:X}\",statement=\"{16}\",error_number=\"{17}\",success={18} {9}",
                                                                                                parsedEvent.server_instance_name, parsedEvent.client_hostname, parsedEvent.database_id,
                                                                                                parsedEvent.AxUser, parsedEvent.AxSession,
                                                                                                parsedEvent.plan_handle,
                                                                                                parsedEvent.is_system ? "t" : "f",
                                                                                                parsedEvent.session_id,
                                                                                                parsedEvent.last_error,
                                                                                                parsedEvent.nsTimeStampEpoch,
                                                                                                parsedEvent.transaction_id,
                                                                                                parsedEvent.transaction_sequence,
                                                                                                parsedEvent.query_hash,
                                                                                                parsedEvent.tsql_frame,
                                                                                                parsedEvent.tsql_stack,
                                                                                                parsedEvent.query_plan_hash,
                                                                                                parsedEvent.statement,
                                                                                                parsedEvent.error_number,
                                                                                                parsedEvent.success ? "t" : "f");
                                            sender.PublishMessage(message);
                                        }
                                        break;
                                }
                                totalEventCount++;
                                if (totalEventCount % 1000 == 0)//parametrizzare!!
                                    Logging.Log.Instance.Trace("Events {0} -- Exceptions {1}", totalEventCount, totalExceptionCount);
                            }
                        }
                    }
                    catch (System.Data.SqlClient.SqlException e)
                    {
                        Logging.Log.Instance.Trace(e, "OnlineSession -> SqlException");
                    }
                    catch (EventEnumerationException e)
                    {
                        Logging.Log.Instance.Trace(e, "OnlineSession -> SESSIONE SOSPESA");
                    }
                    catch (System.IO.IOException e)
                    {
                        Logging.Log.Instance.Trace(e, "OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
                    }
                    catch (Exception e)
                    {
                        Logging.Log.Instance.Trace(e, "SQLsessionTransactions OnlineSession -> Generica Exception {1} {0} {2}", e.Message, e.Source, evtName);
                    }
                    totalExceptionCount++;


                    System.Threading.Thread.Sleep(1000);
                }
            }

            private void PublishTransaction(long transaction_id, string type)
            {
                var tmp = opentranDictionary[transaction_id];
                string message = string.Format("sqlXeTransactions2,SqlInstance={0}  "
                    + tmp.Message
                    + (tmp.bts_Exists ? tmp.bts_Message : "")
                    + tmp.stb_Message
                    + (tmp.btc_Exists ? tmp.btc_Message : "")
                    + (tmp.cts_Exists ? tmp.cts_Message : "")
                    + (tmp.stc_Exists ? tmp.stc_Message : "")
                    + (tmp.ctc_Exists ? tmp.ctc_Message : "")
                    + (tmp.rts_Exists ? tmp.rts_Message : "")
                    + (tmp.str_Exists ? tmp.str_Message : "")
                    + (tmp.rtc_Exists ? tmp.rtc_Message : "")
                    + (tmp.stcsp_Exists ? tmp.stcsp_Message : "")
                    + ",close=\"{2}\" {1}",
                                    tmp.server_instance_name,
                                    tmp.nsTimeStampEpoch,
                                    type
                                    );
                sender.PublishMessage(message);

                opentranDictionary.Remove(transaction_id);
            }
        }

        private void checkTransactionIsOpen()
        {
            string sql = "select d_tst.session_id,d_tst.transaction_id from sys.dm_tran_session_transactions d_tst";

            List<ulong> todelete = new List<ulong>();
            foreach (var locktimeout in locktimeoutDictionary)
            {
                if (EpochNanoseconds.UtcNow.Value - locktimeout.Value.nsTimeStampEpoch > 900000000000) // verificare se supera n_minuti eliminare
                {
                    todelete.Add(locktimeout.Key);
                }
            }
            foreach (var key in todelete)
                locktimeoutDictionary.Remove(key);
        }
    }
}
