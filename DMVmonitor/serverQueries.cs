﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SQLsessions
{
    public struct FetchSqlPlan
    {
        public int cursor_id;
        public string sql_text;
        public ulong query_hash;
        public ulong query_plan_hash;
        public byte[] plan_handle;
        public string query_plan;

        private static byte[] Reverse(byte[] inArray)
        {
            Array.Reverse(inArray);
            return inArray;
        }
        public FetchSqlPlan(SqlString sql_text, SqlBinary query_hash, SqlBinary query_plan_hash, SqlBinary plan_handle, SqlXml query_plan) : this()
        {
            if (!sql_text.IsNull) this.sql_text = sql_text.Value;

            if (!query_hash.IsNull) this.query_hash = BitConverter.ToUInt64(Reverse(query_hash.Value), 0);
            if (!query_plan_hash.IsNull) this.query_plan_hash = BitConverter.ToUInt64(Reverse(query_plan_hash.Value), 0);
            if (!plan_handle.IsNull) this.plan_handle = plan_handle.Value;
            if (!query_plan.IsNull)
            {
                this.query_plan = query_plan.Value;

                int ParameterList_openTag_index = query_plan.Value.IndexOf("<ParameterList>");
                int ParameterList_closeTag_index = query_plan.Value.IndexOf("</ParameterList>");
                if (ParameterList_openTag_index > 0 && ParameterList_closeTag_index > ParameterList_openTag_index)
                {
                    var ParameterListXml = query_plan.Value.Substring(ParameterList_openTag_index, 16 + ParameterList_closeTag_index - ParameterList_openTag_index);

                    XmlSerializer serializer = new XmlSerializer(typeof(ParameterList));
                    StringReader rdr = new StringReader(ParameterListXml);
                    ParameterList parameterList = (ParameterList)serializer.Deserialize(rdr);
                }
            }
        }

        public override string ToString()
        {
            return string.Format("{0} 0x{1:X} 0x{2:X}", cursor_id, query_hash, query_plan_hash);
        }
    }

    class serverQueries
    {
        private string connectionString;

        public serverQueries(string datasource)
        {
            var sb = new SqlConnectionStringBuilder();
            sb.DataSource = datasource;
            sb.InitialCatalog = "master";
            sb.ConnectTimeout = 60;
            sb.IntegratedSecurity = true;
            sb.ApplicationIntent = ApplicationIntent.ReadOnly;
            this.connectionString = sb.ConnectionString;
        }

        private delegate void ExecuteDelegate(List<FetchSqlPlan> result, SqlDataReader reader, object args);
        private const int MAX_RETRY = 2;
        private const double LONG_WAIT_SECONDS = 5;
        private const double SHORT_WAIT_SECONDS = 0.5;
        private static readonly TimeSpan longWait = TimeSpan.FromSeconds(LONG_WAIT_SECONDS);
        private static readonly TimeSpan shortWait = TimeSpan.FromSeconds(SHORT_WAIT_SECONDS);
        private enum RetryableSqlErrors
        {
            Timeout = -2,
            NoLock = 1204,
            Deadlock = 1205,
            WordbreakerTimeout = 30053,
        }
        private List<FetchSqlPlan> Execute(string cmdText, ExecuteDelegate del, object args)
        {
            try
            {
                var retryCount = 0;
                for (; retryCount < 3;)
                {
                    List<FetchSqlPlan> result = new List<FetchSqlPlan>();
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlConnection.Open();
                        using (SqlCommand cmd = sqlConnection.CreateCommand())
                        {
                            cmd.CommandText = cmdText;
                            cmd.CommandType = CommandType.Text;
                            
                            try
                            {
                                using (SqlDataReader reader = cmd.ExecuteReader())
                                {
                                    try
                                    {
                                        while (reader.Read())
                                        {
                                            del(result, reader, args);
                                        }
                                        return result;
                                    }
                                    catch (SqlException ex) when (Enum.IsDefined(typeof(RetryableSqlErrors), ex.Number) && retryCount < MAX_RETRY)
                                    {
                                        System.Threading.Thread.Sleep(shortWait);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logging.Log.Instance.Trace("ex: {1}\n{0}", ex.Message, ex.GetType());
                                    }
                                    retryCount++;
                                }
                            }
                            catch (Exception ex)
                            {
                                Logging.Log.Instance.Trace(ex.Message);
                            }
                        }
                        sqlConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Trace(ex.Message);
            }
            return new List<FetchSqlPlan>();
        }
        
        private static void Execute_getQueryPlans(List<FetchSqlPlan> result, SqlDataReader reader, object args)
        {
            var res = new FetchSqlPlan(reader.GetSqlString(0), reader.GetSqlBinary(1), reader.GetSqlBinary(2), reader.GetSqlBinary(3), reader.GetSqlXml(4));
            res.cursor_id = Convert.ToInt32(args);
            result.Add(res);
        }
        private static void Execute_getQueryPlans_fromQH(List<FetchSqlPlan> result, SqlDataReader reader, object args)
        {
            var res = new FetchSqlPlan(reader.GetSqlString(0), reader.GetSqlBinary(1), reader.GetSqlBinary(2), reader.GetSqlBinary(3), reader.GetSqlXml(4));
            result.Add(res);
        }

        public IEnumerable<FetchSqlPlan> getQueryPlans(int session_id, int cursor_id)
        {
            string cmdText = string.Format("select stxt.text,qst.query_hash,qst.query_plan_hash,qst.plan_handle,splan.query_plan from sys.dm_exec_cursors({0}) c left join sys.dm_exec_query_stats qst on qst.sql_handle = c.sql_handle cross apply sys.dm_exec_sql_text(c.sql_handle) stxt cross apply sys.dm_exec_query_plan(qst.plan_handle) splan where c.cursor_id={1}", session_id, cursor_id);
            return Execute(cmdText, Execute_getQueryPlans, cursor_id);
        }
        public IEnumerable<FetchSqlPlan> getQueryPlans_fromQH(ulong query_hash, ulong query_plan_hash)
        {
            string cmdText = string.Format("select top 1 stxt.text, qst.query_hash, qst.query_plan_hash, qst.plan_handle, splan.query_plan from sys.dm_exec_query_stats qst cross apply sys.dm_exec_sql_text(qst.sql_handle) stxt cross apply sys.dm_exec_query_plan(qst.plan_handle) splan where qst.query_hash = 0x{0:X16} and qst.query_plan_hash=0x{1:X16}", query_hash, query_plan_hash);
            return Execute(cmdText, Execute_getQueryPlans_fromQH, null);
        }
    }
}
