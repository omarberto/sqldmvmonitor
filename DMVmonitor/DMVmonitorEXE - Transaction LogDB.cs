﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DMVmonitor
{
    internal class DMVmonitorEXE_TL
    {
        internal DMVmonitorEXE_TL()
        {
        }

        public static bool active = true;

        internal void DoWork()
        {
            Logging.Log.Instance.Trace("DMVmonitorEXE_TL starting");

            Reader2();
        }
        

        private static string sql_host;
        private static string sql_instance;
        //private static string currentSpid;
        private static Dictionary<int, string> databases;

        public class directReader
        {
            public string Name;

            public int n = 0;
            public int polling_interval;
            public SqlCommand command;

            public DMVmonitor.Configuration.DX.EnumD EnumsD;
            public DMVmonitor.Configuration.DX.MapD MapsD;

            public string formatString;
            [Nett.TomlIgnore]
            public bool simpleVersion { get { return !string.IsNullOrEmpty(formatString); } }
            public DMVmonitor.Configuration.DX.EnumTest.InfluxFormat[] influx;

            public string tags_FormatString;
            public DMVmonitor.Configuration.DX.EnumTest.InfluxFormat[] tags;
            public string fields_FormatString;
            public DMVmonitor.Configuration.DX.EnumTest.InfluxFieldFormat[] fields;
            public DMVmonitor.Configuration.DX.EnumTest.InfluxOrderFunction[] orderby;
            public int timeindex;

            public directReader(string Name, DMVmonitor.Configuration.DX.EnumTest config, SqlConnection connection)
            {
                this.Name = Name;

                this.polling_interval = config.polling_interval / 1000;
                this.command = connection.CreateCommand();
                this.command.CommandText = config.sql;
                this.command.CommandType = CommandType.Text;

                this.formatString = config.formatString;
                this.influx = config.influx;
                this.EnumsD = config.EnumsD;
                this.MapsD = config.MapsD;

                this.tags_FormatString = config.tags_FormatString;
                this.tags = config.tags;
                this.fields_FormatString = config.fields_FormatString;
                this.fields = config.fields;

                this.orderby = config.orderbyFunc;

                this.timeindex = config.timeindex;

            }

            public void Close()
            {
                this.command.Connection.Close();
            }
        }

        public class directReaderGroup
        {
            public directReaderGroup(DMVmonitor.Configuration.DX.DynamicDMV config)
            {
                SqlConnection connection = new SqlConnection(config.connectionString);
                connection.Open();

                foreach (var DX in config.DX)
                    readers.Add(new directReader(DX.Key, DX.Value, connection));
            }
            public directReaderGroup(string name, DMVmonitor.Configuration.DX.DynamicDMV config)
            {
                SqlConnection connection = new SqlConnection(config.connectionString);
                connection.Open();

                foreach (var DX in config.DX)
                    readers.Add(new directReader(name + "." + DX.Key, DX.Value, connection));
            }

            public List<directReader> readers { get; private set; } = new List<directReader>();
        }

        private void Reader2()
        {
            bool headblockers_active = !DMVmonitor.Configuration.Configuration.config.DMV_HB.disabled;
            Logging.Log.Instance.Debug("STATUS {0} {1}"
                , headblockers_active
                , DMVmonitor.Configuration.Configuration.config.dmv.DXValues.Count);

            int n_HB = 0;

            int pollinginterval_HB = headblockers_active ? DMVmonitor.Configuration.Configuration.config.DMV_HB.polling_interval / 1000 : 0;

            //occhio, gestisci ndatabases --> meglio se senza initialCatalog
            //using (var sqlConnection = new SqlConnection(DMVmonitor.Configuration.Configuration.config.dmv.connectionString))


            //var connection = new SqlConnection(config.connectionString);
            //connection.Open();


            if (string.IsNullOrEmpty(DMVmonitorEXE_TL.sql_host) || string.IsNullOrEmpty(DMVmonitorEXE_TL.sql_instance))
            {
                using (var sqlConnection = new SqlConnection(DMVmonitor.Configuration.Configuration.config.dmv.connectionString))
                {

                    sqlConnection.Open();
                    var globalTags = new GlobalTags(sqlConnection);
                    DMVmonitorEXE_TL.sql_host = globalTags.sql_host;
                    DMVmonitorEXE_TL.sql_instance = globalTags.sql_instance;
                    //DMVmonitorEXE_TL.currentSpid = Read_Spid(sqlConnection);
                    DMVmonitorEXE_TL.databases = Read_DatabaseNames(sqlConnection);
                }
            }
            {
                var DirectSQL1_Array =
                    DMVmonitor.Configuration.Configuration.config.dmv.DXValues.Count == 0 ?
                    new Dictionary<string, directReader>()
                    :
                    DMVmonitor.Configuration.Configuration.config.dmv.DXValues.Select(DXValue => new directReaderGroup(DXValue.Key, DXValue.Value).readers)
                    .SelectMany(dr => dr).ToDictionary(dr => dr.Name, dr => dr);
                
                var sqlConnectionHB = new SqlConnection(DMVmonitor.Configuration.Configuration.config.dmv.connectionString);
                sqlConnectionHB.Open();
                
                Miscellaneous.MovingAverage avg =
                    Outputs.Configuration.Configuration.config.output.useTornado?
                    new Miscellaneous.MovingAverage(Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_avg_window)
                    :
                    null;
                System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                while (active)
                {
                    if (headblockers_active && n_HB++ % pollinginterval_HB == 0)
                        Read_HB(sqlConnectionHB, DMVmonitor.Configuration.Configuration.config.DMV_HB.min_wait_duration, DMVmonitor.Configuration.Configuration.config.DMV_HB);
                    
                    foreach (var DirectSQL1 in DirectSQL1_Array)
                    {
                        if (DirectSQL1.Value.n++ % DirectSQL1.Value.polling_interval == 0) Read_Direct(DirectSQL1.Key, DirectSQL1.Value);
                    }
                    
                    long deltaMilliseconds = 1000L - stopwatch.ElapsedMilliseconds;
                    if (deltaMilliseconds < 0L)
                    {
                        if (Outputs.Configuration.Configuration.config.output.useTornado)
                        {
                            avg.ComputeAverage(deltaMilliseconds / -1000L);
                            Tornado.TornadoEnpointData.values.seconds_delay_dmv_queries = Convert.ToInt32(avg.Average);
                        }
                    }
                    else
                    {
                        Thread.Sleep(Convert.ToInt32(deltaMilliseconds));
                    }
                    stopwatch.Reset();
                }
                stopwatch.Stop();

                Logging.Log.Instance.Debug("DMVmonitorEXE_TL.DoWork Closing");
                //
                sqlConnectionHB.Close();
                foreach (var el in DirectSQL1_Array)
                {
                    el.Value.Close();
                }
                Logging.Log.Instance.Debug("DMVmonitorEXE_TL.DoWork Closed");
            }

            //if (Config.TomlConfiguration.config.dmvmonitorProbe_LT.toNATS)
            //    RawStats.Close(Config.TomlConfiguration.config.dmvmonitorProbe_LT.toNATS);

        }
        
        private string Read_Spid(SqlConnection sqlConnection)
        {
            string result = string.Empty;
            using (SqlCommand cmd = sqlConnection.CreateCommand())
            {
                cmd.CommandText = "set nocount on; set transaction isolation level read uncommitted; select @@spid;";
                cmd.CommandType = CommandType.Text;

                try
                {
                    result = cmd.ExecuteScalar() as string;
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Trace(ex.Message);
                }
            }
            return result;
        }
        private Dictionary<int, string> Read_DatabaseNames(SqlConnection sqlConnection)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            using (SqlCommand cmd = sqlConnection.CreateCommand())
            {
                cmd.CommandText = "select database_id,name from master.sys.databases";
                cmd.CommandType = CommandType.Text;

                try
                {

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(reader.GetSqlInt32(0).Value, reader.GetSqlString(1).Value);
                            }
                        }
                        catch (SqlException ex)
                        {

                        }
                        catch (Exception ex)
                        {
                            Logging.Log.Instance.Trace("ex: {1}\n{0}", ex.Message, ex.GetType());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Trace(ex.Message);
                }
            }
            return result;
        }
        
        private class DMVheadblocksSession
        {
            public long polling_time { get; private set; }

            public int session_id { get; private set; }
            public string login_name { get; private set; }
            public string host_name { get; private set; }
            public string program_name { get; private set; }
            public string wait_resource { get; private set; }
            public int wait_time { get; private set; }
            public int wait_time_insec { get { return wait_time / 1000; } }
            public string wait_type { get; private set; }
            public long? transaction_id { get; private set; }
            private int database_id;
            public string database_name
            {
                get
                {
                    string name = string.Empty;
                    DMVmonitorEXE_TL.databases.TryGetValue(database_id, out name);
                    return name;
                }
            }
            private DMVmonitor.Events.AxContextInfo context_info;
            public string ax_user { get { return context_info.AxUserName; } }
            public string ax_session { get { return context_info.AxSessionId > 0 ? context_info.AxSessionId.ToString() : "-"; } }
            public int blocking_session_id { get; private set; }
            public bool is_root { get { return blocking_session_id == 0; } }

            public DMVheadblocksSession(SqlDataReader reader)
            {
                this.session_id = reader.GetSqlInt16(0).Value;
                this.login_name = reader.IsDBNull(1) ? string.Empty : reader.GetSqlString(1).Value;
                this.host_name = reader.IsDBNull(2) ? string.Empty : reader.GetSqlString(2).Value;
                this.program_name = reader.IsDBNull(3) ? string.Empty : reader.GetSqlString(3).Value;
                this.wait_resource = reader.IsDBNull(4) ? string.Empty : reader.GetSqlString(4).Value;
                this.wait_time = reader.IsDBNull(5) ? 0 : reader.GetSqlInt32(5).Value;//forse va bene anche 0
                this.wait_type = reader.IsDBNull(6) ? string.Empty : reader.GetSqlString(6).Value;
                this.transaction_id = reader.IsDBNull(7) ? new long?() : reader.GetSqlInt64(7).Value;//??? null?? --> sembra di si
                this.database_id = reader.IsDBNull(8) ? 0 : reader.GetSqlInt16(8).Value; //??? null?? --> sembra di si
                this.context_info = reader.GetSqlBytes(9).IsNull ? new DMVmonitor.Events.AxContextInfo("") : new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(reader.GetSqlBytes(9).Value));
                this.blocking_session_id = reader.IsDBNull(10) ? 0 : reader.GetSqlInt16(10).Value; //??? null?? --> sembra di si

                this.polling_time = new EpochNanoseconds(reader.GetSqlDateTime(11).Value.ToUniversalTime()).Value;//sistemeremo offset....
            }
        }

        private class DMVheadblockerTreenode
        {
            private readonly DMVheadblocksSession _session;
            private readonly List<DMVheadblockerTreenode> _children = new List<DMVheadblockerTreenode>();

            public DMVheadblockerTreenode(DMVheadblocksSession session)
            {
                _session = session;
            }

            public DMVheadblockerTreenode this[int i]
            {
                get { return _children[i]; }
            }

            public DMVheadblockerTreenode Parent { get; private set; }

            public DMVheadblocksSession Session { get { return _session; } }

            //public ReadOnlyCollection<DMVheadblockerTreenode> Children
            //{
            //    get { return _children.AsReadOnly(); }
            //}
            
            public bool AddChild(DMVheadblocksSession session)
            {
                if (this.Session.session_id == session.blocking_session_id)
                {
                    var node = new DMVheadblockerTreenode(session) { Parent = this };
                    _children.Add(node);
                    return true;
                }
                else
                {
                    foreach (var child in _children)
                    {
                        if(child.AddChild(session)) return true;
                    }
                }
                return false;
                //var node = new DMVheadblockerTreenode(session) { Parent = this };
                //_children.Add(node);
                //return node;
            }



            /***********************************************************/

            public string toNATS(string measurement)
            {
                /*
                > show tag keys from SQLHeadBlockers
                ------
                AXUser
                ClientHostName
                HeadBlockerIdx  --> boh
                LoginName
                SQLInstance
                customer  --> boh
                host  --> boh


                > show field keys from SQLHeadBlockers
                --------                 ---------
                ALLWaitsInSecList        string
                AXSession                float     --> string
                BlockedAxSessionsChain   string
                BlockedAxUsersChain      string
                BlockedSQLClientHostName string
                BlockedSQLSessionsChain  string
                MaxVictimWaitInSec       float     --> int
                MaxWaitInSec             float     --> int
                NrOfWaitingSessions      float     --> int
                SQLSession               float     --> string
                */
                
                var message = new Influx.FormatMessage(measurement);
                message.AddTag("AXUser", this.Session.ax_user);
                message.AddTag("ClientHostName", this.Session.host_name);
                message.AddTag("LoginName", this.Session.login_name);
                message.AddTag("SQLInstance", DMVmonitorEXE_TL.sql_instance);
                message.AddTag("host", DMVmonitorEXE_TL.sql_host);
                message.AddTag("uniquifier", Uniquifier.uniquifier(UniquifierClass.SQLHeadBlockers, this.Session.polling_time).ToString());//questa parte potremmo inglobarla
                
                message.AddStringField("AXSession", this.Session.ax_session);
                message.AddStringField("program_name", this.Session.program_name);
                message.AddStringField("transaction_id", this.Session.transaction_id.HasValue ? this.Session.transaction_id.Value.ToString() : "");
                message.AddStringField("SQLSession", this.Session.session_id, "{0}");
                message.AddStringField("database_name", this.Session.database_name);
                message.AddIntegerField("max_victim_wait_time", this.max_victim_wait_time);
                message.AddIntegerField("max_wait_time", this.max_wait_time);
                message.AddIntegerField("NrOfWaitingSessions", this.waiting_sessions_count - 1);
                message.AddIntegerField("waiting_sessions_depth", this.waiting_sessions_depth);
                message.AddStringField("BlockedSQLSessionsChain", this.blocked_session_id_chain);
                message.AddStringField("blocked_wait_resource_chain", this.blocked_wait_resource_chain);
                message.AddStringField("blocked_wait_type_chain", this.blocked_wait_type_chain);
                message.AddStringField("BlockedSQLClientHostName", this.blocked_host_name_chain);
                message.AddStringField("BlockedAxSessionsChain", this.blocked_ax_session_chain);
                message.AddStringField("BlockedAxUsersChain", this.blocked_ax_user_chain);
                message.AddStringField("AllWaitsinSecList", this.blocked_wait_time_chain);
                message.AddIntegerField("MaxVictimWaitInSec", this.max_victim_wait_time / 1000);
                message.AddIntegerField("MaxWaitInSec", this.max_wait_time / 1000);

                message.AddEpochNs(this.Session.polling_time);

                return message.value;

                //return string.Format(measurement + ",AXUser={0},ClientHostName={1},LoginName={2},SQLInstance={3},host={4},uniquifier={5} " +
                //                "AXSession=\"{6}\",program_name=\"{7}\",transaction_id=\"{8}\",SQLSession=\"{9}\",database_name=\"{10}\"" +
                //                ",max_victim_wait_time={11}i" +
                //                ",max_wait_time={12}i" +
                //                ",NrOfWaitingSessions={13}i" +
                //                ",waiting_sessions_depth={14}i" +
                //                ",BlockedSQLSessionsChain=\"{15}\"" +
                //                ",blocked_wait_resource_chain=\"{16}\"" +
                //                ",blocked_wait_type_chain=\"{17}\"" +
                //                ",BlockedSQLClientHostName=\"{18}\"" +
                //                ",BlockedAxSessionsChain=\"{19}\"" +
                //                ",BlockedAxUsersChain=\"{20}\"" +
                //                ",AllWaitsinSecList=\"{21}\"" +
                //                ",MaxVictimWaitInSec={22}i" +
                //                ",MaxWaitInSec={23}i" +

                //                " {24}",
                //                this.Session.ax_user, this.Session.host_name, this.Session.login_name, DMVmonitorEXE_TL.sql_instance, DMVmonitorEXE_TL.sql_host, Uniquifier.uniquifier(UniquifierClass.SQLHeadBlockers, this.Session.polling_time),
                                
                //                this.Session.ax_session
                //                , this.Session.program_name
                //                , this.Session.transaction_id.HasValue ? this.Session.transaction_id.Value.ToString() : ""
                //                , this.Session.session_id
                //                , this.Session.database_name
                                
                //                , this.max_victim_wait_time
                //                , this.max_wait_time
                //                , this.waiting_sessions_count - 1
                //                , this.waiting_sessions_depth
                //                , this.blocked_session_id_chain
                //                , this.blocked_wait_resource_chain
                //                , this.blocked_wait_type_chain
                //                , this.blocked_host_name_chain
                //                , this.blocked_ax_session_chain
                //                , this.blocked_ax_user_chain
                //                , this.blocked_wait_time_chain
                //                , this.max_victim_wait_time / 1000
                //                , this.max_wait_time / 1000

                //                , this.Session.polling_time
                //    );
            }
            public string toNATS2(string measurement)
            {
                var message = new Influx.FormatMessage(measurement);
                message.AddTag("sql_instance", DMVmonitorEXE_TL.sql_instance);
                message.AddTag("session_id", this.Session.session_id.ToString());

                message.AddStringField("ax_user", this.Session.ax_user);
                message.AddStringField("host_name", this.Session.host_name);
                message.AddStringField("login_name", this.Session.login_name.Replace("\\", "\\\\"));
                message.AddStringField("ax_session", this.Session.ax_session);
                message.AddStringField("program_name", this.Session.program_name);
                message.AddStringField("transaction_id", this.Session.transaction_id.HasValue ? this.Session.transaction_id.Value.ToString() : "");
                message.AddStringField("database_name", this.Session.database_name);
                message.AddIntegerField("max_victim_wait_time", this.max_victim_wait_time);
                message.AddIntegerField("max_wait_time", this.max_wait_time);
                message.AddIntegerField("waiting_sessions_count", this.waiting_sessions_count - 1);
                message.AddIntegerField("waiting_sessions_depth", this.waiting_sessions_depth);
                message.AddStringField("blocked_session_id_chain", this.blocked_session_id_chain);
                message.AddStringField("blocked_wait_resource_chain", this.blocked_wait_resource_chain);
                message.AddStringField("blocked_wait_type_chain", this.blocked_wait_type_chain);
                message.AddStringField("blocked_host_name_chain", this.blocked_host_name_chain);
                message.AddStringField("blocked_ax_session_chain", this.blocked_ax_session_chain);
                message.AddStringField("blocked_ax_user_chain", this.blocked_ax_user_chain);

                message.AddEpochNs(this.Session.polling_time);

                return message.value;

                //return string.Format(measurement + ",sql_instance={3},session_id={7} " +

                //                "ax_user=\"{0}\",host_name=\"{1}\",login_name=\"{2}\",ax_session=\"{4}\",program_name=\"{5}\",transaction_id=\"{6}\",database_name=\"{8}\"" +
                //                ",max_victim_wait_time={11}i" +
                //                ",max_wait_time={12}i" +
                //                ",waiting_sessions_count={13}i" +
                //                ",waiting_sessions_depth={14}i" +
                //                ",blocked_session_id_chain=\"{15}\"" +
                //                ",blocked_wait_resource_chain=\"{16}\"" +
                //                ",blocked_wait_type_chain=\"{17}\"" +
                //                ",blocked_host_name_chain=\"{18}\"" +
                //                ",blocked_ax_session_chain=\"{19}\"" +
                //                ",blocked_ax_user_chain=\"{20}\"" +

                //                " {21}",
                //                this.Session.ax_user, 
                //                this.Session.host_name, 
                //                this.Session.login_name.Replace("\\", "\\\\"), 
                //                DMVmonitorEXE_TL.sql_instance,
                //                this.Session.ax_session, 
                //                this.Session.program_name, 
                //                this.Session.transaction_id.HasValue ? this.Session.transaction_id.Value.ToString() : "", 
                //                this.Session.session_id, 
                //                this.Session.database_name
                //                , "", ""
                //                , this.max_victim_wait_time
                //                , this.max_wait_time
                //                , this.waiting_sessions_count - 1
                //                , this.waiting_sessions_depth
                //                , this.blocked_session_id_chain
                //                , this.blocked_wait_resource_chain
                //                , this.blocked_wait_type_chain
                //                , this.blocked_host_name_chain
                //                , this.blocked_ax_session_chain
                //                , this.blocked_ax_user_chain

                //                , this.Session.polling_time
                //    );

            }


            public int max_victim_wait_time
            {
                get
                {
                    var value = _children.Count > 0 ? _children.Max(c => c.max_victim_wait_time) : 0;
                    if (Parent != null)
                        return Session.wait_time > value ? Session.wait_time : value;
                    else
                        return value;
                }
            }
            public int max_wait_time
            {
                get
                {
                    return max_victim_wait_time > Session.wait_time ? max_victim_wait_time : Session.wait_time;
                }
            }
            public int waiting_sessions_count
            {
                get
                {
                    return _children.Count > 0 ? (1 + _children.Sum(c => c.waiting_sessions_count)) : 1;
                }
            }
            public int waiting_sessions_depth
            {
                get
                {
                    return _children.Count > 0 ? (1 + _children.Max(c => c.waiting_sessions_depth)) : 0;
                }
            }

            public string blocked_session_id_chain
            {
                get
                {
                    return Session.session_id.ToString() + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_session_id_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
            public string blocked_host_name_chain
            {
                get
                {
                    return Session.host_name.ToString() + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_host_name_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
            public string blocked_wait_time_chain
            {
                get
                {
                    //espresso in secondi
                    return Session.wait_time_insec.ToString() + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_wait_time_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
            public string blocked_wait_resource_chain
            {
                get
                {
                    return Session.wait_resource.ToString() + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_wait_resource_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
            public string blocked_wait_type_chain
            {
                get
                {
                    return Session.wait_type.ToString() + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_wait_type_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }

            public string blocked_ax_session_chain
            {
                get
                {
                    return Session.ax_session + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_ax_session_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
            public string blocked_ax_user_chain
            {
                get
                {
                    return Session.ax_user + (_children.Count > 0 ? " >> " : "") + (_children.Count > 1 ? "(" : "") + _children.Select(child => child.blocked_ax_user_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (_children.Count > 1 ? ")" : "");
                }
            }
        }

        private void Read_HB(SqlConnection sqlConnection, int min_wait_duration, DMVmonitor.Configuration.DX.DMV_HB DMV_HB)
        {
            Logging.Log.Instance.Trace("Read_HB");
            try
            {
                if (sqlConnection.State != ConnectionState.Open)
                {
                    Logging.Log.Instance.Debug("Read_HB opening connection {0}", sqlConnection.State);
                    sqlConnection.Open();
                    //currentSpid = Read_Spid(sqlConnection);
                    //Logging.Log.Instance.Trace("opened connection spid {0}", currentSpid);
                }

                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText =
                        "DECLARE @pollingTime datetime; set @pollingTime=GETDATE(); " +
                                    "select " +
                                    "d_es.session_id," +
                                    "d_es.login_name, d_es.host_name, d_es.program_name," +
                                    "d_er.wait_resource, d_er.wait_time, d_er.wait_type," +
                                    "d_er.transaction_id, d_es.database_id," +
                                    "d_es.context_info," +
                                    "d_er.blocking_session_id," +
                                    "@pollingTime" +
                                    " from sys.dm_exec_sessions as d_es" +
                                    " left outer join sys.dm_exec_requests as d_er on d_es.session_id = d_er.session_id" +
                            " option (loop join)";

                    cmd.CommandType = CommandType.Text;


                    List<DMVheadblocksSession> allsessions = new List<DMVheadblocksSession>();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                var res = new DMVheadblocksSession(reader);
                                allsessions.Add(res);
                            }
                                                    }
                        catch (SqlException ex)
                        {
                            Logging.Log.Instance.Error("Read_HB sql ex: {0}", ex.Message);
                            Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                        }
                        catch (Exception ex)
                        {
                            Logging.Log.Instance.Error("Read_HB ex: {1}\n{0}", ex.Message, ex.GetType());
                        }
                    }

                    var blockchains = allsessions.Where(el => el.is_root).ToList();
                    var unassingedsessions = allsessions.Where(el => !el.is_root).ToDictionary(el=>el.session_id);
                    Logging.Log.Instance.Trace("HEADBLOCKER LOOP {0} and {1}", unassingedsessions.Count, blockchains.Count);

                    //creo gruppi con sessioni bloccate
                    //creo dictionary livello//sessioni 
                    
                    List<DMVheadblockerTreenode> DMVheadblockerTree = new List<DMVheadblockerTreenode>();
                    List<int> torem = new List<int>();
                    DMVheadblockerTree.AddRange(blockchains.Select(el => new DMVheadblockerTreenode(el)));
                    while (unassingedsessions.Count > 0)
                    {
                        foreach (var unassingedsession in unassingedsessions)
                        {
                            foreach (var headbl in DMVheadblockerTree)
                            {
                                if (headbl.AddChild(unassingedsession.Value))
                                    torem.Add(unassingedsession.Key);
                            }
                        }
                        foreach (var id in torem)
                            unassingedsessions.Remove(id);

                        if (torem.Count == 0) break;

                        torem.Clear();
                    }
                    
                    //qui verifichiamo quante sono null
                    foreach (var res1 in DMVheadblockerTree.Where(el => el.waiting_sessions_depth > 0))
                    {
                        Logging.Log.Instance.Trace("HEADBLOCKER {0}: {1} {2}", res1.Session.session_id, res1.max_wait_time, (res1.max_wait_time > min_wait_duration * 1000));
                        if (res1.max_wait_time > min_wait_duration * 1000)
                        {
                            Logging.Log.Instance.Trace("HEADBLOCKER {0}: {1} {2} {3}", res1.Session.session_id, res1.max_wait_time, DMV_HB.toNATS1, DMV_HB.toNATS2);
                            if (DMV_HB.toNATS1)
                            {
                                string snats1 = res1.toNATS(DMV_HB.measurementType1);
                                Logging.Log.Instance.Trace("HEADBLOCKER {0}", snats1);


                                //TODO: add rpTag
                                DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(snats1);
                            }
                            if (DMV_HB.toNATS2)
                            {
                                string rpTag = Outputs.Configuration.Configuration.config.output.nats.getRetentionTag(DMV_HB.measurementType1);
                                string snats2 = res1.toNATS2(DMV_HB.measurementType2 + rpTag);
                                Logging.Log.Instance.Trace("HEADBLOCKER {0}", snats2);

                                //TODO: add rpTag
                                DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(snats2);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Error("Read_HB ConnectionState {1} : {0}", ex.Message, sqlConnection.State);
            }
        }
        

        private void Read_Direct(string input_name, directReader dr)
        {

            Logging.Log.Instance.Info("Read_Direct: {0}", input_name);
            try
            {
                if (dr.command.Connection.State != ConnectionState.Open)
                {
                    Logging.Log.Instance.Info("Read_HB opening connection {0}", dr.command.Connection.State);
                    dr.command.Connection.Open();
                    //currentSpid = Read_Spid(sqlConnection);
                    //Logging.Log.Instance.Trace("opened connection spid {0}", currentSpid);
                }

                List<DMVdirectREAD> values = new List<DMVdirectREAD>();
                using (SqlDataReader reader = dr.command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            var res = new DMVdirectREAD(reader, dr);
                            values.Add(res);
                        }
                    }
                    catch (SqlException ex)
                    {
                        Logging.Log.Instance.Error("SqlException: {0}", ex.Message);
                        Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("Read_Direct ex: {1}\n{0}", ex.Message, ex.GetType());
                    }

                    //comincio con vecchia versione per vedere se funziona ancora uguale
                    //tolgo if else per avere tutti e due
                    if (dr.simpleVersion)
                    {
                        List<DMVdirectREAD.PrintVers1result> results = new List<DMVdirectREAD.PrintVers1result>();
                        foreach (var value in values)
                        {
                            results.Add(value.PrintVers1(dr));
                        }
                        foreach (var gvalue in results.GroupBy(result => result.uniquifierString))
                        {
                            uint uniquifierId = 0;
                            foreach (var value in gvalue)
                            {
                                //TODO
                                value.results.Add(uniquifierId.ToString());
                                uniquifierId++;
                                
                                string snats = string.Format(dr.formatString, value.results.ToArray());
                                int i1 = snats.IndexOf(",");
                                if (i1 > 0)
                                {
                                    snats = snats.Insert(i1, Outputs.Configuration.Configuration.config.output.nats.getRetentionTag(dr.Name));
                                }
                                Logging.Log.Instance.Info("ReadDirect {0}", snats);

                                //TODO: add rpTag
                                DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(snats);
                            }
                        }

                        
                    }
                    else
                    {
                        //qui uniquifier per ora non lo metto, visto che aggregate è sui tags e quindi vado a perderlo, devo elaborare strategia migliore
                        foreach (var g in values.GroupBy(el => el.tags))
                        {
                            var orderedvalues = g.Select(el => el.values);
                            if(dr.orderby.Length>0)
                                orderedvalues = DMVdirectREAD.OrderBy(dr, g.Select(el => el.values));
                            
                            var gvalues = DMVdirectREAD.Aggregate(dr, orderedvalues);
                            
                            string rpTag = Outputs.Configuration.Configuration.config.output.nats.getRetentionTag(dr.Name);
                            string snats = g.Key + rpTag + " " + string.Format(dr.fields_FormatString, gvalues.ToArray()) + " " + g.First().time;
                            
                            Logging.Log.Instance.Info("ReadDirect {0}", snats);

                            //TODO: add rpTag
                            DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(snats);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Error("Read_Direct ConnectionState {1} : {0}", ex.Message, dr.command.Connection.State);
            }
        }
        
        public class DMVdirectREAD
        {
            public object[] values { private set; get; }
            public string tags { get; private set; }
            public Configuration.DX.EnumTest.InfluxOrderFunction[] orderby { get; private set; }
            public string time { get; private set; }

            
            //context_info: opzione 1 si fa in tsql, opzione 2 si fa un elemento speciale in sonda, opzione 3 si fa in sonda con funzioni di parsing




            //polling time: qui fare post processing specifico

            //database_name: opzione 1 TSQL, opzione 2 creare dei mapping iniziali....

            //il problema è come tradurre in toml tutto il formaTTING EVOLUTO...


            public DMVdirectREAD(SqlDataReader reader, directReader dr)
            {
                object[] tmp = new object[reader.FieldCount];
                reader.GetValues(tmp);
                
                if (dr.simpleVersion)
                {
                    this.values = tmp;
                }
                else
                {
                    this.tags = PrintTags(dr, tmp);
                    this.values = EvaluateFields(dr, tmp).ToArray();
                    this.time = new EpochNanoseconds(((DateTime)tmp[dr.timeindex]).ToUniversalTime()).Value.ToString();
                    this.orderby = dr.orderby;
                }
            }

            private string FieldEscaping(string s)
            {
                return s.Replace(@"\", @"\\");
            }
            private string TagEscaping(string s)
            {
                if (string.IsNullOrEmpty(s)) s = " ";
                return s.Replace(@" ", @"\ ").Replace(@"=", @"\=").Replace(@",", @"\,");
            }
            private string PrintTags(directReader dr, params object[] args)
            {
                List<string> results = new List<string>();
                foreach (var influx in dr.tags)
                {
                    string res = string.Empty;
                    switch (influx.type)
                    {
                        case "object":
                            res = args[influx.index].ToString();
                            if (influx.itemFormat && !string.IsNullOrEmpty(res))
                            {
                                res = string.Format(influx.formatString, res);
                            }
                            break;
                        case "double":
                            res = Convert.ToDouble(args[influx.index]).ToString(System.Globalization.CultureInfo.InvariantCulture);
                            break;
                        case "database_id":
                            res = DMVmonitorEXE_TL.databases[Convert.ToInt32(args[influx.index])];
                            break;
                        case "sql_instance":
                            res = DMVmonitorEXE_TL.sql_instance;
                            break;
                        case "sql_host":
                            res = DMVmonitorEXE_TL.sql_host;
                            break;
                        case "ax_context_info.ax_session":
                            res = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(args[influx.index] as byte[])).AxSessionId.ToString();
                            break;
                        case "ax_context_info.ax_user":
                            res = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(args[influx.index] as byte[])).AxUserName;
                            break;
                        case "enum":
                            res = dr.EnumsD.Get(influx.enumname, Convert.ToInt32(args[influx.index]));
                            break;
                        case "map":
                            res = dr.MapsD.Get(influx.enumname, Convert.ToString(args[influx.index]));
                            break;
                        case "time":
                            res = new EpochNanoseconds(((DateTime)args[influx.index]).ToUniversalTime()).Value.ToString();
                            break;
                    }
                    
                    if (influx.tagEscape)
                    {
                        res = TagEscaping(res);
                    }

                    results.Add(res);
                }

                return string.Format(dr.tags_FormatString, results.ToArray());

                //qui faccio sprintf finale e ho finito!!!
            }
            private List<object> EvaluateFields(directReader dr, params object[] args)
            {
                List<object> results = new List<object>();
                foreach (var influx in dr.fields)
                {
                    object value = null;
                    
                    switch (influx.type)
                    {
                        case "double":
                            value = args[influx.index] == DBNull.Value ? 0D : Convert.ToDouble(args[influx.index]);
                            break;



                        case "int32":
                            value = args[influx.index] == DBNull.Value ? 0 : Convert.ToInt32(args[influx.index]);
                            break;
                        case "int64":
                            value = args[influx.index] == DBNull.Value ? 0L : Convert.ToInt64(args[influx.index]);
                            break;



                        case "object":
                            string s = args[influx.index].ToString();
                            if (influx.itemFormat && !string.IsNullOrEmpty(s))
                            {
                                value = string.Format(influx.formatString, s);
                            }
                            break;
                            

                        case "ax_context_info.ax_session":
                            value = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(args[influx.index] as byte[])).AxSessionId.ToString();
                            break;
                        case "ax_context_info.ax_user":
                            value = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(args[influx.index] as byte[])).AxUserName;
                            break;
                        case "database_id":
                            value = DMVmonitorEXE_TL.databases[Convert.ToInt32(args[influx.index])];
                            break;
                        case "sql_instance":
                            value = DMVmonitorEXE_TL.sql_instance;
                            break;
                        case "sql_host":
                            value = DMVmonitorEXE_TL.sql_host;
                            break;
                        case "enum":
                            value = dr.EnumsD.Get(influx.enumname, Convert.ToInt32(args[influx.index]));
                            break;
                        case "map":
                            value = dr.MapsD.Get(influx.enumname, Convert.ToString(args[influx.index]));
                            break;
                    }
                    
                    results.Add(value);
                }
                
                return results;

                //qui faccio sprintf finale e ho finito!!!
            }

            internal static List<string> Aggregate(directReader dr, IGrouping<string, DMVdirectREAD> group)
            {
                List<string> retValues = new List<string>();
                for(int i = 0; i < dr.fields.Length; i++)
                {
                    var gvalues = group.Select(el => el.values[i]);

                    foreach (var aggr in dr.fields[i].aggrList)
                    {
                        object tmp = null;
                        switch (aggr.type)
                        {
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Count:
                                tmp = gvalues.Count();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Concat:
                                tmp = gvalues.Select(el => el.ToString()).Aggregate("", (t, el) => t = t + el + "|").TrimEnd('|');
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.First:
                                tmp = gvalues.First();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.FirstAsc:
                                tmp = group.OrderBy(el => el.values[aggr.extref]).First().values[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.FirstDesc:
                                tmp = group.OrderByDescending(el => el.values[aggr.extref]).First().values[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Last:
                                tmp = gvalues.Last();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.LastAsc:
                                tmp = group.OrderBy(el => el.values[aggr.extref]).Last().values[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.LastDesc:
                                tmp = group.OrderByDescending(el => el.values[aggr.extref]).Last().values[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Max:
                                tmp = gvalues.Max();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Min:
                                tmp = gvalues.Min();
                                break;
                        }
                        retValues.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture,"{0}", tmp));
                    }
                }
                return retValues;
            }

            internal static List<string> Aggregate(directReader dr, IEnumerable<object[]> values)
            {
                List<string> retValues = new List<string>();
                for (int i = 0; i < dr.fields.Length; i++)
                {
                    var gvalues = values.Select(el => el[i]);

                    foreach (var aggr in dr.fields[i].aggrList)
                    {
                        object tmp = null;
                        switch (aggr.type)
                        {
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Count:
                                tmp = gvalues.Count();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Concat:
                                tmp = gvalues.Select(el => el == null? string.Empty : el.ToString()).Aggregate("", (t, el) => t = t + el + "|").TrimEnd('|');
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.First:
                                tmp = gvalues.First();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.FirstAsc:
                                tmp = values.OrderBy(array => array[aggr.extref]).First()[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.FirstDesc:
                                tmp = values.OrderByDescending(array => array[aggr.extref]).First()[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Last:
                                tmp = gvalues.Last();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.LastAsc:
                                tmp = values.OrderBy(array => array[aggr.extref]).Last()[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.LastDesc:
                                tmp = values.OrderByDescending(array => array[aggr.extref]).Last()[i];
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Max:
                                tmp = gvalues.Max();
                                break;
                            case DMVmonitor.Configuration.DX.EnumTest.FieldAggregationFunctionTypes.Min:
                                tmp = gvalues.Min();
                                break;
                        }
                        if (tmp == null) tmp = string.Empty;
                        retValues.Add(string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}", tmp));
                    }
                }
                return retValues;
            }
            internal static IEnumerable<object[]> OrderBy(directReader dr, IEnumerable<object[]> fields)
            {
                foreach (var orderby in dr.orderby)
                {
                    if (orderby.isdesc)
                        fields = fields.OrderByDescending(el => el[orderby.index]);
                    else
                        fields = fields.OrderBy(el => el[orderby.index]);
                }
                return fields;
            }

            public class PrintVers1result
            {
                public string uniquifierString = string.Empty;
                public List<string> results = new List<string>();

            }
            public PrintVers1result PrintVers1(directReader dr)
            {
                PrintVers1result result = new PrintVers1result();

                foreach (var influx in dr.influx)
                {
                    string res = string.Empty;
                    switch (influx.type)
                    {
                        case "object":
                            res = this.values[influx.index].ToString();
                            if (influx.itemFormat && !string.IsNullOrEmpty(res))
                            {
                                res = string.Format(influx.formatString, res);
                            }
                            break;
                        case "double":
                            res = Convert.ToDouble(this.values[influx.index]).ToString(System.Globalization.CultureInfo.InvariantCulture);
                            break;
                        case "database_id":
                            res = DMVmonitorEXE_TL.databases[Convert.ToInt32(this.values[influx.index])];
                            break;
                        case "sql_instance":
                            res = DMVmonitorEXE_TL.sql_instance;
                            break;
                        case "sql_host":
                            res = DMVmonitorEXE_TL.sql_host;
                            break;
                        case "ax_context_info.ax_session":
                            res = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(this.values[influx.index] as byte[])).AxSessionId.ToString();
                            break;
                        case "ax_context_info.ax_user":
                            res = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(this.values[influx.index] as byte[])).AxUserName;
                            break;
                        case "enum":
                            res = dr.EnumsD.Get(influx.enumname, Convert.ToInt32(this.values[influx.index]));
                            break;
                        case "map":
                            res = dr.MapsD.Get(influx.enumname, Convert.ToString(this.values[influx.index]));
                            break;
                        case "time":
                            res = new EpochNanoseconds(((DateTime)this.values[influx.index]).ToUniversalTime()).Value.ToString();
                            break;
                    }

                    //qui metti eventuali specialità come escaping
                    if (influx.fieldEscape)
                    {
                        res = FieldEscaping(res);
                    }
                    if (influx.tagEscape)
                    {
                        res = TagEscaping(res);
                    }
                    if (influx.uniquifier)
                    {
                        result.uniquifierString += res;
                    }

                    result.results.Add(res);
                }

                return result;

                //qui faccio sprintf finale e ho finito!!!
            }
        }
        
        
        public class DMVblockChain
        {
            public long polling_time { get; private set; }
            public int session_id { get; private set; }
            public string login_name { get; private set; }
            public string host_name { get; private set; }
            public string program_name { get; private set; }
            public string wait_resource { get; private set; }
            public int wait_time { get; private set; }
            public string wait_type { get; private set; }
            public long? transaction_id { get; private set; }
            //public enum TransactionIsolationLevel : short
            //{
            //    [Display(Name = "Unspecified")]
            //    Unspecified = 0,
            //    [Display(Name = "ReadUncomitted")]
            //    ReadUncomitted = 1,
            //    [Display(Name = "ReadCommitted")]
            //    ReadCommitted = 2,
            //    [Display(Name = "Repeatable")]
            //    Repeatable = 3,
            //    [Display(Name = "Serializable")]
            //    Serializable = 4,
            //    [Display(Name = "Snapshot")]
            //    Snapshot = 5
            //}
            //public TransactionIsolationLevel transaction_isolation_level { get; private set; }
            public int database_id { get; private set; }
            public string database_name
            {
                get
                {
                    string name = string.Empty;
                    DMVmonitorEXE_TL.databases.TryGetValue(database_id, out name);
                    return name;
                }
            }

            //public long? last_batch { get; private set; }
            //public long ns_wait_time { get { return last_batch.HasValue ? (polling_time - last_batch.Value) : 0L; } }
            //public int wait_seconds { get; private set; }

            //varbinary(128)
            public DMVmonitor.Events.AxContextInfo context_info { get; private set; }
            public string ax_user { get { return context_info.AxUserName; } }
            public string ax_session { get { return context_info.AxSessionId > 0 ? context_info.AxSessionId.ToString() : "-"; } }

            //public byte[] last_sql_handle { get; private set; }
            //public string last_sql_handle_HEX
            //{
            //    get
            //    {
            //        return "0x" + BitConverter.ToString(last_sql_handle).Replace("-", "");
            //    }
            //}
            //public byte[] current_sql_handle { get; private set; }
            //public string current_sql_handle_HEX
            //{
            //    get
            //    {
            //        return "0x" + BitConverter.ToString(current_sql_handle).Replace("-", "");
            //    }
            //}

            public int blocking_session_id { get; private set; }
            public bool is_root { get { return blocking_session_id == 0; } }
            //public int root_session_id { get; private set; }
            public int block_level { get; private set; } = 1;
            private int max_block_level = 1;

            public DMVblockChain(SqlDataReader reader)
            {
                this.session_id = reader.GetSqlInt16(0).Value;
                this.login_name = reader.IsDBNull(1) ? string.Empty : reader.GetSqlString(1).Value;
                this.host_name = reader.IsDBNull(2) ? string.Empty : reader.GetSqlString(2).Value;
                this.program_name = reader.IsDBNull(3) ? string.Empty : reader.GetSqlString(3).Value;

                this.wait_resource = reader.IsDBNull(4) ? string.Empty : reader.GetSqlString(4).Value;
                this.wait_time = reader.IsDBNull(5) ? 0 : reader.GetSqlInt32(5).Value;//forse va bene anche 0
                this.wait_type = reader.IsDBNull(6) ? string.Empty : reader.GetSqlString(6).Value;

                this.transaction_id = reader.IsDBNull(7) ? new long?() : reader.GetSqlInt64(7).Value;//??? null?? --> sembra di si
                //this.transaction_isolation_level = reader.IsDBNull(8) ? TransactionIsolationLevel.Unspecified : (TransactionIsolationLevel)reader.GetSqlInt16(8).Value;
                this.database_id = reader.IsDBNull(8) ? 0 : reader.GetSqlInt16(8).Value; //??? null?? --> sembra di si

                //this.last_batch = reader.IsDBNull(10) ? new long?() : new EpochNanoseconds(reader.GetSqlDateTime(10).Value.ToUniversalTime()).Value;
                //this.wait_seconds = reader.GetSqlInt32(11).Value; //??? null?? --> sembra di si

                this.context_info = reader.GetSqlBytes(9).IsNull ? new DMVmonitor.Events.AxContextInfo("") : new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(reader.GetSqlBytes(9).Value));
                
                //this.last_sql_handle = reader.IsDBNull(13) ? new byte[0] : reader.GetSqlBytes(13).Value;
                //this.current_sql_handle = reader.IsDBNull(14) ? new byte[0] : reader.GetSqlBytes(14).Value;

                this.blocking_session_id = reader.IsDBNull(10) ? 0 : reader.GetSqlInt16(10).Value; //??? null?? --> sembra di si
                //this.root_session_id = reader.IsDBNull(11) ? 0 : reader.GetSqlInt16(11).Value; //??? null?? --> sembra di si
                this.block_level = 1; //??? null?? --> sembra di si
                this.check_session_id = session_id;

                this.polling_time = new EpochNanoseconds(reader.GetSqlDateTime(11).Value.ToUniversalTime()).Value;//sistemeremo offset....
                //Logging.Log.Instance.Trace("FINE CAST");
            }

            public string toNATS()
            {
                return string.Format("sqlDMV_HB,ax_user={0},host_name={1},login_name={2},sql_instance={3} " +

                                "session_id=\"{7}\",ax_session=\"{4}\",program_name=\"{5}\",transaction_id=\"{6}\",database_name=\"{8}\"" +
                                ",max_victim_wait_time={11}i" +
                                ",max_wait_time={12}i" +
                                ",waiting_sessions_count={13}i" +
                                ",waiting_sessions_depth={14}i" +
                                ",blocked_session_id_chain=\"{15}\"" +
                                ",blocked_wait_resource_chain=\"{16}\"" +
                                ",blocked_wait_type_chain=\"{17}\"" +
                                ",blocked_host_name_chain=\"{18}\"" +
                                ",blocked_ax_session_chain=\"{19}\"" +
                                ",blocked_ax_user_chain=\"{20}\"" +

                                " {21}",
                                this.ax_user, this.host_name, this.login_name, DMVmonitorEXE_TL.sql_instance,
                                this.ax_session, this.program_name, this.transaction_id.HasValue ? this.transaction_id.Value.ToString() : "", this.session_id, this.database_name
                                , "", ""
                                , this.max_victim_wait_time
                                , this.max_wait_time
                                , this.waiting_sessions_count - 1
                                , this.waiting_sessions_depth
                                , this.blocked_session_id_chain
                                , this.blocked_wait_resource_chain
                                , this.blocked_wait_type_chain
                                , this.blocked_host_name_chain
                                , this.blocked_ax_session_chain
                                , this.blocked_ax_user_chain

                                , this.polling_time
                    );



                return string.Format("sqlDMVmonitor_HB,ax_user={0},host_name={1},login_name={2},sql_instance={3} " +
                                "ax_session=\"{4}\",program_name=\"{5}\",transaction_id=\"{6}\",session_id=\"{7}\",database_name=\"{8}\"" +
                                ",max_victim_wait_time={11}i" +
                                ",max_wait_time={12}i" +
                                ",waiting_sessions_count={13}i" +
                                ",waiting_sessions_depth={14}i" +

                                ",blocked_session_id_chain=\"{15}\"" +
                                ",blocked_wait_resource_chain=\"{16}\"" +
                                ",blocked_wait_type_chain=\"{17}\"" +
                                ",blocked_host_name_chain=\"{18}\"" +
                                ",blocked_ax_session_chain=\"{19}\"" +
                                ",blocked_ax_user_chain=\"{20}\"" +

                                " {21}",
                                this.ax_user, this.host_name, this.login_name, DMVmonitorEXE_TL.sql_instance,
                                this.ax_session, this.program_name, this.transaction_id.HasValue?this.transaction_id.Value.ToString():"", this.session_id, this.database_name
                                ,"",""
                                , this.max_victim_wait_time
                                , this.max_wait_time
                                , this.waiting_sessions_count
                                , this.waiting_sessions_depth
                                , this.blocked_session_id_chain
                                , this.blocked_wait_resource_chain
                                , this.blocked_wait_type_chain
                                , this.blocked_host_name_chain
                                , this.blocked_ax_session_chain
                                , this.blocked_ax_user_chain

                                , this.polling_time
                    );


            }
            public string toNATS2()
            {
                return string.Format("sqlDMVmonitor_HrB,sql_instance={3},session_id={7} " +

                                "ax_user=\"{0}\",host_name=\"{1}\",login_name=\"{2}\",ax_session=\"{4}\",program_name=\"{5}\",transaction_id=\"{6}\",database_name=\"{8}\"" +
                                ",max_victim_wait_time={11}i" +
                                ",max_wait_time={12}i" +
                                ",waiting_sessions_count={13}i" +
                                ",waiting_sessions_depth={14}i" +
                                ",blocked_session_id_chain=\"{15}\"" +
                                ",blocked_wait_resource_chain=\"{16}\"" +
                                ",blocked_wait_type_chain=\"{17}\"" +
                                ",blocked_host_name_chain=\"{18}\"" +
                                ",blocked_ax_session_chain=\"{19}\"" +
                                ",blocked_ax_user_chain=\"{20}\"" +

                                " {21}",
                                this.ax_user, this.host_name, this.login_name.Replace("\\", "\\\\"), DMVmonitorEXE_TL.sql_instance,
                                this.ax_session, this.program_name, this.transaction_id.HasValue ? this.transaction_id.Value.ToString() : "", this.session_id, this.database_name
                                , "", ""
                                , this.max_victim_wait_time
                                , this.max_wait_time
                                , this.waiting_sessions_count - 1
                                , this.waiting_sessions_depth
                                , this.blocked_session_id_chain
                                , this.blocked_wait_resource_chain
                                , this.blocked_wait_type_chain
                                , this.blocked_host_name_chain
                                , this.blocked_ax_session_chain
                                , this.blocked_ax_user_chain

                                , this.polling_time
                    );
            
            }

            //rivedere il meccanismo, mi sa che servono due livelli in lettura e poi abbinarli quando scatta il successivo oppure ho finito....
            private Dictionary<int, DMVblockChain> childs = new Dictionary<int, DMVblockChain>();
            public int check_session_id { get; private set; }
            public bool AddBlocked(DMVblockChain chain, int block_level)
            {
                this.max_block_level = block_level;
                chain.block_level = block_level;
                check_session_id = chain.session_id;
                

                childs.Add(chain.session_id, chain);

                return true;
            }


            public int max_victim_wait_time
            {
                get
                {
                    var value = childs.Count > 0 ? childs.Max(c => c.Value.max_victim_wait_time) : 0;
                    if (block_level > 1)
                        return wait_time > value ? wait_time : value;
                    else
                        return value;
                }
            }
            public int max_wait_time
            {
                get
                {
                    return max_victim_wait_time > wait_time ? max_victim_wait_time : wait_time;
                }
            }
            public int waiting_sessions_count
            {
                get
                {
                    return childs.Count > 0 ? (1 + childs.Sum(c => c.Value.waiting_sessions_count)) : 1;
                }
            }
            public int waiting_sessions_depth
            {
                get
                {
                    return childs.Count > 0 ? (1 + childs.Max(c => c.Value.waiting_sessions_depth)) : 0;
                }
            }

            public string blocked_session_id_chain
            {
                get
                {
                    return session_id.ToString() + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "")+childs.Select(child=>child.Value.blocked_session_id_chain).Aggregate("",(t,el)=>t+=el+"|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }
            public string blocked_host_name_chain
            {
                get
                {
                    return host_name.ToString() + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "") + childs.Select(child => child.Value.blocked_host_name_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }
            public string blocked_wait_resource_chain
            {
                get
                {
                    return wait_resource.ToString() + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "") + childs.Select(child => child.Value.blocked_wait_resource_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }
            public string blocked_wait_type_chain
            {
                get
                {
                    return wait_type.ToString() + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "") + childs.Select(child => child.Value.blocked_wait_type_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }

            public string blocked_ax_session_chain
            {
                get
                {
                    return ax_session + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "") + childs.Select(child => child.Value.blocked_ax_session_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }
            public string blocked_ax_user_chain
            {
                get
                {
                    return ax_user + (childs.Count > 0 ? " >> " : "") + (childs.Count > 1 ? "(" : "") + childs.Select(child => child.Value.blocked_ax_user_chain).Aggregate("", (t, el) => t += el + "|").TrimEnd('|') + (childs.Count > 1 ? ")" : "");
                }
            }
        }
    }
}
