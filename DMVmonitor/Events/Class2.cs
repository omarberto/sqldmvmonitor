﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Events.Text2
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class deadlock
    {

        private deadlockVictimlist victimlistField;

        private deadlockProcess[] processlistField;

        private deadlockPagelock[] resourcelistField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("victim-list")]
        public deadlockVictimlist victimlist
        {
            get
            {
                return this.victimlistField;
            }
            set
            {
                this.victimlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("process-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("process", IsNullable = false)]
        public deadlockProcess[] processlist
        {
            get
            {
                return this.processlistField;
            }
            set
            {
                this.processlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("resource-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("pagelock", IsNullable = false)]
        public deadlockPagelock[] pagelocks
        {
            get
            {
                return this.resourcelistField;
            }
            set
            {
                this.resourcelistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("resource-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("keylock", IsNullable = false)]
        public deadlockPagelock[] keylocks
        {
            get
            {
                return this.resourcelistField;
            }
            set
            {
                this.resourcelistField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockVictimlist
    {

        private deadlockVictimlistVictimProcess victimProcessField;

        /// <remarks/>
        public deadlockVictimlistVictimProcess victimProcess
        {
            get
            {
                return this.victimProcessField;
            }
            set
            {
                this.victimProcessField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockVictimlistVictimProcess
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockProcess
    {

        private deadlockProcessExecutionStack executionStackField;

        private string inputbufField;

        private string idField;

        private int taskpriorityField;

        private int logusedField;

        private string waitresourceField;

        private uint waittimeField;

        private ulong ownerIdField;

        private string transactionnameField;

        private System.DateTime lasttranstartedField;

        private string xDESField;

        private string lockModeField;

        private int scheduleridField;

        private int kpidField;

        private string statusField;

        private int spidField;

        private int sbidField;

        private int ecidField;

        private int priorityField;

        private int trancountField;

        private System.DateTime lastbatchstartedField;

        private System.DateTime lastbatchcompletedField;

        private System.DateTime lastattentionField;

        private string clientappField;

        private string hostnameField;

        private int hostpidField;

        private string loginnameField;

        private string isolationlevelField;

        private ulong xactidField;

        private int currentdbField;

        private uint lockTimeoutField;

        private uint clientoption1Field;

        private uint clientoption2Field;

        /// <remarks/>
        public deadlockProcessExecutionStack executionStack
        {
            get
            {
                return this.executionStackField;
            }
            set
            {
                this.executionStackField = value;
            }
        }

        /// <remarks/>
        public string inputbuf
        {
            get
            {
                return this.inputbufField;
            }
            set
            {
                this.inputbufField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int taskpriority
        {
            get
            {
                return this.taskpriorityField;
            }
            set
            {
                this.taskpriorityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int logused
        {
            get
            {
                return this.logusedField;
            }
            set
            {
                this.logusedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string waitresource
        {
            get
            {
                return this.waitresourceField;
            }
            set
            {
                this.waitresourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint waittime
        {
            get
            {
                return this.waittimeField;
            }
            set
            {
                this.waittimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong ownerId
        {
            get
            {
                return this.ownerIdField;
            }
            set
            {
                this.ownerIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string transactionname
        {
            get
            {
                return this.transactionnameField;
            }
            set
            {
                this.transactionnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lasttranstarted
        {
            get
            {
                return this.lasttranstartedField;
            }
            set
            {
                this.lasttranstartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string XDES
        {
            get
            {
                return this.xDESField;
            }
            set
            {
                this.xDESField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lockMode
        {
            get
            {
                return this.lockModeField;
            }
            set
            {
                this.lockModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int schedulerid
        {
            get
            {
                return this.scheduleridField;
            }
            set
            {
                this.scheduleridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int kpid
        {
            get
            {
                return this.kpidField;
            }
            set
            {
                this.kpidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int spid
        {
            get
            {
                return this.spidField;
            }
            set
            {
                this.spidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int sbid
        {
            get
            {
                return this.sbidField;
            }
            set
            {
                this.sbidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int ecid
        {
            get
            {
                return this.ecidField;
            }
            set
            {
                this.ecidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int priority
        {
            get
            {
                return this.priorityField;
            }
            set
            {
                this.priorityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int trancount
        {
            get
            {
                return this.trancountField;
            }
            set
            {
                this.trancountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastbatchstarted
        {
            get
            {
                return this.lastbatchstartedField;
            }
            set
            {
                this.lastbatchstartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastbatchcompleted
        {
            get
            {
                return this.lastbatchcompletedField;
            }
            set
            {
                this.lastbatchcompletedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastattention
        {
            get
            {
                return this.lastattentionField;
            }
            set
            {
                this.lastattentionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string clientapp
        {
            get
            {
                return this.clientappField;
            }
            set
            {
                this.clientappField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string hostname
        {
            get
            {
                return this.hostnameField;
            }
            set
            {
                this.hostnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int hostpid
        {
            get
            {
                return this.hostpidField;
            }
            set
            {
                this.hostpidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string loginname
        {
            get
            {
                return this.loginnameField;
            }
            set
            {
                this.loginnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string isolationlevel
        {
            get
            {
                return this.isolationlevelField;
            }
            set
            {
                this.isolationlevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong xactid
        {
            get
            {
                return this.xactidField;
            }
            set
            {
                this.xactidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int currentdb
        {
            get
            {
                return this.currentdbField;
            }
            set
            {
                this.currentdbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint lockTimeout
        {
            get
            {
                return this.lockTimeoutField;
            }
            set
            {
                this.lockTimeoutField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint clientoption1
        {
            get
            {
                return this.clientoption1Field;
            }
            set
            {
                this.clientoption1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint clientoption2
        {
            get
            {
                return this.clientoption2Field;
            }
            set
            {
                this.clientoption2Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockProcessExecutionStack
    {

        private deadlockProcessExecutionStackFrame frameField;

        /// <remarks/>
        public deadlockProcessExecutionStackFrame frame
        {
            get
            {
                return this.frameField;
            }
            set
            {
                this.frameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockProcessExecutionStackFrame
    {

        private string procnameField;

        private int lineField;

        private int stmtstartField;

        private int stmtendField;

        private string sqlhandleField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string procname
        {
            get
            {
                return this.procnameField;
            }
            set
            {
                this.procnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int line
        {
            get
            {
                return this.lineField;
            }
            set
            {
                this.lineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int stmtstart
        {
            get
            {
                return this.stmtstartField;
            }
            set
            {
                this.stmtstartField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int stmtend
        {
            get
            {
                return this.stmtendField;
            }
            set
            {
                this.stmtendField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string sqlhandle
        {
            get
            {
                return this.sqlhandleField;
            }
            set
            {
                this.sqlhandleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockPagelock
    {

        private deadlockPagelockOwnerlist ownerlistField;

        private deadlockPagelockWaiterlist waiterlistField;

        private int fileidField;

        private uint pageidField;

        private int dbidField;

        private string subresourceField;

        private string objectnameField;

        private string idField;

        private string modeField;

        private ulong associatedObjectIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("owner-list")]
        public deadlockPagelockOwnerlist ownerlist
        {
            get
            {
                return this.ownerlistField;
            }
            set
            {
                this.ownerlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("waiter-list")]
        public deadlockPagelockWaiterlist waiterlist
        {
            get
            {
                return this.waiterlistField;
            }
            set
            {
                this.waiterlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int fileid
        {
            get
            {
                return this.fileidField;
            }
            set
            {
                this.fileidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint pageid
        {
            get
            {
                return this.pageidField;
            }
            set
            {
                this.pageidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int dbid
        {
            get
            {
                return this.dbidField;
            }
            set
            {
                this.dbidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string subresource
        {
            get
            {
                return this.subresourceField;
            }
            set
            {
                this.subresourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string objectname
        {
            get
            {
                return this.objectnameField;
            }
            set
            {
                this.objectnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong associatedObjectId
        {
            get
            {
                return this.associatedObjectIdField;
            }
            set
            {
                this.associatedObjectIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockPagelockOwnerlist
    {

        private deadlockPagelockOwnerlistOwner ownerField;

        /// <remarks/>
        public deadlockPagelockOwnerlistOwner owner
        {
            get
            {
                return this.ownerField;
            }
            set
            {
                this.ownerField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockPagelockOwnerlistOwner
    {

        private string idField;

        private string modeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockPagelockWaiterlist
    {

        private deadlockPagelockWaiterlistWaiter waiterField;

        /// <remarks/>
        public deadlockPagelockWaiterlistWaiter waiter
        {
            get
            {
                return this.waiterField;
            }
            set
            {
                this.waiterField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockPagelockWaiterlistWaiter
    {

        private string idField;

        private string modeField;

        private string requestTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string requestType
        {
            get
            {
                return this.requestTypeField;
            }
            set
            {
                this.requestTypeField = value;
            }
        }
    }


}


