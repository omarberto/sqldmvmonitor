﻿using DMVmonitor;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System;
using System.Linq;
using System.Text;

namespace SQLsessions.Events
{
    public enum Result : uint
    {
        OK = 0,
        Error = 1,
        Abort = 2,
        Skipped = 3,
        NONE = 4001
    }
    class CompleteEvent
    {
        public string event_name;
        public ulong query_hash;
        public string host_name;
        public int ax_session;
        public string ax_user;
        public string sql_user;
        public string database_name;
        public Result result;

        public ulong query_plan_hash;
        public ulong? duration;
        public ulong? cpu_time;
        public ulong? physical_reads;
        public ulong? logical_reads;
        public ulong? writes;
        public ulong? row_count;
        public ushort session_id = 0;
        public bool isFetch;
        public long nsTimeStampEpoch;

        public string sql_instance;
        public string sql_host;

        public string sql_text="";
        public ActivityId attach_activity_id;

        public CompleteEvent(PublishedEvent evt, string sql_host, string sql_instance)
        {
            this.event_name = evt.Name;

            
            //Actions
            PublishedAction actionTmp;
            if (evt.Actions.TryGetValue("query_hash", out actionTmp))
                this.query_hash = Convert.ToUInt64(actionTmp.Value);
            this.host_name = Convert.ToString(evt.Actions["client_hostname"].Value).Trim();
            var context_info = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(evt.Actions["context_info"].Value as byte[]));//OK
            this.ax_session = context_info.AxSessionId;
            this.ax_user = context_info.AxUserName;
            if (evt.Actions.TryGetValue("nt_username", out actionTmp))
                this.sql_user = Convert.ToString(actionTmp.Value).Trim();
            if (evt.Actions.TryGetValue("username", out actionTmp))
                this.sql_user = Convert.ToString(actionTmp.Value).Trim();
            this.database_name = Convert.ToString(evt.Actions["database_name"].Value).Trim();
            if (evt.Actions.TryGetValue("session_id", out actionTmp))
                this.session_id = Convert.ToUInt16(actionTmp.Value);
            if (evt.Actions.TryGetValue("query_plan_hash", out actionTmp))
                this.query_plan_hash = Convert.ToUInt64(actionTmp.Value);

            this.sql_host = sql_host;
            this.sql_instance = sql_instance;
            //if (evt.Actions.TryGetValue("server_instance_name", out actionTmp))
            //    this.server_instance_name = Convert.ToString(actionTmp.Value).Trim();
            //else
            //    this.server_instance_name = this.sql_instance;

            //mettere uppercase
            //Logging.Log.Instance.Trace("this.server_instance_name: {0} {1}", this.server_instance_name, DMVmonitor.Configuration.Configuration.config.main.dataSource);

            if (evt.Actions.TryGetValue("sql_text", out actionTmp))
                this.sql_text = Convert.ToString(evt.Actions["sql_text"].Value).Trim();

            if (evt.Actions.TryGetValue("attach_activity_id_xfer", out actionTmp))
            {
                //ActivityId id = (ActivityId)actionTmp.Value;
            }

            if (evt.Actions.TryGetValue("attach_activity_id", out actionTmp))
            {
                this.attach_activity_id = (ActivityId)actionTmp.Value;
            }

            //Fields
            PublishedEventField fieldTmp;
            if (evt.Fields.TryGetValue("result", out fieldTmp))
            {
                this.result = (Result)(fieldTmp.Value as MapValue).Key;
            }
            else this.result = Result.NONE;
            if (evt.Fields.TryGetValue("duration", out fieldTmp))
                this.duration = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("cpu_time", out fieldTmp))
                this.cpu_time = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("physical_reads", out fieldTmp))
                this.physical_reads = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("logical_reads", out fieldTmp))
                this.logical_reads = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("writes", out fieldTmp))
                this.writes = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("row_count", out fieldTmp))
                this.row_count = Convert.ToUInt64(fieldTmp.Value);
            string statement = Convert.ToString(evt.Fields["statement"].Value).Trim();
            this.isFetch = statement.IndexOf("FETCH API_CURSOR") == 0 || statement.IndexOf("exec sp_cursorfetch") == 0;

            

            this.nsTimeStampEpoch = new EpochNanoseconds(evt.Timestamp.UtcDateTime).Value;
        }
        
        internal void PublishNats()
        {
            var message = new Influx.FormatMessage("sqlXeEvents");
            message.AddTag("SqlQueryHash", query_hash.ToString());
            message.AddTag("SqlEventName", event_name);
            message.AddTag("SqlHostName", (string.IsNullOrEmpty(host_name.Trim()) ? "-" : host_name));
            message.AddTag("AxUser", ax_user);
            message.AddTag("SqlUser", DMVmonitor.Outputs.Nats.DataSend.EscapeTag(sql_user));
            message.AddTag("DatabaseName", database_name);
            message.AddTag("result", result.ToString());
            message.AddTag("host", this.sql_host);
            message.AddTag("SQLInstance", this.sql_instance);
            message.AddTag("uniquifier", Uniquifier.uniquifier(UniquifierClass.sqlXeEvents, nsTimeStampEpoch).ToString());//questa parte potremmo inglobarla
            
            message.AddBooleanField("isFetch", isFetch);
            message.AddStringField("queryplan_hash", query_plan_hash, "{0}");
            message.AddStringField("hex_queryplan_hash", query_plan_hash, "0x{0:X}");
            message.AddStringField("hex_query_hash", query_hash, "0x{0:X}");
            message.AddIntegerField("cpu_time", cpu_time);
            message.AddIntegerField("duration", duration);
            message.AddIntegerField("logical_reads", logical_reads);
            message.AddIntegerField("physical_reads", physical_reads);
            message.AddIntegerField("row_count", row_count);
            message.AddIntegerField("writes", writes);
            message.AddStringField("session_id", session_id, "{0}");
            message.AddStringField("AxSession", ax_session < 0 ? "-" : ax_session.ToString(), "{0}");
            
            message.AddEpochNs(nsTimeStampEpoch);




            //string rpTag = DMVmonitor.Configuration.Configuration.config.output.nats.getRetentionTag("sqlXeEvents");

            //string tags = string.Format(",SqlQueryHash={0},SqlEventName={1},SqlHostName={2},AxUser={3},DatabaseName={5},SqlUser={4},result={6},host={7},SQLInstance={8},uniquifier={9}{10}"
            //                         , query_hash
            //                         , event_name
            //                         , (string.IsNullOrEmpty(host_name.Trim()) ? "-" : host_name)
            //                         , ax_user
            //                         , DataSend.EscapeTag(sql_user)
            //                         , database_name
            //                         , result
            //                         , this.sql_host
            //                         , this.sql_instance
            //                         , Uniquifier.uniquifier(UniquifierClass.sqlXeEvents, nsTimeStampEpoch)
            //                         , rpTag);

            //string fields = string.Format(
            //                   "isFetch=" + (isFetch ? "t" : "f")
            //                 + ",queryplan_hash=\"{4}\""
            //                 + (cpu_time.HasValue ? ",cpu_time={0}i" : "")
            //                 + (duration.HasValue ? ",duration={1}i" : "")
            //                 + (logical_reads.HasValue ? ",logical_reads={2}i" : "")
            //                 + (physical_reads.HasValue ? ",physical_reads={3}i" : "")
            //                 + (row_count.HasValue ? ",row_count={5}i" : "")
            //                 + (writes.HasValue ? ",writes={7}i" : "")
            //                 + ",hex_queryplan_hash=\"0x{4:X}\""
            //                 + ",hex_query_hash=\"0x{8:X}\""
            //                 + ",session_id=\"{6}\""
            //                 + ",AxSession=\"{9}\""
            //                 , cpu_time, duration, logical_reads, physical_reads,
            //                 query_plan_hash,
            //                 row_count, session_id, writes, query_hash, (ax_session < 0 ? "-" : ax_session.ToString()));//per ora cosí un po'brutale

            //string message = "sqlXeEvents" + tags + " " + fields + " " + nsTimeStampEpoch;
            //Console.WriteLine(message);

            //TODO: add rpTag (translated)
            DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(message.value);
        }
    }
    class CompleteUnknown
    {
        public CompleteUnknown(PublishedEvent evt)
        {
            Logging.Log.Instance.Trace("parsing CompleteUnknown {0}", evt.Name);
            foreach (var field in evt.Fields.Cast<PublishedEventField>())
                Logging.Log.Instance.Trace("Field {0} {1} {2}", field.Name, field.Type, field.Value);
            foreach (var action in evt.Actions.Cast<PublishedAction>())
                Logging.Log.Instance.Trace("Action {0} {1} {2}", action.Name, action.Type, action.Value);
        }

    }
}
