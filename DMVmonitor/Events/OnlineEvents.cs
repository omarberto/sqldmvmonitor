﻿using DMVmonitor;
using Microsoft.SqlServer.XEvent.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLsessions.Events
{
    class OnlineEvent
    {
        public OnlineEvent(PublishedEvent evt)
        {
            PublishedEventField fieldTmp;
            evt.Fields.TryGetValue("duration", out fieldTmp);
                this.duration = Convert.ToUInt64(fieldTmp.Value);
            
            //Actions
            PublishedAction actionTmp;
            if (evt.Actions.TryGetValue("query_hash", out actionTmp))
                this.query_hash = Convert.ToUInt64(actionTmp.Value);
            if (evt.Actions.TryGetValue("query_plan_hash", out actionTmp))
                this.query_plan_hash = Convert.ToUInt64(actionTmp.Value);
        }

        public ulong duration { get; private set; }
        public ulong query_hash { get; private set; }
        public ulong query_plan_hash { get; private set; }
    }


    class RpcCompleted
    {
        public RpcCompleted(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            try
            {
                //Fields
                this.object_name = Convert.ToString(evt.Fields["object_name"].Value);
                this.statement = Convert.ToString(evt.Fields["statement"].Value);

                PublishedEventField fieldTmp;
                if (evt.Fields.TryGetValue("duration", out fieldTmp))
                    this.duration = Convert.ToUInt64(fieldTmp.Value);
                if (evt.Fields.TryGetValue("cpu_time", out fieldTmp))
                    this.cpu_time = Convert.ToUInt64(fieldTmp.Value);
                if (evt.Fields.TryGetValue("physical_reads", out fieldTmp))
                    this.physical_reads = Convert.ToUInt64(fieldTmp.Value);
                if (evt.Fields.TryGetValue("logical_reads", out fieldTmp))
                    this.logical_reads = Convert.ToUInt64(fieldTmp.Value);
                if (evt.Fields.TryGetValue("writes", out fieldTmp))
                    this.writes = Convert.ToUInt64(fieldTmp.Value);
                if (evt.Fields.TryGetValue("row_count", out fieldTmp))
                    this.row_count = Convert.ToUInt64(fieldTmp.Value);

                //Actions
                this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
                PublishedAction actionTmp;
                if (evt.Actions.TryGetValue("client_hostname", out actionTmp))
                {
                    this.host_name = Convert.ToString(evt.Actions["client_hostname"].Value).Trim();
                    this.has_host_name = !string.IsNullOrEmpty(this.host_name);
                }
                if (evt.Actions.TryGetValue("nt_username", out actionTmp))
                {
                    this.sql_user = Convert.ToString(actionTmp.Value).Trim();
                    this.has_sql_user = !string.IsNullOrEmpty(this.sql_user);
                }
                if (evt.Actions.TryGetValue("username", out actionTmp))
                    this.sql_user = Convert.ToString(actionTmp.Value).Trim();
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Trace("RpcCompleted ({1}) Excpetion {0}", e.Message, evt.Name);
            }
        }
        
        public bool has_host_name { get; private set; } = false;
        public string host_name { get; private set; } = string.Empty;
        public bool has_sql_user { get; private set; } = false;
        public string sql_user { get; private set; } = string.Empty;

        public ulong duration { get; private set; }
        public ulong? cpu_time { get; private set; }
        public ulong? logical_reads { get; private set; }
        public ulong? physical_reads { get; private set; }
        public ulong? row_count { get; private set; }
        public ulong? writes { get; private set; }

        public string object_name { get; private set; }
        public bool isFetch {
            get { return object_name == "sp_cursorfetch"; }//lo do per scontato poi!!
        }
        public string statement { get; private set; }
        public int protocol_execution_id
        {
            get { return int.Parse(statement.Substring(20, statement.IndexOf(',') - 20)); }//do per scontato che cursor é lungo 9??
        }
        public int session_id { get; private set; }
        public DateTimeOffset time_stamp { get; private set; }

        public long nsTimeStampEpoch
        {
            get
            {
                return new EpochNanoseconds(time_stamp.UtcDateTime).Value;
            }
        }

    }

    class CursorOpenEvent
    {
        public CursorOpenEvent(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            this.protocol_execution_id = Convert.ToInt32(evt.Fields["protocol_execution_id"].Value);

            //Actions
            try
            {
                this.server_instance_name = Convert.ToString(evt.Actions["server_instance_name"].Value);
                this.query_hash = Convert.ToUInt64(evt.Actions["query_hash"].Value);
                this.query_plan_hash = Convert.ToUInt64(evt.Actions["query_plan_hash"].Value);
                this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
                var context_info = new DMVmonitor.Events.AxContextInfo(Encoding.UTF8.GetString(evt.Actions["context_info"].Value as byte[]));//OK
                this.AxSession = context_info.AxSessionId;
                this.AxUser = context_info.AxUserName;
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Trace("CursorOpenEvent ({1}) Excpetion {0}", e.Message, evt.Name);
            }
        }
        
        public int AxSession { get; private set; }
        public string AxUser { get; private set; }
        public int protocol_execution_id { get; private set; }
        public ulong query_hash { get; private set; }
        public ulong query_plan_hash { get; private set; }
        public string server_instance_name { get; private set; }
        public int session_id { get; private set; }
        public DateTimeOffset time_stamp { get; private set; }
    }
    class CursorCloseEvent
    {
        public CursorCloseEvent(PublishedEvent evt)
        {
            this.time_stamp = evt.Timestamp;

            this.protocol_execution_id = Convert.ToInt32(evt.Fields["protocol_execution_id"].Value);
            try
            {
                this.session_id = Convert.ToInt32(evt.Actions["session_id"].Value);
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Trace("CursorCloseEvent ({1}) Excpetion {0}", e.Message, evt.Name);
            }
        }

        public int protocol_execution_id { get; private set; }
        public int session_id { get; private set; }
        public DateTimeOffset time_stamp { get; private set; }

        public long nsTimeStampEpoch
        {
            get
            {
                return new EpochNanoseconds(time_stamp.UtcDateTime).Value;
            }
        }
    }

    class OnlineUnknown
    {
        public OnlineUnknown(PublishedEvent evt)
        {
            Logging.Log.Instance.Trace("parsing OnlineUnknown {0}", evt.Name);
            foreach (var field in evt.Fields.Cast<PublishedEventField>())
                Logging.Log.Instance.Trace("Field {0} {1} {2}", field.Name, field.Type, field.Value);
            foreach (var action in evt.Actions.Cast<PublishedAction>())
                Logging.Log.Instance.Trace("Action {0} {1} {2}", action.Name, action.Type, action.Value);
            Logging.Log.Instance.Trace("parsed OnlineUnknown");
        }

        public ulong duration { get; private set; }
        public ulong query_hash { get; private set; }
        public ulong query_plan_hash { get; private set; }
    }
}
