﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Events
{
    internal class AxContextInfo
    {
        public override string ToString()
        {
            return string.Format("Session: {0} User:{1} | ({2}//{3}//{4})", AxSessionId, AxUserName, ConnectionType1, ConnectionType2, Number);
        }

        public AxContextInfo(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < 2) return;
            try
            {
                var indexS1 = value.IndexOf(' ', 2);

                var indexM = value.IndexOf('-', indexS1);

                this.AxUserName = value.Substring(1, indexS1).Trim();
                var indexS2 = value.IndexOf(' ', indexS1 + 1);
                this.AxSessionId = int.Parse(value.Substring(indexS1 + 1, indexS2 - indexS1 - 1));

                if (indexM > 0)
                {
                    this.ConnectionType1 = value.Substring(indexS2 + 1, indexM - indexS2 - 2);
                    var indexS3 = value.IndexOf(' ', indexM + 2);
                    this.ConnectionType2 = value.Substring(indexM + 2, indexS3 - indexM - 2);
                    this.Number = int.Parse(value.Substring(indexS3));
                }
            }
            catch (Exception e)
            {
                Logging.Log.Instance.Trace("{{{0}}}", value);
                throw e;
            }

        }

        public int AxSessionId { get; private set; } = -1;
        public string AxUserName { get; private set; } = "NONE";
        public string ConnectionType1 { get; private set; }
        public string ConnectionType2 { get; private set; }
        public int Number { get; private set; }
    }
}
