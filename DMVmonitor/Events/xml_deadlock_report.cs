﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace SQLsessions.Events.Deadlocks
{
    public class DeserializeDeadlock
    {
    }
    
    [SerializableAttribute()]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class deadlock
    {
        [SerializableAttribute()]
        [DesignerCategoryAttribute("code")]
        [XmlTypeAttribute(AnonymousType = true)]
        public class victimProcess
        {
            private string idField;
            [XmlAttributeAttribute()]
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }
        }
        private victimProcess[] victimlistField;
        [XmlArrayAttribute("victim-list")]
        [XmlArrayItemAttribute("victimProcess", IsNullable = false)]
        public victimProcess[] victimlist
        {
            get
            {
                return this.victimlistField;
            }
            set
            {
                this.victimlistField = value;
            }
        }


        [SerializableAttribute()]
        [DesignerCategoryAttribute("code")]
        [XmlTypeAttribute(AnonymousType = true)]
        public class deadlockProcess
        {
            [SerializableAttribute()]
            [DesignerCategoryAttribute("code")]
            [XmlTypeAttribute(AnonymousType = true)]
            public class processFrame
            {

                private string procnameField;

                private uint lineField;

                private uint stmtstartField;

                private bool stmtstartFieldSpecified;

                private uint stmtendField;

                private bool stmtendFieldSpecified;

                private string sqlhandleField;

                private string valueField;

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string procname
                {
                    get
                    {
                        return this.procnameField;
                    }
                    set
                    {
                        this.procnameField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public uint line
                {
                    get
                    {
                        return this.lineField;
                    }
                    set
                    {
                        this.lineField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public uint stmtstart
                {
                    get
                    {
                        return this.stmtstartField;
                    }
                    set
                    {
                        this.stmtstartField = value;
                    }
                }

                /// <remarks/>
                [XmlIgnoreAttribute()]
                public bool stmtstartSpecified
                {
                    get
                    {
                        return this.stmtstartFieldSpecified;
                    }
                    set
                    {
                        this.stmtstartFieldSpecified = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public uint stmtend
                {
                    get
                    {
                        return this.stmtendField;
                    }
                    set
                    {
                        this.stmtendField = value;
                    }
                }

                /// <remarks/>
                [XmlIgnoreAttribute()]
                public bool stmtendSpecified
                {
                    get
                    {
                        return this.stmtendFieldSpecified;
                    }
                    set
                    {
                        this.stmtendFieldSpecified = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string sqlhandle
                {
                    get
                    {
                        return this.sqlhandleField;
                    }
                    set
                    {
                        this.sqlhandleField = value;
                    }
                }

                /// <remarks/>
                [XmlTextAttribute()]
                public string Value
                {
                    get
                    {
                        return this.valueField;
                    }
                    set
                    {
                        this.valueField = value;
                    }
                }
            }
            private processFrame[] executionStackField;
            private string inputbufField;
            private string idField;
            private uint taskpriorityField;
            private uint logusedField;
            private string waitresourceField;
            private uint waittimeField;
            private ulong ownerIdField;
            private string transactionnameField;
            private DateTime lasttranstartedField;
            private string xDESField;
            private string lockModeField;
            private uint scheduleridField;
            private uint kpidField;
            private string statusField;
            private uint spidField;
            private uint sbidField;
            private uint ecidField;
            private uint priorityField;
            private uint trancountField;
            private DateTime lastbatchstartedField;
            private DateTime lastbatchcompletedField;
            private DateTime lastattentionField;
            private string clientappField;
            private string hostnameField;
            private uint hostpidField;
            private string loginnameField;
            private string isolationlevelField;
            private ulong xactidField;
            private uint currentdbField;
            private uint lockTimeoutField;
            private uint clientoption1Field;
            private uint clientoption2Field;

            [XmlArrayItemAttribute("frame", IsNullable = false)]
            public processFrame[] executionStack
            {
                get
                {
                    return this.executionStackField;
                }
                set
                {
                    this.executionStackField = value;
                }
            }

            public string inputbuf
            {
                get
                {
                    return this.inputbufField;
                }
                set
                {
                    this.inputbufField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint taskpriority
            {
                get
                {
                    return this.taskpriorityField;
                }
                set
                {
                    this.taskpriorityField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint logused
            {
                get
                {
                    return this.logusedField;
                }
                set
                {
                    this.logusedField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string waitresource
            {
                get
                {
                    return this.waitresourceField;
                }
                set
                {
                    this.waitresourceField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint waittime
            {
                get
                {
                    return this.waittimeField;
                }
                set
                {
                    this.waittimeField = value;
                }
            }

            [XmlAttributeAttribute()]
            public ulong ownerId
            {
                get
                {
                    return this.ownerIdField;
                }
                set
                {
                    this.ownerIdField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string transactionname
            {
                get
                {
                    return this.transactionnameField;
                }
                set
                {
                    this.transactionnameField = value;
                }
            }

            [XmlAttributeAttribute()]
            public DateTime lasttranstarted
            {
                get
                {
                    return this.lasttranstartedField;
                }
                set
                {
                    this.lasttranstartedField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string XDES
            {
                get
                {
                    return this.xDESField;
                }
                set
                {
                    this.xDESField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string lockMode
            {
                get
                {
                    return this.lockModeField;
                }
                set
                {
                    this.lockModeField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint schedulerid
            {
                get
                {
                    return this.scheduleridField;
                }
                set
                {
                    this.scheduleridField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint kpid
            {
                get
                {
                    return this.kpidField;
                }
                set
                {
                    this.kpidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string status
            {
                get
                {
                    return this.statusField;
                }
                set
                {
                    this.statusField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint spid
            {
                get
                {
                    return this.spidField;
                }
                set
                {
                    this.spidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint sbid
            {
                get
                {
                    return this.sbidField;
                }
                set
                {
                    this.sbidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint ecid
            {
                get
                {
                    return this.ecidField;
                }
                set
                {
                    this.ecidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint priority
            {
                get
                {
                    return this.priorityField;
                }
                set
                {
                    this.priorityField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint trancount
            {
                get
                {
                    return this.trancountField;
                }
                set
                {
                    this.trancountField = value;
                }
            }

            [XmlAttributeAttribute()]
            public DateTime lastbatchstarted
            {
                get
                {
                    return this.lastbatchstartedField;
                }
                set
                {
                    this.lastbatchstartedField = value;
                }
            }

            [XmlAttributeAttribute()]
            public DateTime lastbatchcompleted
            {
                get
                {
                    return this.lastbatchcompletedField;
                }
                set
                {
                    this.lastbatchcompletedField = value;
                }
            }

            [XmlAttributeAttribute()]
            public DateTime lastattention
            {
                get
                {
                    return this.lastattentionField;
                }
                set
                {
                    this.lastattentionField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string clientapp
            {
                get
                {
                    return this.clientappField;
                }
                set
                {
                    this.clientappField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string hostname
            {
                get
                {
                    return this.hostnameField;
                }
                set
                {
                    this.hostnameField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint hostpid
            {
                get
                {
                    return this.hostpidField;
                }
                set
                {
                    this.hostpidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string loginname
            {
                get
                {
                    return this.loginnameField;
                }
                set
                {
                    this.loginnameField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string isolationlevel
            {
                get
                {
                    return this.isolationlevelField;
                }
                set
                {
                    this.isolationlevelField = value;
                }
            }

            [XmlAttributeAttribute()]
            public ulong xactid
            {
                get
                {
                    return this.xactidField;
                }
                set
                {
                    this.xactidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint currentdb
            {
                get
                {
                    return this.currentdbField;
                }
                set
                {
                    this.currentdbField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint lockTimeout
            {
                get
                {
                    return this.lockTimeoutField;
                }
                set
                {
                    this.lockTimeoutField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint clientoption1
            {
                get
                {
                    return this.clientoption1Field;
                }
                set
                {
                    this.clientoption1Field = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint clientoption2
            {
                get
                {
                    return this.clientoption2Field;
                }
                set
                {
                    this.clientoption2Field = value;
                }
            }
        }
        private deadlockProcess[] processlistField;
        [XmlArrayAttribute("process-list")]
        [XmlArrayItemAttribute("process", IsNullable = false)]
        public deadlockProcess[] processlist
        {
            get
            {
                return this.processlistField;
            }
            set
            {
                this.processlistField = value;
            }
        }


        /* part added to have generic resourcelement */
        [SerializableAttribute()]
        [DesignerCategoryAttribute("code")]
        [XmlTypeAttribute(AnonymousType = true)]
        [XmlRootAttribute(Namespace = "", IsNullable = false)]
        public class resourceLock
        {
            [SerializableAttribute()]
            [DesignerCategoryAttribute("code")]
            [XmlTypeAttribute(AnonymousType = true)]
            public class resourceOwner
            {

                private string idField;

                private string modeField;

                private string requestTypeField;

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string id
                {
                    get
                    {
                        return this.idField;
                    }
                    set
                    {
                        this.idField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string mode
                {
                    get
                    {
                        return this.modeField;
                    }
                    set
                    {
                        this.modeField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string requestType
                {
                    get
                    {
                        return this.requestTypeField;
                    }
                    set
                    {
                        this.requestTypeField = value;
                    }
                }
            }
            private resourceOwner[] ownerlistField;

            [SerializableAttribute()]
            [DesignerCategoryAttribute("code")]
            [XmlTypeAttribute(AnonymousType = true)]
            public partial class resourceWaiter
            {

                private string idField;

                private string modeField;

                private string requestTypeField;

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string id
                {
                    get
                    {
                        return this.idField;
                    }
                    set
                    {
                        this.idField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string mode
                {
                    get
                    {
                        return this.modeField;
                    }
                    set
                    {
                        this.modeField = value;
                    }
                }

                /// <remarks/>
                [XmlAttributeAttribute()]
                public string requestType
                {
                    get
                    {
                        return this.requestTypeField;
                    }
                    set
                    {
                        this.requestTypeField = value;
                    }
                }
            }
            private resourceWaiter[] waiterlistField;

            private uint dbidField;

            private string objectnameField;

            private string indexnameField;

            private string idField;

            private string modeField;

            private ulong associatedObjectIdField;


            [XmlArrayAttribute("owner-list")]
            [XmlArrayItemAttribute("owner", IsNullable = false)]
            public resourceOwner[] ownerlist
            {
                get
                {
                    return this.ownerlistField;
                }
                set
                {
                    this.ownerlistField = value;
                }
            }

            [XmlArrayAttribute("waiter-list")]
            [XmlArrayItemAttribute("waiter", IsNullable = false)]
            public resourceWaiter[] waiterlist
            {
                get
                {
                    return this.waiterlistField;
                }
                set
                {
                    this.waiterlistField = value;
                }
            }

            [XmlAttributeAttribute()]
            public uint dbid
            {
                get
                {
                    return this.dbidField;
                }
                set
                {
                    this.dbidField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string objectname
            {
                get
                {
                    return this.objectnameField;
                }
                set
                {
                    this.objectnameField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string indexname
            {
                get
                {
                    return this.indexnameField;
                }
                set
                {
                    this.indexnameField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string id
            {
                get
                {
                    return this.idField;
                }
                set
                {
                    this.idField = value;
                }
            }

            [XmlAttributeAttribute()]
            public string mode
            {
                get
                {
                    return this.modeField;
                }
                set
                {
                    this.modeField = value;
                }
            }

            [XmlAttributeAttribute()]
            public ulong associatedObjectId
            {
                get
                {
                    return this.associatedObjectIdField;
                }
                set
                {
                    this.associatedObjectIdField = value;
                }
            }

            [XmlIgnore()]
            public string elementname { get; internal set; }
        }
        private resourceLock[] resourcelistField;
        [XmlArrayAttribute("resource-list")]
        [XmlArrayItemAttribute("resourceLock")]
        public resourceLock[] resourcelist
        {
            get
            {
                return this.resourcelistField;
            }
            set
            {
                this.resourcelistField = value;
            }
        }
    }
}
