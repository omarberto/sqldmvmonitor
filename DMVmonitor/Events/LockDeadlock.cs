﻿using System;
using DMVmonitor;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System.IO;

namespace SQLsessions.Events
{
    internal class LockDeadlock
    {
        public long nsTimeStampEpoch;

        public ushort session_id;


        public uint deadlock_id;
        public ulong associated_object_id;
        public uint database_id;
        public ulong duration;
        public ulong transaction_id;
        public string resource_description;
        public uint resource_0;
        public uint resource_1;
        public uint resource_2;
        public string mode;


        public LockDeadlock(PublishedEvent evt)
        {
            //Actions
            //PublishedAction actionTmp;
            this.session_id = Convert.ToUInt16(evt.Actions["session_id"].Value);


            //Field
            PublishedEventField fieldTmp;
            if (evt.Fields.TryGetValue("deadlock_id", out fieldTmp))
                this.deadlock_id = Convert.ToUInt32(fieldTmp.Value);
            if (evt.Fields.TryGetValue("associated_object_id", out fieldTmp))
                this.associated_object_id = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("database_id", out fieldTmp))
                this.database_id = Convert.ToUInt32(fieldTmp.Value);
            if (evt.Fields.TryGetValue("duration", out fieldTmp))
                this.duration = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("transaction_id", out fieldTmp))
                this.transaction_id = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("resource_description", out fieldTmp))
                this.resource_description = Convert.ToString(fieldTmp.Value);
            if (evt.Fields.TryGetValue("resource_0", out fieldTmp))
                this.resource_0 = Convert.ToUInt32(fieldTmp.Value);
            if (evt.Fields.TryGetValue("resource_1", out fieldTmp))
                this.resource_1 = Convert.ToUInt32(fieldTmp.Value);
            if (evt.Fields.TryGetValue("resource_2", out fieldTmp))
                this.resource_2 = Convert.ToUInt32(fieldTmp.Value);
            if (evt.Fields.TryGetValue("mode", out fieldTmp))
            {
                this.mode = ((MapValue)fieldTmp.Value).Value;
            }


            this.nsTimeStampEpoch = new EpochNanoseconds(evt.Timestamp.UtcDateTime).Value;
        }

    }

    internal class LockDeadlockExt
    {
        public LockDeadlock deadlock;
        public List<LockDeadlockChain> chain = new List<LockDeadlockChain>();

        public void AddChain(LockDeadlockChain chain)
        {
            this.chain.Add(chain);
        }

        public void AddDeadlock(LockDeadlock deadlock)
        {
            this.deadlock = deadlock;
        }
    }
}
