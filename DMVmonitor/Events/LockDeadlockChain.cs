﻿using System;
using DMVmonitor;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System.IO;

namespace SQLsessions.Events
{
    internal class LockDeadlockChain
    {
        public long nsTimeStampEpoch;

        public uint deadlock_id;
        //public ulong associated_object_id;
        //public uint database_id;
        //public ulong duration;
        public ulong transaction_id;
        //public string resource_description;
        //public uint resource_0;
        //public uint resource_1;
        //public uint resource_2;
        //public string mode;


        public string program_name;
        public string host_name;
        public int ax_session;
        public string ax_user;
        public ulong query_hash;
        public ulong query_plan_hash;

        public LockDeadlockChain(PublishedEvent evt)
        {

            //Field
            PublishedEventField fieldTmp;
            if (evt.Fields.TryGetValue("deadlock_id", out fieldTmp))
                this.deadlock_id = Convert.ToUInt32(fieldTmp.Value);

            //if (evt.Fields.TryGetValue("associated_object_id", out fieldTmp))
            //    this.associated_object_id = Convert.ToUInt64(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("database_id", out fieldTmp))
            //    this.database_id = Convert.ToUInt32(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("duration", out fieldTmp))
            //    this.duration = Convert.ToUInt64(fieldTmp.Value);
            if (evt.Fields.TryGetValue("transaction_id", out fieldTmp))
                this.transaction_id = Convert.ToUInt64(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("resource_description", out fieldTmp))
            //    this.resource_description = Convert.ToString(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("resource_0", out fieldTmp))
            //    this.resource_0 = Convert.ToUInt32(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("resource_1", out fieldTmp))
            //    this.resource_1 = Convert.ToUInt32(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("resource_2", out fieldTmp))
            //    this.resource_2 = Convert.ToUInt32(fieldTmp.Value);
            //if (evt.Fields.TryGetValue("mode", out fieldTmp))
            //{
            //    this.mode = ((MapValue)fieldTmp.Value).Value;
            //}


            this.nsTimeStampEpoch = new EpochNanoseconds(evt.Timestamp.UtcDateTime).Value;
        }

    }
}
