﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor.Events.Test
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class deadlock
    {

        private deadlockVictimProcess[] victimlistField;

        private deadlockProcess[] processlistField;

        private deadlockKeylock[] resourcelistField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("victim-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("victimProcess", IsNullable = false)]
        public deadlockVictimProcess[] victimlist
        {
            get
            {
                return this.victimlistField;
            }
            set
            {
                this.victimlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("process-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("process", IsNullable = false)]
        public deadlockProcess[] processlist
        {
            get
            {
                return this.processlistField;
            }
            set
            {
                this.processlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("resource-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("keylock", IsNullable = false)]
        public deadlockKeylock[] resourcelist
        {
            get
            {
                return this.resourcelistField;
            }
            set
            {
                this.resourcelistField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockVictimProcess
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockProcess
    {

        private deadlockProcessFrame[] executionStackField;

        private string inputbufField;

        private string idField;

        private byte taskpriorityField;

        private ushort logusedField;

        private string waitresourceField;

        private ushort waittimeField;

        private ulong ownerIdField;

        private string transactionnameField;

        private System.DateTime lasttranstartedField;

        private string xDESField;

        private string lockModeField;

        private byte scheduleridField;

        private ushort kpidField;

        private string statusField;

        private ushort spidField;

        private byte sbidField;

        private byte ecidField;

        private byte priorityField;

        private byte trancountField;

        private System.DateTime lastbatchstartedField;

        private System.DateTime lastbatchcompletedField;

        private System.DateTime lastattentionField;

        private string clientappField;

        private string hostnameField;

        private ushort hostpidField;

        private string loginnameField;

        private string isolationlevelField;

        private ulong xactidField;

        private byte currentdbField;

        private uint lockTimeoutField;

        private uint clientoption1Field;

        private uint clientoption2Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("frame", IsNullable = false)]
        public deadlockProcessFrame[] executionStack
        {
            get
            {
                return this.executionStackField;
            }
            set
            {
                this.executionStackField = value;
            }
        }

        /// <remarks/>
        public string inputbuf
        {
            get
            {
                return this.inputbufField;
            }
            set
            {
                this.inputbufField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte taskpriority
        {
            get
            {
                return this.taskpriorityField;
            }
            set
            {
                this.taskpriorityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort logused
        {
            get
            {
                return this.logusedField;
            }
            set
            {
                this.logusedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string waitresource
        {
            get
            {
                return this.waitresourceField;
            }
            set
            {
                this.waitresourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort waittime
        {
            get
            {
                return this.waittimeField;
            }
            set
            {
                this.waittimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong ownerId
        {
            get
            {
                return this.ownerIdField;
            }
            set
            {
                this.ownerIdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string transactionname
        {
            get
            {
                return this.transactionnameField;
            }
            set
            {
                this.transactionnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lasttranstarted
        {
            get
            {
                return this.lasttranstartedField;
            }
            set
            {
                this.lasttranstartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string XDES
        {
            get
            {
                return this.xDESField;
            }
            set
            {
                this.xDESField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string lockMode
        {
            get
            {
                return this.lockModeField;
            }
            set
            {
                this.lockModeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte schedulerid
        {
            get
            {
                return this.scheduleridField;
            }
            set
            {
                this.scheduleridField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort kpid
        {
            get
            {
                return this.kpidField;
            }
            set
            {
                this.kpidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort spid
        {
            get
            {
                return this.spidField;
            }
            set
            {
                this.spidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte sbid
        {
            get
            {
                return this.sbidField;
            }
            set
            {
                this.sbidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ecid
        {
            get
            {
                return this.ecidField;
            }
            set
            {
                this.ecidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte priority
        {
            get
            {
                return this.priorityField;
            }
            set
            {
                this.priorityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte trancount
        {
            get
            {
                return this.trancountField;
            }
            set
            {
                this.trancountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastbatchstarted
        {
            get
            {
                return this.lastbatchstartedField;
            }
            set
            {
                this.lastbatchstartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastbatchcompleted
        {
            get
            {
                return this.lastbatchcompletedField;
            }
            set
            {
                this.lastbatchcompletedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime lastattention
        {
            get
            {
                return this.lastattentionField;
            }
            set
            {
                this.lastattentionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string clientapp
        {
            get
            {
                return this.clientappField;
            }
            set
            {
                this.clientappField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string hostname
        {
            get
            {
                return this.hostnameField;
            }
            set
            {
                this.hostnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort hostpid
        {
            get
            {
                return this.hostpidField;
            }
            set
            {
                this.hostpidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string loginname
        {
            get
            {
                return this.loginnameField;
            }
            set
            {
                this.loginnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string isolationlevel
        {
            get
            {
                return this.isolationlevelField;
            }
            set
            {
                this.isolationlevelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong xactid
        {
            get
            {
                return this.xactidField;
            }
            set
            {
                this.xactidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte currentdb
        {
            get
            {
                return this.currentdbField;
            }
            set
            {
                this.currentdbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint lockTimeout
        {
            get
            {
                return this.lockTimeoutField;
            }
            set
            {
                this.lockTimeoutField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint clientoption1
        {
            get
            {
                return this.clientoption1Field;
            }
            set
            {
                this.clientoption1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint clientoption2
        {
            get
            {
                return this.clientoption2Field;
            }
            set
            {
                this.clientoption2Field = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockProcessFrame
    {

        private string procnameField;

        private byte lineField;

        private byte stmtstartField;

        private bool stmtstartFieldSpecified;

        private ushort stmtendField;

        private bool stmtendFieldSpecified;

        private string sqlhandleField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string procname
        {
            get
            {
                return this.procnameField;
            }
            set
            {
                this.procnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte line
        {
            get
            {
                return this.lineField;
            }
            set
            {
                this.lineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte stmtstart
        {
            get
            {
                return this.stmtstartField;
            }
            set
            {
                this.stmtstartField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool stmtstartSpecified
        {
            get
            {
                return this.stmtstartFieldSpecified;
            }
            set
            {
                this.stmtstartFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ushort stmtend
        {
            get
            {
                return this.stmtendField;
            }
            set
            {
                this.stmtendField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool stmtendSpecified
        {
            get
            {
                return this.stmtendFieldSpecified;
            }
            set
            {
                this.stmtendFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string sqlhandle
        {
            get
            {
                return this.sqlhandleField;
            }
            set
            {
                this.sqlhandleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockKeylock
    {

        private deadlockKeylockOwner[] ownerlistField;

        private deadlockKeylockWaiter[] waiterlistField;

        private ulong hobtidField;

        private byte dbidField;

        private string objectnameField;

        private string indexnameField;

        private string idField;

        private string modeField;

        private ulong associatedObjectIdField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("owner-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("owner", IsNullable = false)]
        public deadlockKeylockOwner[] ownerlist
        {
            get
            {
                return this.ownerlistField;
            }
            set
            {
                this.ownerlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute("waiter-list")]
        [System.Xml.Serialization.XmlArrayItemAttribute("waiter", IsNullable = false)]
        public deadlockKeylockWaiter[] waiterlist
        {
            get
            {
                return this.waiterlistField;
            }
            set
            {
                this.waiterlistField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong hobtid
        {
            get
            {
                return this.hobtidField;
            }
            set
            {
                this.hobtidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte dbid
        {
            get
            {
                return this.dbidField;
            }
            set
            {
                this.dbidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string objectname
        {
            get
            {
                return this.objectnameField;
            }
            set
            {
                this.objectnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string indexname
        {
            get
            {
                return this.indexnameField;
            }
            set
            {
                this.indexnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public ulong associatedObjectId
        {
            get
            {
                return this.associatedObjectIdField;
            }
            set
            {
                this.associatedObjectIdField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockKeylockOwner
    {

        private string idField;

        private string modeField;

        private string requestTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string requestType
        {
            get
            {
                return this.requestTypeField;
            }
            set
            {
                this.requestTypeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class deadlockKeylockWaiter
    {

        private string idField;

        private string modeField;

        private string requestTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string mode
        {
            get
            {
                return this.modeField;
            }
            set
            {
                this.modeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string requestType
        {
            get
            {
                return this.requestTypeField;
            }
            set
            {
                this.requestTypeField = value;
            }
        }
    }




}
