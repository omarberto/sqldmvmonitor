﻿using System;
using DMVmonitor;
using System.Xml.Serialization;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System.IO;

namespace SQLsessions.Events
{
    internal class BlockedProcessReport
    {
        public SQLsessions.Events.Headblockers.blockedprocessreport blocked_process_report;
        public long nsTimeStampEpoch;
        public Guid? attach_activity_id = null;
        public BlockedProcessReport(PublishedEvent evt)
        {
            //attach_activity_id_xfer -->potrebbe essere interessante
            //attach_activity_id -->poco interessante
            

            //Actions
            PublishedAction actionTmp;
            if (evt.Actions.TryGetValue("attach_activity_id", out actionTmp))
            {
                var tmp = (ActivityId)actionTmp.Value;

                this.attach_activity_id = tmp.Id;
            }

            //;
            

            //Field
            PublishedEventField fieldTmp;
            if (evt.Fields.TryGetValue("blocked_process", out fieldTmp))
            {
                var tmp = (XMLData)fieldTmp.Value;
                
                this.blocked_process_report = FromXml(tmp.RawString);
            }
            
            this.nsTimeStampEpoch = new EpochNanoseconds(evt.Timestamp.UtcDateTime).Value;
        }

        protected static SQLsessions.Events.Headblockers.blockedprocessreport FromXml(String xml)
        {
            SQLsessions.Events.Headblockers.blockedprocessreport returnedXmlClass = null;

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass =
                            (SQLsessions.Events.Headblockers.blockedprocessreport)new XmlSerializer(typeof(SQLsessions.Events.Headblockers.blockedprocessreport)).Deserialize(reader);
                    }
                    catch (Exception e)
                    {
                        // String passed is not XML, simply return defaultXmlClass

                        Logging.Log.Instance.Trace(e);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Trace(ex);
            }

            return returnedXmlClass;
        }
    }
}
