﻿using System;
using DMVmonitor;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading.Tasks;
using Microsoft.SqlServer.XEvent;
using Microsoft.SqlServer.XEvent.Linq;
using System.IO;
using SQLsessions.Events.Deadlocks;

namespace SQLsessions.Events
{
    internal class XmlDeadlockReport
    {
        public long nsTimeStampEpoch;

        public string xml = string.Empty;

        public deadlock xml_deadlock_report;
        public bool isValid { get { return xml_deadlock_report != null; } }
        public deadlock.deadlockProcess victimProcess
        {
            get
            {
                return xml_deadlock_report.processlist.First(process => xml_deadlock_report.victimlist[0].id == process.id);
            }
        }
        public deadlock.resourceLock victimKeylock
        {
            get
            {
                return xml_deadlock_report.resourcelist.First(resource => xml_deadlock_report.victimlist[0].id == resource.waiterlist[0].id);
            }
        }


        public class Chain
        {
            public string objectname;
            public string indexname;
            public uint database_id;
            public ulong associated_object_id;//hobt è un doppione?????
            public string mode;

            public uint logused;
            public string clientapp;
            public string hostname;
            public string isolationlevel;
            public string lockMode;
            public uint lockTimeout;
            public string loginname;
            public string status;
            public string waitresource;
            public uint waittime;

            public bool isVictim;
        }
        public Chain getProcess(ulong transaction_id)
        {
            //var victim = xml_deadlock_report.victimlist.LastOrDefault();


            deadlock.deadlockProcess process = xml_deadlock_report.processlist.FirstOrDefault(p => p.ownerId == transaction_id);
            if (process == null)
            {
                Logging.Log.Instance.Trace("NO PROCESS FOUND!!!", transaction_id);
                //
                foreach (var p in xml_deadlock_report.processlist)
                {
                    Logging.Log.Instance.Trace("{2} --> {0} {1}",p.ownerId, p.xactid, transaction_id);
                }
                return null;
            }
            deadlock.resourceLock keylock = xml_deadlock_report.resourcelist.FirstOrDefault(r => r.waiterlist[0].id == process.id);
            if (keylock == null)
            {
                Logging.Log.Instance.Trace("NO keylock FOUND!!!");
                return null;
            }

            bool isVictim = (xml_deadlock_report.victimlist.Count(victim => victim.id == process.id) > 0) ;

            return new Chain()
            {
                isVictim = isVictim,

                objectname = keylock.objectname,
                indexname = keylock.indexname,
                associated_object_id = keylock.associatedObjectId,//doppione con altri eventi
                mode = keylock.mode,//doppione
                database_id = keylock.dbid,//doppione

                logused = process.logused,
                clientapp = process.clientapp,
                //logused = process.executionStack, --> confronto interessante, forse diventa molto inutile aspettare certi eventi
                hostname = process.hostname,
                //logused = process.inputbuf, --> confronto interessante, da qui o da sql_handle posso ricavare query_hash
                isolationlevel = process.isolationlevel,
                lockMode = process.lockMode,
                lockTimeout = process.lockTimeout,
                loginname = process.loginname,
                status = process.status,
                waitresource = process.waitresource,
                waittime = process.waittime
            };
        }

        public class DeadlockEvt
        {
            public string ClientHostName { get; internal set; }
            public string LoginName { get; internal set; }
            public int NrOfVictims { get; internal set; }
            public uint SQLSessionId { get; internal set; }
            public long Time { get; internal set; }
            public string Elementname { get; internal set; }
            public string WaitIndexname { get; internal set; }
            public string WaitMode { get; internal set; }
            public string WaitObjectname { get; internal set; }
            public string WaitResource { get; internal set; }
            public string WaitStatement { get; internal set; }
            public uint Waittime { get; internal set; }
        }
        public DeadlockEvt deadlockEvt { get; private set; } = null;
        public bool hasDeadlockEvt { get { return deadlockEvt != null; } }
        public void getProcess()
        {

            var victim = xml_deadlock_report.victimlist.LastOrDefault();
            if (victim == null)
            {
                Logging.Log.Instance.Warn("deadlocksOnlineSession no victim found");
                return;
            }
            var processid = victim.id;

            var killedprocess = xml_deadlock_report.processlist.FirstOrDefault(p => p.id == processid);
            if (killedprocess == null)
            {
                Logging.Log.Instance.Warn("deadlocksOnlineSession no killed found");
                return;
            }

            var waiter = xml_deadlock_report.resourcelist.FirstOrDefault(el => el.waiterlist.Count(w => w.id == processid) == 1);
            if (waiter == null)
                this.deadlockEvt = new DeadlockEvt()
                {
                    ClientHostName = string.IsNullOrEmpty(killedprocess.hostname) ? "NONE" : killedprocess.hostname,
                    LoginName = string.IsNullOrEmpty(killedprocess.loginname) ? "NONE" : killedprocess.loginname,
                    WaitObjectname = "NONE",
                    WaitIndexname = "NONE",
                    Elementname = "NONE",

                    NrOfVictims = xml_deadlock_report.victimlist.Length,
                    SQLSessionId = killedprocess.spid,
                    Waittime = killedprocess.waittime,
                    WaitResource = killedprocess.waitresource,
                    WaitMode = "-",
                    WaitStatement = killedprocess.inputbuf.Trim().Replace("\n","; ").Replace("\r", "; ").Replace("\t", " "),

                    Time = nsTimeStampEpoch
                };
            else
                this.deadlockEvt = new DeadlockEvt()
                {
                    ClientHostName = string.IsNullOrEmpty(killedprocess.hostname) ? "NONE" : killedprocess.hostname,
                    LoginName = string.IsNullOrEmpty(killedprocess.loginname) ? "NONE" : killedprocess.loginname,
                    WaitObjectname = string.IsNullOrEmpty(waiter.objectname) ? "NONE" : waiter.objectname,
                    WaitIndexname = string.IsNullOrEmpty(waiter.indexname) ? "NONE" : waiter.indexname,
                    Elementname = waiter.elementname,

                    NrOfVictims = xml_deadlock_report.victimlist.Length,
                    SQLSessionId = killedprocess.spid,
                    Waittime = killedprocess.waittime,
                    WaitResource = killedprocess.waitresource,
                    WaitMode = waiter.mode,
                    WaitStatement = killedprocess.inputbuf.Trim().Replace("\n", "; ").Replace("\r", "; ").Replace("\t", " "),

                    Time = nsTimeStampEpoch
                };


            //"SQLDeadlockStats,host={0},SQLInstance={1},ClientHostName={2},LoginName={3},WaitObjectname={4},WaitIndexname={5} NrOfVictims={6}i,SQLSessionId=\"{7}\",Waittime={8}i,WaitResource=\"{9}\",WaitObject=\"{10}\",WaitIndex=\"{11}\",WaitMode=\"{12}\",WaitStatement=\"{13}\" {14}"

        }

        public XmlDeadlockReport(PublishedEvent evt)
        {
            //attach_activity_id_xfer -->potrebbe essere interessante
            //attach_activity_id -->poco interessante
            
            //Field
            PublishedEventField fieldTmp;
            if (evt.Fields.TryGetValue("xml_report", out fieldTmp))
            {
                var tmp = (XMLData)fieldTmp.Value;

                this.xml = tmp.RawString;
                this.xml_deadlock_report = FromXml(tmp.RawString);
            }

            this.nsTimeStampEpoch = new EpochNanoseconds(evt.Timestamp.UtcDateTime).Value;

            this.getProcess();
        }

        protected deadlock FromXml(String xml)
        {
            var xmlSerializer = new XmlSerializer(typeof(deadlock));
            xmlSerializer.UnknownElement += xmlSerializer_UnknownElement;

            deadlock returnedXmlClass = null;

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (deadlock)xmlSerializer.Deserialize(reader);
                        returnedXmlClass.resourcelist = resourcelist.ToArray();
                    }
                    catch (Exception e)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                        Logging.Log.Instance.Error(e);
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.Log.Instance.Error(ex, "XmlDeadlockReport.FromXml");
            }

            return returnedXmlClass;
        }
        private List<deadlock.resourceLock> resourcelist = new List<deadlock.resourceLock>();
        private void xmlSerializer_UnknownElement(object sender, XmlElementEventArgs e)
        {
            if (e.ExpectedElements != ":resourceLock") return;

            string tmp = e.Element.OuterXml.Replace(e.Element.Name, "resourceLock");
            
            using (TextReader reader = new StringReader(tmp))
            {
                try
                {
                    var returnedXmlClass =
                        (deadlock.resourceLock)new XmlSerializer(typeof(deadlock.resourceLock)).Deserialize(reader);

                    returnedXmlClass.elementname = e.Element.Name;

                    resourcelist.Add(returnedXmlClass);
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Error(ex, "XmlDeadlockReport.xmlSerializer_UnknownElement");
                }
            }
        }
    }
}
