﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    internal class QuerySave
    {
        private string connectionString;

        public QuerySave(string connectionString)
        {
            this.connectionString = connectionString;
        }

        internal void Save(ulong query_hash, ulong query_plan_hash, DateTime lastExecutionTime)
        {
            SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => Query(query_hash, query_plan_hash, lastExecutionTime), query_hash, query_plan_hash, lastExecutionTime));
        }


        private void Query(ulong query_hash, ulong query_plan_hash, DateTime lastExecutionTime)
        {
            string query_text;
            if(ReadQueryText(query_hash, query_plan_hash, out query_text))
                SQLdetailsStorage.Storage.Instance.InsertQuery(query_hash, query_text, lastExecutionTime);
        }
        private bool ReadQueryText(ulong query_hash, ulong query_plan_hash, out string query_text)
        {
            query_text = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 text from sys.dm_exec_query_stats cross apply sys.dm_exec_sql_text(sql_handle) where query_hash = @p1";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    //cmd.Parameters.Add(new SqlParameter("@p2", query_plan_hash.ToString()));
                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                query_text = datareader.GetString(0);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //Logging.Log.Instance.Trace(ex.Message);
                        Logging.Log.Instance.Error("QUERY ERROR {0}", ex.Message);
                    }
                }
                sqlConnection.Close();
            }
            return !string.IsNullOrEmpty(query_text);
        }
    }
    internal class PlanSave
    {
        private string connectionString;

        public PlanSave(string connectionString)
        {
            this.connectionString = connectionString;
        }

        //internal void Save(ulong query_hash, ulong query_plan_hash)
        //{
        //    //if (query_hash == 1254045809749244406)
        //    //    return;
        //    SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(() => Query(query_hash, query_plan_hash));
        //}
        internal void Save(ulong query_hash, ulong query_plan_hash, DateTime lastExecutionTime)
        {
            Logging.Log.Instance.Info("PlanSave.Save CREATION {0} {1} - {2}", query_hash, query_plan_hash, lastExecutionTime);
            SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => Plan(query_hash, query_plan_hash, lastExecutionTime), query_hash, query_plan_hash, lastExecutionTime));
        }
        private void Plan(ulong query_hash, ulong query_plan_hash, DateTime lastExecutionTime)
        {
            string plan_text;
            if (ReadPlanText(query_hash, query_plan_hash, out plan_text))
                SQLdetailsStorage.Storage.Instance.InsertPlan(query_plan_hash, plan_text, lastExecutionTime);

            //DateTime? creationTime;
            //if (ReadPlanCreationTime(query_hash, query_plan_hash, out creationTime))
            //{
            //    if (!SQLdetailsStorage.Storage.Instance.HasPlan(query_plan_hash, creationTime.Value))
            //    {
            //        string plan_text;
            //        if (ReadPlanText(query_hash, query_plan_hash, out plan_text, out creationTime))
            //            SQLdetailsStorage.Storage.Instance.InsertPlan(query_plan_hash, plan_text, creationTime.Value);
            //    }
            //}
        }
        private bool ReadPlanCreationTime(ulong query_hash, ulong query_plan_hash, out DateTime? creationTime)
        {
            creationTime = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 creation_time from sys.dm_exec_query_stats where query_hash = @p1 and query_plan_hash = @p2";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.BigInt) { Value = (long)query_plan_hash });

                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                creationTime = datareader.GetDateTime(0);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("PLAN ERROR {1} {2} : {0}", ex.Message, query_hash, query_plan_hash);
                    }
                }
                sqlConnection.Close();
            }
            
            return creationTime.HasValue;
        }
        private bool ReadPlanText(ulong query_hash, ulong query_plan_hash, out string plan_text)
        {
            plan_text = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 query_plan from sys.dm_exec_query_stats cross apply sys.dm_exec_query_plan(plan_handle) where query_hash = @p1 and query_plan_hash = @p2";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.BigInt) { Value = (long)query_plan_hash });

                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                plan_text = datareader.GetString(0);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("PLAN ERROR {1} {2} : {0}", ex.Message, query_hash, query_plan_hash);
                    }
                }
                sqlConnection.Close();
            }

            if (string.IsNullOrEmpty(plan_text))
            {
                Logging.Log.Instance.Debug("no plan read {0}//{2} | {1}//{3}", query_hash, query_plan_hash, (long)query_hash, (long)query_plan_hash);
                ReadPlanPlainText(query_hash, query_plan_hash, out plan_text);
                return !string.IsNullOrEmpty(plan_text);
            }
            else
            {
                //Logging.Log.Instance.Debug("plan read {0} {1}", query_hash, query_plan_hash);
                return true;
            }
        }
        private bool ReadPlanText(ulong query_hash, ulong query_plan_hash, out string plan_text, out DateTime? creationTime)
        {
            plan_text = null;
            creationTime = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 query_plan,creation_time from sys.dm_exec_query_stats cross apply sys.dm_exec_query_plan(plan_handle) where query_hash = @p1 and query_plan_hash = @p2";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.BigInt) { Value = (long)query_plan_hash });

                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                plan_text = datareader.GetString(0);
                                creationTime = datareader.GetDateTime(1);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("PLAN ERROR {1} {2} : {0}",ex.Message, query_hash, query_plan_hash);
                    }
                }
                sqlConnection.Close();
            }

            if (string.IsNullOrEmpty(plan_text))
            {
                Logging.Log.Instance.Debug("no plan read {0}//{2} | {1}//{3}", query_hash, query_plan_hash, (long)query_hash, (long)query_plan_hash);
                ReadPlanPlainText(query_hash, query_plan_hash, out plan_text, out creationTime);
            }
            else
            {
                //Logging.Log.Instance.Debug("plan read {0} {1}", query_hash, query_plan_hash);
            }
            return creationTime.HasValue;
        }

        private bool ReadPlanPlainText(ulong query_hash, ulong query_plan_hash, out string plan_text)
        {
            plan_text = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 query_plan from sys.dm_exec_query_stats cross apply sys.dm_exec_text_query_plan(plan_handle, statement_start_offset, statement_end_offset) where query_hash = @p1 and query_plan_hash = @p2";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.BigInt) { Value = (long)query_plan_hash });

                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                plan_text = datareader.GetString(0);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("PLAN ERROR {0}", ex.Message);
                    }
                }
                sqlConnection.Close();
            }
            
            return !string.IsNullOrEmpty(plan_text);
        }
        private bool ReadPlanPlainText(ulong query_hash, ulong query_plan_hash, out string plan_text, out DateTime? creationTime)
        {
            plan_text = null;
            creationTime = null;

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = sqlConnection.CreateCommand())
                {
                    cmd.CommandText = "select top 1 query_plan,creation_time from sys.dm_exec_query_stats cross apply sys.dm_exec_text_query_plan(plan_handle, statement_start_offset, statement_end_offset) where query_hash = @p1 and query_plan_hash = @p2";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.BigInt) { Value = (long)query_hash });
                    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.BigInt) { Value = (long)query_plan_hash });

                    try
                    {
                        using (var datareader = cmd.ExecuteReader())
                        {
                            while (datareader.Read())
                            {
                                plan_text = datareader.GetString(0);
                                creationTime = datareader.GetDateTime(1);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logging.Log.Instance.Error("PLAN ERROR {0}", ex.Message);
                    }
                }
                sqlConnection.Close();
            }

            if (!creationTime.HasValue)
            {
                Logging.Log.Instance.Debug("no plan read plain text {0}//{2} | {1}//{3}", query_hash, query_plan_hash, (long)query_hash, (long)query_plan_hash);
            }
            else
            {
                Logging.Log.Instance.Debug("SUCCESS plan read plain text {0}//{2} | {1}//{3}", query_hash, query_plan_hash, (long)query_hash, (long)query_plan_hash);
            }
            return creationTime.HasValue;
        }
    }
    internal class DeadlockSave
    {
        private string xml;
        public DeadlockSave(string xml)
        {
            this.xml = xml;
        }

        internal string Save(string Elementname, string WaitObjectname, string WaitIndexname, DateTime lastExecutionTimeUTC)
        {
            DateTime lastExecutionTime = lastExecutionTimeUTC.ToLocalTime();
            ulong deadlock_key = Convert.ToUInt64(new EpochNanoseconds(lastExecutionTimeUTC).Value);
            
            SQLdetailsStorage.Queued.QueueAppendable.Instance.Append(new SQLdetailsStorage.Queued.FileSaveAction(() => Deadlock(deadlock_key, lastExecutionTime), deadlock_key, lastExecutionTime));

            //"deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}"
            return string.Format("deadlock_{0:X16}_{1:yyyyMMdd_HHmmss}", deadlock_key, lastExecutionTime);
        }


        private void Deadlock(ulong deadlock_key, DateTime lastExecutionTime)
        {
            SQLdetailsStorage.Storage.Instance.InsertDeadlock(deadlock_key, this.xml, lastExecutionTime);
        }
    }
}
