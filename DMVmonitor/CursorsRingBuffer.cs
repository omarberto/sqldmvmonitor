﻿using System;
using System.Collections.Generic;

namespace DMVmonitor
{
    internal class CursorsRingBuffer
    {
        private int lastIndex = 0;
        private SessionCursorOnExecution[] buffer;

        public CursorsRingBuffer(int length)
        {
            this.buffer = new SessionCursorOnExecution[length];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = new SessionCursorOnExecution();
            }
        }

        internal bool TryGetCursor(int session_id, int protocol_execution_id, out CursorOnExecution cursorOnExecution)
        {
            for (int i = 0; i < buffer.Length; i++)
            {
                if (buffer[i].session_id == session_id && buffer[i].protocol_execution_id == protocol_execution_id)
                {
                    cursorOnExecution = buffer[i].cursorOnExecution;
                    return true;
                }
            }
            cursorOnExecution = null;
            return false;
        }
        internal void Add(int session_id, int protocol_execution_id, CursorOnExecution cursorOnExecution)
        {
            buffer[lastIndex].session_id = session_id;
            buffer[lastIndex].protocol_execution_id = protocol_execution_id;
            buffer[lastIndex].cursorOnExecution = cursorOnExecution;

            lastIndex = (lastIndex + 1) % buffer.Length;
        }

        private class SessionCursorOnExecution
        {
            public int session_id { get; internal set; }
            public int protocol_execution_id { get; internal set; }
            public CursorOnExecution cursorOnExecution { get; internal set; }
        }

    }
}