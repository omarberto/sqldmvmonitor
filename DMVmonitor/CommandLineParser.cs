﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    internal class CommandLineParser
    {
        internal bool isUserlevel { private set; get; } = false;
        internal bool isInstall { private set; get; } = false;
        internal bool isUninstall { private set; get; } = false;

        internal string ConfigPath { private set; get; }

        public string SvcName { get; set; } = "DMVmonitor$I00";
        public string DisplayName { private set; get; } = "SQL DMV Monitor - I00";
        public string Instance { get; internal set; }
        public string UserName { private set; get; } = "";
        public bool settedUsername { get { return !string.IsNullOrEmpty(UserName); } }
        private string _Password = "";
        public string Password
        {
            private set { _Password = value; }
            get
            {
                if (settedPassword)
                    return _Password;
                else
                    return PromptForPassword();
            }
        }
        public bool settedPassword { get { return !string.IsNullOrEmpty(_Password); } }

        public bool test_tornado { get; private set; }

        internal enum TestRemoteStorageType : byte
        {
            send,
            touch,
            clean,
            reload
        }
        public bool test_remotestorage { get; private set; }
        public TestRemoteStorageType? test_remotestorageType { get; private set; }

        internal string xelPath { private set; get; }

        internal CommandLineParser(string[] args)
        {
            string tmp = string.Empty;

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i].ToLower();
                switch (arg)
                {
                    case "--userlevel":
                    case "-userlevel":
                        isUserlevel = true;
                        break;
                    case "--install":
                    case "-install":
                        isInstall = true;
                        break;
                    case "--uninstall":
                    case "-uninstall":
                        isUninstall = true;
                        break;
                    case "--name":
                    case "-name":
                        this.SvcName = args[++i];
                        break;
                    case "--displayname":
                    case "-displayname":
                        this.DisplayName = args[++i];
                        break;
                    case "--username":
                    case "-username":
                        this.UserName = args[++i];
                        break;
                    case "--password":
                    case "-password":
                        this.Password = args[++i];
                        break;

                    case "--instanceid":
                    case "-instanceid":
                        this.Instance = args[++i];
                        break;

                    case "--config":
                    case "-config":
                        this.ConfigPath = args[++i];
                        break;

                    case "--test-tornado":
                        this.test_tornado = true;
                        break;

                    case "--test-remotestorage":
                        this.test_remotestorage = true;
                        break;
                    case "/s":
                        if (this.test_remotestorageType.HasValue && this.test_remotestorageType.Value != TestRemoteStorageType.send)
                        {
                            throw new Exception(string.Format("bad command line arguments already set {0} attempting send", this.test_remotestorageType.Value.ToString()));
                        }
                        this.test_remotestorageType = TestRemoteStorageType.send;
                        break;
                    case "/t":
                        if (this.test_remotestorageType.HasValue && this.test_remotestorageType.Value != TestRemoteStorageType.touch)
                        {
                            throw new Exception(string.Format("bad command line arguments already set {0} attempting touch", this.test_remotestorageType.Value.ToString()));
                        }
                        this.test_remotestorageType = TestRemoteStorageType.touch;
                        break;
                    case "/c":
                        if (this.test_remotestorageType.HasValue && this.test_remotestorageType.Value != TestRemoteStorageType.clean)
                        {
                            throw new Exception(string.Format("bad command line arguments already set {0} attempting clean", this.test_remotestorageType.Value.ToString()));
                        }
                        this.test_remotestorageType = TestRemoteStorageType.clean;
                        break;
                    case "/r":
                        if (this.test_remotestorageType.HasValue && this.test_remotestorageType.Value != TestRemoteStorageType.reload)
                        {
                            throw new Exception(string.Format("bad command line arguments already set {0} attempting reload", this.test_remotestorageType.Value.ToString()));
                        }
                        this.test_remotestorageType = TestRemoteStorageType.reload;
                        break;

                    default:
                        if (string.IsNullOrEmpty(tmp)) tmp = args[i];
                        break;
                }
                

            }

            //checks (forse poi si può mettere in altra classe


            if (this.isInstall && this.isUninstall)
                throw new Exception("bad command line arguments, install - uninstall");

            if (this.test_remotestorageType.HasValue && (this.test_remotestorageType.Value == TestRemoteStorageType.send || this.test_remotestorageType.Value == TestRemoteStorageType.touch))
            {
                if(string.IsNullOrEmpty(tmp))
                    throw new Exception("test_remotestorage implies a filepath as input");
                this.xelPath = tmp;
            }

            if (this.test_tornado)
            {
                if (string.IsNullOrEmpty(this.Instance))
                    throw new Exception("argument instanceid missing");
            }

            if (string.IsNullOrEmpty(this.Instance) && this.SvcName.Length > 4 && !this.SvcName.Substring(this.SvcName.Length - 3).Equals(this.Instance))
            {
                throw new Exception("service name and instanceid are different");
            }
            if (string.IsNullOrEmpty(this.Instance) && this.DisplayName.Length > 4 && !this.DisplayName.Substring(this.DisplayName.Length - 3).Equals(this.Instance))
            {
                throw new Exception("displayname and instanceid are different");
            }
        }

        internal string PromptForPassword()
        {
            Console.Write("\npassword:");
            string pass = "";
            do
            {
                ConsoleKeyInfo key = Console.ReadKey(true);
                // Backspace Should Not Work
                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pass += key.KeyChar;
                    Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                    {
                        pass = pass.Substring(0, (pass.Length - 1));
                        Console.Write("\b \b");
                    }
                    else if (key.Key == ConsoleKey.Enter)
                    {
                        break;
                    }
                }
            } while (true);
            return pass;
        }

        public static void ExecuteCmd(string cmdText)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = string.Format("/C {0}", cmdText);
            process.StartInfo = startInfo;
            process.Start();
        }
    }
}
