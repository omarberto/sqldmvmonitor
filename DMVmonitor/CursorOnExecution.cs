﻿using System;
using SQLsessions.Events;

namespace DMVmonitor
{
    internal class CursorOnExecution
    {
        public DateTimeOffset time_stamp { get; private set; }

        public CursorOnExecution(CursorOpenEvent parsedEvent)
        {
            this.time_stamp = parsedEvent.time_stamp;
            this.server_instance_name = parsedEvent.server_instance_name;
            this.query_hash = parsedEvent.query_hash;
            this.query_plan_hash = parsedEvent.query_plan_hash;
            this.AxSession = parsedEvent.AxSession;
            this.AxUser = parsedEvent.AxUser;
        }

        public int AxSession { get; private set; }
        public string AxUser { get; private set; }
        public ulong query_hash { get; private set; }
        public ulong query_plan_hash { get; private set; }
        public string server_instance_name { get; private set; }

        internal double DurationMicroseconds(CursorCloseEvent parsedEvent)
        {
            return (parsedEvent.time_stamp - this.time_stamp).TotalMilliseconds * 1000;
        }
    }
}