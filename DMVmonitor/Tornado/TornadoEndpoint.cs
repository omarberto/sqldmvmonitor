﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMVmonitor.Tornado
{
    class TornadoEndpoint
    {
        private string json_keepAliveHeader = string.Empty;
        private string json_keepAliveFormatBody = string.Empty;

        private string json_perfDataHeader = string.Empty;
        private string json_perfDataFormatBody = string.Empty;


        internal TornadoEndpoint(string ServiceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyName = assembly.GetName().Name;
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            
            var IPGlobalProperties = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties();
            string hostnameformat = Outputs.Configuration.Configuration.config.output.tornado.hostnameformat == "hostonly" ? 
                "{0}" 
                :
                "{0}.{1}";
            string hostName = string.Format(hostnameformat, IPGlobalProperties.HostName, IPGlobalProperties.DomainName);

            //keepalive id = 1
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.keepAliveHeader.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var jsonFormatHeader = reader.ReadToEnd();
                    this.json_keepAliveHeader = string.Format(jsonFormatHeader
                        , ServiceName
                        , hostName
                        , fileVersionInfo.FileMajorPart, fileVersionInfo.FileMinorPart, fileVersionInfo.FileBuildPart
                        , Configuration.Configuration.config.main.dataSource.Replace("\\", "\\\\"));
                }
            }
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.keepAliveBody.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    this.json_keepAliveFormatBody = reader.ReadToEnd();
                }
            }
            
            SQLdetailsStorage.Storage.Instance.newLocalFileSaved += newLocalFileSaved;
            SQLdetailsStorage.Storage.Instance.newRemoteFileSent += newRemoteFileSent;
            SQLdetailsStorage.Storage.Instance.newRemoteFileFailed += newRemoteFileFailed;
            //perfdata id = 2
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.perfDataHeader.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    var jsonFormatHeader = reader.ReadToEnd();
                    this.json_perfDataHeader = string.Format(jsonFormatHeader
                        , ServiceName
                        , hostName
                        , Configuration.Configuration.config.main.dataSource.Replace("\\", "\\\\"));
                }
            }
            using (var stream = assembly.GetManifestResourceStream(assemblyName + ".Tornado.perfDataBody.jfmt"))
            {
                using (var reader = new StreamReader(stream))
                {
                    this.json_perfDataFormatBody = reader.ReadToEnd();
                }
            }
        }

        private void newLocalFileSaved()
        {
            //Console.WriteLine("\n\n\nTORNADO: SAVED NEW FILE {0}\n\n\n",SQLdetailsStorage.Storage.Instance.total_bytes_saved_on_filesystem);
            Tornado.TornadoEnpointData.values.number_queryfiles_saved_to_filesystem++;
        }
        private void newRemoteFileSent()
        {
            //Console.WriteLine("\n\n\nTORNADO: SAVED NEW FILE {0}\n\n\n",SQLdetailsStorage.Storage.Instance.total_bytes_saved_on_filesystem);
            Tornado.TornadoEnpointData.values.number_queryfiles_sent_to_server++;
        }
        private void newRemoteFileFailed()
        {
            //Console.WriteLine("\n\n\nTORNADO: SAVED NEW FILE {0}\n\n\n",SQLdetailsStorage.Storage.Instance.total_bytes_saved_on_filesystem);
            Tornado.TornadoEnpointData.values.number_queryfiles_send_to_server_failed++;
        }

        public static bool active = true;
        internal void DoWork()
        {
            System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            int elapsedSeconds = 0;

            while (active)
            {
                //chiedere alessandro se questo output ha qualocosa di predefinito come subject o se devo aggiungere in config
                elapsedSeconds++;
                if (elapsedSeconds % Outputs.Configuration.Configuration.config.output.tornado.keepaliveInterval == 0)
                {
                    //keepalive
                    string json = this.json_keepAliveHeader + string.Format(this.json_keepAliveFormatBody
                        , EpochNanoseconds.UtcNow.Value / 1000000L);

                    DMVmonitor.Outputs.Nats.Client.Instance["TORNADO"].PublishMessage(Outputs.Configuration.Configuration.config.output.tornado.topic, json);
                }

                if (elapsedSeconds % Outputs.Configuration.Configuration.config.output.tornado.monitordataInterval == 0)
                {
                    //perfdata
                    TornadoEnpointData interval_values = TornadoEnpointData.interval_values;

                    int exit_status = interval_values.sqlconnection_exceptions < 1 ? 0 : 2;
                    //TODO: add to config file
                    exit_status = (interval_values.seconds_delay_dmv_queries < Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_wrn) ? exit_status : ((exit_status < 2) ? ((interval_values.seconds_delay_dmv_queries < Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_err) ? 1 : 2) : 2);
                    
                    string json = this.json_perfDataHeader + string.Format(this.json_perfDataFormatBody
                        , EpochNanoseconds.UtcNow.Value / 1000000L
                        , interval_values.number_of_events_read
                        , interval_values.number_of_events_processed
                        , interval_values.number_of_measurments_sent_to_nats
                        , interval_values.number_queryfiles_sent_to_server
                        , interval_values.number_queryfiles_send_to_server_failed
                        , interval_values.number_queryfiles_saved_to_filesystem
                        , interval_values.total_mbytes_saved_on_filesystem
                        , interval_values.sqlconnection_exceptions, 1, 1 //fixed limits
                        //TODO: add to config file
                        , interval_values.seconds_delay_dmv_queries, Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_wrn, Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_err//usare per status
                        , exit_status);

                    DMVmonitor.Outputs.Nats.Client.Instance["TORNADO"].PublishMessage(Outputs.Configuration.Configuration.config.output.tornado.topic, json);
                }

                long deltaMilliseconds = 1000L - stopwatch.ElapsedMilliseconds;
                if (deltaMilliseconds > 0L)
                {
                    Thread.Sleep(Convert.ToInt32(deltaMilliseconds));
                }
                stopwatch.Reset();
            }
            stopwatch.Stop();
        }

        internal void Test()
        {
            {
                //keepalive
                string json = this.json_keepAliveHeader + string.Format(this.json_keepAliveFormatBody
                    , EpochNanoseconds.UtcNow.Value / 1000000L);

                DMVmonitor.Outputs.Nats.Client.Instance["TORNADO"].PublishMessage(Outputs.Configuration.Configuration.config.output.tornado.topic, json);
            }

            {
                //perfdata
                TornadoEnpointData interval_values = TornadoEnpointData.interval_values;

                int exit_status = interval_values.sqlconnection_exceptions < 1 ? 0 : 2;
                //TODO: add to config file
                exit_status = (interval_values.seconds_delay_dmv_queries < Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_wrn) ? exit_status : ((exit_status < 2) ? ((interval_values.seconds_delay_dmv_queries < Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_err) ? 1 : 2) : 2);

                string json = this.json_perfDataHeader + string.Format(this.json_perfDataFormatBody
                    , EpochNanoseconds.UtcNow.Value / 1000000L
                    , interval_values.number_of_events_read
                    , interval_values.number_of_events_processed
                    , interval_values.number_of_measurments_sent_to_nats
                    , interval_values.number_queryfiles_sent_to_server
                    , interval_values.number_queryfiles_send_to_server_failed
                    , interval_values.number_queryfiles_saved_to_filesystem
                    , interval_values.total_mbytes_saved_on_filesystem
                    , interval_values.sqlconnection_exceptions, 1, 1 //fixed limits
                                                                     //TODO: add to config file
                    , interval_values.seconds_delay_dmv_queries, Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_wrn, Outputs.Configuration.Configuration.config.output.tornado.seconds_delay_dmv_queries_err//usare per status
                    , exit_status);

                DMVmonitor.Outputs.Nats.Client.Instance["TORNADO"].PublishMessage(Outputs.Configuration.Configuration.config.output.tornado.topic, json);
            }
        }
    }

    class TornadoEnpointData
    {
        internal static TornadoEnpointData values { set; get; } = new TornadoEnpointData();
        private static TornadoEnpointData old_values = new TornadoEnpointData();
        internal static TornadoEnpointData interval_values
        {
            get
            {
                int number_of_events_read = values.number_of_events_read - old_values.number_of_events_read;
                old_values.number_of_events_read = values.number_of_events_read;
                int number_of_events_processed = values.number_of_events_processed - old_values.number_of_events_processed;
                old_values.number_of_events_processed = values.number_of_events_processed;
                int number_of_measurments_sent_to_nats = values.number_of_measurments_sent_to_nats - old_values.number_of_measurments_sent_to_nats;
                old_values.number_of_measurments_sent_to_nats = values.number_of_measurments_sent_to_nats;
                int number_queryfiles_sent_to_server = values.number_queryfiles_sent_to_server - old_values.number_queryfiles_sent_to_server;
                old_values.number_queryfiles_sent_to_server = values.number_queryfiles_sent_to_server;
                int number_queryfiles_send_to_server_failed = values.number_queryfiles_send_to_server_failed - old_values.number_queryfiles_send_to_server_failed;
                old_values.number_queryfiles_send_to_server_failed = values.number_queryfiles_send_to_server_failed;
                int sqlconnection_exceptions = values.getAndReset_sqlConnectionExceptionStatus();

                int number_queryfiles_saved_to_filesystem = values.number_queryfiles_saved_to_filesystem - old_values.number_queryfiles_saved_to_filesystem;
                old_values.number_queryfiles_saved_to_filesystem = values.number_queryfiles_saved_to_filesystem;
                double total_mbytes_saved_on_filesystem = SQLdetailsStorage.Storage.Instance.total_bytes_saved_on_filesystem / 1048576D;

                return new TornadoEnpointData()
                {
                    number_of_events_read = number_of_events_read,
                    number_of_events_processed = number_of_events_processed,
                    number_of_measurments_sent_to_nats = number_of_measurments_sent_to_nats,
                    number_queryfiles_sent_to_server = number_queryfiles_sent_to_server,
                    number_queryfiles_send_to_server_failed = number_queryfiles_send_to_server_failed,
                    number_queryfiles_saved_to_filesystem = number_queryfiles_saved_to_filesystem,
                    sqlconnection_exceptions = sqlconnection_exceptions,
                    //parte non diff
                    total_mbytes_saved_on_filesystem = total_mbytes_saved_on_filesystem,
                    seconds_delay_dmv_queries = values.seconds_delay_dmv_queries
                };
            }
        }

        internal int number_of_events_read = 0;
        internal int number_of_events_processed = 0;
        internal int number_of_measurments_sent_to_nats;
        internal int number_queryfiles_sent_to_server;//per ora fisso a 0
        //internal int queryfiles_sent_to_server_in_kbytes;//per ora fisso a zero
        internal int number_queryfiles_send_to_server_failed;
        internal int number_queryfiles_saved_to_filesystem;//per ora fisso a 0
        internal double total_mbytes_saved_on_filesystem { get; private set; }//per ora fisso a zero, devo mettere i counters nello storage

        internal int sqlconnection_exceptions;
        private bool sqlConnectionExceptionStatus = false;
        internal int getAndReset_sqlConnectionExceptionStatus()
        {
            int retvalue = sqlConnectionExceptionStatus ? 1 : 0;
            sqlConnectionExceptionStatus = false;
            return retvalue;
        }
        internal void newSqlconnection_exception()
        {
            sqlConnectionExceptionStatus = true;
        }

        internal int seconds_delay_dmv_queries;//difficile, NB: non va azzerato

        //internal TornadoEnpointData Clone()
        //{
        //    return (TornadoEnpointData)this.MemberwiseClone();
        //}
        
    }
}
