﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    class GlobalTags
    {
        private SqlConnection sqlConnection;

        public string sql_instance;
        private string Read_InstanceName()
        {
            string result = string.Empty;
            using (SqlCommand cmd = sqlConnection.CreateCommand())
            {
                cmd.CommandText = "select replace (@@servername,'\\',':')";
                cmd.CommandType = CommandType.Text;

                try
                {
                    result = cmd.ExecuteScalar() as string;
                    //if (!DMVmonitor.Configuration.Configuration.config.main.isStandardTcpPort)
                    //    result += ":" + DMVmonitor.Configuration.Configuration.config.main.dataSourceTcpPort;
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Error(ex.Message);
                }
            }
            return result;
        }

        public string sql_host;

        private string Read_HostName()
        {
            string result = string.Empty;
            using (SqlCommand cmd = sqlConnection.CreateCommand())
            {
                cmd.CommandText = "SELECT SERVERPROPERTY('MachineName')";
                cmd.CommandType = CommandType.Text;

                try
                {
                    result = cmd.ExecuteScalar() as string;
                }
                catch (Exception ex)
                {
                    Logging.Log.Instance.Error(ex.Message);
                }
            }
            return result;
        }

        public GlobalTags(SqlConnection sqlConnection)
        {
            this.sqlConnection = sqlConnection;
            this.sql_host = Read_HostName();
            this.sql_instance = Read_InstanceName();
        }
    }
}
