﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.ServiceProcess;

namespace DMVmonitor
{
    class Program : ServiceBase
    {
        public Program()
        {
        }
        public static string ConfigPath { get; internal set; }
        public static string Instance { get; internal set; }
        //public static string DisplayName { get; internal set; } = "SQLmonitor";
        //public static string UserName { get; internal set; } = "";
        //public static string Password { get; internal set; } = "";

        protected String GetServiceName()
        {
            // Calling System.ServiceProcess.ServiceBase::ServiceNamea allways returns
            // an empty string,
            // see https://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=387024

            // So we have to do some more work to find out our service name, this only works if
            // the process contains a single service, if there are more than one services hosted
            // in the process you will have to do something else

            int processId = System.Diagnostics.Process.GetCurrentProcess().Id;
            String query = "SELECT * FROM Win32_Service where ProcessId = " + processId;
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                return queryObj["Name"].ToString();
            }

            Logging.Log.Instance.Error("Can not get the ServiceName");

            if (string.IsNullOrEmpty(Instance))
                throw new Exception("Can not get the ServiceName and no instance");

            return string.Format("DMVmonitor${0}", Instance);
        }
        protected static String GetServiceNameFromExe()
        {

            string exePath = System.Reflection.Assembly.GetEntryAssembly().Location.Replace("\\", "\\\\");
            String query = "SELECT * FROM Win32_Service WHERE PathName LIKE '%" + exePath + "%'";
            System.Management.ManagementObjectSearcher searcher = new System.Management.ManagementObjectSearcher(query);

            foreach (System.Management.ManagementObject queryObj in searcher.Get())
            {
                Logging.Log.Instance.Info("Get the ServiceName {0}", queryObj["Name"].ToString());
                return queryObj["Name"].ToString();
            }

            //Logging.Log.Instance.Error("Can not get the ServiceName");

            return string.Empty;
        }


        private static Service svc = null;
        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            Logging.Log.Instance.Trace("Service->OnStart");

            svc = new Service(GetServiceName());
            svc.Start(ConfigPath);
        }

        protected override void OnStop()
        {
            Logging.Log.Instance.Debug("Service->OnStop");

            svc.Stop();
            
            NLog.LogManager.Shutdown();  // TODO: Remember to flush

            base.OnStop();
        }

        public class Serializer
        {
            public T Deserialize<T>(string input) where T : class
            {
                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

                using (StringReader sr = new StringReader(input))
                {
                    return (T)ser.Deserialize(sr);
                }
            }
        }

        static void Main(string[] args)
        {
  
            //// EXAMPLE 1
            //path = @"C:\Users\pb00238\Installazioni\Polonia\Test\xml_deadlock_report_(20190115_1408170884165).xml";
            //xmlInputData = File.ReadAllText(path);

            //DMVmonitor.Events.deadlock customer = ser.Deserialize<DMVmonitor.Events.deadlock>(xmlInputData);

            //SQLdetailsStorage.Config.TomlConfiguration.Init("config.conf");
            //DMVmonitorEXE.test();
            //////server_instance_name --> aggiungere nel nuovo reader
            //return;

            //mica scemo, funziona!!!!!
            //NLog.Common.InternalLogger.LogLevel = LogLevel.Debug;
            //NLog.Common.InternalLogger.LogToConsole = true;

            Logging.Log.Instance.Trace("Main START");



            //bool isUserlevel = false;
            //bool isInstall = false;
            //bool isUninstall = false;


            var cmdLine = new CommandLineParser(args);
            ConfigPath = cmdLine.ConfigPath; //vediamo se funziona.....
            Instance = cmdLine.Instance;

            Logging.Log.Instance.Trace("Main START {0} {1} {2}", cmdLine.DisplayName, cmdLine.UserName, Program.ConfigPath);
            
            if (System.Environment.UserInteractive || cmdLine.isUserlevel)
            {
                string parameter = string.Concat(args);
                if (cmdLine.isInstall)
                {
                    Program.ConfigPath = Path.GetFullPath(cmdLine.ConfigPath);

                    string cmdText = string.Format("sc create {0} binpath=\"{1} -config {2}\" start=auto", cmdLine.DisplayName, Assembly.GetExecutingAssembly().Location, Program.ConfigPath);

                    if (cmdLine.settedUsername)
                    {
                        cmdText += string.Format(" obj={0} password=\"{1}\"", cmdLine.UserName, cmdLine.Password);
                    }

                    CommandLineParser.ExecuteCmd(cmdText);
                }
                else if (cmdLine.isUninstall)
                {
                    string cmdText = string.Format("sc stop {0}", cmdLine.DisplayName);
                    CommandLineParser.ExecuteCmd(cmdText);
                    cmdText = string.Format("sc delete {0}", cmdLine.DisplayName);
                    CommandLineParser.ExecuteCmd(cmdText);
                }
                else if (cmdLine.test_tornado)
                {
                    Logging.Log.Instance.Trace("Main START Console DMV - TestTornado");
                    var svcname = GetServiceNameFromExe();
                    if (!string.IsNullOrEmpty(svcname))
                    {
                        svc = new Service(svcname);
                        if (svc.StartTornado(ConfigPath))
                            Console.WriteLine("Tornado  messages sent to {1} on topic {0}", Outputs.Configuration.Configuration.config.output.tornado.topic, Outputs.Configuration.Configuration.config.output.tornado.address);
                    }
                    else
                        Console.WriteLine("Application not installed, no message sent!");
                }
                else if (cmdLine.test_remotestorage)
                {
                    svc = new Service();
                    if (svc.StartRemoteStorage(ConfigPath))
                    {
                        if (cmdLine.test_remotestorageType.HasValue)
                        {

                            switch (cmdLine.test_remotestorageType.Value)
                            {
                                case CommandLineParser.TestRemoteStorageType.send:
                                    {
                                        if (cmdLine.xelPath.EndsWith(".stmt.txt"))
                                        {
                                            SQLdetailsStorage.Storage.Instance.InsertQueryTest(cmdLine.xelPath);
                                            Console.ReadKey(true);
                                        }
                                        else if (cmdLine.xelPath.EndsWith(".sqlplan.zip"))
                                        {
                                            SQLdetailsStorage.Storage.Instance.InsertPlanTest(cmdLine.xelPath);
                                            Console.ReadKey(true);
                                        }
                                        else if (cmdLine.xelPath.EndsWith(".xdl"))
                                        {
                                            SQLdetailsStorage.Storage.Instance.InsertDeadlockTest(cmdLine.xelPath);
                                            Console.ReadKey(true);
                                        }
                                        else
                                        {
                                            Console.WriteLine("file type unknown");
                                        }
                                    }
                                    break;
                                case CommandLineParser.TestRemoteStorageType.touch:
                                    {
                                        string sHash = Path.GetFileName(cmdLine.xelPath);
                                        if (sHash.Contains(".")) sHash = sHash.Substring(0, sHash.IndexOf('.'));

                                        if (cmdLine.xelPath.EndsWith(".stmt.txt"))
                                        {
                                            ulong uHash;
                                            if (ulong.TryParse(sHash, out uHash))
                                            {
                                                SQLdetailsStorage.Storage.Instance.TouchQuery(uHash, DateTime.Now);
                                                Console.ReadKey(true);
                                            }
                                            else
                                                Console.WriteLine("file name not valid");
                                        }
                                        else if (cmdLine.xelPath.EndsWith(".sqlplan.zip"))
                                        {
                                            ulong uHash;
                                            if (ulong.TryParse(sHash, out uHash))
                                            {
                                                SQLdetailsStorage.Storage.Instance.TouchPlan(uHash, DateTime.Now);
                                                Console.ReadKey(true);
                                            }
                                            else
                                                Console.WriteLine("file name not valid");
                                        }
                                        else
                                        {
                                            Console.WriteLine("file type unknown");
                                        }
                                    }
                                    break;
                                case CommandLineParser.TestRemoteStorageType.clean:
                                    {
                                        SQLdetailsStorage.Storage.Instance.CleanOld();
                                        Console.ReadKey(true);
                                    }
                                    break;
                                case CommandLineParser.TestRemoteStorageType.reload:
                                    {
                                        SQLdetailsStorage.Storage.Instance.ReloadAndClean();
                                        Console.ReadKey(true);
                                    }
                                    break;
                            }
                        }
                        //else Console.ReadKey(true);
                    }
                    else
                        Console.WriteLine("StartRemoteStorage not working, check settings!");


                   
                }
                else
                {
                    Logging.Log.Instance.Trace("Main START Console DMV");
                    svc = new Service();
                    svc.Start(ConfigPath);
                    svc.Join();
                    Logging.Log.Instance.Trace("Main END Console DMV");
                }
            }
            else
            {
                ServiceBase.Run(new Program());
            }

            Logging.Log.Instance.Trace("Main END");

            NLog.LogManager.Shutdown();  // Remember to flush
        }
    }
}