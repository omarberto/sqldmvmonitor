﻿using Microsoft.SqlServer.XEvent.Linq;
using SQLsessions.Events;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace DMVmonitor
{
    internal class SQLsessionQueries
    {
        private bool isReader;
        private string connectionString;
        private string sessionName;
        private string sql_host;
        private string sql_instance;

        public SQLsessionQueries(string connectionString, string sessionName)
        {
            this.isReader = true;
            this.connectionString = connectionString;
            this.sessionName = sessionName;
        }
        public SQLsessionQueries(string sessionName)
        {
            this.isReader = false;
            this.sessionName = sessionName;
        }

        internal void DoWork()
        {
            if (isReader)
            {
                OnlineSession();
            }
            else
            {
                //listDBfiles();
            }
        }
        
        private void OnlineSession()
        {
            int number_of_errors = 0;
            string evtName = "";
            while (true)
            {
                try
                {
                    if (string.IsNullOrEmpty(this.sql_host) || string.IsNullOrEmpty(this.sql_instance))
                    {
                        using (var sqlConnection = new SqlConnection(connectionString))
                        {
                            sqlConnection.Open();
                            var globalTags = new GlobalTags(sqlConnection);
                            this.sql_host = globalTags.sql_host;
                            this.sql_instance = globalTags.sql_instance;
                        }
                    }
                    using (QueryableXEventData stream = new QueryableXEventData(this.connectionString, this.sessionName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                    {
                        Logging.Log.Instance.Trace("Connection!!!!");
                        foreach (PublishedEvent evt in stream)
                        {
                            Tornado.TornadoEnpointData.values.number_of_events_read++;
                            evtName = evt.Name;
                            Console.WriteLine("SQLsessionQueries {0}" , evtName);
                            switch (evt.Name)
                            {
                                case "sp_statement_completed":
                                    {
                                        var parsedEvent = new SQLsessions.Events.OnlineEvent(evt);
                                        if (parsedEvent.query_hash != 0)
                                        {
                                            if (!SQLdetailsStorage.Storage.Instance.HasQuery(parsedEvent.query_hash, evt.Timestamp.DateTime.ToLocalTime()))
                                                new QuerySave(this.connectionString).Save(parsedEvent.query_hash, parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime());
                                            if (!SQLdetailsStorage.Storage.Instance.HasPlan(parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime()))
                                                new PlanSave(this.connectionString).Save(parsedEvent.query_hash, parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime());
                                        }

                                        if (DMVmonitor.Outputs.Nats.Client.Instance.hasSender("DATA"))
                                        {
                                            var evtParse = new CompleteEvent(evt, this.sql_host, this.sql_instance);
                                            try
                                            {
                                                evtParse.PublishNats();
                                            }
                                            catch (Exception natIOexc)
                                            {
                                                Logging.Log.Instance.Error(natIOexc, "OnlineSession -> evtParse.PublishNats {1} {0}", natIOexc.Message, natIOexc.Source);
                                            }
                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;
                                case "sql_statement_completed":
                                    {
                                        var parsedEvent = new SQLsessions.Events.OnlineEvent(evt);
                                        if (parsedEvent.query_hash != 0)
                                        {
                                            if (!SQLdetailsStorage.Storage.Instance.HasQuery(parsedEvent.query_hash, evt.Timestamp.DateTime.ToLocalTime()))
                                                new QuerySave(this.connectionString).Save(parsedEvent.query_hash, parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime());
                                            if (!SQLdetailsStorage.Storage.Instance.HasPlan(parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime()))
                                                new PlanSave(this.connectionString).Save(parsedEvent.query_hash, parsedEvent.query_plan_hash, evt.Timestamp.DateTime.ToLocalTime());
                                        }

                                        if (DMVmonitor.Outputs.Nats.Client.Instance.hasSender("DATA"))
                                        {
                                            var evtParse = new CompleteEvent(evt, this.sql_host, this.sql_instance);
                                            try
                                            {
                                                evtParse.PublishNats();
                                            }
                                            catch (Exception natIOexc)
                                            {
                                                Logging.Log.Instance.Error(natIOexc, "OnlineSession -> evtParse.PublishNats {1} {0}", natIOexc.Message, natIOexc.Source);
                                            }
                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;
                                case "rpc_completed":
                                    {
                                        if (DMVmonitor.Outputs.Nats.Client.Instance.hasSender("DATA"))
                                        {
                                            var evtParse = new CompleteEvent(evt, this.sql_host, this.sql_instance);
                                            try
                                            {
                                                evtParse.PublishNats();
                                            }
                                            catch (Exception natIOexc)
                                            {
                                                Logging.Log.Instance.Error(natIOexc, "OnlineSession -> evtParse.PublishNats {1} {0}", natIOexc.Message, natIOexc.Source);
                                            }
                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> SqlException");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (Microsoft.SqlServer.XEvent.Linq.EventEnumerationException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> SESSIONE SOSPESA");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (System.IO.IOException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
                }
                catch (System.Threading.ThreadAbortException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> System.Threading.ThreadAbortException {1} {0}", e.Message, e.Source);
                    break;
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "SQLsessionQueries {2} OnlineSession -> Generica Exception {1} {0}", e.Message, e.Source, evtName);
                }
                number_of_errors++;


                System.Threading.Thread.Sleep(1000);
            }
            Logging.Log.Instance.Trace("OnlineSession -> ExitThread");
        }


        public void OfflineFileUpload(string sql_host, string sql_instance, string filepathFormat)
        {
            if (!Path.IsPathRooted(filepathFormat))
            {
                filepathFormat = Path.Combine(Environment.CurrentDirectory, filepathFormat.TrimStart('.', '\\'));
            }
            
            foreach (var finfo in new DirectoryInfo(Path.GetDirectoryName(filepathFormat)).GetFiles(Path.GetFileName(filepathFormat).Replace("{0}", "*")).OrderBy(file => file.LastWriteTime))
            {
                int nevents = 0;
                Logging.Log.Instance.Trace("start loading file {0}", finfo.FullName);
                using (QueryableXEventData events = new QueryableXEventData(finfo.FullName))
                {
                    foreach (PublishedEvent evt in events.ToList().OrderBy(ev => ev.Timestamp))
                    {
                        if (DMVmonitor.Outputs.Nats.Client.Instance.hasSender("DATA"))
                        {
                            var evtParse = new CompleteEvent(evt, sql_host, sql_instance);
                            try
                            {
                                evtParse.PublishNats();
                            }
                            catch (Exception natIOexc)
                            {
                                Logging.Log.Instance.Error(natIOexc, "OnlineSession -> evtParse.PublishNats {1} {0}", natIOexc.Message, natIOexc.Source);
                            }
                        }
                        nevents++;
                    }
                }
                Logging.Log.Instance.Trace("end for {0} found {1} events", finfo.FullName, nevents);
            }
        }

        private void ParseEventsOLD(PublishedEvent evt)
        {
            //switch (evt.Name)
            //{
            //    case "sp_statement_completed":
            //    case "sql_statement_completed":
            //    case "rpc_completed":
            //        {
            //            var evtParse = new CompleteEvent(evt);
            //            evtParse.PublishNats(sender);
            //        }
            //        break;
            //    default:
            //        {
            //            var evtParse = new CompleteUnknown(evt);
            //        }
            //        break;
            //}
        }
    }
}