﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMVmonitor
{
    internal class FetchesPolling
    {
        internal void getFetchesRequests()
        {
            string cmdtext = "select qt.text as fetchapi, qs.query_hash,qs.query_plan_hash" + //cast(context_info as varchar(128)) as contextinfo, --> non é strettamente necessario
                                " from sys.dm_exec_requests er cross apply sys.dm_exec_sql_text(sql_handle) qt" + //avessi sql_handle da evento potrei collegare
                                " cross apply sys.dm_exec_cursors(session_id) ec" +
                                " inner join sys.dm_exec_query_stats qs on qs.sql_handle=ec.sql_handle" +
//" cross apply sys.dm_exec_sql_text(ec.sql_handle) qt2" //non metterlo, importante é hash!
                                " where command='SELECT' and er.query_hash=0 or er.query_hash is null";
            //" where command='SELECT' and er.query_hash=0 or er.query_hash is null and qt.text='{0}'"; //alternativo se si va a caccia di un solo FETCH, opzione in

            //va definito un intervallo di polling e un timeout

            Logging.Log.Instance.Trace(cmdtext);
        }
    }
}
