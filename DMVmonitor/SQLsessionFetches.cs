﻿using Microsoft.SqlServer.XEvent.Linq;
using SQLsessions.Events;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DMVmonitor
{
    internal class SQLsessionFetches
    {
        private bool isReader;
        private string connectionString;
        private string sessionName;
        private ulong fetchesFilter = 0;
        private string sql_host;
        private string sql_instance;

        public SQLsessionFetches(string connectionString, string sessionName, ulong fetchesFilter)
        {
            this.isReader = true;
            this.connectionString = connectionString;
            this.sessionName = sessionName;
            this.fetchesFilter = fetchesFilter;
        }

        internal void DoWork()
        {
            if (isReader)
            {
                OnlineSession();
            }
            else
            {
            }
        }

        private CursorsRingBuffer cursorsRingBuffer = new CursorsRingBuffer(100);
        private Dictionary<int, Dictionary<int, CursorOnExecution>> cursorDictionary = new Dictionary<int, Dictionary<int, CursorOnExecution>>();
        private void OnlineSession()
        {
            int number_of_errors = 0;
            while (true)
            {
                try
                {
                    if (string.IsNullOrEmpty(this.sql_host) || string.IsNullOrEmpty(this.sql_instance))
                    {
                        using (var sqlConnection = new SqlConnection(connectionString))
                        {

                            sqlConnection.Open();
                            var globalTags = new GlobalTags(sqlConnection);
                            this.sql_host = globalTags.sql_host;
                            this.sql_instance = globalTags.sql_instance;
                        }
                    }
                    using (QueryableXEventData stream = new QueryableXEventData(this.connectionString, this.sessionName, EventStreamSourceOptions.EventStream, EventStreamCacheOptions.DoNotCache))
                    {
                        Logging.Log.Instance.Trace("Connection!!!!");
                        foreach (PublishedEvent evt in stream)
                        {
                            Tornado.TornadoEnpointData.values.number_of_events_read++;
                            switch (evt.Name)
                            {
                                case "rpc_completed":
                                    {
                                        var parsedEvent = new SQLsessions.Events.RpcCompleted(evt);

                                        try
                                        {
                                            //per ora non verifico di essere su sonda cursors...
                                            bool tryLast = false;

                                            Dictionary<int, CursorOnExecution> cursorsOnExecution;
                                            if (this.cursorDictionary.TryGetValue(parsedEvent.session_id, out cursorsOnExecution))
                                            {
                                                CursorOnExecution cursorOnExecution;
                                                if (cursorsOnExecution.TryGetValue(parsedEvent.protocol_execution_id, out cursorOnExecution))
                                                {
                                                    SendFetches(parsedEvent, cursorOnExecution, evt.Timestamp.DateTime.ToLocalTime());
                                                }
                                                else
                                                {
                                                    tryLast = true;
                                                }
                                            }
                                            else
                                            {
                                                tryLast = true;
                                            }

                                            if (tryLast)
                                            {
                                                CursorOnExecution cursorOnExecution;
                                                if (this.cursorsRingBuffer.TryGetCursor(parsedEvent.session_id, parsedEvent.protocol_execution_id, out cursorOnExecution))
                                                {
                                                    SendFetches(parsedEvent, cursorOnExecution, evt.Timestamp.DateTime.ToLocalTime());
                                                }
                                                else
                                                    Logging.Log.Instance.Debug("IDs protocol_execution_id not found for RPC fetch error {0}//{1}", parsedEvent.session_id, parsedEvent.protocol_execution_id);
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            Logging.Log.Instance.Error("RpcCompleted - elaboration ({1}) Excpetion {0}", e.Message, evt.Name);
                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;

                                //cursors
                                case "cursor_open":
                                case "cursor_execute":
                                    {
                                        var parsedEvent = new SQLsessions.Events.CursorOpenEvent(evt);

                                        if (!this.cursorDictionary.ContainsKey(parsedEvent.session_id))
                                            this.cursorDictionary.Add(parsedEvent.session_id, new Dictionary<int, CursorOnExecution>());
                                        if (this.cursorDictionary[parsedEvent.session_id].ContainsKey(parsedEvent.protocol_execution_id))
                                        {
                                            Logging.Log.Instance.Debug("IDs duplicate error {0}//{1}", parsedEvent.session_id, parsedEvent.protocol_execution_id);
                                            this.cursorDictionary[parsedEvent.session_id].Remove(parsedEvent.protocol_execution_id);
                                        }

                                        this.cursorDictionary[parsedEvent.session_id].Add(parsedEvent.protocol_execution_id, new CursorOnExecution(parsedEvent));
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;

                                case "cursor_close":
                                    {
                                        var parsedEvent = new SQLsessions.Events.CursorCloseEvent(evt);

                                        Dictionary<int, CursorOnExecution> cursorsOnExecution;
                                        if (this.cursorDictionary.TryGetValue(parsedEvent.session_id, out cursorsOnExecution))
                                        {
                                            CursorOnExecution cursorOnExecution;
                                            if (cursorsOnExecution.TryGetValue(parsedEvent.protocol_execution_id, out cursorOnExecution))
                                            {
                                                double durationMicroseconds = cursorOnExecution.DurationMicroseconds(parsedEvent);
                                                
                                                if (false)
                                                {
                                                    SaveCursor(parsedEvent, cursorOnExecution, durationMicroseconds, evt.Timestamp.DateTime.ToLocalTime());
                                                }

                                                if (durationMicroseconds > fetchesFilter)
                                                {
                                                    this.cursorsRingBuffer.Add(parsedEvent.session_id, parsedEvent.protocol_execution_id, cursorOnExecution);
                                                    //forse si riesce a togliere....
                                                }

                                                cursorsOnExecution.Remove(parsedEvent.protocol_execution_id);
                                            }
                                            else
                                            {
                                                Logging.Log.Instance.Debug("IDs protocol_execution_id not found error {0}//{1}", parsedEvent.session_id, parsedEvent.protocol_execution_id);
                                            }
                                            if (cursorsOnExecution.Count == 0)
                                                this.cursorDictionary.Remove(parsedEvent.session_id);
                                        }
                                        else
                                        {
                                            Logging.Log.Instance.Debug("IDs sessionId not found error {0}//{1}", parsedEvent.session_id, parsedEvent.protocol_execution_id);
                                        }
                                    }
                                    Tornado.TornadoEnpointData.values.number_of_events_processed++;
                                    break;
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> SqlException");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (Microsoft.SqlServer.XEvent.Linq.EventEnumerationException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> SESSIONE SOSPESA");
                    Tornado.TornadoEnpointData.values.newSqlconnection_exception();
                }
                catch (System.IO.IOException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> System.IO.IOException {1} {0}", e.Message, e.Source);
                }
                catch (System.Threading.ThreadAbortException e)
                {
                    Logging.Log.Instance.Error(e, "OnlineSession -> System.Threading.ThreadAbortException {1} {0}", e.Message, e.Source);
                    break;
                }
                catch (Exception e)
                {
                    Logging.Log.Instance.Error(e, "SQLsessionFetches OnlineSession -> Generica Exception {1} {0}", e.Message, e.Source);
                }
                number_of_errors++;


                System.Threading.Thread.Sleep(1000);
            }
            Logging.Log.Instance.Trace("OnlineSession -> ExitThread");
        }

        private void SaveCursor(CursorCloseEvent parsedEvent, CursorOnExecution cursorOnExecution, double durationMicroseconds, DateTime lastExecutionTime)
        {
            //string rpTag = DMVmonitor.Configuration.Configuration.config.output.nats.getRetentionTag("sqlXeCursors");
            //string message = string.Format("sqlXeCursors{11},SQLInstance={0},host={10},query_hash={1},query_hash_hex=0x{1:X},query_plan_hash={2},query_plan_hash_hex=0x{2:X},AxUser={3},uniquifier={9} duration={5}i,session_id={7}i,cursor_id={8}i,AxSession=\"{4}\" {6}",
            //                                                        this.sql_instance/*cursorOnExecution.server_instance_name*/, 
            //                                                        cursorOnExecution.query_hash, 
            //                                                        cursorOnExecution.query_plan_hash,
            //                                                        cursorOnExecution.AxUser, 
            //                                                        cursorOnExecution.AxSession,
            //                                                        durationMicroseconds,
            //                                                        parsedEvent.nsTimeStampEpoch,
            //                                                        parsedEvent.session_id,
            //                                                        parsedEvent.protocol_execution_id,
            //                                                        Uniquifier.uniquifier(UniquifierClass.sqlXeFetches, parsedEvent.nsTimeStampEpoch),
            //                                                        this.sql_host,
            //                                                        rpTag);


            var message = new Influx.FormatMessage("sqlXeCursors");
            message.AddTag("SQLInstance", this.sql_instance);
            message.AddTag("host", this.sql_host);
            message.AddTag("query_hash", cursorOnExecution.query_hash.ToString());
            message.AddTag("query_hash_hex", "0x" + cursorOnExecution.query_hash.ToString("X"));
            message.AddTag("query_plan_hash", cursorOnExecution.query_plan_hash.ToString());
            message.AddTag("query_plan_hash_hex", "0x" + cursorOnExecution.query_plan_hash.ToString("X"));
            message.AddTag("AxUser", cursorOnExecution.AxUser);
            message.AddTag("uniquifier", Uniquifier.uniquifier(UniquifierClass.sqlXeFetches, parsedEvent.nsTimeStampEpoch).ToString());

            message.AddIntegerField("duration", Convert.ToUInt64(durationMicroseconds));
            message.AddStringField("AxSession", cursorOnExecution.AxSession < 0 ? "-" : cursorOnExecution.AxSession.ToString(), "{0}");
            message.AddStringField("session_id", parsedEvent.session_id.ToString(), "{0}");
            message.AddStringField("cursor_id", parsedEvent.protocol_execution_id.ToString(), "{0}");

            message.AddEpochNs(parsedEvent.nsTimeStampEpoch);

            DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(message.value);

            SaveFiles(cursorOnExecution, lastExecutionTime);
        }

        private void SendFetches(RpcCompleted parsedEvent, CursorOnExecution cursorOnExecution, DateTime lastExecutionTime)
        {
            //string rpTag = DMVmonitor.Configuration.Configuration.config.output.nats.getRetentionTag("sqlXeFetches");
            //string messageOLD = string.Format("sqlXeFetches{18},SQLInstance={0},host={10},query_hash={1},query_hash_hex=0x{1:X},query_plan_hash={2},query_plan_hash_hex=0x{2:X},AxUser={3},uniquifier={9}{11}{12} duration={5}i,session_id={7}i,cursor_id={8}i,AxSession=\"{4}\"{13}{14}{15}{16}{17} {6}",
            //                                                    this.sql_instance/*cursorOnExecution.server_instance_name*/,
            //                                                    cursorOnExecution.query_hash, 
            //                                                    cursorOnExecution.query_plan_hash,
            //                                                    cursorOnExecution.AxUser, 
            //                                                    cursorOnExecution.AxSession,
            //                                                    parsedEvent.duration,
            //                                                    parsedEvent.nsTimeStampEpoch,
            //                                                    parsedEvent.session_id,
            //                                                    parsedEvent.protocol_execution_id,
            //                                                    Uniquifier.uniquifier(UniquifierClass.sqlXeFetches, parsedEvent.nsTimeStampEpoch),
            //                                                    this.sql_host,
            //                                                    (parsedEvent.has_host_name ? string.Format(",SqlHostName={0}", parsedEvent.host_name) : ""),
            //                                                    (parsedEvent.has_sql_user ? string.Format(",SqlUser={0}", parsedEvent.sql_user) : "")
            //                                                    ,
            //                                                    (parsedEvent.cpu_time.HasValue ? string.Format(",cpu_time={0}i", parsedEvent.cpu_time.Value) : ""),
            //                                                    (parsedEvent.logical_reads.HasValue ? string.Format(",logical_reads={0}i", parsedEvent.logical_reads.Value) : ""),
            //                                                    (parsedEvent.physical_reads.HasValue ? string.Format(",physical_reads={0}i", parsedEvent.physical_reads.Value) : ""),
            //                                                    (parsedEvent.row_count.HasValue ? string.Format(",row_count={0}i", parsedEvent.row_count.Value) : ""),
            //                                                    (parsedEvent.writes.HasValue ? string.Format(",writes={0}i", parsedEvent.writes.Value) : ""),
            //                                                    rpTag);

            var message = new Influx.FormatMessage("sqlXeFetches");
            message.AddTag("SQLInstance", this.sql_instance);
            message.AddTag("host", this.sql_host);
            message.AddTag("query_hash", cursorOnExecution.query_hash.ToString());
            message.AddTag("query_hash_hex", "0x" + cursorOnExecution.query_hash.ToString("X"));
            message.AddTag("query_plan_hash", cursorOnExecution.query_plan_hash.ToString());
            message.AddTag("query_plan_hash_hex", "0x" + cursorOnExecution.query_plan_hash.ToString("X"));
            message.AddTag("AxUser", cursorOnExecution.AxUser);
            message.AddTag("uniquifier", Uniquifier.uniquifier(UniquifierClass.sqlXeFetches, parsedEvent.nsTimeStampEpoch).ToString());
            if (parsedEvent.has_host_name) message.AddTag("SqlHostName", parsedEvent.host_name);
            if (parsedEvent.has_sql_user) message.AddTag("SqlUser", parsedEvent.sql_user);

            message.AddIntegerField("duration", parsedEvent.duration);
            message.AddStringField("AxSession", cursorOnExecution.AxSession < 0 ? "-" : cursorOnExecution.AxSession.ToString(), "{0}");
            //TODO: capire se integer ha senso o se i riesce a cambiare....
            message.AddStringField("session_id", parsedEvent.session_id.ToString(), "{0}");
            message.AddStringField("cursor_id", parsedEvent.protocol_execution_id.ToString(), "{0}");
            message.AddIntegerField("session_id", parsedEvent.session_id);
            message.AddIntegerField("cursor_id", parsedEvent.protocol_execution_id);

            message.AddIntegerField("cpu_time", parsedEvent.cpu_time);
            message.AddIntegerField("logical_reads", parsedEvent.logical_reads);
            message.AddIntegerField("physical_reads", parsedEvent.physical_reads);
            message.AddIntegerField("row_count", parsedEvent.row_count);
            message.AddIntegerField("writes", parsedEvent.writes);
            
            message.AddEpochNs(parsedEvent.nsTimeStampEpoch);

            DMVmonitor.Outputs.Nats.Client.Instance["DATA"].PublishMessage(message.value);

            SaveFiles(cursorOnExecution, lastExecutionTime);
        }

        private void SaveFiles(CursorOnExecution cursorOnExecution, DateTime lastExecutionTime)
        {
            if (cursorOnExecution.query_hash != 0)
            {
                if (!SQLdetailsStorage.Storage.Instance.HasQuery(cursorOnExecution.query_hash, lastExecutionTime.ToLocalTime()))
                    new QuerySave(this.connectionString).Save(cursorOnExecution.query_hash, cursorOnExecution.query_plan_hash, lastExecutionTime.ToLocalTime());
                if (!SQLdetailsStorage.Storage.Instance.HasPlan(cursorOnExecution.query_plan_hash, lastExecutionTime.ToLocalTime()))
                    new PlanSave(this.connectionString).Save(cursorOnExecution.query_hash, cursorOnExecution.query_plan_hash, lastExecutionTime.ToLocalTime());
            }
        }
    }
}