﻿
using System.Xml.Serialization;

namespace SQLsessions
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ParameterList
    {

        private ParameterListColumnReference[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ColumnReference", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ParameterListColumnReference[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ParameterListColumnReference
    {

        private string columnField;

        private string parameterDataTypeField;

        private string parameterCompiledValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Column
        {
            get
            {
                return this.columnField;
            }
            set
            {
                this.columnField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ParameterDataType
        {
            get
            {
                return this.parameterDataTypeField;
            }
            set
            {
                this.parameterDataTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ParameterCompiledValue
        {
            get
            {
                return this.parameterCompiledValueField;
            }
            set
            {
                this.parameterCompiledValueField = value;
            }
        }
    }
}