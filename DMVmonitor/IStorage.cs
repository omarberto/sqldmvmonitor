﻿using System;

namespace DMVmonitor
{
    public interface IStorage
    {
        //void selectMeasurements(string dm_exec_query_stat_tableName);
        void InsertDmExecQueryStat(long query_hash, long query_plan_hash, DateTime creation_time, long plan_generation_num, DateTime last_execution_time, DateTime polling_time, long execution_count, long total_cpu, long total_duration, long total_physical_reads, long total_logical_reads, long total_writes, long total_clr_time, long polling_interval, bool isNewFile);
    }
}